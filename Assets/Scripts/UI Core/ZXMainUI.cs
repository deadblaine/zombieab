﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class ZXMainUI : MonoBehaviour
{
    [SerializeField] private List<GameObject> enableList;

    private UnityAction<Scene, LoadSceneMode> d_SceneLoaded;

    private static ZXMainUI singleton;

    private void Awake()
    {
        singleton = this;

        foreach (GameObject Obj in enableList)
        {
            Obj.SetActive(true);
        }

        d_SceneLoaded = new UnityAction<Scene, LoadSceneMode>(SceneStarted);

        SceneManager.sceneLoaded += d_SceneLoaded;
    }

    private void Start()
    {
        /*
        ZXTutorial tutorial = GameObject.Find("Temp_Tutorial").GetComponent<ZXTutorial>();

        if (!tutorial.TutorialShown)
            tutorial.Show();
        else
            tutorial.gameObject.SetActive(false);
        */
    }

    private void SceneStarted(Scene scene, LoadSceneMode Mode)
    {
        
    }



    private void OnDisable()
    {
        SceneManager.sceneLoaded -= d_SceneLoaded;
    }
}
