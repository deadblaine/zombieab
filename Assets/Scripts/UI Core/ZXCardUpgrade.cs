﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DarkTonic.MasterAudio;
using TMPro;
using DG.Tweening;

public class ZXCardUpgrade : MonoBehaviour
{
    private const float minDisplayTime = 2f;
    private const string pp_key = "first_upgrade";

    [SerializeField] private TextMeshProUGUI name;
    [SerializeField] private ZXUpgradeCard cardDisplay;
    [SerializeField] private List<ZXUpgradeProperty> upgradeProperties;

    private RectTransform nameRect;
    private RectTransform cardRect;

    private bool FirstUpgrade
    {
        get
        {
            return PlayerPrefs.HasKey(pp_key);
        }
        set
        {
            if (value)
                PlayerPrefs.SetInt(pp_key, 0);
        }
    }

    private float displayCounter = 0f;

    private static ZXCardUpgrade singleton;

    private void Awake()
    {
        singleton = this;

        nameRect = name.rectTransform;
        cardRect = cardDisplay.GetComponent<RectTransform>();

        gameObject.SetActive(false);
    }

    public static void UpgradeCard(ZXCard Card)
    {
        singleton.ShowUpgrade(Card);
    }

    private void ShowUpgrade(ZXCard Card)
    {


        name.text = Card.Name;
        nameRect.localScale = Vector3.zero;
        nameRect.DOScale(1f, 0.3f);

        cardDisplay.ShowUpgrade(Card);
        cardRect.localScale = Vector3.zero;
        cardRect.DOScale(1f, 0.2f);

        List<ZXCard.Property> properties = Card.cardProperties.FindAll(delegate (ZXCard.Property P) { return P.Upgrade; });

        for (int i = 0; i < upgradeProperties.Count; i++)
        {
            if (i < properties.Count)
            {
                upgradeProperties[i].Set(properties[i], i * 0.15f);
            }
            else
                upgradeProperties[i].Disable();
        }

        gameObject.SetActive(true);

        MasterAudio.PlaySound("UI_Upgrading_Card_ZW");
    }

    private void Update()
    {
        displayCounter = Mathf.Clamp(displayCounter - Time.deltaTime, 0f, minDisplayTime);
    }

    public void Close()
    {
        if (displayCounter > 0)
            return;

        gameObject.SetActive(false);

        MasterAudio.FadeOutAllOfSound("UI_Upgrading_Card_ZW", 1f);

        if (!FirstUpgrade)
        {
            FirstUpgrade = true;
            ZXConversationPopup.PlaySequence(4);
        }
    }
}
