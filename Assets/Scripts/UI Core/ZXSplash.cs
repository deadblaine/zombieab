﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class ZXSplash : MonoBehaviour
{
    [SerializeField] private VideoPlayer Splash;

    private void Start()
    {
        StartCoroutine(WaitForVideo());
    }

    private IEnumerator WaitForVideo()
    {
        //Start loading the loading scene
        //AsyncOperation loading = SceneManager.LoadSceneAsync((int)ZXDataManager.Scenes.Loading);

        //loading.allowSceneActivation = false;

        #if UNITY_WEBGL
        Splash.url = "https://s3.us-east-2.amazonaws.com/blitzproject.com/StreamingAssets/StreamingAssets/Nutaku_Publishing_Logo_Intro_3_1.mp4";
        #endif

        yield return new WaitForSeconds(0.25f);

        //Play the video
        Splash.Play();

        yield return new WaitForSeconds(3f);

        //loading.allowSceneActivation = true;

        SceneManager.LoadScene((int)ZXDataManager.Scenes.Loading);
    }
}
