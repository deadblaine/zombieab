﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ZXDeckButton : MonoBehaviour
{
    //Important Objects
    private Image background;
    private Button button;

    [SerializeField] private bool defaultSelected;

    //Static Variables
    private static ZXDeckButton current;

    //Private Variables
    private UnityAction d_Select;

    //Initalization
    private void Awake()
    {
        background = GetComponent<Image>();
        button = GetComponent<Button>();

        d_Select = new UnityAction(Select);
    }

    private void OnEnable()
    {
        button.onClick.AddListener(d_Select);

        if (defaultSelected)
            Select();
    }

    //Selection Functions
    public void Select()
    {
        if (current != null)
            current.DeSelect();

        background.color = ZXDataManager.GetColor("Light Red");

        current = this;
    }

    private void DeSelect()
    {
        background.color = ZXDataManager.GetColor("Orange");
    }

    //Deconstruction
    private void OnDisable()
    {
        button.onClick.RemoveListener(d_Select);

        if (current == this)
            DeSelect();
    }
}
