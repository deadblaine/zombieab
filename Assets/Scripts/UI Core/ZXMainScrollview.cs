﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using ZXCore;

public class ZXMainScrollview : ScrollRect
{
    //Constants
    private const int sectionCount = 5;
    private const float sectionIdexes = 4f;
    public const string scrollTweenID = "tw_scrollview_tween";

    //Inspector variables
    [SerializeField] private float nextVelocity = 1500;
    [SerializeField] private float scrollVelocity = 4000;
    [SerializeField] private float maxScrollTime = 0.3f;

    //Accessors
    public static bool Tweening
    {
        get
        {
            return DOTween.IsTweening(scrollTweenID) || tweening;
        }
        set
        {
            tweening = value; 

            if (e_Tweening != null)
                e_Tweening(value);
        }
    }
    public static ScrollRect MainScrollRect
    {
        get
        {
            return singleton.GetComponent<ScrollRect>();
        }
    }
    public static ZXSection.Type Current
    {
        get
        {
            return current;
        }
    }

    //Events
    public static event System.Action<bool> e_Tweening;
    public static event System.Action<ZXSection.Type, float> e_NextSection;

    //Private variables
    private RectTransform rect;
    private float width;
    private static bool tweening = false;
    private static ZXSection.Type current = ZXSection.Type.Mission;
    private static Dictionary<ZXSection.Type, ZXScrollview> sections;
    private static Dictionary<ZXSection.Type, float> positions;
    private static Dictionary<string, ZXScrollview.Bookmark> bookmarks;
    private static ZXMainScrollview singleton;

    //Initalization
    protected override void Awake()
    {
        singleton = this;

        base.Awake();

        rect = GetComponent<RectTransform>();
        width = content.rect.width;

        if (positions == null)
        {
            positions = new Dictionary<ZXSection.Type, float>(sectionCount);

            positions.Add(ZXSection.Type.Shop, (float)ZXSection.Type.Shop / sectionIdexes);
            positions.Add(ZXSection.Type.Deck, (float)ZXSection.Type.Deck / sectionIdexes);
            positions.Add(ZXSection.Type.Mission, (float)ZXSection.Type.Mission / sectionIdexes);
            positions.Add(ZXSection.Type.Daily, (float)ZXSection.Type.Daily / sectionIdexes);
            positions.Add(ZXSection.Type.Inventory, (float)ZXSection.Type.Inventory / sectionIdexes);
        }
    }

    //Child scrollview registration
    public void Register(ZXSection.Type section, ZXScrollview scrollview)
    {
        if (sections == null)
            sections = new Dictionary<ZXSection.Type, ZXScrollview>(5);

        sections.Add(section, scrollview);
    }

    public void DeRegister(ZXSection.Type section)
    {
        if (sections != null && sections.ContainsKey(section))
            sections.Remove(section);
    }

    //Event handlers
    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);

        if (Mathf.Abs(velocity.x) >= nextVelocity)
            ChangeSection((ZXSection.Type)Mathf.Clamp((int)current + -Mathf.Sign(velocity.x), 0, sectionIdexes));
        else
            ChangeSection((ZXSection.Type)Mathf.Clamp(Mathf.RoundToInt(horizontalNormalizedPosition * sectionIdexes), 0, sectionIdexes));
    }

    public void ChangeSection(ZXSection.Type NextSection)
    {
        float start = horizontalNormalizedPosition * width;
        float end = positions[NextSection] * width;
        float distance = Mathf.Abs(end - start);
        float seconds;
        Ease ease;

        Tweening = true;

        //Make sure velocity is above minimum
        if (Mathf.Abs(velocity.x) > scrollVelocity)
        {
            seconds = distance / Mathf.Abs(velocity.x);
            ease = Ease.OutCirc; 
        }
        else
        {
            seconds = distance / scrollVelocity;
            ease = Ease.OutCirc;
        }

        //Make sure time is below minimum
        if (seconds > maxScrollTime)
            seconds = maxScrollTime;

        this.DOHorizontalNormalizedPos(positions[NextSection], seconds).SetEase(ease).SetId(scrollTweenID).OnComplete(delegate () { Tweening = false; });

        if (current != NextSection && e_NextSection != null)
            e_NextSection(NextSection, seconds);

        current = NextSection;

        ZXCore.ZXPopup.HideActive();
    }

    public static void SnapToSection(ZXSection.Type NextSection)
    {
        singleton.horizontalNormalizedPosition = positions[NextSection];

        if (current != NextSection && e_NextSection != null)
            e_NextSection(NextSection, 0);

        current = NextSection;
    }

    public static ZXScrollview GetScrollView(ZXSection.Type Section)
    {
        return sections[Section];
    }
}
