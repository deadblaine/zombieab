﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ZXCardInfoPopup : ZXCore.ZXPopup
{
    /*Inspector Variables*/
    [SerializeField] private GameObject costDisplay;
    [SerializeField] private GameObject classDisplay;
    [SerializeField] private GameObject rankDisplay;
    [SerializeField] private GameObject adultDisplay;
    [SerializeField] private GameObject virusDisplay;
    [SerializeField] private GameObject storyDisplay;
    [SerializeField] private GameObject imageDisplay;

    [SerializeField] private Image cardImage;
    [SerializeField] private Image classImage;
    [SerializeField] private TextMeshProUGUI nameText;
    [SerializeField] private TextMeshProUGUI costText;
    [SerializeField] private TextMeshProUGUI classText;
    [SerializeField] private TextMeshProUGUI levelText;
    [SerializeField] private TextMeshProUGUI rankText;
    [SerializeField] private TextMeshProUGUI cardsText;
    [SerializeField] private TextMeshProUGUI rarityText;
    [SerializeField] private TextMeshProUGUI typeText;
    [SerializeField] private TextMeshProUGUI storyText;
    [SerializeField] private Image cardFill;

    [SerializeField] private Button upgradeLevelButton;
    [SerializeField] private TextMeshProUGUI upgradeLevelCost;
    [SerializeField] private Button upgradeRankButton;
    [SerializeField] private TextMeshProUGUI upgradeRankPrimaryCost;
    [SerializeField] private TextMeshProUGUI upgradeRankSecondaryCost;
    [SerializeField] private Button virusButton;
    [SerializeField] private Button storyButton;

    [SerializeField] private Image image; 

    [SerializeField] private TextMeshProUGUI virusText;
    [SerializeField] private Image virusFill;
    [SerializeField] private Image virusImage;

    [SerializeField] private List<ZXProperty> properties;
    [SerializeField] private List<ZXRomance> romanceImages;

    private static ZXCardInfoPopup singleton;
    private static ZXCard card;

    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();

        singleton = this;

        gameObject.SetActive(false);
	}
	
    public static void Open(ZXCard Card)
    {
        card = Card;

        singleton.Populate();

        singleton.Show();
    }

    private void Populate()
    {
        //Basic card info
        cardImage.sprite = card.Card_Image;
        nameText.text = card.Name;
        levelText.text = card.LevelText;
        rankText.text = card.RankText;
        rankText.color = card.Rank_Color;

        //Cards collected
        cardFill.color = card.Cards_Progress_Color;
        cardFill.fillAmount = card.CardsProgress;
        cardsText.text = card.CardsProgressText;

        switch (card.Type)
        {
            case ZXCard.Card_Type.Hero:
                rankDisplay.SetActive(true);
                virusDisplay.SetActive(false);
                break;

            case ZXCard.Card_Type.Spell:
                rankDisplay.SetActive(false);
                virusDisplay.SetActive(false);
                break;

            case ZXCard.Card_Type.Waifu:
                rankDisplay.SetActive(true);
                virusDisplay.SetActive(true);
                break;
        }

        //Disable virus display for spells
        if (card.Type == ZXCard.Card_Type.Spell)
        {
            classDisplay.SetActive(false);
        }
        else
        {
            classText.color = card.Class_Color;
            classText.text = card.ClassText;
            classImage.sprite = card.Class_Image;
            classDisplay.SetActive(true);
        }

        //Virus collected
        virusFill.color = card.Virus_Color;
        virusFill.fillAmount = card.VirusProgress;
        virusText.text = card.VirusProgressText;

        //Cost display
        costDisplay.SetActive(card.Cost > 0);
        costText.text = card.Cost.ToString();

        //Rarity and type
        rarityText.text = card.Rarity.ToString();
        rarityText.color = card.Rarity_Color;
        typeText.text = card.Type.ToString();

        //Cofigure properties display
        for (int i = 0; i < properties.Count; i++)
        {
            if (i < card.cardProperties.Count)
                properties[i].Set(card.cardProperties[i]);
            else
                properties[i].Disable();
        }

        //Story
        storyText.text = card.Description;

        //Romance
        if (card.cardRomance.Count > 0)
        {
            storyDisplay.SetActive(false);
            storyButton.gameObject.SetActive(true);
            virusButton.gameObject.SetActive(true);
            adultDisplay.SetActive(true);

            //Configure romance images
            for (int i = 0; i < romanceImages.Count; i++)
            {
                if (i < card.cardRomance.Count)
                    romanceImages[i].Set(card.cardRomance[i]);
                else
                    romanceImages[i].Disable();
            }
        }
        else
        {
            storyDisplay.SetActive(true);
            adultDisplay.SetActive(false);
            storyButton.gameObject.SetActive(false);
            virusButton.gameObject.SetActive(false);
        }

        //Upgrade buttons
        if (card.CanRank)
        {
            upgradeLevelButton.gameObject.SetActive(false);
            upgradeRankButton.gameObject.SetActive(!card.MaxRanked);

            upgradeRankPrimaryCost.text = card.NextRankCost.ToString();
        }
        else
        {
            upgradeLevelButton.gameObject.SetActive(!card.MaxLeveled);
            upgradeRankButton.gameObject.SetActive(false);

            upgradeLevelCost.text = card.NextLevelCost.ToString();
        }
    }
        /*
        if (card.Card.Cost > 0)
        {
            costDisplay.SetActive(true);
            costText.text = card.cost.ToString();
        }
        else
            costDisplay.SetActive(false);

        levelText.text = "LEVEL " + card.Level.ToString();
        cardsText.text = card.Cards.ToString() + " of " + card.CardsNeededToLevel.ToString();
        cardFill.fillAmount = card.CardsProgress;

        if (card.cardType != ZXCardUI.CardType.Ability)
            classDisplay.SetActive(true);
        else
            classDisplay.SetActive(false);

        upgradeButton.interactable = card.CanLevel;

        rarityText.text = card.rarity.ToString();

        nameText.text = card.name;

        if (card.cardType == ZXCardUI.CardType.Waifu && card.romanceImages != null && card.romanceImages.Count > 0)
        {
            virusDisplay.SetActive(true);

            virusText.text = card.Virus.ToString() + " of " + ZXCardUI.maxVirus.ToString();
            virusFill.fillAmount = card.VirusProgress;

            virusButton.interactable = card.CanVirus;

            virusImage.sprite = card.romanceImages[Random.Range(0, card.romanceImages.Count)];
        }
        else
            virusDisplay.SetActive(false);
    }

    public void Upgrade()
    {
        if (card.CanLevel)
        {
            card.Cards = 0;
            card.Level += 1;
        }

        upgradeButton.interactable = card.CanLevel;

        card.LoadCard();
        Populate();
    }

    public void FightVirus()
    {
        ZXRomanceGamePopup.Open(card);
    }
    */

    public static void ForceUpgrade()
    {
        singleton.UpgradeLevel();
    }

    public void UpgradeLevel()
    {
        if (card.HasLevelResources)
        {
            ZXDataManager.Silver -= card.NextLevelCost;
            card.Cards -= card.NextLevelCards;
            card.Level = card.Level + 1;

            ZXCardUpgrade.UpgradeCard(card);

            Hide();
        }
        else
        {
            if (card.Cards < card.NextLevelCards)
            {
                ZXMessageScreen.ShowMessage("You need " + (card.NextLevelCards - card.Cards).ToString() + " more " + card.Name + " cards to upgrade!", 2f);
            }
            else if (ZXDataManager.Silver < card.NextLevelCost)
            {
                ZXMessageScreen.ShowMessage("You need " + (card.NextLevelCost - ZXDataManager.Silver).ToString() + " more silver to upgrade " + card.Name, 2f);
            }
        }
    }

    public void UpgradeRank()
    {
        if (ZXDataManager.Kits >= card.NextRankCost)
        {
            ZXDataManager.Kits -= card.NextRankCost;

            card.Rank = card.Rank + 1;

            Hide();
        }
        else
        {
            ZXMessageScreen.ShowMessage("You need " + card.NextRankCost + " more kits to increase rank!", 2f);
        }
    }

    public void SetImage(bool Active, Sprite Image)
    {
        imageDisplay.SetActive(Active);
        image.sprite = Image;
    }

    public void SetStory(bool Active)
    {
        storyDisplay.SetActive(Active);
        adultDisplay.SetActive(!Active);
    }

    public void OpenRomance()
    {
        ZXRomanceGamePopup.Open(card, ZXDataManager.GetHero());

        Hide();
    }

    public override void Hide()
    {
        card = null;

        base.Hide();
    }
}
