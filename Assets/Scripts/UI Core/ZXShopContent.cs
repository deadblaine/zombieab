﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZXShopContent : ZXCore.ZXSection, ZXCore.IZXContent
{
    public enum CheatPurchase
    {
        Gems = 0,
        Locker = 1,
        Silver = 2,
        Energy = 3,
        Kits = 4
    }

    public static void GetCheatPurchase(CheatPurchase Purchase)
    {
        switch (Purchase)
        {
            case CheatPurchase.Gems:
                ZXDataManager.Gems += 1000;
                break;

            case CheatPurchase.Locker:
                if (ZXDataManager.Gems >= 100)
                {
                    ZXDataManager.Gems -= 100;
                    ZXChestRewards.GrantChestRewards();
                }
                break;

            case CheatPurchase.Silver:
                if (ZXDataManager.Gems >= 100)
                {
                    ZXDataManager.Gems -= 100;
                    ZXDataManager.Silver += 1000;
                }
                break;

            case CheatPurchase.Energy:
                if (ZXDataManager.Gems >= 100)
                {
                    ZXDataManager.Gems -= 100;
                    ZXDataManager.Fuel = ZXDataManager.maxFuel;
                }
                break;

            case CheatPurchase.Kits:
                if (ZXDataManager.Silver >= 100)
                {
                    ZXDataManager.Silver -= 100;
                    ZXDataManager.Kits += 1;
                }
                break;
        }
    }
}
