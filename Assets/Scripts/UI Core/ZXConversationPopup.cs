﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class ZXConversationPopup : MonoBehaviour, IPointerClickHandler
{
    private const float clickSpeed = 0.5f;

    public class Series
    {
        public int weight;
        public System.Action condition;
        public List<Slide> slides; 

        private int index = 0;

        public Series(int Weight, System.Action Condition, List<Slide> Slides)
        {
            this.weight = Weight;
            this.condition = Condition;
            this.slides = Slides;
        }

        public bool Complete
        {
            get
            {
                return !(index < slides.Count);
            }
        }

        public System.Type NextType()
        {
            if (index < slides.Count)
            {
                if (slides[index].conversation != null)
                    return typeof(Conversation);
                else if (slides[index].nagivation != null)
                    return typeof(Navigation);
                else
                    return null;
            }
            else
                return null;
        }

        public Conversation NextConversation()
        {
            Conversation conversation = null;

            if (index < slides.Count)
            {
                conversation = slides[index].conversation;
                index++;
            }

            return conversation;
        }

        public Navigation NextNavigation()
        {
            Navigation navigation = null;

            if (index < slides.Count)
            {
                navigation = slides[index].nagivation;
                index++;
            }

            return navigation;
        }
    }

    public class Slide
    {
        public int index;
        public Conversation conversation;
        public Navigation nagivation;

        public Slide(int index, Conversation conversation, Navigation navigation)
        {
            this.index = index;
            this.conversation = conversation;
            this.nagivation = navigation;
        }
    }

    public class Conversation
    {
        public Sprite SlideSprite
        {
            get
            {
                return card.Slide_Image;
            }
        }
        public string SlideName
        {
            get
            {
                return card.Name;
            }
        }
        public string SlideText
        {
            get
            {
                return text;
            }
        }
        public ZXConversationSlide.Position Position
        {
            get
            {
                return card.Type == ZXCard.Card_Type.Hero ? ZXConversationSlide.Position.Left : ZXConversationSlide.Position.Right;
            }
        }

        private ZXCard card;
        private string text;

        public Conversation(string Key, string Text)
        {
            card = ZXDataManager.cards[Key];
            this.text = Text;
        }
    }

    public class Navigation
    {
        public System.Action navInstrc;
        public Vector2 cursorPosition;

        public Navigation(Vector2 Position, System.Action Navigate)
        {
            cursorPosition = Position;
            navInstrc = Navigate;
        }
    }

    [SerializeField] private ZXConversationSlide leftSlide;
    [SerializeField] private ZXConversationSlide rightSlide;
    [SerializeField] private GameObject screen;

    private static ZXConversationPopup singleton;
    private List<Series> series;
    private Series currentSeries = null;
    private bool navShowing = false;
    private float clickCounter = 0f;

    protected void Awake()
    {
        singleton = this;

        gameObject.SetActive(false);

        BuildSeries();

        if (ZXMissionManager.TotalMissionCount == 0)
            PlaySequence(0);
        else if (ZXMissionManager.TotalMissionCount == 1)
            PlaySequence(1);
        else if (ZXMissionManager.TotalMissionCount == 4)
            PlaySequence(3);
    }

    public static void PlaySequence(int index)
    {
        singleton.currentSeries = singleton.series[index];
        singleton.clickCounter = 0f;

        singleton.gameObject.SetActive(true);

        singleton.NextSlide();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        NextSlide();
    }

    public void NextSlide()
    {
        if (clickCounter > 0f)
            return;

        clickCounter = clickSpeed;

        if (currentSeries != null && !currentSeries.Complete)
        {
            System.Type type = currentSeries.NextType();

            if (type == typeof(Conversation))
            {
                Conversation current = currentSeries.NextConversation();

                singleton.screen.SetActive(true);

                switch (current.Position)
                {
                    case ZXConversationSlide.Position.Left:
                        leftSlide.ShowText(current.SlideName, current.SlideText, current.SlideSprite);
                        break;

                    case ZXConversationSlide.Position.Right:
                        rightSlide.ShowText(current.SlideName, current.SlideText, current.SlideSprite);
                        break;
                }
            }
            else if (type == typeof(Navigation))
            {
                Navigation current = currentSeries.NextNavigation();

                if (current.navInstrc != null)
                    current.navInstrc();

                if (current.cursorPosition != Vector2.zero)
                {
                    HidePanels();
                    ZXMessageScreen.ShowHandPosition(current.cursorPosition);
                }
                else
                {
                    ZXMessageScreen.HideHand();
                    clickCounter = 0f;
                    NextSlide();
                }
            }
        }
        else
        {
            ZXMessageScreen.HideHand();
            HidePanels();
            Hide();
        }
    }

    public void HidePanels()
    {
        leftSlide.Hide();
        rightSlide.Hide();

        screen.SetActive(false);
    }

    public void Hide()
    {
        currentSeries = null;

        gameObject.SetActive(false);
    }

    private void Update()
    {
        clickCounter = Mathf.Clamp(clickCounter - Time.deltaTime, 0f, clickSpeed);
    }

    private void BuildSeries()
    {
        series = new List<Series>(5);

        //First series (Introducing Camp Missions)
        List<Slide> tempSlides = new List<Slide>(5);

        tempSlides.Add(new Slide(0, new Conversation("waifu_11", "Thank goodness you are here!"), null));
        tempSlides.Add(new Slide(0, new Conversation("hero_01", "I just need some supplies and I will be on my way."), null));
        tempSlides.Add(new Slide(0, new Conversation("waifu_11", "Please, we need your help. Our friend Angel is trapped!"), null));
        tempSlides.Add(new Slide(0, new Conversation("waifu_11", "We can discuss your reward later. Click on the Launch Button and be careful!"), null));
        tempSlides.Add(new Slide(0, null, new Navigation(new Vector2(-225, -35), 
            delegate() 
            {
                ZXMissionManager.StartMission(ZXMissionManager.Type.Campaign, ZXMissionManager.Difficulty.Easy, 0, ZXCampaignMission.FindMission(0).Mission_ID);
            })));

        Series tempSeries = new Series(1, null, tempSlides);

        series.Add(tempSeries);

        //Second series (Introducing Curing Virus)
        tempSlides = new List<Slide>(10);

        tempSlides.Add(new Slide(0, new Conversation("waifu_02", "Thanks for the assist but the Virus is effecting my abilities and making me... Horny!!"), null));
        tempSlides.Add(new Slide(0, new Conversation("hero_01", "You're infected? One more reason to be on my way!"), null));
        tempSlides.Add(new Slide(0, new Conversation("waifu_11", "Wait, please! The only way to fight the Virus is with the cum of an immune Hero!"), null));
        tempSlides.Add(new Slide(0, new Conversation("waifu_02", "I can't explain it but I want your cum... I want your cum all over me!"), null));
        tempSlides.Add(new Slide(0, null, new Navigation(new Vector2(690, -475), 
            delegate() 
            {
                ZXMainScrollview.SnapToSection(ZXCore.ZXSection.Type.Deck);
                ZXDataManager.cards["hero_01"].Virus = 100;
                ZXDataManager.cards["waifu_02"].Virus = 100;
                ZXCardInfoPopup.Open(ZXDataManager.cards["waifu_02"]);
            })));
        tempSlides.Add(new Slide(0, null, new Navigation(Vector2.zero,
            delegate ()
            {
                ZXMissionManager.TotalMissionCount++;
                ZXRomanceGamePopup.Open(ZXDataManager.cards["waifu_02"], ZXDataManager.cards["hero_01"]);
            })));
        tempSlides.Add(new Slide(0, new Conversation("waifu_11", "Tap on Angel until the virus is cured! I bet she will enjoy it."), null));
        tempSlides.Add(new Slide(0, null, new Navigation(new Vector2(115f, -60), null)));

        tempSeries = new Series(1, null, tempSlides);

        series.Add(tempSeries);

        //Third series (Introducing Climax -> Card Replacement -> Raid Missions)
        tempSlides = new List<Slide>(5);

        tempSlides.Add(new Slide(0, new Conversation("hero_01", "I am almost there"), null));
        tempSlides.Add(new Slide(0, null, new Navigation(new Vector2(780, -450), null)));
        tempSlides.Add(new Slide(0, null, new Navigation(Vector2.zero,
            delegate ()
            {
                ZXRomanceGamePopup.ForceHeal();
                ZXRomanceGamePopup.ForceClimax();
            })));
        tempSlides.Add(new Slide(0, new Conversation("waifu_02", "Oh my gosh, that felt so good. I can't believe how good you are at that."), null));
        tempSlides.Add(new Slide(0, null, new Navigation(Vector2.zero,
            delegate ()
            {
                ZXMainScrollview.SnapToSection(ZXCore.ZXSection.Type.Deck);
                ZXRomanceGamePopup.ForceClose();
                ZXDeckManager.UseCard(ZXCardUI.cards["waifu_02"]);
            })));
        tempSlides.Add(new Slide(0, new Conversation("waifu_02", "Now that I am feeling better please take me into battle with you!"), null));
        tempSlides.Add(new Slide(0, new Conversation("waifu_02", "Just select a card to replace me with."), null));
        tempSlides.Add(new Slide(0, null, new Navigation(new Vector2(-450, 150), null)));
        tempSlides.Add(new Slide(0, null, new Navigation(Vector2.zero,
            delegate ()
            {
                ZXDeckManager.SwapCard(ZXCardUI.cards["waifu_01"]);
            })));
        tempSlides.Add(new Slide(0, new Conversation("waifu_02", "Now let's go do a Raid Mission together."), null));
        tempSlides.Add(new Slide(0, null, new Navigation(Vector2.zero,
            delegate ()
            {
                ZXMainScrollview.SnapToSection(ZXCore.ZXSection.Type.Mission);
                ZXCampaignPopup.ShowPopup(false);
            })));
        tempSlides.Add(new Slide(0, null, new Navigation(new Vector2(100, -250), null)));
        tempSlides.Add(new Slide(0, null, new Navigation(Vector2.zero,
            delegate ()
            {
                ZXMissionManager.TotalMissionCount++;
                ZXMissionManager.StartMission(ZXMissionManager.Type.Battle, ZXMissionManager.Difficulty.Easy, 0, null);
            })));

        tempSeries = new Series(1, null, tempSlides);

        series.Add(tempSeries);

        //Fourth series (Upgrading Chars)
        tempSlides = new List<Slide>(5);

        tempSlides.Add(new Slide(0, new Conversation("waifu_02", "That was so much fun!"), null));
        tempSlides.Add(new Slide(0, new Conversation("waifu_02", "After that mission you now have enough cards to upgrade me."), null));
        tempSlides.Add(new Slide(0, null, new Navigation(new Vector2(100, -450),
            delegate ()
            {
                ZXMainScrollview.SnapToSection(ZXCore.ZXSection.Type.Deck);
                ZXDataManager.cards["waifu_02"].Cards = ZXDataManager.cards["waifu_02"].NextLevelCards;
                ZXCardInfoPopup.Open(ZXDataManager.cards["waifu_02"]);
            })));
        tempSlides.Add(new Slide(0, null, new Navigation(Vector2.zero,
            delegate ()
            {
                ZXMissionManager.TotalMissionCount++;
                ZXCardInfoPopup.ForceUpgrade();
            })));

        tempSeries = new Series(1, null, tempSlides);

        series.Add(tempSeries);

        //Fifth series (Card Upgraded)
        tempSlides = new List<Slide>(5);

        tempSlides.Add(new Slide(0, new Conversation("waifu_02", "Thank you precious! Now I am level 2."), null));
        tempSlides.Add(new Slide(0, new Conversation("waifu_11", "Keep playing to rescue more of us!"), null));

        tempSeries = new Series(1, null, tempSlides);

        series.Add(tempSeries);
    }
}
