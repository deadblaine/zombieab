﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ZXPopup : MonoBehaviour
{
    //Static variables
    private static ZXPopup activePopup;
    private static bool enablePopups = true;

    //Public events
    public event System.Action e_Closed;

    //Public accessors
    public static bool isPopupOpen
    {
        get
        {
            return activePopup != null;
        }
    }

    //Static functions
    public static void CloseActivePopup()
    {
        if (activePopup != null)
            activePopup.Close();
    }

    public static void EnablePopups()
    {
        enablePopups = true;
    }

    public static void DisablePopups()
    {
        enablePopups = false;

        CloseActivePopup();
    }

    //Initalization
    private void Awake()
    {
        Build();
    }

    protected virtual void Build()
    {
    }

    //Private functions
    protected virtual void Open()
    {
        if (enablePopups)
        {
            CloseActivePopup();
            activePopup = this;

            gameObject.SetActive(true);
            gameObject.transform.localScale = Vector3.zero;
            gameObject.transform.DOScale(Vector3.one, 0.5f).SetUpdate(true).SetEase(Ease.OutBack);
        }
    }

    //Public functions
    public virtual void Close()
    {
        activePopup = null;

        gameObject.SetActive(false);

        if (e_Closed != null)
            e_Closed();
    }
}
