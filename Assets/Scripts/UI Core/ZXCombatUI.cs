﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using System;
using DG.Tweening;
using ZXCore;
using UnityEngine.Analytics;
using DarkTonic.MasterAudio;

public class ZXCombatUI : MonoBehaviour
{
    /* Global Variable, Delegate, and Event Declarations */
    #region
    /* Support Enums */
    public enum State
    {
        Main = 0,
        Combat = 1,
        Victory = 2,
        Defeat = 3,
        Paused = 4
    }

    /* Public Constant Variables */
    public const float _tickSpeed = 0.05f;                  // Use for threading wait speed 
    public const float _timeOut = 2f;                       // Use for threading timeout 
    public const float _tickAISpeed = 2f;                // Used for AI objects logic speed
    public const float _tickActSpeed = 1f;                // Used for AI objects act speed
    public const int _targetFPS = 60;                       // Target framerate
    public const char _parseChar = ',';                     // Use for string parsing
    public const float _loadLevelDel = 2f;                  // The wait time before starting next scene

    /* Public Static Accessors */
    public static bool PerInit
    {
        get
        {
            return fpsBuffer.Initalized;
        }
    }                          
    public static float AvgFPS                           
    {
        get
        {
            return fpsBuffer.Average;
        }
    }
    public static Vector2 FpsSlope
    {
        get
        {
            return fpsBuffer.Slope;
        }
    }                     
    public static Vector2 FpsLongSlope
    {
        get
        {
            return fpsBuffer.LongSlope;
        }
    }                 
    public static State CurrentState                          
    {
        get
        {
            return curState;
        }
    }

    public static ZXSpawnManager Spawner
    {
        get
        {
            return spawner;
        }
    }
    public static Transform ObjectMask
    {
        get
        {
            return singleton.objectMask;
        }
    }
    public static float SeaLevel
    {
        get
        {
            return seaLevel;
        }
    }

    /* Global Update Events */
    public static event System.Action<State> e_StateChange;               // Triggers when game state changes  

    /* Time Events */
    public static event System.Action<float> e_MissionClock;            // Signals mission time
    public static event System.Action<float> e_TimeChanged;              // Signals time dilation change

    /* Global Tick Events */
    public static event System.Action<float> e_TickUpdate;               // Triggers each Unity Ud_obpdate()
    public static event System.Action<float> e_TickLazyUpdate;           // Use for things that need to run almost every update
    public static event System.Action<float> e_TickSystem;               // Use for frequent system checks
    public static event System.Action<float> e_TickLogic;                // Use for AI and logic events
    public static event System.Action<float> e_TickLazySystem;           // Use for infrequent system tasks  
    public static event System.Action<float> e_TickManage;               // Use for infrequent managment tasks
    public static event System.Action<float> e_TickClean;                // Use for data managment and cleaning tasks
    #endregion

    /* Global Object Pools */
    #region

    /* FX Pools */
    public static ZXPoolLite<ZXParticleEffect> p_BloodSplatter;                 // Blood splatter Pool
    [SerializeField] private int p_BloodSplatterSize;
    [SerializeField] private ZXParticleEffect[] p_BloodSplatterPrefabs;

    public static ZXPoolLite<ZXParticleEffect> p_BloodPoolLarge;                 // Blood splatter Pool
    [SerializeField] private int p_BloodPoolLargeSize;
    [SerializeField] private ZXParticleEffect[] p_BloodPoolLargePrefabs;

    public static ZXPoolLite<ZXParticleEffect> p_Poof;                           // Ground poof
    [SerializeField] private int p_PoofSize;
    [SerializeField] private ZXParticleEffect[] p_PoofPrefab;

    public static ZXPoolLite<ZXParticleEffect> p_Fire;                            // Fire effect
    [SerializeField] private int p_FireSize;
    [SerializeField] private ZXParticleEffect[] p_FirePrefab;

    public static ZXPoolLite<ZXParticleEffect> p_BulletImpact;                    // Generic Impact effect
    [SerializeField] private int p_BulletImpactSize;
    [SerializeField] private ZXParticleEffect[] p_BulletImpactPrefab;

    public static ZXPoolLite<ZXParticleEffect> p_LargeBulletImpact;               // Large Ground poof
    [SerializeField] private int p_LargeBulletImpactSize;
    [SerializeField] private ZXParticleEffect[] p_LargeBulletImpactPrefab;
    #endregion

    /* Global Texture Fader */
    [SerializeField] private Material[] materials;
    [SerializeField] private float fadeDelay;
    [SerializeField] private float fadeTime;
    private Color startDecalColor = new Color(1f, 1f, 1f, 0f);
    private Color endDecalColor;
    private Color debugDecalColor = new Color(0f, 0f, 0f, 0f);

    /* Enable List */
    [SerializeField] private List<GameObject> enableList;

    public static event System.Action<Color, float, float> e_DecalTo;

    /* Private Static */
    #region
    //Event tick speeds
    private const int eTickLazyUpdate = 5;
    private const int eTickSystem = 30;
    private const int eTickLogic = 50;
    private const int eTickLazySystem = 150;
    private const int eTickManage = 300;
    private const int eTickClean = 1200;
    #endregion

    /* ------------------ Start Actual Class ------------------ */

    /* Private Variables */
    #region
    //Private Constant Variables
    private const int bufferSize = 25;

    //Counters, Timers and Tracking
    private static ZXfBuffer fpsBuffer;
    private static List<IZXAI> aiThreader;
    private static State curState = State.Combat;
    private static float cTickLazyUpdate = 0f;
    private static float cTickSystem = 0f;
    private static float cTickLogic = 0f;
    private static float cTickLazySystem = 0f;
    private static float cTickManage = 0f;
    private static float cTickClean = 0f;
    private static int cAIIndex = 0;
    private static float cAITimer = 0f;
    private static int cActIndex = 0;
    private static float cActTimer = 0f;
    private static int frameCounter = 0;
    private static int sampleFrameCounter = 0;
    [SerializeField] private float missionClock;
    private static float curMissionClock;
    private static float seaLevel;

    public static List<IZXInstructable> ActivePlayers
    {
        get
        {
            return activePlayers;
        }
    }

    //Registered objects
    private static List<IZXInstructable> activePlayers;

    //Private delegates
    private static Action<float> d_SamplePerformance = new System.Action<float>(SamplePerformance);
    private static Action<float> d_UpdatePerformance = new System.Action<float>(UpdatePerformance);
    private static Action<float> d_TickMissionClock = new System.Action<float>(TickMissionClock);
    private static UnityAction<Scene, LoadSceneMode> d_SceneLoaded;

    #endregion

    [SerializeField] private ZXEndOfMissionScreen endOfMissionPopup;
    [SerializeField] private ZXPausePopup pausePopup;
    [SerializeField] private ZXTutorial tutorial;

    //Singleton
    public static ZXCombatUI singleton;
    private static ZXSpawnManager spawner;

    //Flags
    private static bool holdMissionClock = true;
    public static bool alreadyFaded = false;

    //Object mask
    private Transform objectMask;
    private string track;

    /* Initalization */
    #region
    private void Awake()
    {
        singleton = this;

        alreadyFaded = false;

        //Create the object mask
        objectMask = Instantiate(Resources.Load<Transform>("Ball"));
        objectMask.gameObject.name = "Global Object Mask";
        objectMask.SetParent(null);
        objectMask.localScale = new Vector3(1f, 1f, 1f);
        objectMask.position = Vector3.zero;
        objectMask.rotation = Quaternion.identity;

        //Create buffers
        fpsBuffer = new ZXfBuffer(bufferSize);
        aiThreader = new List<IZXAI>(100);

        //Register internal functions with events
        d_SceneLoaded = new UnityAction<Scene, LoadSceneMode>(SceneLoaded);

        e_TickSystem += d_SamplePerformance;
        e_TickLazySystem += d_UpdatePerformance;
        e_TickSystem += d_TickMissionClock;

        SceneManager.sceneLoaded += d_SceneLoaded;

        activePlayers = new List<IZXInstructable>(50);

        curMissionClock = missionClock;

        if (materials != null && materials.Length > 0)
        {
            if (materials[0] != null && materials[0].HasProperty("_DecalColor"))
            {
                endDecalColor = materials[0].GetColor("_DecalColor");
            }

            foreach (Material mat in materials)
            {
                if (mat != null && mat.HasProperty("_DecalColor"))
                    mat.SetColor("_DecalColor", startDecalColor);
            }
        }

        foreach (GameObject Obj in enableList)
        {
            Obj.SetActive(true);
        }
    }

    private void Start()
    {
        curState = State.Combat;

        FindSeaLevel();

        ZXMissionClock.instance.ShowClock();
        ZXMissionClock.instance.ShowStars();

        ZXManaBar.Show();

        StartMissionClock();

        LoadCombatPools();

        spawner.InitalizeSpawner(ZXSpawnData.SpawnType.Zombie, ZXSpawnData.SpawnDifficulty.Level_2, ZXSpawnData.DeckType.Town2, false);
    }

    //Level load event
    private void SceneLoaded(Scene Scene, LoadSceneMode Mode)
    {
        if (e_StateChange != null)
            e_StateChange(curState);

        spawner.BeginSpawning(2f);

        StartCoroutine(SpawnHero());
    }

    /*Creates global collections and connect to events*/
    private IEnumerator SpawnHero()
    {
        yield return new WaitForSeconds(2f);

        ZXHeroSpawn spawn = ZXReflectingPool.FindClosest<ZXHeroSpawn>(transform);

        if (spawn == null)
            spawn = GameObject.Find("HeroSpawn").GetComponent<ZXHeroSpawn>();

        ZXHuman hero = ZXCombatDeck.p_Hero.Request() as ZXHuman;

        hero.Place(spawn.transform.position + new Vector3(0f, 2.5f, 0f), Quaternion.Euler(0f, 270f, 0f), ZXToon.Action.DragFromUI);

        yield return new WaitForSeconds(0.5f);

        hero.Place(spawn.transform.position + new Vector3(0f, 2.5f, 0f), Quaternion.Euler(0f, 270f, 0f), ZXToon.Action.LandFromFalling);

        ActivePlayers.Clear();
        ActivePlayers.Add(hero);

        hero.OverrideDefaultBehaviorPattern(ZXHuman.BehaviorPattern.Player);

        ZXGroundEvents.CanMoveHero = true;

        if (!tutorial.TutorialShown)
            tutorial.Show();
    }

    private void LoadCombatPools()
    {
        p_BloodSplatter = new ZXPoolLite<ZXParticleEffect>(ObjectMask, p_BloodSplatterPrefabs, p_BloodSplatterSize);
        p_BloodPoolLarge = new ZXPoolLite<ZXParticleEffect>(ObjectMask, p_BloodPoolLargePrefabs, p_BloodPoolLargeSize);
        p_Poof = new ZXPoolLite<ZXParticleEffect>(ObjectMask, p_PoofPrefab, p_PoofSize);
        p_Fire = new ZXPoolLite<ZXParticleEffect>(ObjectMask, p_FirePrefab, p_FireSize);
        p_BulletImpact = new ZXPoolLite<ZXParticleEffect>(ObjectMask, p_BulletImpactPrefab, p_BulletImpactSize);
        p_LargeBulletImpact = new ZXPoolLite<ZXParticleEffect>(ObjectMask, p_LargeBulletImpactPrefab, p_LargeBulletImpactSize);
    }

    #endregion

    public void FadeTextures()
    {
        if (!alreadyFaded)
        {
            alreadyFaded = true;

            if (materials != null && materials.Length > 0)
            {
                foreach (Material mat in materials)
                {

                    if (mat != null && mat.HasProperty("_DecalColor"))
                        mat.DOColor(endDecalColor, "_DecalColor", fadeTime).SetDelay(fadeDelay);
                }
            }

            if (e_DecalTo != null)
                e_DecalTo(endDecalColor, fadeTime, fadeDelay);
        }
    }

    private void FindSeaLevel()
    {
        //Using a fixed value for the time being
        seaLevel = 0.1f;
    }

    /* Update */
    #region   
    private void Update()
    {
        //if(SceneManager. == 
        //Update tick counters
        cTickLazyUpdate += Time.deltaTime;
        cTickSystem += Time.deltaTime;
        cTickLogic += Time.deltaTime;
        cTickLazySystem += Time.deltaTime;
        cTickManage += Time.deltaTime;
        cTickClean += Time.deltaTime;

        //Update frameCounters and time stamp
        frameCounter++;
        sampleFrameCounter++;

        //Test for max value
        if (frameCounter == int.MaxValue)
            frameCounter = 0;

        /* Test for event triggers */

        //Resignals update to any class that needs it
        if (e_TickUpdate != null)
            e_TickUpdate(Time.deltaTime);

        //Raise LazyUpdate
        if (Math.Truncate((float)frameCounter % eTickLazyUpdate) == 0)
        {
            if (e_TickLazyUpdate != null)
                e_TickLazyUpdate(cTickLazyUpdate);

            cTickLazyUpdate = 0f;
        }

        //Raise System
        if (Math.Truncate((float)frameCounter % eTickSystem) == 0)
        {
            if (e_TickSystem != null)
                e_TickSystem(cTickSystem);

            cTickSystem = 0f;
        }

        //Raise Logic
        if (Math.Truncate((float)frameCounter % eTickLogic) == 0)
        {
            if (e_TickLogic != null)
                e_TickLogic(cTickLogic);

            cTickLogic = 0f;
        }

        //Raise LazySystem
        if (Math.Truncate((float)frameCounter % eTickLazySystem) == 0)
        {
            if (e_TickLazySystem != null)
                e_TickLazySystem(cTickLazySystem);

            cTickLazySystem = 0f;
        }

        //Raise Manage
        if (Math.Truncate((float)frameCounter % eTickManage) == 0)
        {
            if (e_TickManage != null)
                e_TickManage(cTickManage);

            cTickManage = 0f;
        }

        //Raise Clean
        if (Math.Truncate((float)frameCounter % eTickClean) == 0)
        {
            if (e_TickClean != null)
                e_TickClean(cTickClean);

            cTickClean = 0f;
        }

        if (curState == State.Combat)
        {
            if (aiThreader.Count > 0)
            {
                cAITimer += Time.deltaTime;
                cActTimer += Time.deltaTime;

                int threadAIAmount = Mathf.FloorToInt(cAITimer / (_tickAISpeed / aiThreader.Count));
                int threadActAmount = Mathf.FloorToInt(cActTimer / (_tickActSpeed / aiThreader.Count));

                //Spool through analyze AI
                for (int i = 0; i < threadAIAmount; i++, cAIIndex++)
                {
                    //Reset check
                    if (cAIIndex >= aiThreader.Count)
                        cAIIndex = 0;

                    aiThreader[cAIIndex].Analyze();

                    cAITimer -= _tickAISpeed / aiThreader.Count;
                }

                //Spool through act AI
                for (int i = 0; i < threadActAmount; i++, cActIndex++)
                {
                    //Reset check
                    if (cActIndex >= aiThreader.Count)
                        cActIndex = 0;

                    if (!aiThreader[cActIndex].ActionWait)
                        aiThreader[cActIndex].Act();

                    cActTimer -= _tickActSpeed / aiThreader.Count;
                }
            }
        }
    }
    #endregion

    /* Private Static Functions */
    #region

    //Signals mission time
    public static void StartMissionClock()
    {
        holdMissionClock = false;
    }

    private static void TickMissionClock(float deltaTime)
    {
        if (curState == State.Combat)
        {
            if (!holdMissionClock)
                curMissionClock -= deltaTime;

            if (curMissionClock <= 0)
            {
                if(ZXMissionClock.StarCount < 2)
                {
                    ChangeMissionState(State.Defeat);
                }
                else
                {
                    ChangeMissionState(State.Victory);
                }
            }

            if (e_MissionClock != null)
                e_MissionClock(curMissionClock);
        }
    }

    //Updates game state
    public void PauseMission()
    {
        if (CurrentState != State.Paused)
            ChangeMissionState(State.Paused);
    }

    public static void ChangeMissionState(State NewState)
    {
        if (NewState == curState || curState == State.Victory || curState == State.Defeat)
            return;

        curState = NewState;

        switch (NewState)
        {
            case State.Combat:
                ScaleTime(1f);
                break;
            case State.Paused:
                singleton.pausePopup.Show();
                ScaleTime(0f);
                break;
            case State.Victory:
                singleton.endOfMissionPopup.Show();
                holdMissionClock = true;
                ScaleTime(1f);
                break;
            case State.Defeat:
                singleton.endOfMissionPopup.Show();
                holdMissionClock = true;
                ScaleTime(1f);
                break;
            default:
                ScaleTime(1f);
                break;
        }

        if (e_StateChange != null)
            e_StateChange(curState);
    }

    //Logs performance data and stores it in back buffer
    private static void SamplePerformance(float deltaTime)
    {
        //Record data into a XoTPoint
        XoTPoint newSample = new XoTPoint(sampleFrameCounter, deltaTime);

        //Add the data to back buffer for later analysis
        fpsBuffer.Add(newSample);

        //Reset counters
        sampleFrameCounter = 0;
    }

    //Orders analysis on buffered data
    private static void UpdatePerformance(float deltaTime)
    {
        fpsBuffer.Analyze();
    }
    #endregion

    /* Global Static Functions */
    #region

    private static void LevelEnd()
    {
        curMissionClock = singleton.missionClock;

        aiThreader.Clear();
    }

    //Scales the flow of time
    public static void ScaleTime(float Scale)
    {
        if (Time.timeScale != Scale)
        {
            DOTween.Kill("time");
            DOTween.To(delegate () { return Time.timeScale; }, delegate (float var) { Time.timeScale = var; }, Scale, 0.25f).SetId("time").SetUpdate(true);
        }

        if (e_TimeChanged != null)
            e_TimeChanged(Scale);
    }

    //Registration Functions
    public static void Register(IZXInstructable Unit)
    {
        activePlayers.Add(Unit);
    }

    public static void Register(ZXSpawnManager Spawner)
    {
        spawner = Spawner;
    }
    public static void Register(IZXAI AI)
    {
        aiThreader.Add(AI);
    }

    public static void DeRegister(IZXAI AI)
    {
        if (aiThreader.Contains(AI))
            aiThreader.Remove(AI);
    }

    //Reset
    private void CleanUp()
    {
        //Reset events
        e_MissionClock = null;
        e_StateChange = null;
    }
    #endregion

    /*Destruction*/
    private void OnDisable()
    {
        if (materials != null && materials.Length > 0)
        {
            foreach (Material mat in materials)
            {
                if (mat != null && mat.HasProperty("_DecalColor"))
                    mat.SetColor("_DecalColor", endDecalColor);
            }
        }

        spawner = null;
    }

    private void OnDestroy()
    {
        e_TickSystem -= d_SamplePerformance;
        e_TickLazySystem -= d_UpdatePerformance;
        e_TickSystem -= d_TickMissionClock;

        SceneManager.sceneLoaded -= d_SceneLoaded;

        DOTween.KillAll();
    }
}
