﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class ZXRomanceGamePopup : ZXCore.ZXPopup
{
    //Inspector variables
    [SerializeField] private Image mainImage;
    [SerializeField] private Image heroFill;
    [SerializeField] private TextMeshProUGUI heroText;
    [SerializeField] private Image waifuFill;
    [SerializeField] private TextMeshProUGUI waifuText;
    [SerializeField] private Image climax1Fill;
    [SerializeField] private Image climax2Fill;
    [SerializeField] private Image climax3Fill;
    [SerializeField] private Image climaxComplete;
    [SerializeField] private ZXFloatingVirus virusIcon;
    [SerializeField] private List<ZXRomanceButton> romanceButtons;

    //Private game variables
    private int maxVirility = 0;
    private int virility = 0;

    private int Virility
    {
        get
        {
            return virility;
        }
        set
        {
            virility = Mathf.Clamp(value, 0, maxVirility);

            hero.Virus = virility;

            heroFill.fillAmount = (float)virility / (float)maxVirility;

            heroText.text = string.Format("{0:0%}", (float)virility / (float)maxVirility);
        }
    }

    private int maxVirus = 0;
    private int virus = 0;

    private int Virus
    {
        get
        {
            return virus;
        }
        set
        {
            virus = Mathf.Clamp(value, 0, maxVirus);

            card.Virus = virus;

            waifuFill.fillAmount = (float)virus / (float)maxVirus;

            waifuText.text = string.Format("{0:0%}", (float)virus / (float)maxVirus);
        }
    }

    private const int maxClimax = 100;
    private int climax = 0;
    private int climaxIndex = 0;
    private bool canClimax = false;

    private int Climax
    {
        get
        {
            return climax;
        }
        set
        {
            climax = Mathf.Clamp(value, 0, maxClimax);

            if (climax >= maxClimax)
            {
                ClimaxIndex++;
                climax = 0;
            }
            else
            {
                float climaxPercent = (float)climax / (float)maxClimax;

                switch (climaxIndex)
                {
                    case 1:
                        climax1Fill.fillAmount = climaxPercent;
                        break;
                    case 2:
                        climax2Fill.fillAmount = climaxPercent;
                        break;
                    case 3:
                        climax3Fill.fillAmount = climaxPercent;
                        break;
                }
            }
        }
    }

    private int ClimaxIndex
    {
        get
        {
            return climaxIndex;
        }
        set
        {
            climaxIndex = Mathf.Clamp(value, 1, 4);

            switch (climaxIndex)
            {
                case 1:
                    climax1Fill.fillAmount = 0f;
                    climax2Fill.fillAmount = 0f;
                    climax3Fill.fillAmount = 0f;
                    climaxComplete.color = Color.white;
                    break;
                case 2:
                    climax1Fill.fillAmount = 1f;
                    climax2Fill.fillAmount = 0f;
                    climax3Fill.fillAmount = 0f;
                    climaxComplete.color = Color.green;
                    canClimax = true;
                    if (FirstClimax)
                    {
                        FirstClimax = true;
                        ZXConversationPopup.PlaySequence(2);
                    }
                    break;
                case 3:
                    climax1Fill.fillAmount = 1f;
                    climax2Fill.fillAmount = 1f;
                    climax3Fill.fillAmount = 0f;
                    climaxComplete.color = Color.green;
                    canClimax = true;
                    break;
                case 4:
                    climax1Fill.fillAmount = 1f;
                    climax2Fill.fillAmount = 1f;
                    climax3Fill.fillAmount = 1f;
                    climaxComplete.color = Color.green;
                    canClimax = true;
                    break;
            }
        }
    }

    private bool FirstClimax
    {
        get
        {
            return !PlayerPrefs.HasKey("first_climax");
        }
        set
        {
            if (value)
                PlayerPrefs.SetInt("first_climax", 0);
        }
    }

    //Private variables
    private static ZXRomanceGamePopup singleton;
    private static ZXCard card;
    private static ZXCard hero;

    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();

        singleton = this;

        gameObject.SetActive(false);
    }

    public static void Open(ZXCard Card, ZXCard Hero)
    {
        card = Card;
        hero = Hero;

        singleton.Show();
    }

    public override void Show()
    {
        ResetGame();
        LoadRomance();

        base.Show();
    }

    private void LoadRomance()
    {
        foreach (ZXRomanceButton button in romanceButtons)
        {
            button.gameObject.SetActive(false);
        }

        if (card.cardRomance != null && card.cardRomance.Count > 0)
        {
            for (int i = 0; i < card.cardRomance.Count; i++)
            {
                romanceButtons[i].LoadImage(card.cardRomance[i].Image, card.cardRomance[i].Thumb, card.Rank < card.cardRomance[i].Requiered_Rank);
                romanceButtons[i].gameObject.SetActive(true);
            }

            romanceButtons[0].Select();
        }
    }

    public static void ShowNewRomanceImage(Sprite sprite)
    {
        singleton.mainImage.sprite = sprite;
    }

    /*Mini-Game*/
    private void ResetGame()
    {
        maxVirility = hero.MaxVirus;
        Virility = hero.Virus;

        maxVirus = card.MaxVirus;
        Virus = card.Virus;

        Climax = 0;
        ClimaxIndex = 1;
        canClimax = false;
        climaxComplete.color = Color.white;
    }

    private int tapVirility = 1;
    private int tapVirus = 2;
    private int tapClimax = 15;
    private int useClimax = 15;
    private float vSprd = 200;

    public void ImageTap()
    {
        if (Virility > 0 && Virus > 0)
        {
            Virility -= tapVirility;
            Virus -= tapVirus;
            Climax += tapClimax;

            CreateVirusIcon();
        }
        //else
            //ResetGame();
    }

    private void CreateVirusIcon()
    {
        ZXFloatingVirus newVirus = Instantiate<ZXFloatingVirus>(virusIcon);

        newVirus.gameObject.transform.SetParent(mainImage.transform);
        newVirus.GetComponent<RectTransform>().anchoredPosition = new Vector3(Random.Range(-vSprd, vSprd), Random.Range(-vSprd, vSprd), Random.Range(-vSprd, vSprd));

        newVirus.StartFalling();
    }

    public static void ForceHeal()
    {
        singleton.Virus = 0;
    }

    public static void ForceClimax()
    {
        singleton.ClimaxTap();
    }

    public static void ForceClose()
    {
        singleton.Hide();
    }

    public void ClimaxTap()
    {
        if (canClimax)
        {
            canClimax = false;
            climaxComplete.color = Color.white;

            if (ClimaxIndex == 4)
                Virus -= useClimax * 3;
            else
                Virus -= useClimax;

            //if (Virility == 0 || Virus == 0)
                //ResetGame();
        }
    }

    /*Disable*/ 
    public override void Hide()
    {
        hero = null;
        card = null;

        base.Hide();
    }

    private void OnDisable()
    {
        DOTween.Kill(this);
        StopAllCoroutines();
    }
}
