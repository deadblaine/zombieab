﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using DG.Tweening;
using DarkTonic.MasterAudio;

public class ZXCreepingBarrage : ZXSpell
{
    [SerializeField] private ZXExplosive explosivePrefab;
    private ZXPoolLite<ZXProjectile> explosives;

    [SerializeField] private int shellNumber;
    [SerializeField] private float shellDelay;
    [SerializeField] private float shellSpacing;
    [SerializeField] private float dropHeight;
    [SerializeField] private float hitRadius;
    [SerializeField] private float timeOnTarget;
    private int explosiveDamage;

    [SoundGroupAttribute] [SerializeField] protected string s_Shot;

    protected override void Awake()
    {
        base.Awake();

        explosives = new ZXPoolLite<ZXProjectile>(transform, explosivePrefab, shellNumber);
    }

    public override void Place(Vector3 location, Quaternion rotation, object args)
    {
        ZXMapExit exit = ZXReflectingPool.FindClosest<ZXMapExit>(transform, delegate (ZXMapExit obj) { return obj.Race == ToonRace.Human; });

        if (exit == null)
            exit = GameObject.Find("MapExit").GetComponent<ZXMapExit>();

        Vector3 origin = exit.transform.position;
        Vector3 direction;

        //create direction
        direction = (location - origin).normalized;

        //Create rotation
        if (direction != Vector3.zero)
            rotation = Quaternion.LookRotation(direction);

        base.Place(location, rotation, args);

        if (args is bool && (bool)args)
        {
            StartCoroutine(PlaceArtillery(location));
        }
    }

    private IEnumerator PlaceArtillery(Vector3 location)
    {
        Vector3 currentLocation;
        Vector3 origin;
        Ray ray;
        float currentDistance;
        ZXExplosive explosive;

        ZXMapExit exit = ZXReflectingPool.FindClosest<ZXMapExit>(transform, delegate (ZXMapExit obj) { return obj.Race == ToonRace.Human; });

        if (exit == null)
            exit = GameObject.Find("MapExit").GetComponent<ZXMapExit>();

        origin = exit.transform.position;
        ray = new Ray(origin, (location - origin).normalized);
        currentDistance = Vector3.Distance(location, origin);

        origin.y = dropHeight;

        if (image != null)
            image.FadeOutPartial();

        for (int i = 0; i < shellNumber; i++)
        {
            currentLocation = ray.GetPoint(currentDistance) + new Vector3(Random.Range(-hitRadius, hitRadius), 0f, Random.Range(-hitRadius, hitRadius));

            //Increase the firing location
            currentDistance += shellSpacing;

            explosive = explosives.Request() as ZXExplosive;

            if (explosive != null)
            {
                //explosive.SetExplosiveDamage(explosiveDamage);
                explosive.Place(origin, Quaternion.identity, null);
                explosive.Arm();
                explosive.Launch(currentLocation, timeOnTarget);
            }

            MasterAudio.PlaySound3DAtTransformAndForget(s_Shot, transform);

            yield return new WaitForSeconds(shellDelay);
        }

        yield return new WaitForSeconds(timeOnTarget);

        if (image != null)
            image.FadeOut();

        yield return new WaitForSeconds(ZXSpellTargetingImage._fadeOutTime);

        Disable();
    }

    public override void AdjustValues(int Index, int Level)
    {
    }
}
