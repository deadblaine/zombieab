﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using DG.Tweening;
using DarkTonic.MasterAudio;

public class ZXFlare : ZXSpell
{
    [SoundGroupAttribute] [SerializeField] protected string s_Throw;

    [SerializeField] private ZXExplosive flarePrefab;

    [SerializeField] private float dropHeight;
    [SerializeField] private float hitRadius;
    [SerializeField] private float spawnSpeed;
    [SerializeField] private float timeOnTarget;

    private ZXExplosive flare;
    private ZXInterestPoint attractor;

    public float Duration
    {
        get
        {
            return duration; 
        }
    }

    private float duration;

    protected override void Awake()
    {
        base.Awake();

        flare = Instantiate<ZXExplosive>(flarePrefab);
        attractor = flare.GetComponent<ZXInterestPoint>();
    }

    public override void Place(Vector3 location, Quaternion rotation, object args)
    {
        base.Place(location, rotation, args);

        if (args is bool && (bool)args)
        {
            StartCoroutine(PlaceFlare(location));
        }
    }

    private IEnumerator PlaceFlare(Vector3 location)
    {
        Vector3 currentLocation;
        Vector3 origin;

        ZXMapExit exit = ZXReflectingPool.FindClosest<ZXMapExit>(transform, delegate (ZXMapExit obj) { return obj.Race == ToonRace.Human; });

        if (exit == null)
            exit = GameObject.Find("MapExit").GetComponent<ZXMapExit>();

        origin = exit.transform.position;
        origin.y = dropHeight;

        currentLocation = location + new Vector3(Random.Range(-hitRadius, hitRadius), 0f, Random.Range(-hitRadius, hitRadius));

        if (image != null)
            image.FadeOutPartial();

        if (flare != null)
        {
            flare.Place(origin, Quaternion.identity, null);
            flare.Arm();
            flare.Launch(currentLocation, timeOnTarget);
        }

        MasterAudio.PlaySound3DAtTransformAndForget(s_Throw, transform);

        yield return new WaitForSeconds(timeOnTarget);

        if (image != null)
            image.FadeOut();

        yield return new WaitForSeconds(ZXSpellTargetingImage._fadeOutTime);

        Disable();
    }

    public override void AdjustValues(int Index, int Level)
    {
    }
}