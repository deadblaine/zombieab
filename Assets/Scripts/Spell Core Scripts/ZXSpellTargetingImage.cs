﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using ZXCore;
using DG.Tweening;
using System.Collections.Generic;

public class ZXSpellTargetingImage : MonoBehaviour
{
    //Consants
    private const float _pulseScale = 1.1f;
    private const float _pulseTime = 0.2f;
    private const float _fadeInTime = 0.25f;
    public const float _fadeOutTime = 3f;
    private const float _fadePartialValue = 0.5f;

    private Image targetImage;
    private Color targetImageColor;
    private RectTransform rectTrans;

    private int uid;
    private string r_ColorID;
    private string r_ScaleID;
    private string r_RotationID;

    private void Awake()
    {
        //Get UID
        uid = gameObject.GetInstanceID();

        //Get important components
        targetImage = GetComponent<Image>();
        targetImageColor = targetImage.color;
        rectTrans = GetComponent<RectTransform>();

        //Configure IDs
        r_ColorID = uid + "_colorID";
        r_ScaleID = uid + "_scaleID";
        r_RotationID = uid + "_rotationID";
    }

    private void OnEnable()
    {
        targetImage.color = Color.clear;
    }

    public void FadeIn()
    {
        if (DOTween.IsTweening(r_ColorID))
            return;

        targetImage.DOColor(targetImageColor, _fadeInTime).SetId(r_ColorID).SetUpdate(true);
    }

    public void FadeOutPartial()
    {
        targetImage.DOFade(_fadePartialValue, _fadeInTime).SetId(r_ColorID).SetUpdate(true);
    }

    public void FadeOut()
    {
        if (DOTween.IsTweening(r_ColorID))
            return;

        targetImage.DOColor(Color.clear, _fadeOutTime).SetId(r_ColorID).SetUpdate(true);
    }

    public void Pulse()
    {
        if (DOTween.IsTweening(r_ScaleID))
            return;

        rectTrans.DOScale(_pulseScale, _pulseTime).SetId(r_ScaleID).SetLoops(2, LoopType.Yoyo);
    }
}
