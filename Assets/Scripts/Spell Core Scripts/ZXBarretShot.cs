﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using DG.Tweening;
using DarkTonic.MasterAudio;

public class ZXBarretShot : ZXSpell
{
    [SerializeField] private float radius;
    [SerializeField] private int shotDamage;
    private ToonRace race = ToonRace.Human;
    [SerializeField] private float delay;
	[SerializeField] private float particleDelay;
    [SerializeField] private ParticleSystem effect;
    [SerializeField] private float forceMod;

    [SoundGroupAttribute] [SerializeField] protected string s_BulletWhiz;

    protected override void Awake()
    {
        base.Awake();

        if (effect != null)
        {
            effect = Instantiate<ParticleSystem>(effect);
            effect.transform.SetParent(transform);
            effect.transform.localPosition = Vector3.zero;
            effect.transform.rotation = Quaternion.identity; 
        }
    }

    public override void Place(Vector3 location, Quaternion rotation, object args)
    {
        Vector3 newLocation = location;
        Quaternion newRotation = Quaternion.identity;

        ZXMapExit exit = ZXReflectingPool.FindClosest<ZXMapExit>(transform, delegate (ZXMapExit obj) { return obj.Race == ToonRace.Human; });

        if (exit == null)
            exit = GameObject.Find("MapExit").GetComponent<ZXMapExit>();

        Vector3 origin = exit.transform.position;
        Vector3 direction;

        //Adjust height
        newLocation.y = 2.2f;
        origin.x += 100f;
        origin.y = 2.2f;

        //create direction
        direction = (newLocation - origin).normalized;

        //Create rotation
        if (direction != Vector3.zero)
            rotation = Quaternion.LookRotation(direction);

        base.Place(origin, rotation, args);

        if (args is bool && (bool)args)
        {
            StartCoroutine(FireShot());

            MasterAudio.PlaySound3DAtTransform(s_BulletWhiz, transform);
        }
    }

    private IEnumerator FireShot()
    {
		//Wait for the particle delay
		yield return new WaitForSeconds(particleDelay);

		if (effect != null)
			effect.Play();

        MasterAudio.PlaySound3DAtTransformAndForget(s_Spawn, transform);

        //Subtract the particle delay from the delay to trigger the actual effect
        yield return new WaitForSeconds(delay - particleDelay);

        Ray traj = new Ray(transform.position, transform.forward);
        ZXDamage damage = new ZXDamage(shotDamage, 1f, forceMod, 0f, ToonRace.Human, ZXDamage.Type.Projectile, traj, ZXDamage.SpecialEffect.ForceRagdoll);

        List<IZXDamageable> DamageableList = ZXReflectingPool.SphereCast<IZXDamageable>(traj, radius, 500f, delegate (IZXDamageable dmg) { return dmg.Race != race; });

        if (DamageableList != null && DamageableList.Count > 0)
        {
            for (int i = 0; i < DamageableList.Count; i++)
            {
                //Look on the objects parent
                if (DamageableList[i] != null)
                {
                    DamageableList[i].TakeDamage(damage);
                }
            }
        }

        if (image != null)
            image.FadeOut();

        yield return new WaitForSeconds(effect.duration + 0.25f);

        if (effect != null)
            effect.Stop();

        MasterAudio.FadeOutSoundGroupOfTransform(transform, s_BulletWhiz, 0.25f);

        Disable();
    }

    public override void AdjustValues(int Index, int Level)
    {
    }
}
