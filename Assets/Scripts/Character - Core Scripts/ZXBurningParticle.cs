﻿using UnityEngine;
using System.Collections;
using ZXCore;

public class ZXBurningParticle : MonoBehaviour {

	ZXToon toon;
	ZXDamage damageVar;

	void Awake()
	{
		toon = GetComponentInParent<ZXToon>();

		Ray ray = new Ray(transform.position, transform.position);
	}
	
	// Update is called once per frame
	void Update () 
	{
		toon.TakeDamage(damageVar);
	}
}
