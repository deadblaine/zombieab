﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using Vectrosity;
using DG.Tweening;
using DarkTonic.MasterAudio;

[System.Serializable]
public class ZXHuman : ZXToon, IZXInstructable
{
    private const float _FleeDistance = 3f;
    private const float _FleeRange = 10f;
    private const float _IdleRange = 10f;
    private const float _RescueRange = 10f;
    private const float _dropHeight = 3f;
    private const float _guardRadius = 10f;
    private const float _heroHealRate = 50f;

    /* Support Types */
    public enum BehaviorPattern
    {       
        Nothing = 0,
        Idle = 1,
        Corpse = 2,
        Player = 3,
        RangedDefense = 4,
        RangedAssualt = 5,
        MeleeAssualt = 7,
        Civilian = 9,
        FleeMap = 10,
    }
    public enum CallbackCode
    {
        //Default
        None = 0,

        //Weapon callbacks
        StartReload = 100,
        EndReload = 101,
        MeleeDamage = 102,
        GenericMeleeDamage = 103,
        MeleeDamageFrontRight = 104,
        MeleeDamageFrontLeft = 105,
        MeleeDamageRearRight = 106,
        MeleeDamageRearLeft = 107,
        WeaponTrailStart = 120,
        WeaponTrailStop = 121,

        //Char callbacks
        WeaponSwap = 200,
        ReleaseThrown = 201,
    }
	public enum MovementType 
	{
		BaseMovement = 0,
		NoWeapon = 1
	}

    /* Inspector Variables */
    [SerializeField] protected BehaviorPattern toonBehavior;
    protected BehaviorPattern originalToonBehavior;

    [SoundGroupAttribute][SerializeField] private string s_Voice;
    [SoundGroupAttribute][SerializeField] protected string s_GroundImpact;

    //Accessors
    public ZXWeapon CurrentWeapon
    {
        get
        {
            return primaryWeapon;
        }
    }

    //Important Transforms
    protected ZXHolster t_LeftHandHolster;
    protected ZXHolster t_RightHandHolster;
    protected ZXHolster t_BackHolster;
    protected ZXHolster t_BackAltHolster;
    protected ZXHolster t_BackpackHolster;
    protected ZXHolster t_LeftHipFrontHolster;
    protected ZXHolster t_LeftHipBackHolster;
    protected ZXHolster t_LeftShieldHolster;
    protected ZXHolster t_RightHipHolster;
    protected List<ZXHolster> t_Holsters;

    //Gear
    [SerializeField] private ZXWeapon primaryWeaponPrefab;
    protected ZXWeapon primaryWeapon;
    [SerializeField] private ZXEquipment backpackPrefab;
    protected ZXEquipment backpack;
    [SerializeField] private ZXEquipment shieldPrefab;
    protected ZXEquipment shield;

    //Events
    public event System.Action<ZXWeapon> e_WeaponSwap;

    //Behavior delegates
    private System.Func<int> d_Player;
    private System.Func<int> d_RangedDefense;
    private System.Func<int> d_RangedAssualt;
    private System.Func<int> d_MeleeAssualt;
    private System.Func<int> d_Civilian;
    private System.Func<int> d_FleeMap;
    protected System.Predicate<ZXToon> d_StdMeleeCheck;
    protected System.Predicate<ZXObjective> d_StdObjMeleeCheck;

    protected System.Action d_Reload;

    //Private variables
    protected float damageMod = 1f;
    protected float healthMod = 1f;

    private ZXObjective tetherObjective = null;
    private bool weaponDrawn = false;

    private System.Action<ZXObjective> d_ObjectiveCompleted;

    /* Initalization */
    #region
    protected override void Awake()
    {
        base.Awake();

        originalToonBehavior = toonBehavior;

        //Create internal delegates
        d_Player = new System.Func<int>(PlayerBehavior);
        d_RangedDefense = new System.Func<int>(RangedDefenseBehavior);
        d_RangedAssualt = new System.Func<int>(RangedAssualtBehavior);
        d_MeleeAssualt = new System.Func<int>(MeleeAssualtBehvaior);
        d_Civilian = new System.Func<int>(CivilianBehavior);
        d_FleeMap = new System.Func<int>(FleeMap);
        d_ObjectiveCompleted = new System.Action<ZXObjective>(FindObjective);

        d_Reload = new System.Action(Reload);

        LoadHolsters();

        LoadEquipment();
        LoadWeapons();

        t_Holsters = new List<ZXHolster>(GetComponentsInChildren<ZXHolster>());
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        UpdateAnimatorMovementType();

        MountEquipment();
        MountWeapons();

        FindObjective();
    }

    protected void LoadHolsters()
    {
        //Instantiate and initalize holster mounts
        t_LeftHandHolster = Instantiate(Resources.Load<GameObject>("MountBall")).GetComponent<ZXHolster>();
        t_LeftHandHolster.SetType(t_LeftHand.transform, ZXHolster.Holster.Left_Hand);

        t_RightHandHolster = Instantiate(Resources.Load<GameObject>("MountBall")).GetComponent<ZXHolster>();
        t_RightHandHolster.SetType(t_RightHand.transform, ZXHolster.Holster.Right_Hand);

        t_BackHolster = Instantiate(Resources.Load<GameObject>("MountBall")).GetComponent<ZXHolster>();
        t_BackHolster.SetType(t_Body.transform, ZXHolster.Holster.Back_Holster);

        t_BackpackHolster = Instantiate(Resources.Load<GameObject>("MountBall")).GetComponent<ZXHolster>();
        t_BackpackHolster.SetType(t_Body.transform, ZXHolster.Holster.Backpack_Holster);

        t_LeftHipFrontHolster = Instantiate(Resources.Load<GameObject>("MountBall")).GetComponent<ZXHolster>();
        t_LeftHipFrontHolster.SetType(rb_BodyParts.Find(delegate (ZXBodypart Object) { return Object.Part == ZXBodypart.Bodypart.Hips; }).transform, ZXHolster.Holster.Left_Front_Hip_Holster);

        t_LeftHipBackHolster = Instantiate(Resources.Load<GameObject>("MountBall")).GetComponent<ZXHolster>();
        t_LeftHipBackHolster.SetType(rb_BodyParts.Find(delegate (ZXBodypart Object) { return Object.Part == ZXBodypart.Bodypart.Hips; }).transform, ZXHolster.Holster.Left_Back_Hip_Holster);

        t_LeftShieldHolster = Instantiate(Resources.Load<GameObject>("MountBall")).GetComponent<ZXHolster>();
        t_LeftShieldHolster.SetType(rb_BodyParts.Find(delegate (ZXBodypart Object) { return Object.Part == ZXBodypart.Bodypart.LowerArm_Left; }).transform, ZXHolster.Holster.Left_Shield_Holster);

        t_RightHipHolster = Instantiate(Resources.Load<GameObject>("MountBall")).GetComponent<ZXHolster>();
        t_RightHipHolster.SetType(rb_BodyParts.Find(delegate (ZXBodypart Object) { return Object.Part == ZXBodypart.Bodypart.Hips; }).transform, ZXHolster.Holster.Right_Hip_Holster);
    }

    protected void LoadEquipment()
    {
        if (backpackPrefab != null)
        {
            backpack = Instantiate<ZXEquipment>(backpackPrefab);
        }
        if (shieldPrefab != null)
        {
            shield = Instantiate<ZXEquipment>(shieldPrefab);
        }
    }

    protected void LoadWeapons()
    {
        //Instantiate weapons
        if (primaryWeaponPrefab != null)
        {
            primaryWeapon = Instantiate<ZXWeapon>(primaryWeaponPrefab);
            primaryWeapon.SetToon(this as ZXToon);

            if (primaryWeapon is ZXFirearm)
                (primaryWeapon as ZXFirearm).e_Reload += d_Reload;
        }
    }

    protected void MountEquipment()
    {
        if (backpack != null)
        {
            backpack.Set();
            backpack.SnapToTransform(t_BackpackHolster);
            t_BackAltHolster = backpack.AltBackHolster;
        }

        if (shield != null)
        {
            shield.Set();
            shield.SnapToTransform(t_LeftShieldHolster);
        }
    }

    protected void MountWeapons()
    {
        if (primaryWeapon != null)
        {
            primaryWeapon.SnapToTransform(t_RightHandHolster);

            m_Animator.SetInteger(m_WeaponHashID, (int)primaryWeapon.WeaponType);

            if (primaryWeapon is ZXFirearm)
            {
                ik_AimIK.transform = primaryWeapon.AimTransform;
                attackRange = primaryWeapon.WeaponRange;
                defaultAction = Action.Aim;
            }
            else if (primaryWeapon is ZXMeleeWeapon)
            {
                ik_AimIK.transform = null;
                defaultAction = Action.Idle;
            }
        }
    }

    public override void AdjustValues(int Level, int Index)
    {
        /*
        if (level < 1)
            return;

        //level
        level = Mathf.Max(0, Level - 1);

        //Health
        startingHealth = ZXGLOBALS.dc[Index].hitpoints[0];
        armour = ZXGLOBALS.dc[Index].armour[0];

        //Adjust speed if the toon is a hero
        if (Index == ZXUIManager.instance.heroIndex)
        {
            ChangeDefautSpeed((Speed)(Mathf.Min(ZXGLOBALS.dc[Index].speed[0] + 2, 5)));
        }
        else
        {
            ChangeDefautSpeed((Speed)ZXGLOBALS.dc[Index].speed[0]);
        }

        startingHealth = startingHealth * Mathf.Pow(1 + ZXGLOBALS.CivilianHealthModPerLevel, level);

        meleeDamage = Mathf.RoundToInt((original_Damage * Mathf.Pow(1 + ZXGLOBALS.CivilianDamageModPerLevel, Level)));

        if (primaryWeapon != null)
            primaryWeapon.SetWeaponDamage(Mathf.RoundToInt(ZXGLOBALS.dc[Index].damage[0] * Mathf.Pow(1 + ZXGLOBALS.CivilianDamageModPerLevel, level)));

        health = startingHealth;

        ChangedLevel(Level);
        */
    }

	public override void AdjustValues(int Level)
	{
        /*
		if (level == Level)
			return;

		//level
		level = Level;

        //Health
		startingHealth = original_Health * Mathf.Pow(1 + ZXGLOBALS.CivilianHealthModPerLevel, Level);

		health = startingHealth;

		meleeDamage = Mathf.RoundToInt((original_Damage * Mathf.Pow(1 + ZXGLOBALS.CivilianDamageModPerLevel, Level)));

		if (primaryWeapon != null)
			primaryWeapon.SetWeaponDamage(Mathf.RoundToInt(primaryWeapon.WeaponDamage * Mathf.Pow(1 + ZXGLOBALS.CivilianDamageModPerLevel, level)));

        ChangedLevel(Level);
        */
    }
    #endregion

    /* Runtime Updates */

    /* Movement Functions */

    public override void Place(Vector3 location, Quaternion rotation, object args)
    {
        if (logDebugging)
            Debug.Log(args);

        if (args is Action && (Action)args == Action.DragFromUI)
        {
            //Make char invuln
            invulnerable = true;

            //Update start position
            gameObject.SetActive(true);
            transform.position = location;
            transform.rotation = rotation;

            newRotation = rotation;
        
            //Mount weapon on back
            if (primaryWeapon != null)
                primaryWeapon.SnapToTransform(t_Holsters.Find(delegate (ZXHolster Holster) { return Holster.HolsterType == primaryWeapon.TargetHolster; }));

            //Turn off agent and physics
            PhysicsOverride(true);
            AnimatorOverride(AnimState.Override, (int)Action.DragFromUI);
            SetBehavior(BehaviorPattern.Nothing);
            actionWaitFlag = true;
            falling = true;

            return;
        }
        if (args is Action && (Action)args == Action.LandFromFalling)
        {
            FindNavMeshTeather();

            //Play spawn sound
            if (ZXSpawnManager.PlaySpawnSounds)
                MasterAudio.PlaySound3DAtTransform(s_SpawnSound, transform);

            falling = true;

            transform.DOMove(teather.Value, 0.5f, false).SetEase(Ease.InQuad).OnComplete(delegate ()
            {
                PhysicsOverride(false);
                ResetIKBalls();

                SetBehavior(originalToonBehavior);

                EnqueueAction((int)Action.LandFromFalling);
                EnqueueAction((int)Action.Swap);

                Register(true, true);
			
                //Place ground poof
                ZXParticleEffect poof = ZXCombatUI.p_Poof.Request();

                MasterAudio.PlaySound3DAtTransformAndForget(s_GroundImpact, transform);

                if (poof != null)
                {
                    List<RaycastHit> hits = new List<RaycastHit>(Physics.RaycastAll(Center, Vector3.down, 10f));
                    RaycastHit groundHit = hits.Find(delegate (RaycastHit hit)
                    {
                        ZXFXable fxable = hit.transform.GetComponentInParent<ZXFXable>();

                        if (fxable != null)
                            return fxable.MaterialType == ZXFXable.Material.Ground;
                        else
                            return false;
                    });

                    if (groundHit.collider != null)
                    {
                        poof.Place(groundHit.point, Quaternion.Euler(groundHit.normal), null);
                    }
                }

                falling = false;

                invulnerable = false;
                de_CurrentMoveTranslation = d_TrackTranslation;
            });

            return;
        }

        //If not a special circumstance call base
        base.Place(location, rotation, null);

        if (args is BehaviorPattern)
            SetBehavior((BehaviorPattern)args);
        else
            SetBehavior(originalToonBehavior);
    }

    public void FindObjective()
    {
        FindObjective(null);
    }

    public void FindObjective(ZXObjective OldObjective)
    {
        ZXHeroSpawn heroSpawn = ZXReflectingPool.FindClosest<ZXHeroSpawn>(transform);
        ZXObjective.ObjectiveOrientation targetOrientation;
        ZXObjective targetObjective = null;

        if (heroSpawn != null)
        {
            targetOrientation = transform.position.z < heroSpawn.transform.position.z ? ZXObjective.ObjectiveOrientation.Left : ZXObjective.ObjectiveOrientation.Right;
            targetObjective = ZXReflectingPool.FindClosest<ZXObjective>(transform, delegate (ZXObjective Obj) { return Obj.Orientation == targetOrientation; });
        }

        if (targetObjective == null)
            targetObjective = ZXReflectingPool.FindClosest<ZXObjective>(transform);

        //De-register
        if (tetherObjective != null)
        {
            tetherObjective.e_Complete -= d_ObjectiveCompleted;
            tetherObjective.e_Failed -= d_ObjectiveCompleted;
        }

        tetherObjective = targetObjective;

        //Register
        if (tetherObjective != null)
        {
            tetherObjective.e_Complete += d_ObjectiveCompleted;
            tetherObjective.e_Failed += d_ObjectiveCompleted;
        }
    }

    public void PingObjective()
    {
        if (tetherObjective != null)
        {
            tetherObjective.Blink();
            tetherObjective.Scale();
        }
    }

    /* Action */

    public void SetBehavior(BehaviorPattern Behavior)
    {
        //Clear existing AI data
        ResetZXAI();
        toonBehavior = Behavior;

        //Attach behavior delegate
        switch (toonBehavior)
        {
            case BehaviorPattern.Nothing:
                de_CurrentMoveTranslation = null;
                de_CurrentBehaviorTranslation = null;
                break;

            case BehaviorPattern.Idle:
                de_CurrentBehaviorTranslation = d_Idle;
                de_CurrentMoveTranslation = null;
                break;

            case BehaviorPattern.Corpse:
                de_CurrentBehaviorTranslation = null;
                ChangePosture(Posture.Ragdoll);
                break;

            case BehaviorPattern.RangedDefense:
                de_CurrentBehaviorTranslation = d_RangedDefense;
                de_CurrentMoveTranslation = null;
                break;

            case BehaviorPattern.RangedAssualt:
                de_CurrentBehaviorTranslation = d_RangedAssualt;
                break;

            case BehaviorPattern.Player:
                de_CurrentBehaviorTranslation = d_Player;
                break;

            case BehaviorPattern.MeleeAssualt:
                de_CurrentBehaviorTranslation = d_MeleeAssualt;
                break;

            case BehaviorPattern.Civilian:
                de_CurrentBehaviorTranslation = d_Civilian;
                break;

            case BehaviorPattern.FleeMap:
                de_CurrentBehaviorTranslation = d_FleeMap;
                break;
        }

        if (logDebugging)
            Debug.Log("Toon behavior set: " + Behavior);
    }

    public void OverrideDefaultBehaviorPattern(BehaviorPattern Behavior)
    {
        originalToonBehavior = Behavior;
        toonBehavior = Behavior;
    }

    public override void Analyze()
    {
        base.Analyze();
    }

    protected override void PerformAction(int ActionCode)
    {
        switch (ActionCode)
        {
            case (int)Action.Idle:
                SetIKState(IKState.Aim, false);
                SetIKState(IKState.Look, false);
                break;

            case (int)Action.Aim:
                if (primaryWeapon is ZXFirearm)
                {
                    SetIKState(IKState.Aim, true);

                    actionWaitFlag = true;
                    Invoke("EndAction", 0.2f);
                }

                SetIKState(IKState.Look, true);
                break;

            case (int)Action.Reload:
                SetIKState(IKState.Aim, false);
                SetIKState(IKState.Look, true);
                actionWaitFlag = true;
                break;

            case (int)Action.Fire:
                if (primaryWeapon is ZXFirearm)
                {
                    SetIKState(IKState.Aim, true);
                    lockAim = true;
                }

                if (primaryWeapon is ZXMeleeWeapon)
                {
                    MasterAudio.PlaySound3DAtTransformAndForget(s_meleeAttackSound, transform);
                }

                SetIKState(IKState.Look, true);
                actionWaitFlag = true;

                if (primaryWeapon is ZXFirearm)
                    Invoke("EndAction", (primaryWeapon as ZXFirearm).FireTime);

                break;

            case (int)Action.Swap:
                SetIKState(IKState.Aim, false);
                SetIKState(IKState.Look, false);
                actionWaitFlag = true;
                break;

            case (int)Action.Throw:
                SetIKState(IKState.Aim, false);
                SetIKState(IKState.Look, true);
                actionWaitFlag = true;
                break;

            case (int)Action.MeleeGeneric:
                attackTarget = FollowObject;
                SetIKState(IKState.Aim, false);
                SetIKState(IKState.Look, true);
                actionWaitFlag = true;
                break;

            case (int)Action.SpecialAttack:
                attackTarget = FollowObject;
                SetIKState(IKState.Aim, false);
                SetIKState(IKState.Look, true);
                actionWaitFlag = true;
                break;

            case (int)Action.TakeDamage:
                break;

            case (int)Action.LandFromFalling:
                SetIKState(IKState.Aim, false);
                SetIKState(IKState.Look, false);
                actionWaitFlag = true;
                break;

            default:
                base.PerformAction(ActionCode);
                break;
        }
    }

    public override void AnimationCallback(int Code)
    {
        if (logDebugging)
            Debug.Log("AnimCallback:" + (CallbackCode)Code);

        switch (Code)
        {
            case (int)CallbackCode.StartReload:
                if (primaryWeapon != null)
                    (primaryWeapon as ZXFirearm).StartReload();
                break;

            case (int)CallbackCode.EndReload:
                if (primaryWeapon != null)
                    (primaryWeapon as ZXFirearm).EndReload();

                EndAction();
                break;

            case (int)CallbackCode.MeleeDamage:
                if (attackTarget != null && primaryWeapon != null && primaryWeapon is ZXMeleeWeapon)
                    (primaryWeapon as ZXMeleeWeapon).DealDamage(attackTarget);

                EndAction();
                break;

            case (int)CallbackCode.MeleeDamageFrontRight:
                if (primaryWeapon != null && primaryWeapon is ZXMeleeWeapon)
                    (primaryWeapon as ZXMeleeWeapon).DealDamage(ZXMeleeWeapon.Quad.Front_Right);

                EndAction();
                break;

            case (int)CallbackCode.MeleeDamageFrontLeft:
                if (primaryWeapon != null && primaryWeapon is ZXMeleeWeapon)
                    (primaryWeapon as ZXMeleeWeapon).DealDamage(ZXMeleeWeapon.Quad.Front_Left);

                EndAction();
                break;

            case (int)CallbackCode.MeleeDamageRearRight:
                if (primaryWeapon != null && primaryWeapon is ZXMeleeWeapon)
                    (primaryWeapon as ZXMeleeWeapon).DealDamage(ZXMeleeWeapon.Quad.Rear_Right);

                EndAction();
                break;

            case (int)CallbackCode.MeleeDamageRearLeft:
                if (primaryWeapon != null && primaryWeapon is ZXMeleeWeapon)
                    (primaryWeapon as ZXMeleeWeapon).DealDamage(ZXMeleeWeapon.Quad.Rear_Left);

                EndAction();
                break;

            case (int)CallbackCode.GenericMeleeDamage:
                if (attackTarget != null)
                {
                    Ray ray = new Ray(transform.position, (FollowObject.LookAtTransform.position - transform.position).normalized);
                    ZXDamage damage = new ZXDamage(meleeDamage, 0f, 1f, 0f, Race, ZXDamage.Type.Impact, ray, meleeDamageEffect);
                    attackTarget.TakeDamage(damage);
                }
                break;

            case (int)CallbackCode.WeaponTrailStart:
                if (primaryWeapon != null && primaryWeapon is ZXMeleeWeapon)
                    (primaryWeapon as ZXMeleeWeapon).StartTrail();
                break;

            case (int)CallbackCode.WeaponTrailStop:
                if (primaryWeapon != null && primaryWeapon is ZXMeleeWeapon)
                    (primaryWeapon as ZXMeleeWeapon).StopTrail();
                break;

            case (int)CallbackCode.WeaponSwap:
                if (primaryWeapon != null)
                    primaryWeapon.SnapToTransform(t_RightHandHolster);

                weaponDrawn = true;

                if (logDebugging)
                    Debug.Log("Swap completed");

                break;

            default:
                throw new System.NotImplementedException();
        }
    }

    protected override void TakeDamageEffect(ZXDamage Damage)
    {
        //No effects are applied to humans
    }

    protected virtual void Reload()
    {
        EnqueueAction((int)Action.Reload);
    }

    protected bool ShouldFire()
    {
        float pointAngle;
        float aimAngle;

        //Return under these circumstances
        if (FollowObject == null || primaryWeapon is ZXFirearm && (primaryWeapon as ZXFirearm).isFiring)
            return false;

        if (ik_PointBall.position == transform.TransformPoint(ik_DefaultAimPosition))
            return false;

        pointAngle = Vector3.Angle(LookAtTransform.position, ik_PointBall.position);
        aimAngle = Vector3.Angle(LookAtTransform.position, ik_AimBall.position);

        if (Mathf.Abs(aimAngle - pointAngle) < 10f)
            return true;
        else
            return false;
    }

    public override void RecieveMessage(ZXInterestPoint Sender, Vector3 Location, ZXInterestPoint.Interest Header, ZXInterestPoint.BroadcastMessage Message)
    {

    }

    /* Behaviors */

    protected virtual int PlayerBehavior()
    {
        IZXTargetable bestTarget = null;
        float distance;

        //Make sure the play is stopped
        if (waypoint != null || waypoints != null)
            return (int)defaultMoveAction;

        //Make sure weapon is drawn
        if (primaryWeapon != null && !weaponDrawn)
            return (int)Action.Swap;

        //Check visible targets list, default to an objective
        if (tr_VisibleTargets.Count > 0)
            bestTarget = tr_VisibleTargets.Values[0];

        //If no target is found
        if (bestTarget == null)
            return (int)defaultAction;

        //Only replace FollowObject if different
        else if (bestTarget != FollowObject)
            FollowObject = bestTarget;

        //Compute distance
        distance = FollowObject.GetDistance(Center);

        if (distance <= meleeRange)
        {
            ChangeSpeed(Speed.Stop);
            attackTarget = FollowObject;
            return (int)Action.MeleeGeneric;
        }
        else if (distance <= attackRange)
        {
            ChangeSpeed(Speed.Stop);

            if (FollowObject != null && Vector3.Distance(ik_AimBall.position, ik_PointBall.position) < 0.2 && ik_PointBall.position != transform.TransformPoint(ik_DefaultAimPosition))
            {
                if (primaryWeapon is ZXFirearm && !(primaryWeapon as ZXFirearm).isFiring)
                    return primaryWeapon.Fire();
            }
        }

        return (int)defaultAction;
    } //Player AI

    protected virtual int RangedDefenseBehavior()
    {
        if (tr_VisibleTargets.Count > 0)
        {
            FollowObject = tr_VisibleTargets.Values[0];
        }

        if (FollowObject != null)
        {
            //Compute distance
            float distance = FollowObject.GetDistance(Center);

            if (distance <= attackRange)
            {
                if (ShouldFire())
                    return primaryWeapon.Fire();
            }

            return (int)defaultAction;
        }

        return (int)defaultAction;
    }

    protected virtual int RangedAssualtBehavior()
    {
        IZXTargetable bestTarget = null;
        int bestAction = speed == Speed.Stop ? (int)defaultAction : (int)defaultMoveAction;
        float distance;

        //If in ragdoll, stand up
        if (posture == Posture.Ragdoll)
            return (int)Action.FromRagdoll;

        //Make sure weapon is drawn
        if (primaryWeapon != null && !weaponDrawn)
            return (int)Action.Swap;

        //Check visible targets list, default to an objective
        if (tr_VisibleTargets.Count > 0)
            bestTarget = tr_VisibleTargets.Values[0];
        else if (tetherObjective != null)
        {
            Vector3 point = tetherObjective.GetKOTHInteractionPoint(Base);

            if (Vector3.Distance(Base, point) > attackRange && speed == Speed.Stop)
            {
                Move(defaultMoveSpeed, point);
                return (int)defaultMoveAction;
            }

            return bestAction;
        }

        //If no target is found
        if (bestTarget == null)
        {
            return bestAction;
        }
        //Only replace FollowObject if different
        else if (bestTarget != FollowObject)
            FollowObject = bestTarget;

        //Compute distance
        distance = FollowObject.GetDistance(Center);

        if (distance > attackRange)
        {
            if (speed == Speed.Stop)
                Move(defaultMoveSpeed, FollowObject.Center);

            return (int)defaultMoveAction;
        }
        else if (distance <= meleeRange)
        {
            ChangeSpeed(Speed.Stop);
            attackTarget = FollowObject;
            return (int)Action.MeleeGeneric;
        }
        else if (distance <= attackRange)
        {
            ChangeSpeed(Speed.Stop);

            if (ShouldFire())
                return primaryWeapon.Fire();
        }

        return bestAction;
    }

    protected virtual int MeleeAssualtBehvaior()
    {
        IZXTargetable bestTarget = null;
        int bestAction = speed == Speed.Stop ? (int)defaultAction : (int)defaultMoveAction;
        float distance;

        //If in ragdoll, stand up
        if (posture == Posture.Ragdoll)
            return (int)Action.FromRagdoll;

        //Make sure weapon is drawn
        if (primaryWeapon != null && !weaponDrawn)
            return (int)Action.Swap;

        //Check visible targets list, default to an objective
        if (tr_VisibleTargets.Count > 0)
            bestTarget = tr_VisibleTargets.Values[0];
        else if (tetherObjective != null)
        {
            Vector3 point = tetherObjective.GetKOTHInteractionPoint(Base);

            if (Vector3.Distance(Base, point) > n_Agent.stoppingDistance && speed == Speed.Stop)
            {
                Move(defaultMoveSpeed, point);
            }

            return bestAction;
        }

        //If no target is found
        if (bestTarget == null)
        {
            ChangeSpeed(Speed.Stop);
            return bestAction;
        }
        //Only replace FollowObject if different
        else if (bestTarget != FollowObject)
            FollowObject = bestTarget;

        //Compute distance
        distance = FollowObject.GetDistance(Center);

        if (distance > attackRange)
        {
            Move(defaultMoveSpeed, FollowObject.Center);

            return (int)defaultMoveAction;
        }
        else if (distance <= meleeRange && !actionWaitFlag)
        {
            ChangeSpeed(Speed.Stop);
            attackTarget = FollowObject;
            return primaryWeapon.Fire();
        }

        return bestAction;
    }

	//Used to set the movement type should be set in the animator depending on the weapon they are holding
	private void UpdateAnimatorMovementType ()
    {
		MovementType movementType = DetermineMovementType(CurrentWeapon);
		m_Animator.SetFloat(m_MotionTypeHashID,(int)movementType);
	}
		
	private MovementType DetermineMovementType (ZXWeapon weapon)
    {
		MovementType result = MovementType.BaseMovement;

        if (weapon == null)
            return MovementType.NoWeapon;

        switch (weapon.WeaponType) {
        case ZXWeapon.Type.None:
            result = MovementType.NoWeapon;
            break;
        case ZXWeapon.Type.Knife:
            result = MovementType.NoWeapon;
            break;
		case ZXWeapon.Type.Katana:
			result = MovementType.NoWeapon;
			break;
		case ZXWeapon.Type.Machete:
			result = MovementType.NoWeapon;
			break;
		case ZXWeapon.Type.Spear:
			result = MovementType.BaseMovement;
			break;
		case ZXWeapon.Type.Pickaxe:
			result = MovementType.BaseMovement;
			break;
		case ZXWeapon.Type.Pistol:
			result = MovementType.BaseMovement;
//			result = MovementType.NoWeapon;
			break;
		case ZXWeapon.Type.Bat:
			result = MovementType.BaseMovement;
			break;
		case ZXWeapon.Type.Cleaver:
			result = MovementType.NoWeapon;
			break;

		default:
			break;
		}
		return result;
	}

	private float DetermineCrouchAmount (ZXWeapon weapon) {

		//Let's randmonize the base crouch a little
//		float result = 1.0f;
//		float result = Random.Range(.4f,1f);
		float result = 1.0f;

		if (weapon == null){
			return result;
		}

		switch (weapon.WeaponType) {

		//Range Weapons
		case ZXWeapon.Type.None:
			result = 0;
			break;
		case ZXWeapon.Type.AssualtRifle:
			result = 1.0f;
			break;
		case ZXWeapon.Type.Crossbow:
			result = 1.0f;
			break;
		case ZXWeapon.Type.FlameThrower:
			result = 0;
			break;
		case ZXWeapon.Type.MiniGun:
			result = 0;
			break;
		case ZXWeapon.Type.Pistol:
			result = 0;
			break;
		case ZXWeapon.Type.Rifle:
			result = 0;
			break;
		case ZXWeapon.Type.RPG:
			result = 0;
			break;
		case ZXWeapon.Type.Shotgun:
			result = .0f;
			break;
		case ZXWeapon.Type.SubMachineGun:
			result = .0f;
			break;
	
		//Mele Weapons
		case ZXWeapon.Type.Katana:
			result = .5f;
			break;
		case ZXWeapon.Type.Knife:
			result = .5f;
			break;
		case ZXWeapon.Type.Machete:
			result = 0;
			break;
		case ZXWeapon.Type.Pickaxe:
			result = 0;
			break;
		case ZXWeapon.Type.Spear:
			result = 1f;
			break;
		default:
			break;
		}
		return result;
	}

    private int CivilianBehavior()
    {
        if (waypoint != null)
            return (int)defaultAction;

        ZXZombie nearestZombie= ZXReflectingPool.FindClosest<ZXZombie>(transform);
        UnityEngine.AI.NavMeshHit navWaypoint;

        if (nearestZombie != null)
        {
            float distance = Vector3.Distance(transform.position, nearestZombie.LookAtTransform.position);
            Vector3 direction = (transform.position - nearestZombie.LookAtTransform.position).normalized;

            if (distance < _FleeRange)
            {
                if (UnityEngine.AI.NavMesh.SamplePosition(transform.position + (direction * _FleeDistance), out navWaypoint, _FleeDistance, UnityEngine.AI.NavMesh.AllAreas))
                {
                    Move(Speed.Run, navWaypoint.position);
                }
            }
        }

        if (speed == Speed.Stop)
        {
            Vector3 targetPostion = new Vector3(Random.Range(-_IdleRange, _IdleRange), 0f, Random.Range(-_IdleRange, _IdleRange));

            if (UnityEngine.AI.NavMesh.SamplePosition(transform.position + targetPostion, out navWaypoint, 10f, UnityEngine.AI.NavMesh.AllAreas))
                Move(defaultMoveSpeed, navWaypoint.position);
        }

        return (int)defaultAction;
    }

    private int FleeMap()
    {
        if (tr_ClosestExit == null)
        {
            //SetBehavior(BehaviorPattern.Civilian);
        }
        else if (speed == Speed.Stop)
        {
            if (Vector3.Distance(transform.position, tr_ClosestExit.Base) <= n_Agent.stoppingDistance)
            {
                SetBehavior(BehaviorPattern.Nothing);
                ExitScene();
                return (int)defaultAction;
            }
            else
            {
                Move(defaultMoveSpeed, tr_ClosestExit.Base);
                return (int)defaultAction;
            }
        }

        return (int)defaultMoveAction;
    }

    public override void PlayerInstruction(Instruction Instruct, Input In, object Args)
    {
        base.PlayerInstruction(Instruct, In, Args);

        MasterAudio.PlaySound3DAtTransform(s_Voice, transform);
    }

    /* Destruction */
    protected override void StateChanged(ZXCombatUI.State State)
    {
        if (State == ZXCombatUI.State.Defeat)
        {
            if (primaryWeapon != null)
            {
                primaryWeapon.Drop();
            }

            SetBehavior(BehaviorPattern.FleeMap);
        }
    }

    protected override void Die()
    {
        base.Die();

        //Drop weapons
        if (primaryWeapon != null)
            primaryWeapon.Drop();

        //Activate corpse script
        rb_Corpse.enabled = true;
        rb_Corpse.Broadcast<ZXToon>(ZXInterestPoint.BroadcastMessage.NewCorpse, 15f);
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        rb_Corpse.enabled = false;
    }
    /* Support Functions */
}
