﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using DG.Tweening;

public class ZXAura : MonoBehaviour
{
    public enum Effect
    {
        Healing = 0
    }

    //Inspector variables
    [SerializeField] private Effect effectType;
    [SerializeField] private ToonRace targetRace;
    [SerializeField] private float effectRadius;
    [SerializeField] private float effectPulseSpeed;
    [SerializeField] private float effectDuration;
    [SerializeField] private float effectAmount;
    [SerializeField] private ParticleSystem effect;
    [SerializeField] private ZXParticleEffect targetEffect;

    private ZXPoolLite<ZXParticleEffect> p_TargetEffects;
    private int targetEffectsSize = 8;

    //Private variables
    private bool active = false;
    private System.Predicate<ZXToon> targetParams;
    private ZXToon toon;
    private int level = 1;

    //Initalization
    private void Awake()
    {
        targetParams = new System.Predicate<ZXToon>(targetingParams);
        toon = GetComponentInParent<ZXToon>();
    }

    private void Start()
    {
        if (effect != null)
        {
            effect = Instantiate<ParticleSystem>(effect);

            if (toon != null)
                effect.transform.SetParent(toon.LookAtTransform);
            else
                effect.transform.SetParent(transform);

//            effect.transform.localPosition = Vector3.zero;
			effect.transform.localPosition = new Vector3(1.2f,0,.41f);
            effect.transform.rotation = Quaternion.identity;
        }

        if (targetEffect != null)
            p_TargetEffects = new ZXPoolLite<ZXParticleEffect>(transform, targetEffect, targetEffectsSize);

        this.enabled = false;
    }

    private void OnEnable()
    {
        active = true;
        StartCoroutine(Pulse());

        if (effect != null)
            effect.Play();

        if (toon != null)
        {
            level = toon.ToonLevel;
            //effectAmount += effectAmount * (level * ZXGLOBALS.HumanHealModPerLevel);
        }
    }

    //Main function
    private IEnumerator Pulse()
    {
        List<ZXToon> targets = null;
        ZXParticleEffect tEffect = null;

        while (active)
        {
            targets = ZXReflectingPool.FindInSphere<ZXToon>(transform.position, effectRadius, targetingParams);

            if (targets != null && targets.Count > 0)
            {
                targets.ForEach(delegate (ZXToon target) 
                {
                    target.Buff(effectType, effectAmount, effectDuration);

                    if (p_TargetEffects != null)
                    {
                        tEffect = p_TargetEffects.Request();

                        if (tEffect != null)
                        {
                            if (effectType == Effect.Healing)
                                tEffect.Place(target.HeadTransform.position, Quaternion.LookRotation(Vector3.up), null);
                            else
                                tEffect.Place(target.HeadTransform.position, Quaternion.LookRotation(Vector3.up), target.HeadTransform);
                        }
                    }
                });
            }

            targets = null;
            tEffect = null;

            yield return new WaitForSeconds(effectPulseSpeed);
        }
    }

    private bool targetingParams(ZXToon target)
    {
        if (toon != null)
            return target.Race == targetRace && target != toon;
        else
            return target.Race == targetRace;
    }

    //Destruction
    private void OnDisable()
    {
        active = false;
        StopAllCoroutines();

        if (effect = null)
            effect.Stop();
    }

}
