﻿using UnityEngine;
using System.Collections;
using ZXCore;

public class ZXCorpse : ZXInterestPoint
{
    protected override void Awake()
    {
        base.Awake();

        pointType = Interest.Corpse;
        maxInteractions = 3;
        interactionRadius = 2.5f;
    }

    public override int Interact(IZXTargetable Actor)
    {
        return 0;
    }
}
