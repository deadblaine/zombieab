﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using DG.Tweening;

public class ZXRaider : ZXHuman
{
	public override void AdjustValues(int Level, int Index)
	{
		AdjustValues(Level);
	}

	public override void AdjustValues(int Level)
	{
        /*
		if (level == Level)
			return;

		//level
		level = Level;

		//Health
		startingHealth = original_Health * Mathf.Pow(1 + ZXGLOBALS.RaidersHealhModPerLevel, Level);
		health = startingHealth;

		meleeDamage = Mathf.RoundToInt(original_Damage * Mathf.Pow(1 + ZXGLOBALS.RaidersDamageModPerLevel, Level));

		if (primaryWeapon != null)
			primaryWeapon.SetWeaponDamage(Mathf.RoundToInt(primaryWeapon.WeaponDamage * Mathf.Pow(1 + ZXGLOBALS.RaidersDamageModPerLevel, Level)));

        ChangedLevel(Level);
        */
    }

    protected override int RangedAssualtBehavior()
    {
        IZXTargetable bestTarget = null;
        int bestAction = speed == Speed.Stop ? (int)defaultAction : (int)defaultMoveAction;
        float distance;

        //Check visible targets list, default to an objective
        if (tr_VisibleTargets.Count > 0)
            bestTarget = tr_VisibleTargets.Values[0];
        else if (speed == Speed.Stop)
        {
            Vector3 targetPosition;
            Quaternion targetRotation;

            //Create random point nearby
            targetRotation = transform.rotation;
            targetPosition = new Vector3(Random.Range(-2, 2), 0f, Random.Range(5, 10));

            if (tr_ClosestNexus != null)
            {
                Vector3 targetDirection = (tr_ClosestNexus.transform.position - transform.position).normalized;

                if (targetDirection != Vector3.zero)
                    targetRotation = Quaternion.LookRotation(targetDirection, Vector3.up);
                else
                    targetRotation = Quaternion.identity;

                targetRotation = Quaternion.Inverse(transform.rotation) * targetRotation;
                targetPosition = transform.TransformPoint(targetRotation * targetPosition);
            }
            else
            {
                targetPosition = transform.TransformPoint(targetPosition);
            }

            Move(defaultMoveSpeed, targetPosition);

            return bestAction;
        }

        //If no target is found
        if (bestTarget == null)
        {
            return bestAction;
        }
        //Only replace FollowObject if different
        else if (bestTarget != FollowObject)
            FollowObject = bestTarget;

        //Compute distance
        distance = FollowObject.GetDistance(Center);

        if (distance > attackRange)
        {
            if (speed == Speed.Stop)
                Move(defaultMoveSpeed, FollowObject.Center);

            return (int)defaultMoveAction;
        }
        else if (distance <= meleeRange)
        {
            ChangeSpeed(Speed.Stop);
            attackTarget = FollowObject;
            return (int)Action.MeleeGeneric;
        }
        else if (distance <= attackRange)
        {
            ChangeSpeed(Speed.Stop);

            if (ShouldFire())
                return primaryWeapon.Fire();
        }

        return bestAction;
    }

    protected override int MeleeAssualtBehvaior()
    {
        IZXTargetable bestTarget = null;
        int bestAction = speed == Speed.Stop ? (int)defaultAction : (int)defaultMoveAction;
        float distance;

        //If in ragdoll, stand up
        if (posture == Posture.Ragdoll)
            return (int)Action.FromRagdoll;

        //Check visible targets list, default to an objective
        if (tr_VisibleTargets.Count > 0)
            bestTarget = tr_VisibleTargets.Values[0];
        else if (speed == Speed.Stop)
        {
            Vector3 targetPosition;
            Quaternion targetRotation;

            //Create random point nearby
            targetRotation = transform.rotation;
            targetPosition = new Vector3(Random.Range(-2, 2), 0f, Random.Range(5, 10));

            if (tr_ClosestNexus != null)
            {
                Vector3 targetDirection = (tr_ClosestNexus.transform.position - transform.position).normalized;

                if (targetDirection != Vector3.zero)
                    targetRotation = Quaternion.LookRotation(targetDirection, Vector3.up);
                else
                    targetRotation = Quaternion.identity;

                targetRotation = Quaternion.Inverse(transform.rotation) * targetRotation;
                targetPosition = transform.TransformPoint(targetRotation * targetPosition);
            }
            else
            {
                targetPosition = transform.TransformPoint(targetPosition);
            }

            Move(defaultMoveSpeed, targetPosition);

            return bestAction;
        }

        //If no target is found
        if (bestTarget == null)
        {
            return bestAction;
        }
        //Only replace FollowObject if different
        else if (bestTarget != FollowObject)
            FollowObject = bestTarget;

        //Compute distance
        distance = FollowObject.GetDistance(Center);

        if (distance > attackRange)
        {
            if (speed == Speed.Stop)
                Move(defaultMoveSpeed, FollowObject.Center);

            return (int)defaultMoveAction;
        }
        else if (distance <= meleeRange)
        {
            ChangeSpeed(Speed.Stop);
            attackTarget = FollowObject;
            return (int)Action.MeleeGeneric;
        }

        return bestAction;
    }

    protected override void StateChanged(ZXCombatUI.State State)
    {
        if (State == ZXCombatUI.State.Victory)
        {
            Die();
        }
    }
}
