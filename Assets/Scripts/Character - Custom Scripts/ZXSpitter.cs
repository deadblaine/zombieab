﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using DG.Tweening;
using DarkTonic.MasterAudio;

public sealed class ZXSpitter : ZXZombie
{
    //Sounds
    [SoundGroupAttribute] [SerializeField] private string s_Spit;

    //Inspector fields
    [SerializeField] private ZXExplosive spitPrefab;
    [SerializeField] private float spitRange;
    [SerializeField] private float spitFlightSpeed;
    [SerializeField] private float spitSpread;
    [SerializeField] private float spitRateOfFire;

    //Projectile pool
    private ZXPoolLite<ZXProjectile> p_SpitPool;
    private int p_Size = 5;

    //Private variables
    private ZXExplosive loadedSpit;
    private float spitTimer;
    private Vector3 attackPoint;

    //Initalization
    protected override void Awake()
    {
        base.Awake();

        p_SpitPool = new ZXPoolLite<ZXProjectile>(t_Head.transform, spitPrefab, p_Size);

        attackRange = spitRange;
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        LoadSpit();

        spitTimer = spitRateOfFire;
    }

    public override void AdjustValues(int Level)
    {
        base.AdjustValues(Level);
    }

    //Runtime
    protected override void Update()
    {
        base.Update();

        if (spitTimer > 0)
            spitTimer -= Time.deltaTime;
    }

    //Action functions
    protected override void PerformAction(int ActionCode)
    {
        switch (ActionCode)
        {
            case (int)Action.Spit:
                ChangeSpeed(Speed.Stop);
                actionWaitFlag = true;
                break;

            default:
                base.PerformAction(ActionCode);
                break;
        }
    }

    private void LoadSpit()
    {
        if (loadedSpit == null)
            loadedSpit = p_SpitPool.Request() as ZXExplosive;

        if (loadedSpit != null)
        {
            loadedSpit.transform.SetParent(t_Head.transform);
            loadedSpit.transform.localPosition = Vector3.zero;
            loadedSpit.transform.localRotation = Quaternion.identity;
            loadedSpit.gameObject.SetActive(true);

            loadedSpit.SetDamage(Mathf.RoundToInt(loadedSpit.ExplosiveDamage * zombieDamageMod));
        }
    }

    public override void AnimationCallback(int Code)
    {
        if (logDebugging)
            Debug.Log("AnimCallback:" + (CallbackCode)Code);

        switch (Code)
        {
            case (int)CallbackCode.Spit:

                if (loadedSpit != null)
                {
                    attackPoint += new Vector3(Random.Range(-spitSpread, spitSpread), 0f, Random.Range(-spitSpread, spitSpread));

                    loadedSpit.Arm();
                    loadedSpit.AdjustDamage(level);
                    loadedSpit.Launch(attackPoint, Vector3.Distance(attackPoint, Center) / spitFlightSpeed);
                    loadedSpit = null;
                    spitTimer = spitRateOfFire;

                    MasterAudio.PlaySound3DAtTransformAndForget(s_Spit, transform);
                }

                LoadSpit();
                break;

            default:
                base.AnimationCallback(Code);
                break;
        }
    }

    //New attack move behavior
    protected override int AttackMove()
    {
        IZXTargetable bestTarget = null;
        float distance;

        //Check visible targets list, default to an objective
        if (tr_VisibleTargets.Count > 0)
            bestTarget = tr_VisibleTargets.Values[0];

        //If no target is found
        if (bestTarget == null)
        {
            SetBehavior(BehaviorPattern.Lurk);
            return (int)defaultAction;
        }
        //Only replace FollowObject if different
        else if (bestTarget != FollowObject)
            FollowObject = bestTarget;

        //Compute distance
        distance = FollowObject.GetDistance(Center);

        //Check range
        if (distance <= meleeRange)
        {
            ChangeSpeed(Speed.Stop);
            return (int)Action.MeleeGeneric;
        }
        else if (distance > spitRange)
        {
            if (speed == Speed.Stop)
                Move(defaultMoveSpeed, FollowObject.Base);

            return (int)defaultMoveAction;
        }
        else if (spitTimer <= 0f)
        {
            ChangeSpeed(Speed.Stop);
            attackPoint = FollowObject.Center;
            return (int)Action.Spit;
        }
        else
        {
            return (int)defaultAction;
        }
    }
}
