﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using DG.Tweening;

public sealed class ZXHeroSpawn : ZXInterestPoint
{
    //Initalization
    protected override void Awake()
    {
        base.Awake();

        pointType = Interest.HeroSpawn;

        ZXReflectingPool.Register<ZXHeroSpawn>(this, transform);
    }

    private void Start()
    {
        /*
        if (ZXPrefManager.isTutorial)
        {
            List<ZXTutorialMarker> markers = ZXTutorialMarker.getGroup(ZXTutorialMarker.Series.alpha);

            transform.position = markers[0].transform.position;
        }
        */
    }

    public override int Interact(IZXTargetable Actor)
    {
        return 0;
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        ZXReflectingPool.DeRegister<ZXHeroSpawn>(transform);
    }
}
