﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;

public class ZXEventSpawnNodeShowcase : MonoBehaviour
{
    [SerializeField] private ZXHuman toon;
    [SerializeField] private ZXHuman.BehaviorPattern behavior;
    private new Collider collider;

    private void Awake()
    {
        collider = GetComponent<Collider>();

        if (collider != null)
            collider.enabled = false;       
    }

    private void Start()
    {
        if (toon != null)
        {
            toon = Instantiate<ZXHuman>(toon);
            toon.Place(transform.position, Quaternion.Euler(Vector3.up), behavior);
        }
    }

    public void EndShowcase()
    {
        if (toon != null)
            toon.ExitScene();
    }
}
