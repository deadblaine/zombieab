﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using ZXSpawnData;
using DG.Tweening;
using Vectrosity;

public class ZXSpawnManager : MonoBehaviour
{
    //Private constants
    private const float _challengeDelay = 5f;
    private const int _riseFromGroundEffects = 8;
    private const int _spawnCap = 25;
    private const int _heroBlobSize = 4;

    [SerializeField] private float challengeTimeMod;
    [SerializeField] private bool logDebugging;
    [SerializeField] private bool disableSpawning;

    //Enemy pools
    #region
    public static ZXPoolLite<ZXToon> p_Zombies;                     // Walker Zombie Pool
    [SerializeField] private int p_ZombiesPrimarySize;
    [SerializeField] private int p_ZombiesSecondarySize;
    [SerializeField] private ZXToon[] p_ZombiesPrefabs;

    public static ZXPoolLite<ZXToon> p_ZombiesFast;                 // Fast Zombie Pool
    [SerializeField] private int p_ZombiesFastPrimarySize;
    [SerializeField] private int p_ZombiesFastSecondarySize;
    [SerializeField] private ZXToon[] p_ZombiesFastPrefabs;

    public static ZXPoolLite<ZXToon> p_ZombiesArmoured;             // Armoured Zombie Pool
    [SerializeField] private int p_ZombiesArmouredPrimarySize;
    [SerializeField] private int p_ZombiesArmouredSecondarySize;
    [SerializeField] private ZXToon[] p_ZombiesArmouredPrefabs;

    public static ZXPoolLite<ZXToon> p_Tanks;                       // Tank Zombie Pool
    [SerializeField] private int p_TanksPrimarySize;
    [SerializeField] private int p_TanksSecondarySize;
    [SerializeField] private ZXToon[] p_TanksPrefabs;

    public static ZXPoolLite<ZXToon> p_Spitters;                    // Spitter Zombie Pool
    [SerializeField] private int p_SpittersPrimarySize;
    [SerializeField] private int p_SpittersSecondarySize;
    [SerializeField] private ZXToon[] p_SpittersPrefabs;

    public static ZXPoolLite<ZXToon> p_Exploders;                   // Exploder Zombie Pool
    [SerializeField] private int p_ExplodersPrimarySize;
    [SerializeField] private int p_ExplodersSecondarySize;
    [SerializeField] private ZXToon[] p_ExplodersPrefabs;

    public static ZXPoolLite<ZXToon> p_RaidersMelee;                // Melee Raider Pool
    [SerializeField] private int p_RaidersMeleePrimarySize;
    [SerializeField] private int p_RaidersMeleeSecondarySize;
    [SerializeField] private ZXToon[] p_RaidersMeleePrefabs;

    public static ZXPoolLite<ZXToon> p_RaidersRanged;               // Ranged Raider Pool
    [SerializeField] private int p_RaidersRangedPrimarySize;
    [SerializeField] private int p_RaidersRangedSecondarySize;
    [SerializeField] private ZXToon[] p_RaidersRangedPrefabs;

    public static ZXPoolLite<ZXToon> p_RaidersHeavy;               // Heavy Raider Pool
    [SerializeField] private int p_RaidersHeavyPrimarySize;
    [SerializeField] private int p_RaidersHeavySecondarySize;
    [SerializeField] private ZXToon[] p_RaidersHeavyPrefabs;
    #endregion

    //Accessors
    public static SpawnDifficulty Difficulty
    {
        get
        {
            return difficulty;
        }
    }     
    public static int SpawnedEnemies
    {
        get
        {
            return spawnedActors;
        }
    }
    public static int HeroCount
    {
        get
        {
            if (regions != null && regions.Count > 0)
            {
                int count = 0;

                for (int i = 0; i < regions.Count; i++)
                {
                    count += regions[i].HeroCount;
                }

                return count;
            }
            else
                return 0;
        }
    }
    public static int SpawnCap
    {
        get
        {
            return _spawnCap;
        }
    }
    public static bool PlaySpawnSounds
    {
        get
        {
            return playSpawnSounds;
        }
    }

    //Particle effects
    [SerializeField] private ZXParticleEffect p_RiseFromGroundParticle;
    public static ZXPoolLite<ZXParticleEffect> p_RiseEffect;

    //Regions list
    private static List<ZXSpawnRegion> regions;
    private static ZXSpawnRegion lastActiveRegion = null;

    //Spawn tracking
    private static List<IZXTargetable> actors;
    private static int spawnedActors = 0;
    private static System.Action<IZXTargetable> d_deadActor;

    //Spawn values
    private static SpawnType type;
    private static SpawnDifficulty difficulty;
    private static DeckType deck;

    //Spawn params
    private float _ambiantSpawnTime = 5f;
    private float _challengeSpawnTime = 15f;
    private float _eventSpawnTime = 45f;

    //Spawn flags
    private bool ambientSpawn = false;
    private bool challengeSpawn = false;
    private bool eventSpawn = false;
    private static bool playSpawnSounds = false;

    //Spawn lists
    private static List<IZXSpawnInstruction> mstPreInst;
    private static List<IZXSpawnInstruction> mstAmbInst;
    private static List<IZXSpawnInstruction> mstChgInst;

    //Enemy levels array
    private static int[] levels;

    //Singleton
    private static ZXSpawnManager singleton;

    //Initalization
    private void Awake()
    {
        singleton = this;

        //Register with GameManager
        ZXCombatUI.Register(this);

        //Create rise from ground pool 
        if (p_RiseFromGroundParticle != null)
        {
            p_RiseEffect = new ZXPoolLite<ZXParticleEffect>(ZXCombatUI.ObjectMask, p_RiseFromGroundParticle, _riseFromGroundEffects);
        }

        regions = new List<ZXSpawnRegion>(6);
        actors = new List<IZXTargetable>(_spawnCap);

        d_deadActor = new System.Action<IZXTargetable>(DeadActor);

        spawnedActors = 0;

        mstPreInst = new List<IZXSpawnInstruction>(ZXPreSpawnInstruction.preSpawns);
        mstAmbInst = new List<IZXSpawnInstruction>(ZXSpawnInstruction.ambientSpawns);
        mstChgInst = new List<IZXSpawnInstruction>(ZXSpawnInstruction.challengeSpawns);
    }

    //Public spawning control
    public void InitalizeSpawner(SpawnType Type, SpawnDifficulty Difficulty, DeckType Deck, bool Exclusive)
    {
        type = Type;
        difficulty = Difficulty;
        deck = Deck;

        //Load pools
        switch (type)
        {
            case SpawnType.Zombie:
                p_Zombies = new ZXPoolLite<ZXToon>(ZXCombatUI.ObjectMask, p_ZombiesPrefabs, p_ZombiesPrimarySize);
                p_ZombiesFast = new ZXPoolLite<ZXToon>(ZXCombatUI.ObjectMask, p_ZombiesFastPrefabs, p_ZombiesFastPrimarySize);
                p_ZombiesArmoured = new ZXPoolLite<ZXToon>(ZXCombatUI.ObjectMask, p_ZombiesArmouredPrefabs, p_ZombiesArmouredPrimarySize);
                p_Tanks = new ZXPoolLite<ZXToon>(ZXCombatUI.ObjectMask, p_TanksPrefabs, p_TanksPrimarySize);
                p_Spitters = new ZXPoolLite<ZXToon>(ZXCombatUI.ObjectMask, p_SpittersPrefabs, p_SpittersPrimarySize);
                p_Exploders = new ZXPoolLite<ZXToon>(ZXCombatUI.ObjectMask, p_ExplodersPrefabs, p_ExplodersPrimarySize);

                p_RaidersMelee = new ZXPoolLite<ZXToon>(ZXCombatUI.ObjectMask, p_RaidersMeleePrefabs, p_RaidersMeleeSecondarySize);
                p_RaidersRanged = new ZXPoolLite<ZXToon>(ZXCombatUI.ObjectMask, p_RaidersRangedPrefabs, p_RaidersRangedSecondarySize);
                p_RaidersHeavy = new ZXPoolLite<ZXToon>(ZXCombatUI.ObjectMask, p_RaidersHeavyPrefabs, p_RaidersHeavySecondarySize);
                break;

            case SpawnType.Raider:
                p_Zombies = new ZXPoolLite<ZXToon>(ZXCombatUI.ObjectMask, p_ZombiesPrefabs, p_ZombiesSecondarySize);
                p_ZombiesFast = new ZXPoolLite<ZXToon>(ZXCombatUI.ObjectMask, p_ZombiesFastPrefabs, p_ZombiesFastSecondarySize);
                p_ZombiesArmoured = new ZXPoolLite<ZXToon>(ZXCombatUI.ObjectMask, p_ZombiesArmouredPrefabs, p_ZombiesArmouredSecondarySize);
                p_Tanks = new ZXPoolLite<ZXToon>(ZXCombatUI.ObjectMask, p_TanksPrefabs, p_TanksSecondarySize);
                p_Spitters = new ZXPoolLite<ZXToon>(ZXCombatUI.ObjectMask, p_SpittersPrefabs, p_SpittersSecondarySize);
                p_Exploders = new ZXPoolLite<ZXToon>(ZXCombatUI.ObjectMask, p_ExplodersPrefabs, p_ExplodersSecondarySize);

                p_RaidersMelee = new ZXPoolLite<ZXToon>(ZXCombatUI.ObjectMask, p_RaidersMeleePrefabs, p_RaidersMeleePrimarySize);
                p_RaidersRanged = new ZXPoolLite<ZXToon>(ZXCombatUI.ObjectMask, p_RaidersRangedPrefabs, p_RaidersRangedPrimarySize);
                p_RaidersHeavy = new ZXPoolLite<ZXToon>(ZXCombatUI.ObjectMask, p_RaidersHeavyPrefabs, p_RaidersHeavyPrimarySize);
                break;
        }

        //Load difficulty array
        levels = new int[] { Mathf.Clamp((int)difficulty + 1, 1, 11), Mathf.Clamp((int)difficulty + 2, 1, 12) };

        //Purge spawn lists
        mstPreInst.RemoveAll(delegate (IZXSpawnInstruction Inst) { return !Inst.LevelValidation(type, difficulty, deck, false); });
        mstAmbInst.RemoveAll(delegate (IZXSpawnInstruction Inst) { return !Inst.LevelValidation(type, difficulty, deck, false); });
        mstChgInst.RemoveAll(delegate (IZXSpawnInstruction Inst) { return !Inst.LevelValidation(type, difficulty, deck, true); });
    }

    public void BeginSpawning(float Delay)
    {
        float delay = Mathf.Clamp(Delay, 0.5f, 10f);

        if (!disableSpawning)
        {
            //Start ambient spawning
            ambientSpawn = true;
            StartCoroutine(AmbientSpawning(delay));

            //Start challenge spawning
            challengeSpawn = true;
            StartCoroutine(ChallengeSpawning(delay + _challengeDelay));

            //Start challenge spawning
            eventSpawn = true;

            PreSpawn();
        }
    }

    public void BoostDifficulty()
    {
        _challengeSpawnTime = _challengeSpawnTime + (_challengeSpawnTime * challengeTimeMod);
    }

    /* Spawning Functions */
    #region
    public void PreSpawn()
    {
        IZXSpawnInstruction Instruction;

        regions.ForEach(delegate (ZXSpawnRegion Region) { Region.Initalize(type, difficulty); });

        regions.ForEach(delegate (ZXSpawnRegion Reg)
        {
            Instruction = mstPreInst[ZXMath.RandomFromSet(mstPreInst.Count, ZXMath.Randomization.None)];

            if (Instruction.SpawnValidation(this))
            {
                Instruction.SpawnNow();
            }
        });

        regions.ForEach(delegate (ZXSpawnRegion Reg) { Reg.PurgePreSpawners(); });
    }

    public IEnumerator AmbientSpawning(float Delay)
    {
        yield return new WaitForSeconds(Delay);

        playSpawnSounds = true;

        IZXSpawnInstruction Instruction;

        while (ambientSpawn && !disableSpawning)
        {
            Instruction = mstAmbInst[Random.Range(0, mstAmbInst.Count)];

            if (Instruction.SpawnValidation(this))
            {
                Instruction.SpawnNow();
            }

            yield return new WaitForSeconds(Random.Range(_ambiantSpawnTime / 1.25f, _ambiantSpawnTime * 1.25f));
        }
    }

    public IEnumerator ChallengeSpawning(float Delay)
    {
        IZXSpawnInstruction Instruction;
        List<IZXSpawnInstruction> ValidInstructions;

        int highCount;
        int highIndex;

        yield return new WaitForSeconds(Delay);

        playSpawnSounds = true;

        while (challengeSpawn && !disableSpawning)
        {
            highCount = lastActiveRegion == null ? 0 : lastActiveRegion.HeroCount;
            highIndex = 0;

            //Find the most active region
            for (int i = 0; i < regions.Count; i++)
            {
                if (regions[i].HeroCount > highCount)
                {
                    highCount = regions[i].HeroCount;
                    highIndex = i;
                }
            }

            lastActiveRegion = regions[highIndex];

            //Clone and purge instructions
            ValidInstructions = mstChgInst.FindAll(delegate (IZXSpawnInstruction Inst) { return Inst.SpawnValidation(this); });

            //Process instruction
            if (ValidInstructions != null && ValidInstructions.Count > 0)
            {
                //Pick spawn instruction
                Instruction = ValidInstructions[Random.Range(0, ValidInstructions.Count)];

                //Spawn
                Instruction.SpawnNow();

                if (logDebugging)
                    Debug.Log("Spawn manager instruction: " + Instruction.Name + " - " + lastActiveRegion.LaneID);

                //Wait full timer
                yield return new WaitForSeconds(_challengeSpawnTime);
            }
            else
            {
                if (logDebugging)
                {
                    Debug.Log("Spawn manager could not find any valid instructions");
                    DebugLogPools();
                }

                //Wait for 1 second and try again
                yield return new WaitForSeconds(1f);
            }

            ValidInstructions = null;
        }
    }

    public IEnumerator EventSpawning(float Delay)
    {
        yield return new WaitForSeconds(Delay);

        playSpawnSounds = true;

        while (eventSpawn && !disableSpawning)
        {
            yield return new WaitForSeconds(Random.Range(_eventSpawnTime / 1.25f, _eventSpawnTime * 1.25f));
        }
    }
    #endregion

    //Spawn tracking
    public static void SpawnedActor(IZXTargetable Actor)
    {
        if (actors != null & !actors.Contains(Actor))
        {
            actors.Add(Actor);
            Actor.e_Dead += d_deadActor;
            spawnedActors++;
        }
    }

    private static void DeadActor(IZXTargetable Actor)
    {
        if (actors != null & actors.Contains(Actor))
        {
            Actor.e_Dead -= d_deadActor;
            actors.Remove(Actor);
            spawnedActors--;
        }
    }

    //Public registration functions
    public void Register(ZXSpawnRegion Region)
    {
        if (regions != null && !regions.Contains(Region))
        {
            regions.Add(Region);
        }
    }

    public void DeRegister(ZXSpawnRegion Region)
    {
        if (regions != null && regions.Contains(Region))
        {
            regions.Remove(Region);
        }
    }

    /* Destruction */
    public void EndSpawning()
    {
        ambientSpawn = false;
        challengeSpawn = false;
        eventSpawn = false;

        StopAllCoroutines();
        CancelInvoke();
    }

    private void OnDisable()
    {
        playSpawnSounds = false; 

        spawnedActors = 0;
    }

    /* Support Functions */
    public static ZXSpawnRegion FindRegion(Region SearchRegion)
    {
        List<ZXSpawnRegion> outList = new List<ZXSpawnRegion>(2);

        switch (SearchRegion)
        {
            //Find regions with little activity
            case Region.Ambient:

                outList = regions.FindAll(delegate (ZXSpawnRegion Reg)
                {
                    return Reg.HeroCount <= 1 && Reg.RaiderCount <= 1;
                });

                break;

            //Find most active region
            case Region.Active:

                outList.Add(lastActiveRegion);

                break;
            
            //Find region after most active region
            case Region.ActiveNext:

                if (lastActiveRegion == null)
                    outList = null;
                else if (lastActiveRegion.NextRegion != null)
                    outList.Add(lastActiveRegion.NextRegion);
                else
                    outList.Add(lastActiveRegion);

                break;
            
            //Find region before most active region
            case Region.ActiveStart:

                if (lastActiveRegion == null)
                    outList = null;
                else if (lastActiveRegion.PreviousRegion != null)
                    outList.Add(lastActiveRegion.PreviousRegion);
                else
                    outList.Add(lastActiveRegion);

                break;

            //Find region at the end of most active lane
            case Region.ActiveEnd:

                if (lastActiveRegion == null)
                    outList = null;
                else
                {
                    ZXSpawnRegion targetRegion = regions.Find(delegate (ZXSpawnRegion Reg)
                    {
                        return Reg.LaneID == lastActiveRegion.LaneID && Reg.NextRegion == null;
                    });

                    if (targetRegion != null)
                        outList.Add(targetRegion);
                    else
                        outList = null;
                }

                break;

            //Find region with at least 4 heroes
            case Region.HeroBlob:

                if (lastActiveRegion != null && lastActiveRegion.HeroCount >= _heroBlobSize)
                    outList.Add(lastActiveRegion);
                else
                    outList = null;

                break;

            //Find region with at least 4 heroes
            case Region.UnPreSpawned:

                outList = regions.FindAll(delegate (ZXSpawnRegion Reg)
                {
                    return !Reg.preSpawned;
                });

                break;

            default:
                return null;
        }

        if (outList == null)
            return null;
        else
            return outList[Random.Range(0, outList.Count)];
    }

    public static int RandomLevel()
    {
        return levels[ZXMath.RandomFromSet(levels.Length, ZXMath.Randomization.Normal)];
    }

    public static Quaternion RandomY()
    {
        return Quaternion.Euler(new Vector3(0f, Random.Range(0, 360f), 0f));
    }

    public static T PoolRequest<T>(Spawn Request) where T : ZXToon
    {
        switch (Request)
        {
            //Other
            case Spawn.Civilian:
                return null;

            //Zombies
            case Spawn.Zombie:
                return p_Zombies.Request() as T;

            case Spawn.ZombieFast:
                return p_ZombiesFast.Request() as T;

            case Spawn.ZombieArmoured:
                return p_ZombiesArmoured.Request() as T;

            case Spawn.ZombieExploder:
                return p_Exploders.Request() as T;

            case Spawn.ZombieSpitter:
                return p_Spitters.Request() as T;

            case Spawn.ZombieTank:
                return p_Tanks.Request() as T;

            //Raiders
            case Spawn.RaiderMelee:
                return p_RaidersMelee.Request() as T;

            case Spawn.RaiderRanged:
                return p_RaidersRanged.Request() as T;

            case Spawn.RaiderHeavyWeapon:
                return p_RaidersHeavy.Request() as T;

            default:
                return null;
        }
    }

    public static bool PoolCheck(Spawn Check, int Count)
    {
        switch (Check)
        {
            //Other
            case Spawn.Civilian:
                return false;
            
            //Zombies
            case Spawn.Zombie:
                return p_Zombies.count >= Count;

            case Spawn.ZombieFast:
                return p_ZombiesFast.count >= Count;

            case Spawn.ZombieArmoured:
                return p_ZombiesArmoured.count >= Count;

            case Spawn.ZombieExploder:
                return p_Exploders.count >= Count;

            case Spawn.ZombieSpitter:
                return p_Spitters.count >= Count;

            case Spawn.ZombieTank:
                return p_Tanks.count >= Count;

            //Raiders
            case Spawn.RaiderMelee:
                return p_RaidersMelee.count >= Count;

            case Spawn.RaiderRanged:
                return p_RaidersRanged.count >= Count;

            case Spawn.RaiderHeavyWeapon:
                return p_RaidersHeavy.count >= Count;

            default:
                return false;
        }
    }

    public static void DebugLogPools()
    {
        //Debug.Log("Z_Normal: " + p_Zombies.count);
        //Debug.Log("Z_Fast: " + p_ZombiesFast.count);
        //Debug.Log("Z_Armour: " + p_ZombiesArmoured.count);
        //Debug.Log("Z_Exploder: " + p_Exploders.count);
        //Debug.Log("Z_Spitter: " + p_Spitters.count);
        //Debug.Log("Z_Tank: " + p_Tanks.count);
        //Debug.Log("R_Melee: " + p_RaidersMelee.count);
        //Debug.Log("R_Ranged: " + p_RaidersRanged.count);
        //Debug.Log("R_Heavy: " + p_RaidersHeavy.count);
    }
}
