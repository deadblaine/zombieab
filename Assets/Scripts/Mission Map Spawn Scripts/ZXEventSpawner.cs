﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using DG.Tweening;

public class ZXEventSpawner : MonoBehaviour
{
    [SerializeField] private ZXToon[] toons;
    [SerializeField] private int spawnTotalAmount;
    [SerializeField] private int spawnWaveAmount;
    [SerializeField] private float spawnDelay;
    [SerializeField] private float spawnWaveDelay;

    private ZXPoolLite<ZXToon> p_Toons;
    private List<ZXEventSpawnNode> nodes;
    private List<ZXEventSpawnNodeShowcase> showcases;

	// Use this for initialization
	private void Start ()
    {
        p_Toons = new ZXPoolLite<ZXToon>(ZXCombatUI.ObjectMask, toons, spawnTotalAmount);       
    }

    private void Awake()
    {
        nodes = new List<ZXEventSpawnNode>(GetComponentsInChildren<ZXEventSpawnNode>());
        showcases = new List<ZXEventSpawnNodeShowcase>(GetComponentsInChildren<ZXEventSpawnNodeShowcase>());

        if (nodes == null)
            throw new System.Exception("ZXEventSpawner must have children with ZXEventSpawnerNode script attached");
    }

    public void StartSpawning()
    {
        showcases.ForEach(delegate (ZXEventSpawnNodeShowcase showcase)
        {
            showcase.EndShowcase();
        });

        nodes.ForEach(delegate (ZXEventSpawnNode Object) { Object.SnapToNavMesh(); });
        StartCoroutine(SpawnTranslation());
    }

    private IEnumerator SpawnTranslation()
    {
        ZXToon toon;
        int spawnIndex = 0;

        for (int i = 0; i < Mathf.RoundToInt(spawnTotalAmount / spawnWaveAmount); i++)
        {
            for (int x = 0; x < spawnWaveAmount; x++)
            {
                toon = p_Toons.Request();

                if (toon != null)
                {
                    toon.Place(nodes[spawnIndex].transform.position, nodes[spawnIndex].transform.rotation, null);

                    spawnIndex++;

                    if (spawnIndex >= nodes.Count)
                        spawnIndex = 0;
                }

                yield return new WaitForSeconds(spawnDelay);
            }

            yield return new WaitForSeconds(spawnWaveDelay);
        }

        this.enabled = false;

        yield return null;
    }

    private void OnDisable()
    {
        nodes.ForEach(delegate (ZXEventSpawnNode Object) { Object.SnapToStart(); });
    }
}
