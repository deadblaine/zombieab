﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using ZXSpawnData;
using DG.Tweening;

public class ZXSpawnRegion : MonoBehaviour
{
    //Private constants
    private const float _maxGroundSpawnDelay = 3f;

    //Inspector fields
    [SerializeField] private int laneID;

    //Accessors
    public int LaneID
    {
        get
        {
            return laneID;
        }
    }
    public int SpawnerCount
    {
        get
        {
            if (locations != null)
                return locations.Count;
            else
                return 0;
        }
    }
    public bool HasTankSpawnLocation
    {
        get
        {
            return tankSpawn;
        }
    }
    public bool HasPreSpawnLocations
    {
        get
        {
            return preSpawns;
        }
    }
    public bool IsEndRegion
    {
        get
        {
            return nextRegion == null;
        }
    }
    public bool IsStartRegion
    {
        get
        {
            return previousRegion == null;
        }
    }
    public ZXSpawnRegion NextRegion
    {
        get
        {
            return nextRegion;
        }
    }
    public ZXSpawnRegion PreviousRegion
    {
        get
        {
            return previousRegion;
        }
    }
    public float SpawnDistance
    {
        get
        {
            return spawnDistance;
        }
    }
    public int TotalCount
    {
        get
        {
            return heroCount + zombieCount + raiderCount;
        }
    }
    public int HeroCount
    {
        get
        {
            return heroCount;
        }
    }
    public int ZombieCount
    {
        get
        {
            return zombieCount;
        }
    }
    public int RaiderCount
    {
        get
        {
            return raiderCount;
        }
    }

    //Private variables
    private List<ZXSpawnLocation> locations;
    private Vector3 regionLocation;
    private List<IZXTargetable> actors;
    private int heroCount = 0;
    private int zombieCount = 0;
    private int raiderCount = 0;

    //Tracking variables
    private ZXHeroSpawn heroSpawn;
    private bool tankSpawn = false;
    private bool preSpawns = false;
    private float spawnDistance;
    private ZXSpawnRegion nextRegion = null;
    private ZXSpawnRegion previousRegion = null;
    private float clearRadius;
    public bool preSpawned; 

    //Delegates
    private System.Action<IZXTargetable> d_actorDeath;

    //Initalization
    private void Awake()
    {
        ZXReflectingPool.Register<ZXSpawnRegion>(this, transform);

        locations = new List<ZXSpawnLocation>(10);
        actors = new List<IZXTargetable>(10);

        regionLocation = transform.position;

        d_actorDeath = new System.Action<IZXTargetable>(DeRegister);
    }

    private void Start()
    {
        ZXCombatUI.Spawner.Register(this);

        heroSpawn = ZXReflectingPool.FindClosest<ZXHeroSpawn>(transform);

        if (heroSpawn != null)
            spawnDistance = Vector3.Distance(transform.position, heroSpawn.transform.position);
    }

    public void Initalize(SpawnType Type, SpawnDifficulty Difficulty)
    {
        UnityEngine.AI.NavMeshHit hit;

        //Find next region in lane
        nextRegion = ZXReflectingPool.FindClosest<ZXSpawnRegion>(transform, delegate (ZXSpawnRegion Region)
        {
            return Region.LaneID == laneID && Region != this && Region.SpawnDistance > spawnDistance;
        });

        //Find previous region in lane
        previousRegion = ZXReflectingPool.FindClosest<ZXSpawnRegion>(transform, delegate (ZXSpawnRegion Region)
        {
            return Region.LaneID == laneID && Region != this && Region.SpawnDistance < spawnDistance;
        });

        //Find nearest NavMesh edge
        if (UnityEngine.AI.NavMesh.FindClosestEdge(transform.position, out hit, UnityEngine.AI.NavMesh.AllAreas))
        {
            clearRadius = Vector3.Distance(transform.position, hit.position);
        }
        else
            Debug.Log(gameObject.name + " failed to find nearest NavMesh edge");

        //Purge invalid spawners
        if (locations != null && locations.Count > 0)
        {
            switch (Type)
            {
                case SpawnType.Zombie:
                    locations.RemoveAll(delegate (ZXSpawnLocation Spawner) { return Spawner.Condition == SpawnCondition.RaiderOnly; });
                    break;

                case SpawnType.Raider:
                    locations.RemoveAll(delegate (ZXSpawnLocation Spawner) { return Spawner.Condition == SpawnCondition.ZombieOnly; });
                    break;
            }
        }

        //Initalize prespawners
        if (locations != null && locations.Count > 0)
        {
            locations.ForEach(delegate (ZXSpawnLocation Loc) { Loc.Initalize(); });
        }
    }

    public void PurgePreSpawners()
    {
        locations.RemoveAll(delegate (ZXSpawnLocation Loc) { return Loc.isPrespawner; });
    }

    public bool GetSpawnLocation(out ZXSpawnLocation NewLocation)
    {
        if (locations != null && locations.Count > 0)
        {
            NewLocation = locations[Random.Range(0, locations.Count)];
            return true;
        }

        NewLocation = null;
        return false;
    }

    public void SpawnFromRegion(ZXZombie toon)
    {
        StartCoroutine(SpawnFromGround(toon));
    }

    public void PreSpawnInRegion(ZXToon toon, object args)
    {
        toon.Place(RandomPointInRegion(), ZXSpawnManager.RandomY(), args);
        toon.AdjustValues(ZXSpawnManager.RandomLevel());
        ZXSpawnManager.SpawnedActor(toon);
    }

    private IEnumerator SpawnFromGround(ZXZombie toon)
    {
        ZXParticleEffect riseEffect = null;
        Vector3 location;
        Quaternion rotation;
        ZXToon nearestEnemy;

        yield return new WaitForSeconds(ZXMath.RandomNormal() * _maxGroundSpawnDelay);

        nearestEnemy = ZXReflectingPool.FindClosest<ZXToon>(transform, delegate (ZXToon Toon) { return Toon.Race != ToonRace.Zombie; });

        location = RandomPointInRegion();

        if (nearestEnemy != null)
        {
            rotation = Quaternion.LookRotation(nearestEnemy.Base - transform.position);
        }
        else
            rotation = ZXSpawnManager.RandomY();

        if (ZXSpawnManager.p_RiseEffect != null)
        {
            riseEffect = ZXSpawnManager.p_RiseEffect.Request();

            if (riseEffect != null)
            {
                riseEffect.Place(location, Quaternion.identity, null);
                riseEffect = null;
            }
        }

        toon.Place(location, rotation, ZXZombie.Action.ZombieRise);
        toon.AdjustValues(ZXSpawnManager.RandomLevel());
        ZXSpawnManager.SpawnedActor(toon);
    }

    //Debug
    private void Update()
    {
        if (Time.frameCount % 240 == 0)
        {
            //Log();
        }
    }

    /*Registration functions*/
    #region

    //Registration
    public void Register(ZXSpawnLocation Location)
    {
        if (locations != null && !locations.Contains(Location))
        {
            locations.Add(Location);

            if (Location.isTankSpawn)
                tankSpawn = true;
        }
    }
    public void Register(IZXTargetable Actor)
    {
        if (actors != null && !actors.Contains(Actor))
        {
            actors.Add(Actor);
            Actor.e_Dead += d_actorDeath;

            switch (Actor.Race)
            {
                case ToonRace.Human:
                    heroCount++;
                    break;

                case ToonRace.Zombie:
                    zombieCount++;
                    break;

                case ToonRace.Raider:
                    raiderCount++;
                    break;
            }
        }
    }
    
    //Deregistration
    public void DeRegister(ZXSpawnLocation Location)
    {
        if (locations != null && locations.Contains(Location))
        {
            locations.Remove(Location);
        }
    }
    public void DeRegister(IZXTargetable Actor)
    {
        if (actors != null && actors.Contains(Actor))
        {
            Actor.e_Dead -= d_actorDeath;
            actors.Remove(Actor);

            switch (Actor.Race)
            {
                case ToonRace.Human:
                    heroCount--;
                    break;

                case ToonRace.Zombie:
                    zombieCount--;
                    break;

                case ToonRace.Raider:
                    raiderCount--;
                    break;
            }
        }
    }
    #endregion

    //Destruction
    private void OnDisable()
    {
        StopAllCoroutines();
        CancelInvoke();

        if (ZXCombatUI.Spawner != null)
        {
            ZXCombatUI.Spawner.DeRegister(this);
        }

        ZXReflectingPool.DeRegister<ZXSpawnRegion>(transform);
    }

    //Support functions
    private Vector3 RandomPointInRegion()
    {
        return new Vector3(regionLocation.x + Random.Range(-clearRadius, clearRadius), regionLocation.y, regionLocation.z + Random.Range(-clearRadius, clearRadius));
    }

    private void Log()
    {
        Debug.Log(
            gameObject.name + " | Lane ID: " + laneID + " | Spawner Count: " + SpawnerCount +
            "\n" + "Humans: " + heroCount + "; Zombies: " + zombieCount + "; raiders: " + raiderCount +
            "\n"  + "Next region: " + nextRegion + " | " + "Previous Region: " + previousRegion);
    }
}
