﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;

/* Instruction Sets */
namespace ZXSpawnData
{
    /* Interface */
    public interface IZXSpawnInstruction
    {
        string Name
        {
            get;
        }

        bool LevelValidation(SpawnType Type, SpawnDifficulty Difficulty, DeckType Deck, bool Exclusive);
        bool SpawnValidation(ZXSpawnManager Manager);
        void SpawnNow();
    }

    /* Enumerations */
    #region
    public struct PreSpawnRequest
    {
        public Spawn spawn;

        public PreSpawnRequest(Spawn Spawn)
        {
            this.spawn = Spawn;
        }
    }
    public struct SpawnRequest
    {
        public Spawn spawn;
        public Region region;

        public SpawnRequest(Spawn Spawn, Region Region)
        {
            this.spawn = Spawn;
            this.region = Region;
        }
    }
    public enum SpawnType
    {
        All = 0,
        Zombie = 1,
        Raider = 2
    }
    public enum SpawnSpecial
    {
        None = 0,
        GroundSwell = 1,
    }
    public enum SpawnDifficulty
    {
        Level_0 = 0,
        Level_1 = 1,
        Level_2 = 2,
        Level_3 = 3,
        Level_4 = 4,
        Level_5 = 5,
        Level_6 = 6,
        Level_7 = 7,
        Level_8 = 8,
        Level_9 = 9,
        Level_10 = 10
    }
    public enum DeckType
    {
        All = 0,
		Town1 = 1,
        Town2 = 2,
		Town3 = 3,
        Town4 = 4,
        Town5 = 5,
        Town6 = 6
    }
    public enum SpawnCondition
    {
        All = 0,
        ZombieOnly = 1,
        RaiderOnly = 2
    }
    public enum Spawn
    {
        Civilian = 0,
        Zombie = 1,
        ZombieFast = 2,
        ZombieArmoured = 3,
        ZombieSpitter = 4,
        ZombieExploder = 5,
        ZombieTank = 6,
        RaiderMelee = 7,
        RaiderRanged = 8,
        RaiderHeavyWeapon = 9,
    }
    public enum Region
    {
        Ambient = 0,
        Active = 1,
        ActiveStart = 2,
        ActiveNext = 3,
        ActiveEnd = 4,
        HeroBlob = 5,
        LoneHero = 6,
        UnPreSpawned = 7
    }
    #endregion

    /* Instruction Classes */
    #region
    public class ZXSpawnInstruction : IZXSpawnInstruction
    {
        /* Constants */
        private const float _maxSpawnDelay = 5f;
        private const int _mediumHeroCap = 3;
        private const int _highHeroCap = 5;

        private string spawnName;
        private SpawnType spawnType;
        private SpawnDifficulty spawnDifficulty;
        private DeckType deckType;
        private List<SpawnRequest> spawnRequests;
        private SpawnSpecial special;
        private System.Predicate<ZXSpawnManager> condition = delegate (ZXSpawnManager Manager) { return true; };

        public ZXSpawnInstruction(string Name, SpawnType Type, SpawnDifficulty Difficulty, DeckType Deck, System.Predicate<ZXSpawnManager> Condition, List<SpawnRequest> Requests) : this (Name, Type, Difficulty, Deck, Condition, Requests, SpawnSpecial.None) { }
        public ZXSpawnInstruction(string Name, SpawnType Type, SpawnDifficulty Difficulty, DeckType Deck, System.Predicate<ZXSpawnManager> Condition, List<SpawnRequest> Requests, SpawnSpecial Special)
        {
            spawnName = Name;
            spawnType = Type;
            spawnDifficulty = Difficulty;
            deckType = Deck;
            spawnRequests = Requests;
            condition = Condition;
            special = Special;
        }

        public string Name
        {
            get
            {
                return spawnName;
            }
        }

        public bool LevelValidation(SpawnType Type, SpawnDifficulty Difficulty, DeckType Deck, bool Exclusive)
        {
            bool spawnValid = spawnType == Type || spawnType == SpawnType.All;
            bool diffValid = (int)spawnDifficulty <= (int)Difficulty;
            bool deckValid; 
            
            if (Exclusive)
                deckValid = deckType == Deck;
            else
                deckValid = deckType == Deck || deckType == DeckType.All;

            return spawnValid && diffValid && deckValid;
        }

        public bool SpawnValidation(ZXSpawnManager Manager)
        {
            int count;

            //Cap validation
            if ((ZXSpawnManager.SpawnedEnemies + spawnRequests.Count) > ZXSpawnManager.SpawnCap)
                return false;
            else
            {
                for (int i = 0; i < spawnRequests.Count; i++)
                {
                    count = spawnRequests.FindAll(delegate (SpawnRequest Req) { return Req.spawn == spawnRequests[i].spawn; }).Count;

                    //Pool and region validation
                    if (!ZXSpawnManager.PoolCheck(spawnRequests[i].spawn, count) || ZXSpawnManager.FindRegion(spawnRequests[i].region) == null)
                        return false;
                }
            }

            //Conditional validation
            if (condition != null)
                return condition(Manager);
            else
                return true;
        }

        public void SpawnNow()
        {
            ZXToon newToon;
            ZXSpawnRegion newRegion;
            ZXSpawnLocation newSpawner;

            for (int i = 0; i < spawnRequests.Count; i++)
            {
                newToon = ZXSpawnManager.PoolRequest<ZXToon>(spawnRequests[i].spawn);
                newRegion = ZXSpawnManager.FindRegion(spawnRequests[i].region);

                if (newToon != null && newRegion != null)
                {
                    if (special == SpawnSpecial.None && newRegion.GetSpawnLocation(out newSpawner))
                    {
                        newSpawner.Spawn(newToon, ZXMath.RandomNormal() * _maxSpawnDelay);
                    }
                    else if (special == SpawnSpecial.GroundSwell)
                    {
                        newRegion.SpawnFromRegion(newToon as ZXZombie);
                    }
                }
                else
                    Debug.Log("ZXSpawnInstruction.SpawnNow() failue");
            }
        }

        #region Data
        public static readonly ZXSpawnInstruction[] ambientSpawns =
        {
            new ZXSpawnInstruction
            (
            /*Spawn Name/UID*/              "Walker Zombie",
			/*Spawn Type*/				    SpawnType.All, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
            /*Deck Type*/                   DeckType.All,
			/*Cond Delegte*/				null,
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[] { new SpawnRequest(Spawn.Zombie, Region.Ambient) })  
            ),

            new ZXSpawnInstruction
            (
            /*Spawn Name/UID*/              "Fast Zombie",
			/*Spawn Type*/				    SpawnType.All, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
            /*Deck Type*/                   DeckType.All,
			/*Cond Delegte*/				null,
			/*Spawns*/                      new List<SpawnRequest>(new SpawnRequest[] { new SpawnRequest(Spawn.ZombieFast, Region.Ambient) })
            ),

            new ZXSpawnInstruction
            (
            /*Spawn Name/UID*/              "Melee Raider",
			/*Spawn Type*/				    SpawnType.Raider, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
			/*Deck Type*/              	 	DeckType.All,
			/*Cond Delegte*/				null,
			/*Spawns*/                      new List<SpawnRequest>(new SpawnRequest[] { new SpawnRequest(Spawn.RaiderMelee, Region.Ambient) })
            )
        };

        public static readonly ZXSpawnInstruction[] challengeSpawns =
        {
            #region Town1
            new ZXSpawnInstruction
			(
				/*Spawn Name/UID*/              "D1 Basic Zombie",
				/*Spawn Type*/				    SpawnType.Zombie, 
				/*Min Difficulty*/			    SpawnDifficulty.Level_0,
				/*Deck Type*/                   DeckType.Town1,
				/*Cond Delegte*/				null,
				/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
					{
						new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
					})
			),

            new ZXSpawnInstruction
            (
				/*Spawn Name/UID*/          "D1 Basic Zombie 2",
				/*Spawn Type*/				SpawnType.Zombie, 
				/*Min Difficulty*/			SpawnDifficulty.Level_0,
				/*Deck Type*/               DeckType.Town1,
				/*Cond Delegte*/			null,
				/*Spawns*/					new List<SpawnRequest>(new SpawnRequest[]
                    {
                        new SpawnRequest(Spawn.Zombie, Region.ActiveEnd),
                    })
            ),

            new ZXSpawnInstruction
            (
				/*Spawn Name/UID*/              "D1 Zombie Basic Endzone",
				/*Spawn Type*/				    SpawnType.Zombie, 
				/*Min Difficulty*/			    SpawnDifficulty.Level_0,
				/*Deck Type*/                   DeckType.Town1,
				/*Cond Delegte*/				null,
				/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                    {
                        new SpawnRequest(Spawn.Zombie, Region.ActiveEnd),
                    })
            ),

            new ZXSpawnInstruction
            (
				/*Spawn Name/UID*/              "D1 Runner Horde Town1",
				/*Spawn Type*/				    SpawnType.Zombie, 
				/*Min Difficulty*/			    SpawnDifficulty.Level_0,
				/*Deck Type*/                   DeckType.Town1,
				/*Cond Delegte*/				null,
				/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                    {
                        new SpawnRequest(Spawn.ZombieFast, Region.ActiveEnd),
                    })
            ),
            #endregion

            #region Town2
            new ZXSpawnInstruction
            (
            /*Spawn Name/UID*/              "D2 Zombie Horde",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
			/*Deck Type*/                   DeckType.Town2,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
                                                return ZXSpawnManager.HeroCount <= _mediumHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),                                                                                        											
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
												new SpawnRequest(Spawn.Zombie, Region.ActiveEnd),
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveEnd),
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveEnd),
                                            })
            ),

            new ZXSpawnInstruction
            (
			/*Spawn Name/UID*/              "D2 Runner Horde",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
			/*Deck Type*/                   DeckType.Town2,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
                                                return ZXSpawnManager.HeroCount <= _mediumHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {                                                                                               
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveEnd),
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveEnd),
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
												new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveEnd),
                                            })
            ),

			new ZXSpawnInstruction
			(
				/*Spawn Name/UID*/              "D2 Intro Armour",
				/*Spawn Type*/				    SpawnType.Zombie, 
				/*Min Difficulty*/			    SpawnDifficulty.Level_0,
				/*Deck Type*/                   DeckType.Town2,
				/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
											{
												return ZXSpawnManager.HeroCount >= _mediumHeroCap && ZXSpawnManager.HeroCount < _highHeroCap;
											}),	
				/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
					{
						new SpawnRequest(Spawn.ZombieArmoured, Region.Ambient),
						new SpawnRequest(Spawn.Zombie, Region.Active),
                                                                       new SpawnRequest(Spawn.ZombieFast, Region.ActiveEnd),
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                    })
			),
				
            new ZXSpawnInstruction
            (
            /*Spawn Name/UID*/              "D2 Intro Spitter",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
			/*Deck Type*/                   DeckType.Town2,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
												return ZXSpawnManager.HeroCount >= _mediumHeroCap && ZXSpawnManager.HeroCount < _highHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {
                                                new SpawnRequest(Spawn.ZombieSpitter, Region.Ambient),
												new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                            })
            ),

            new ZXSpawnInstruction
            (
            /*Spawn Name/UID*/              "D2 Exploder Blitz",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
            /*Deck Type*/                   DeckType.Town2,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
												return ZXSpawnManager.HeroCount >= _mediumHeroCap && ZXSpawnManager.HeroCount < _highHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {                        												                                            
												new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                                new SpawnRequest(Spawn.ZombieExploder, Region.ActiveEnd),
                                            })
            ),

            new ZXSpawnInstruction
            (
            /*Spawn Name/UID*/              "D23 Spitter Ploder Squash",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
            /*Deck Type*/                   DeckType.Town2,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner) 
                                            {
                                                return ZXSpawnManager.HeroCount > _highHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {										 
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
												new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
												new SpawnRequest(Spawn.ZombieSpitter, Region.ActiveEnd),
                                            })
            ),
            #endregion

            #region Town3
            new ZXSpawnInstruction
            (
            /*Spawn Name/UID*/              "D3 Zombie Horde",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
			/*Deck Type*/                   DeckType.Town3,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
                                                return ZXSpawnManager.HeroCount <= _mediumHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),                               
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
												new SpawnRequest(Spawn.Zombie, Region.ActiveEnd),
                                            })
            ),

            new ZXSpawnInstruction
            (
			/*Spawn Name/UID*/              "D3 Runner Horde",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
			/*Deck Type*/                   DeckType.Town3,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
                                                return ZXSpawnManager.HeroCount <= _mediumHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveEnd),
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
												new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
                                            })
            ),

            new ZXSpawnInstruction
            (
		    /*Spawn Name/UID*/              "D3 Armoured Zombies",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
			/*Deck Type*/                   DeckType.Town3,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
                                                return ZXSpawnManager.HeroCount < _highHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                                new SpawnRequest(Spawn.ZombieArmoured, Region.Ambient),
												new SpawnRequest(Spawn.Zombie, Region.ActiveEnd),
                                            })
            ),

            new ZXSpawnInstruction
            (
            /*Spawn Name/UID*/              "D3 Spitter Screen",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
			/*Deck Type*/                   DeckType.Town3,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
												return ZXSpawnManager.HeroCount >= _mediumHeroCap && ZXSpawnManager.HeroCount < _highHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
												new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                                new SpawnRequest(Spawn.ZombieSpitter, Region.ActiveNext),                                             
                                            })
            ),

			new ZXSpawnInstruction
			(
				/*Spawn Name/UID*/              "D3 Dual Exploder Squash",
				/*Spawn Type*/				    SpawnType.Zombie, 
				/*Min Difficulty*/			    SpawnDifficulty.Level_0,
				/*Deck Type*/                   DeckType.Town3,
				/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
					{
												return ZXSpawnManager.HeroCount >= _mediumHeroCap && ZXSpawnManager.HeroCount < _highHeroCap;
					}),	
				/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
					{
						new SpawnRequest(Spawn.Zombie, Region.ActiveEnd),
						new SpawnRequest(Spawn.ZombieExploder, Region.ActiveEnd),
						new SpawnRequest(Spawn.ZombieExploder, Region.Ambient),
					})
			),			

			new ZXSpawnInstruction
			(
				/*Spawn Name/UID*/              "D3 Spitter Twins",
				/*Spawn Type*/				    SpawnType.Zombie, 
				/*Min Difficulty*/			    SpawnDifficulty.Level_0,
				/*Deck Type*/                   DeckType.Town3,
				/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
											{
												return ZXSpawnManager.HeroCount >= _mediumHeroCap && ZXSpawnManager.HeroCount < _highHeroCap;
											}),	
				/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
											{
												new SpawnRequest(Spawn.ZombieSpitter, Region.ActiveEnd),
												new SpawnRequest(Spawn.ZombieSpitter, Region.Ambient),
											})
			),

			new ZXSpawnInstruction
            (
            /*Spawn Name/UID*/              "D3 Tank Squash",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
            /*Deck Type*/                   DeckType.Town3,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
                                                return ZXSpawnManager.HeroCount > _mediumHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {
                                                new SpawnRequest(Spawn.ZombieTank, Region.ActiveEnd),
												new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
												new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                            })
						
			),			

            #endregion

            #region Town4
            new ZXSpawnInstruction
            (
            /*Spawn Name/UID*/              "D4 Zombie Horde",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
			/*Deck Type*/                   DeckType.Town4,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
                                                return ZXSpawnManager.HeroCount <= _mediumHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
												new SpawnRequest(Spawn.Zombie, Region.Active),
                                                new SpawnRequest(Spawn.Zombie, Region.Active),
                                            })
            ),

            new ZXSpawnInstruction
            (
			/*Spawn Name/UID*/              "D4 Runner Horde",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
			/*Deck Type*/                   DeckType.Town4,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
                                                return ZXSpawnManager.HeroCount <= _mediumHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveEnd),
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveEnd),                                             
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
                                            })
            ),

            new ZXSpawnInstruction
            (
		    /*Spawn Name/UID*/              "D4 Armoured Zombies",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
			/*Deck Type*/                   DeckType.Town4,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
                                                return ZXSpawnManager.HeroCount < _highHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
												new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                                new SpawnRequest(Spawn.ZombieArmoured, Region.Ambient),
                                            })
            ),

            new ZXSpawnInstruction
            (
            /*Spawn Name/UID*/              "D4 Spitter Twins",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
			/*Deck Type*/                   DeckType.Town4,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
                                                return ZXSpawnManager.HeroCount >= _mediumHeroCap && ZXSpawnManager.HeroCount < _highHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
                                                new SpawnRequest(Spawn.ZombieSpitter, Region.Ambient),
                                                new SpawnRequest(Spawn.ZombieSpitter, Region.ActiveNext),
												new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
                                            })
            ),

            new ZXSpawnInstruction
            (
            /*Spawn Name/UID*/              "D4 Exploder Blitz",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
            /*Deck Type*/                   DeckType.Town4,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
                                                return ZXSpawnManager.HeroCount >= _mediumHeroCap && ZXSpawnManager.HeroCount < _highHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                                new SpawnRequest(Spawn.ZombieExploder, Region.ActiveNext),
                                                new SpawnRequest(Spawn.ZombieExploder, Region.Ambient),
                                            })
            ),

            new ZXSpawnInstruction
            (
            /*Spawn Name/UID*/              "D4 Tank",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
            /*Deck Type*/                   DeckType.Town4,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
                                                return ZXSpawnManager.HeroCount > _mediumHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {
                                                new SpawnRequest(Spawn.ZombieTank, Region.ActiveEnd),
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
												new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                            })
            ),
            #endregion

            #region Town5
            new ZXSpawnInstruction
            (
            /*Spawn Name/UID*/              "D5 Zombie Horde",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
			/*Deck Type*/                   DeckType.Town5,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
                                                return ZXSpawnManager.HeroCount <= _mediumHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                                new SpawnRequest(Spawn.Zombie, Region.Active),
												new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                            })
            ),

            new ZXSpawnInstruction
            (
			/*Spawn Name/UID*/              "D5 Runner Horde",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
			/*Deck Type*/                   DeckType.Town5,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
                                                return ZXSpawnManager.HeroCount <= _mediumHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveEnd),
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveEnd),
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
                                            })
            ),

            new ZXSpawnInstruction
            (
		    /*Spawn Name/UID*/              "D5 Armoured Zombies",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
			/*Deck Type*/                   DeckType.Town5,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
                                                return ZXSpawnManager.HeroCount < _highHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {
                                                new SpawnRequest(Spawn.ZombieArmoured, Region.ActiveNext),
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
												new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
                                            })
            ),

            new ZXSpawnInstruction
            (
            /*Spawn Name/UID*/              "D5 Spitter Screen",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
			/*Deck Type*/                   DeckType.Town5,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
                                                return ZXSpawnManager.HeroCount >= _mediumHeroCap && ZXSpawnManager.HeroCount < _highHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
                                                new SpawnRequest(Spawn.ZombieSpitter, Region.ActiveNext),
                                                new SpawnRequest(Spawn.ZombieSpitter, Region.Ambient),
                                            })
            ),

            new ZXSpawnInstruction
            (
            /*Spawn Name/UID*/              "D5 Exploder Blitz",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
            /*Deck Type*/                   DeckType.Town5,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
                                                return ZXSpawnManager.HeroCount >= _mediumHeroCap && ZXSpawnManager.HeroCount < _highHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
												new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                                new SpawnRequest(Spawn.ZombieExploder, Region.ActiveNext),
                                                new SpawnRequest(Spawn.ZombieExploder, Region.Ambient),
                                            })
            ),

            new ZXSpawnInstruction
            (
            /*Spawn Name/UID*/              "D5 Tank",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
            /*Deck Type*/                   DeckType.Town5,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
                                                return ZXSpawnManager.HeroCount > _mediumHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {
                                                new SpawnRequest(Spawn.ZombieTank, Region.ActiveEnd),
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
												new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                            })
			),	
			new ZXSpawnInstruction
				(
					/*Spawn Name/UID*/              "D5 SpitterTank",
					/*Spawn Type*/				    SpawnType.Zombie, 
					/*Min Difficulty*/			    SpawnDifficulty.Level_0,
					/*Deck Type*/                   DeckType.Town5,
					/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
						{
							return ZXSpawnManager.HeroCount > _mediumHeroCap;
						}),	
					/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
						{
							new SpawnRequest(Spawn.ZombieTank, Region.ActiveEnd),
							new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
							new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
							new SpawnRequest(Spawn.ZombieSpitter, Region.ActiveNext),
						})
            ),
            #endregion

            #region Town6
            new ZXSpawnInstruction
            (
            /*Spawn Name/UID*/              "D5 Zombie Horde",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
			/*Deck Type*/                   DeckType.Town6,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
                                                return ZXSpawnManager.HeroCount <= _mediumHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                                new SpawnRequest(Spawn.Zombie, Region.Active),
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                            })
            ),

            new ZXSpawnInstruction
            (
			/*Spawn Name/UID*/              "D5 Runner Horde",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
			/*Deck Type*/                   DeckType.Town6,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
                                                return ZXSpawnManager.HeroCount <= _mediumHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveEnd),
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveEnd),
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
                                            })
            ),

            new ZXSpawnInstruction
            (
		    /*Spawn Name/UID*/              "D5 Armoured Zombies",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
			/*Deck Type*/                   DeckType.Town6,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
                                                return ZXSpawnManager.HeroCount < _highHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {
                                                new SpawnRequest(Spawn.ZombieArmoured, Region.ActiveNext),
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
                                            })
            ),

            new ZXSpawnInstruction
            (
            /*Spawn Name/UID*/              "D5 Spitter Screen",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
			/*Deck Type*/                   DeckType.Town6,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
                                                return ZXSpawnManager.HeroCount >= _mediumHeroCap && ZXSpawnManager.HeroCount < _highHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
                                                new SpawnRequest(Spawn.ZombieSpitter, Region.ActiveNext),
                                                new SpawnRequest(Spawn.ZombieSpitter, Region.Ambient),
                                            })
            ),

            new ZXSpawnInstruction
            (
            /*Spawn Name/UID*/              "D5 Exploder Blitz",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
            /*Deck Type*/                   DeckType.Town6,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
                                                return ZXSpawnManager.HeroCount >= _mediumHeroCap && ZXSpawnManager.HeroCount < _highHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                                new SpawnRequest(Spawn.ZombieExploder, Region.ActiveNext),
                                                new SpawnRequest(Spawn.ZombieExploder, Region.Ambient),
                                            })
            ),

            new ZXSpawnInstruction
            (
            /*Spawn Name/UID*/              "D5 Tank",
			/*Spawn Type*/				    SpawnType.Zombie, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
            /*Deck Type*/                   DeckType.Town6,
			/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                                            {
                                                return ZXSpawnManager.HeroCount > _mediumHeroCap;
                                            }),	
			/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                                            {
                                                new SpawnRequest(Spawn.ZombieTank, Region.ActiveEnd),
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
                                                new SpawnRequest(Spawn.ZombieFast, Region.ActiveNext),
                                                new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                                            })
            ),
            new ZXSpawnInstruction
                (
					/*Spawn Name/UID*/              "D5 SpitterTank",
					/*Spawn Type*/				    SpawnType.Zombie, 
					/*Min Difficulty*/			    SpawnDifficulty.Level_0,
					/*Deck Type*/                   DeckType.Town6,
					/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
                        {
                            return ZXSpawnManager.HeroCount > _mediumHeroCap;
                        }),	
					/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
                        {
                            new SpawnRequest(Spawn.ZombieTank, Region.ActiveEnd),
                            new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                            new SpawnRequest(Spawn.Zombie, Region.ActiveNext),
                            new SpawnRequest(Spawn.ZombieSpitter, Region.ActiveNext),
                        })
            ),
            #endregion

			#region Raider 5
			new ZXSpawnInstruction
			(
				/*Spawn Name/UID*/              "D5 Raider Melee Screen",
				/*Spawn Type*/				    SpawnType.Raider, 
				/*Min Difficulty*/			    SpawnDifficulty.Level_0,
				/*Deck Type*/                   DeckType.All,
				/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
					{
						return ZXSpawnManager.HeroCount <= _mediumHeroCap;
					}),
				/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
					{
						new SpawnRequest(Spawn.RaiderMelee, Region.Active),
						new SpawnRequest(Spawn.RaiderRanged, Region.ActiveNext),
						new SpawnRequest(Spawn.RaiderRanged, Region.ActiveEnd),
					})

			),

			new ZXSpawnInstruction
			(
				/*Spawn Name/UID*/              "D5 Raider Melee Assault",
				/*Spawn Type*/				    SpawnType.Raider, 
				/*Min Difficulty*/			    SpawnDifficulty.Level_0,
				/*Deck Type*/                   DeckType.All,
				/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
					{
						return ZXSpawnManager.HeroCount <= _mediumHeroCap;
					}),
				/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
					{
						new SpawnRequest(Spawn.RaiderMelee, Region.ActiveNext),
						new SpawnRequest(Spawn.RaiderMelee, Region.ActiveNext),
						new SpawnRequest(Spawn.RaiderMelee, Region.ActiveEnd),
					})

			),

			new ZXSpawnInstruction
			(
				/*Spawn Name/UID*/              "D5 Raider Gang",
				/*Spawn Type*/				    SpawnType.Raider, 
				/*Min Difficulty*/			    SpawnDifficulty.Level_0,
				/*Deck Type*/                   DeckType.All,
				/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
					{
						return ZXSpawnManager.HeroCount <= _mediumHeroCap;
					}),
				/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
					{
						new SpawnRequest(Spawn.RaiderRanged, Region.ActiveNext),
						new SpawnRequest(Spawn.RaiderMelee, Region.ActiveNext),
						new SpawnRequest(Spawn.RaiderMelee, Region.Active),
					})

			),

			new ZXSpawnInstruction
			(
				/*Spawn Name/UID*/              "D5 Raider Ranged Mid Squash",
				/*Spawn Type*/				    SpawnType.Raider, 
				/*Min Difficulty*/			    SpawnDifficulty.Level_0,
				/*Deck Type*/                   DeckType.All,
				/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
					{
						return ZXSpawnManager.HeroCount  >= _mediumHeroCap && ZXSpawnManager.HeroCount < _highHeroCap;
					}),
				/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
					{
						new SpawnRequest(Spawn.RaiderHeavyWeapon, Region.ActiveNext),
						new SpawnRequest(Spawn.RaiderRanged, Region.ActiveEnd),
						new SpawnRequest(Spawn.RaiderHeavyWeapon, Region.ActiveEnd),
					})

			),

			new ZXSpawnInstruction
			(
				/*Spawn Name/UID*/              "D5 Raider Heavy Squash",
				/*Spawn Type*/				    SpawnType.Raider, 
				/*Min Difficulty*/			    SpawnDifficulty.Level_0,
				/*Deck Type*/                   DeckType.All,
				/*Cond Delegte*/				new System.Predicate<ZXSpawnManager>(delegate (ZXSpawnManager Spawner)
					{
						return ZXSpawnManager.HeroCount  > _mediumHeroCap;
					}),
				/*Spawns*/					    new List<SpawnRequest>(new SpawnRequest[]
					{
						new SpawnRequest(Spawn.RaiderHeavyWeapon, Region.ActiveEnd),
						new SpawnRequest(Spawn.RaiderHeavyWeapon, Region.Ambient),
						new SpawnRequest(Spawn.RaiderRanged, Region.ActiveNext),
						new SpawnRequest(Spawn.RaiderMelee, Region.Active),
					})

			),
			#endregion
        };
        #endregion
    }

    public class ZXPreSpawnInstruction : IZXSpawnInstruction
    {
        private string spawnName;
        private SpawnType spawnType;
        private SpawnDifficulty spawnDifficulty;
        private DeckType deckType;
        private List<PreSpawnRequest> spawnRequests;
        private object instruction;

        public string Name
        {
            get
            {
                return spawnName;
            }
        }

        public ZXPreSpawnInstruction(string Name, SpawnType Type, SpawnDifficulty Difficulty, DeckType Deck, List<PreSpawnRequest> Requests, object Instruction)
        {
            spawnName = Name;
            spawnType = Type;
            spawnDifficulty = Difficulty;
            deckType = Deck;
            spawnRequests = Requests;
            instruction = Instruction;
        }

        public bool LevelValidation(SpawnType Type, SpawnDifficulty Difficulty, DeckType Deck, bool Exclusive)
        {
            bool spawnValid = spawnType == Type || spawnType == SpawnType.All;
            bool diffValid = (int)spawnDifficulty <= (int)Difficulty;
            bool deckValid;

            if (Exclusive)
                deckValid = deckType == Deck;
            else
                deckValid = deckType == Deck || deckType == DeckType.All;

            return spawnValid && diffValid && deckValid;
        }

        public bool SpawnValidation(ZXSpawnManager Manager)
        {
            int count;

            //Cap validation
            if ((ZXSpawnManager.SpawnedEnemies + spawnRequests.Count) > ZXSpawnManager.SpawnCap)
                return false;
            else
            {
                for (int i = 0; i < spawnRequests.Count; i++)
                {
                    count = spawnRequests.FindAll(delegate (PreSpawnRequest Req) { return Req.spawn == spawnRequests[i].spawn; }).Count;

                    //Pool validation
                    if (!ZXSpawnManager.PoolCheck(spawnRequests[i].spawn, count))
                        return false;
                }
            }

            return true;
        }

        public void SpawnNow()
        {
            ZXToon newToon;
            ZXSpawnRegion newRegion;

            for (int i = 0; i < spawnRequests.Count; i++)
            {
                newToon = ZXSpawnManager.PoolRequest<ZXToon>(spawnRequests[i].spawn);
                newRegion = ZXSpawnManager.FindRegion(Region.UnPreSpawned);

                if (newToon != null && newRegion != null)
                {
                    newRegion.PreSpawnInRegion(newToon, instruction);
                    newRegion.preSpawned = true;
                }
                else
                    Debug.Log("ZXPreSpawnInstruction.SpawnNow() failue");
            }
        }

        #region Data
        public static readonly ZXPreSpawnInstruction[] preSpawns =
        {
            new ZXPreSpawnInstruction
            (
            /*Spawn Name/UID*/              "Walker Zombies",
			/*Spawn Type*/				    SpawnType.All, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
            /*Deck Type*/                   DeckType.All,
			/*Spawns*/					    new List<PreSpawnRequest>(new PreSpawnRequest[] { new PreSpawnRequest(Spawn.Zombie) }),
            /*Instruction*/                 null
            ),

            new ZXPreSpawnInstruction
            (
            /*Spawn Name/UID*/              "Raider",
			/*Spawn Type*/				    SpawnType.Raider, 
			/*Min Difficulty*/			    SpawnDifficulty.Level_0,
            /*Deck Type*/                   DeckType.All,
			/*Spawns*/					    new List<PreSpawnRequest>(new PreSpawnRequest[] { new PreSpawnRequest(Spawn.RaiderRanged) }),
            /*Instruction*/                 null
            ),
        };
        #endregion
    }
    #endregion
}
