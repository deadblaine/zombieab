﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ZXCardUIPopup : ZXCore.ZXPopup
{
    [SerializeField] private Image cardImage;
    [SerializeField] private Image classImage;
    [SerializeField] private GameObject costObject;
    [SerializeField] private GameObject classObject;
    [SerializeField] private GameObject useButton;
    [SerializeField] private TextMeshProUGUI costText;
    [SerializeField] private TextMeshProUGUI levelText;
    [SerializeField] private TextMeshProUGUI rankText;

    private float infoHeight = 425f;
    private float useHeight = 510f;

    private static ZXCardUIPopup singleton;
    private RectTransform rect;
    private ZXCardUI cardUI;
    private ZXCard card;

    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();

        singleton = this;

        rect = GetComponent<RectTransform>();

        gameObject.SetActive(false);
    }

    public static void ShowPopup(ZXCard Card, ZXCardUI CardUI)
    {
        singleton.SetCard(Card);
        singleton.SetPosition(CardUI);
        singleton.Show();
    }

    private void SetPosition(ZXCardUI CardUI)
    {
        this.cardUI = CardUI;

        rect.SetParent(CardUI.AnchorTransform);

        if (ZXDeckManager.DeckContains(CardUI))
        {
            rect.sizeDelta = new Vector2(rect.sizeDelta.x, infoHeight);
            rect.localPosition = new Vector3(0, -20, 0);
            useButton.SetActive(false);
        }
        else
        {
            rect.sizeDelta = new Vector2(rect.sizeDelta.x, useHeight);
            rect.localPosition = new Vector3(0, -60, 0);
            useButton.SetActive(true);
        }
    }

    private void SetCard(ZXCard Card)
    {
        this.card = Card;

        //Update sprite
        cardImage.sprite = Card.Card_Image;

        //Set card cost
        if (card.Cost > 0)
            costText.text = Card.Cost.ToString();
        else
            costObject.SetActive(false);

        levelText.text = Card.LevelText;

        //Disable virus display for spells
        if (card.Type == ZXCard.Card_Type.Spell)
        {
            classObject.SetActive(false);
        }
        else
        {
            classImage.sprite = card.Class_Image;
            classObject.SetActive(true);
            rankText.text = Card.Rank.ToString();
        }
    }

    public void OpenInfo()
    {
        ZXCardInfoPopup.Open(card);

        Hide();
    }

    public void UseCard()
    {
        ZXDeckManager.UseCard(cardUI);

        Hide();
    }
}
