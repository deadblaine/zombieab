﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class ZXUpgradeProperty : MonoBehaviour
{
    [SerializeField] private Image icon;
    [SerializeField] private TextMeshProUGUI title;
    [SerializeField] private TextMeshProUGUI amount;
    [SerializeField] private TextMeshProUGUI additional;

    private int DisplayAmount
    {
        get
        {
            return currentAmount;
        }
        set
        {
            currentAmount = value;
            amount.text = value.ToString();
        }
    }

    private int currentAmount = 0;

    public void Set(ZXCard.Property NewProperty, float Delay)
    {
        float transTime = 0.2f;
        float countTime = 0.75f;
        int total = NewProperty.NextLevelAdjustedIntValue + NewProperty.LevelAdjustIntValue;
        RectTransform rect = GetComponent<RectTransform>();

        icon.sprite = NewProperty.Icon;
        title.text = NewProperty.Name;

        currentAmount = NewProperty.LevelAdjustIntValue;
        amount.text = NewProperty.LevelAdjustedValue;
        additional.text = "+" + NewProperty.NextLevelAdjustedValue;

        gameObject.SetActive(true);

        rect.localScale = Vector3.zero;
        rect.DOScale(1f, transTime).SetDelay(Delay);

        DOTween.To(delegate () { return DisplayAmount; }, delegate (int x) { DisplayAmount = x; }, total, countTime).SetDelay(transTime + Delay).SetEase(Ease.OutBack);
    }

    public void Disable()
    {
        gameObject.SetActive(false);
    }
}
