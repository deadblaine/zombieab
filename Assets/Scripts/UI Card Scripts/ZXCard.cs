﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.IO;

public class ZXCard
{
    /* Public types */
    #region
    public enum Card_Type
    {
        Hero = 0,
        Waifu = 1,
        Spell = 2
    }

    public enum Card_Rarity
    {
        Common = 0,
        Rare = 1,
        Epic = 2,
        Legendary = 3
    }

    public struct Card_Level
    {
        public int Value;
        public int Cards;
        public int Cost;
        public int Req_Rank;
        public int Virus;

        public Card_Level(XmlNode node)
        {
            this.Value = int.Parse(node.InnerText);
            this.Cards = int.Parse(node.Attributes["cards"].Value);
            this.Cost = int.Parse(node.Attributes["cost"].Value);
            this.Virus = int.Parse(node.Attributes["virus"].Value);
            this.Req_Rank = int.Parse(node.Attributes["req_rank"].Value);
        }
    }

    public struct Card_Rank
    {
        public string Key;
        public int Value;
        public int Primary_Cost;
        public int Secondary_Cost;
        public int Req_Level;

        public Color Rank_Color
        {
            get
            {
                return ZXDataManager.GetColor(Key);
            }
        }

        public Card_Rank(XmlNode node)
        {
            this.Key = node.Attributes["key"].Value;
            this.Value = int.Parse(node.InnerText);
            this.Primary_Cost = int.Parse(node.Attributes["primary_cost"].Value);
            this.Secondary_Cost = int.Parse(node.Attributes["secondary_cost"].Value);
            this.Req_Level = int.Parse(node.Attributes["req_level"].Value);
        }
    }

    public struct Card_Class
    {
        private string icon_id;

        public Sprite Icon
        {
            get
            {
                return ZXImageServer.FetchSprite(icon_id);
            }
        }

        public Color Class_Color
        {
            get
            {
                return ZXDataManager.GetColor(Key);
            }
        }

        public string Key;
        public string Primary_Resource;
        public string Secondary_Resource;

        public Card_Class(XmlNode node)
        {
            icon_id = node.Attributes["icon"].Value;
            this.Key = node.Attributes["key"].Value;
            this.Primary_Resource = node.Attributes["primary_resource"].Value;
            this.Secondary_Resource = node.Attributes["secondary_resource"].Value;
        }
    }

    public struct Romance
    {
        private string image_id;
        private string thumb_id;
        private ZXCard card;

        public Sprite Image
        {
            get
            {
                return ZXImageServer.FetchSprite(image_id);
            }
        }

        public Sprite Thumb
        {
            get
            {
                return ZXImageServer.FetchSprite(thumb_id);
            }
        }

        public bool Locked
        {
            get
            {
                return Requiered_Rank > card.Rank;
            }
        }

        public int Requiered_Rank;

        public Romance(XmlNode node, ZXCard card)
        {
            image_id = node.Attributes["image"].Value;
            thumb_id = node.Attributes["thumb"].Value;
            this.card = card;
            Requiered_Rank = int.Parse(node.Attributes["req_rank"].Value);
        }
    }

    public struct Property
    {
        private string image_id;

        public Sprite Icon
        {
            get
            {
                return ZXImageServer.FetchSprite(image_id);
            }
        }
        public ZXCard Card;
        public string Name;
        public string Value;
        public bool LookUp;
        public bool Upgrade;
        public float UpgradeIncrease;
        public bool Display;

        public int LevelAdjustIntValue
        {
            get
            {
                if (Upgrade)
                {
                    float basevalue = float.Parse(Value);
                    float increase = basevalue * UpgradeIncrease * (Card.Level - 1);

                    return Mathf.RoundToInt(basevalue + increase);
                }
                else
                    return 0;
            }
        }

        public string LevelAdjustedValue
        {
            get
            {
                if (Upgrade)
                {
                    float basevalue = float.Parse(Value);
                    float increase = basevalue * UpgradeIncrease * (Card.Level - 1);

                    return Mathf.RoundToInt(basevalue + increase).ToString();
                }
                else
                    return Value;
            }
        }

        public int NextLevelAdjustedIntValue
        {
            get
            {
                if (Upgrade)
                {
                    float basevalue = float.Parse(Value);
                    float increase = basevalue * UpgradeIncrease;

                    return Mathf.RoundToInt(increase);
                }
                else
                    return 0;
            }
        }

        public string NextLevelAdjustedValue
        {
            get
            {
                if (Upgrade)
                {
                    float basevalue = float.Parse(Value);
                    float increase = basevalue * UpgradeIncrease;

                    return Mathf.RoundToInt(increase).ToString();
                }
                else
                    return string.Empty;
            }
        }

        public Property(XmlNode Node, string Value, ZXCard Card)
        {
            //Get base property data
            XmlNode prop_node = ZXDataManager.card_Properties[Node.Attributes["key"].Value];

            image_id = prop_node.Attributes["icon"].Value;
            this.Card = Card;
            Name = ZXLocal.LocalText(Node.Attributes["key"].Value);
            LookUp = bool.Parse(prop_node.Attributes["lookup"].Value);

            if (LookUp)
                this.Value = ZXLocal.LocalText(Value);
            else
                this.Value = Value;

            Upgrade = bool.Parse(prop_node.Attributes["upgrade"].Value);

            if (Upgrade)
                UpgradeIncrease = float.Parse(prop_node.Attributes["upgrade_increase"].Value);
            else
                UpgradeIncrease = 0f;

            Display = bool.Parse(prop_node.Attributes["display"].Value);
        }
    }
    #endregion

    /*Private variables*/
    private string key;
    private string cardName;
    private string cardDescription;
    private string imageKey;
    private string slideKey;
    private string resourcePath; 
    private Card_Type cardType;
    private int cardCost;
    private Card_Rarity cardRarity;
    private Card_Class cardClass;

    private Card_Level cardLevel;
    private Card_Rank cardRank;
    public List<Property> cardProperties;
    public List<Romance> cardRomance;

    /*Accessors*/
    private int cards = 0;
    private string pp_cards_count_id;
    private string pp_cards_level_id;
    private string pp_cards_rank_id;
    private int virus = 0;
    private string pp_cards_virus_id;

    public Sprite Card_Image
    {
        get
        {
            return ZXImageServer.FetchSprite(imageKey);
        }
    }

    public Sprite Slide_Image
    {
        get
        {
            return ZXImageServer.FetchSprite(slideKey);
        }
    }

    public Sprite Class_Image
    {
        get
        {
            return cardClass.Icon;
        }
    }

    public GameObject PreFab
    { 
        get
        {
            return Resources.Load<GameObject>(resourcePath);
        }
    }

    public Card_Rarity Rarity
    {
        get
        {
            return cardRarity;
        }
    }

    public Color Rarity_Color
    {
        get
        {
            return ZXDataManager.GetColor(cardRarity.ToString());
        }
    }

    public Color Rank_Color
    {
        get
        {
            return ZXDataManager.GetColor(cardRank.Key);
        }
    }

    public Color Class_Color
    {
        get
        {
            return ZXDataManager.GetColor(cardClass.Key);
        }
    }

    public int Cards
    {
        get
        {
            if (PlayerPrefs.HasKey(pp_cards_count_id))
            {
                return PlayerPrefs.GetInt(pp_cards_count_id);
            }
            else
            {
                return 0;
            }
        }
        set
        {
            cards = value;

            PlayerPrefs.SetInt(pp_cards_count_id, cards);

            if (e_CardChanged != null)
                e_CardChanged();
        }
    }

    public int Level
    {
        get
        {
            if (PlayerPrefs.HasKey(pp_cards_level_id))
            {
                return PlayerPrefs.GetInt(pp_cards_level_id);
            }
            else
            {
                return 1;
            }
        }
        set
        {
            cardLevel = ZXDataManager.card_Levels[value];

            PlayerPrefs.SetInt(pp_cards_level_id, value);

            if (e_CardChanged != null)
                e_CardChanged();               
        }
    }

    public string LevelText
    {
        get
        {
            return "Level " + Level.ToString();
        }
    }

    public string ClassText
    {
        get
        {
            return ZXLocal.LocalText(cardClass.Key);
        }
    }

    public int NextLevelCost
    {
        get
        {
            Card_Level nextLevel;

            if (ZXDataManager.card_Levels.ContainsKey(Level + 1))
                nextLevel = ZXDataManager.card_Levels[Level + 1];
            else
                return 0;

            return nextLevel.Cost;
        }
    }

    public int NextLevelCards
    {
        get
        {
            Card_Level nextLevel;

            if (ZXDataManager.card_Levels.ContainsKey(Level + 1))
                nextLevel = ZXDataManager.card_Levels[Level + 1];
            else
                return 0;

            return nextLevel.Cards;
        }
    }

    public bool CanLevel
    {
        get
        {
            Card_Level nextLevel;

            if (ZXDataManager.card_Levels.ContainsKey(Level + 1))
                nextLevel = ZXDataManager.card_Levels[Level + 1];
            else
                return false;

            return cardRank.Value >= nextLevel.Req_Rank;
        }
    }

    public bool HasLevelResources
    {
        get
        {
            Card_Level nextLevel;

            if (ZXDataManager.card_Levels.ContainsKey(Level + 1))
                nextLevel = ZXDataManager.card_Levels[Level + 1];
            else
                return false;

            return Cards >= nextLevel.Cards && ZXDataManager.Silver >= NextLevelCost;
        }
    }

    public bool MaxLeveled
    {
        get
        {
            return !ZXDataManager.card_Levels.ContainsKey(Level + 1);             
        }
    }

    public int Rank
    {
        get
        {
            if (PlayerPrefs.HasKey(pp_cards_rank_id))
            {
                return PlayerPrefs.GetInt(pp_cards_rank_id);
            }
            else
            {
                return 1;
            }
        }
        set
        {
            cardRank = ZXDataManager.card_Ranks[value];

            PlayerPrefs.SetInt(pp_cards_rank_id, value);

            if (e_CardChanged != null)
                e_CardChanged();
        }
    }

    public string RankText
    {
        get
        {
            return "Rank " + Rank.ToString();
        }
    }

    public int NextRankCost
    {
        get
        {
            Card_Rank nextRank;

            if (ZXDataManager.card_Ranks.ContainsKey(Rank + 1))
                nextRank = ZXDataManager.card_Ranks[Rank + 1];
            else
                return 0;

            return nextRank.Primary_Cost;
        }
    }

    public bool CanRank
    {
        get
        {
            Card_Rank nextRank;

            if (ZXDataManager.card_Ranks.ContainsKey(Rank + 1))
                nextRank = ZXDataManager.card_Ranks[Rank + 1];
            else
                return false;

            return Level == nextRank.Req_Level;
        }
    }

    public bool MaxRanked
    {
        get
        {
            return !ZXDataManager.card_Ranks.ContainsKey(Rank + 1);
        }
    }

    public Color Cards_Progress_Color
    {
        get
        {
            string key;

            if (MaxLeveled)
            {
                key = "CardMaxFill";
            }
            else if (Cards > NextLevelCards)
            {
                key = "GoGreen";
            }
            else
                key = "ProgressBlue";

            return ZXDataManager.GetColor(key);
        }
    }

    public float CardsProgress
    {
        get
        {
            if (ZXDataManager.card_Levels.ContainsKey(Level + 1))
                return Mathf.Clamp01((float)Cards / ZXDataManager.card_Levels[Level + 1].Cards);
            else
                return 1;
        }
    }

    public string CardsProgressText
    {
        get
        {
            if (ZXDataManager.card_Levels.ContainsKey(Level + 1))
                return Cards.ToString() + " / " + ZXDataManager.card_Levels[Level + 1].Cards.ToString();
            else
                return "Max";
        }
    }

    public int Virus
    {
        get
        {
            if (PlayerPrefs.HasKey(pp_cards_virus_id))
            {
                return PlayerPrefs.GetInt(pp_cards_virus_id);
            }
            else
            {
                return 0;
            }
        }
        set
        {
            virus = Mathf.Clamp(value, 0, cardLevel.Virus);

            PlayerPrefs.SetInt(pp_cards_virus_id, virus);

            if (e_CardChanged != null)
                e_CardChanged();
        }
    }

    public Color Virus_Color
    {
        get
        {
            string key = cardType == Card_Type.Hero ? "VirilityFill" : "VirusFill";

            return ZXDataManager.GetColor(key);
        }
    }

    public Color Virus_Icon_Color
    {
        get
        {
            string key = cardType == Card_Type.Hero ? "VirilityIcon" : "VirusIcon";

            return ZXDataManager.GetColor(key);
        }
    }

    public float VirusProgress
    {
        get
        {
            return Mathf.Clamp01((float)Virus / (float)cardLevel.Virus);
        }
    }

    public int MaxVirus
    {
        get
        {
            return cardLevel.Virus;
        }
    }

    public string VirusProgressText
    {
        get
        {
            if (VirusProgress < 1f)
                return Virus.ToString() + " / " + cardLevel.Virus.ToString();
            else
                return "Infected";
        }
    }

    public string Key
    {
        get
        {
            return key;
        }
    }

    public string Name
    {
        get
        {
            return cardName;
        }
    }

    public string Description
    {
        get
        {
            return cardDescription;
        }
    }

    public int Cost
    {
        get
        {
            return cardCost;
        }
    }

    public Card_Type Type
    {
        get
        {
            return cardType;
        }
    }

    /* Events */
    public event System.Action e_CardChanged;

    /* Constructor */
    public ZXCard(XmlNode Node)
    {
        /*
        private Card_Level cardLevel;
        private Card_Rank cardRank;

        private List<Property> cardProperties;
        */
        XmlNodeList collectionNodes;

        this.key = Node.Attributes["key"].Value;
        this.cardName = ZXLocal.LocalText(this.key);
        this.cardDescription = ZXLocal.LocalText(Node.Attributes["descpkey"].Value);
        this.cardType = (Card_Type) System.Enum.Parse(typeof(Card_Type), Node.Attributes["type"].Value);
        this.imageKey = Node.Attributes["image"].Value;
        this.slideKey = Node.Attributes["dialog"].Value;
        this.resourcePath = Path.Combine("Cards", Node.Attributes["resource"].Value);
        this.cardRarity = (Card_Rarity)System.Enum.Parse(typeof(Card_Rarity), Node.Attributes["rarity"].Value);
        this.cardClass = ZXDataManager.card_Classes[Node.Attributes["class"].Value];
        this.cardCost = Node.Attributes["cost"] != null ? int.Parse(Node.Attributes["cost"].Value) : 0;

        pp_cards_level_id = key + "_level_ID";
        pp_cards_rank_id = key + "rank_ID";
        pp_cards_count_id = key + "_count_ID";
        pp_cards_virus_id = key + "_virus_ID";

        cardLevel = ZXDataManager.card_Levels[1];
        cardRank = ZXDataManager.card_Ranks[1];

        //Build properties list
        collectionNodes = Node.SelectNodes("property");

        cardProperties = new List<Property>(collectionNodes.Count);

        foreach (XmlNode node in collectionNodes)
        {
            cardProperties.Add(new Property(node, node.InnerText, this));
        }

        //Build romance list
        collectionNodes = Node.SelectNodes("romance");

        cardRomance = new List<Romance>(collectionNodes.Count);

        foreach (XmlNode node in collectionNodes)
        {
            cardRomance.Add(new Romance(node, this));
        }
    }
}

