﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Xml;
using TMPro;
using System;
using DarkTonic.MasterAudio;

public class ZXCardUI : MonoBehaviour, IPointerDownHandler
{
    //Inspector Variables
    [SerializeField] private Image cardImage;
    [SerializeField] private TextMeshProUGUI costText;
    [SerializeField] private TextMeshProUGUI levelText;
    [SerializeField] private Image classImage;
    [SerializeField] private TextMeshProUGUI rankText;
    [SerializeField] private TextMeshProUGUI cardsText;
    [SerializeField] private Image cardFill;
    [SerializeField] private Image virusFill;
    [SerializeField] private Image virusIcon;

    [SerializeField] private GameObject costObject;
    [SerializeField] private GameObject classObject;
    [SerializeField] private GameObject virusObject;

    public ZXCard Card
    {
        get
        {
            return card;
        }
    }
    public RectTransform AnchorTransform
    {
        get
        {
            return parent;
        }
    }

    //Private variables
    private ZXCard card;
    private RectTransform parent;
    private System.Action d_Refresh;
    public static Dictionary<string, ZXCardUI> cards;

    //Initalizations
    private void Awake()
    {
        d_Refresh = new System.Action(Refresh);
    }

    private void OnEnable()
    {
        if (card != null)
        {
            card.e_CardChanged += d_Refresh;
            Refresh();
        }
    }

    public void Load(ZXCard Card)
    {
        this.card = Card;
        
        //Register with card updates
        card.e_CardChanged += d_Refresh;

        //Set scene view name
        gameObject.name = card.Name;

        if (cards == null)
            cards = new Dictionary<string, ZXCardUI>(30);

        cards.Add(card.Key, this);

        //Update sprite
        cardImage.sprite = card.Card_Image;

        //Set card cost
        if (card.Cost > 0)
            costText.text = card.Cost.ToString();
        else
            costObject.SetActive(false);

        //Disable virus display for spells
        if (card.Type == ZXCard.Card_Type.Spell)
        {
            classObject.SetActive(false);
            virusObject.SetActive(false);
        }
        else
        {
            classImage.sprite = card.Class_Image;

            if (card.Type == ZXCard.Card_Type.Hero)
            {
                virusIcon.sprite = ZXImageServer.FetchSprite("UI_Icon_Virility.png");
                virusIcon.color = Color.white;
            }
            else if (card.Type == ZXCard.Card_Type.Waifu)
            {
                virusIcon.sprite = ZXImageServer.FetchSprite("UI_Icon_Virus.png");
            }
        }

        Refresh();
    }

    private void Refresh()
    {
        //Set level and rank
        levelText.text = card.LevelText;
        rankText.text = card.Rank.ToString();

        //Set virus fills
        virusFill.color = card.Virus_Color;
        virusFill.fillAmount = card.VirusProgress;

        //Set cards fill
        cardFill.color = card.Cards_Progress_Color;
        cardFill.fillAmount = card.CardsProgress;
        cardsText.text = card.CardsProgressText;
    }

    public void UpdateParent(Transform NewParent)
    {
        if (parent == null)
            parent = transform.parent.GetComponentInParent<RectTransform>();

        parent.SetParent(NewParent);
        parent.offsetMin = Vector2.zero;
        parent.offsetMax = Vector2.zero;
        parent.localScale = Vector3.one;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (ZXDeckManager.UseActive && card.Type == ZXDeckManager.UseCardType && ZXDeckManager.UseCardUI != this)
            ZXDeckManager.SwapCard(this);
        else if (!ZXDeckManager.UseActive)
            Select();
    }

    public void Select()
    {
        ZXCardUIPopup.ShowPopup(card, this);

        MasterAudio.PlaySound("UI_Single_Click");
    }

    private void OnDisable()
    {
        if (card != null)
            card.e_CardChanged -= d_Refresh;
    }

    private void OnDestroy()
    {
        cards = null;
    }
}
