﻿using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class ZXTextColor : MonoBehaviour
{
    [SerializeField] private string key;

    private TextMeshProUGUI text;

    private Color color;

    // Use this for initialization
    private void Awake()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    private void OnEnable()
    {
        color = ZXDataManager.GetColor(key);

        if (text != null)
            text.color = color;
    }
}
