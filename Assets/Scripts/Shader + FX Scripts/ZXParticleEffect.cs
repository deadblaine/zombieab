﻿using UnityEngine;
using System.Collections;
using ZXCore;

public class ZXParticleEffect : MonoBehaviour, IZXTracked<ZXParticleEffect>
{
    private ParticleSystem effect;

    //Events
    public event System.Action<ZXParticleEffect> e_OnDisable;
    public event System.Action<ZXParticleEffect> e_OnDestroy;

    // Use this for initialization
    void Awake()
    {
        effect = GetComponent<ParticleSystem>();
	}

    public void Place(Vector3 Location, Quaternion Rotation, object Args)
    {
        if (Args is Transform)
            transform.SetParent(Args as Transform);
        else
            transform.SetParent(null);

		gameObject.SetActive(true);

        transform.position = Location;
        transform.rotation = Rotation;

        effect.Play();

        Invoke("Disable", effect.duration);
    }

    private void Disable()
    {
        transform.SetParent(null);

        gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        effect.Stop();

        CancelInvoke();

        if (e_OnDisable != null)
            e_OnDisable(this);
    }

    private void OnDestroy()
    {
        if (e_OnDestroy != null)
            e_OnDestroy(this);
    }
}
