﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using DG.Tweening;

public class ZXFXable : MonoBehaviour, IZXTargetable
{
    //Global constants
    private const float _highlightTime = 0.5f;
    private Color startDecalColor = new Color(1f, 1f, 1f, 0f);

    //Support types
    public enum Material
    {
        Generic = 0,
        Biological = 1,
        Ground = 2,
        Wood = 3,
        Metal = 4
    }

    //Accessors
    public Vector3 Center
    {
        get
        {
            if (primaryCollider != null)
                return primaryCollider.bounds.center;
            else
                return transform.position;
        }
    }
    public float Size
    {
        get
        {
            if (primaryCollider != null)
                return primaryCollider.bounds.extents.sqrMagnitude;
            else
                return 0f;
        }
    }
    public Vector3 Base
    {
        get
        {
            return transform.position;
        }
    }
    public float Density
    {
        get
        {
            return density;
        }
    }
    public bool BlocksVision
    {
        get
        {
            return blocksVision;
        }
    }
    public Material MaterialType
    {
        get
        {
            return materialType;
        }
    }
    public bool isPhysics
    {
        get
        {
            return rigidbody != null;
        }
    }
    public bool isDestructible
    {
        get
        {
            return startingHealth > 0;
        }
    }
    public bool isAlive
    {
        get
        {
            return startingHealth != 0f && health >= 0;
        }
    }
    public bool isFlammable
    {
        get
        {
            return false;
        }
    }
    public ToonRace Race
    {
        get
        {
            return race;
        }
    }
    public Vector3 CurrentVelocity
    {
        get
        {
            if (rigidbody != null)
                return rigidbody.velocity;
            else
                return Vector3.zero;
        }
    }
    public Transform LookAtTransform
    {
        get
        {
            return transform;
        }
    }

    //Inspector variables
    [SerializeField] protected ToonRace race;
    [SerializeField] private Material materialType;
    [SerializeField] private float health;
    [SerializeField][Range(0f, 0.9f)] private float armour;
    [SerializeField][Range(0, 1)] private float density;
    [SerializeField] private bool particleEffects;
    [SerializeField] private bool blocksVision;
    [SerializeField] private bool fadeDecal;
    [SerializeField] private Color highlightColor;
    [SerializeField] protected GameObject deadPrefab;
    [SerializeField] private Vector3 deadPrefabOffset;
    [SerializeField] private Vector3 deadPrefabRotation;
    [SerializeField] private ParticleSystem dieEffectPrefab;
    [SerializeField] private Vector3 dieEffectOffset;
    [SerializeField] private Vector3 dieEffectRotation;
    [SerializeField] protected float fadeDelay;
    [SerializeField] protected float fadeTime;

    //Events
    public event System.Action<float, float> e_HealthChanged;
    public event System.Action<IZXTargetable> e_Dead;
    public event System.Action<IZXTargetable> e_BreakTargeting;

    //Private variables
    protected ParticleSystem dieEffect;
    protected List<Collider> colliders;
    private new Rigidbody rigidbody;
    protected MeshRenderer[] renderers;
    protected MeshRenderer[] deadRenderers;
    private float startingHealth;
    private float highlightTimer;
    private Collider primaryCollider;

    /* Initalization */
    protected virtual void Awake()
    {
        //Get object collider
        Collider[] tempColliders = gameObject.GetComponents<Collider>();

        if (tempColliders != null)
            colliders = new List<Collider>(tempColliders);   

        //Get rigidbody
        rigidbody = GetComponent<Rigidbody>();

        //Set highlight color
        highlightColor = new Color(0, 0, 0, 0.8f); //Debug

        if (rigidbody != null)
            rigidbody.Sleep();

        startingHealth = health;

        //Instantiate dead prefab
        if (deadPrefab != null)
        {
            deadPrefab = Instantiate(deadPrefab);
            deadPrefab.transform.SetParent(transform);
            deadPrefab.transform.localPosition = deadPrefabOffset;
            deadPrefab.transform.localRotation = Quaternion.Euler(deadPrefabRotation);

            //Make sure the object is not enabled
            deadPrefab.gameObject.SetActive(false);

            //Get renders and set transparent
            deadRenderers = deadPrefab.GetComponentsInChildren<MeshRenderer>();

            if (deadRenderers != null && deadRenderers.Length > 0)
            {
                for (int i = 0; i < deadRenderers.Length; i++)
                {
                    if (deadRenderers[i] != null)
                        deadRenderers[i].material.SetFloat("_Transparency", 0f);
                }
            }
        }

        //Instantiate particle effect
        if (dieEffectPrefab != null)
        {
            dieEffect = Instantiate(dieEffectPrefab);
            dieEffect.transform.SetParent(transform);
            dieEffect.transform.localPosition = dieEffectOffset;
            dieEffect.transform.localRotation = Quaternion.Euler(dieEffectRotation);
        }

        if (colliders != null && colliders.Count > 1)
        {
            //If more than 1 collider find the largest
            Collider lrgCol = colliders[0];

            for (int i = 1; i < colliders.Count; i++)
            {
                if (lrgCol.bounds.size.sqrMagnitude < colliders[i].bounds.size.sqrMagnitude)
                {
                    lrgCol = colliders[i];
                }
            }

            primaryCollider = lrgCol;
        }
        else if (colliders != null)
        {
            primaryCollider = colliders[0];
        }
    }

    protected virtual void Start()
    {
        //Register with reflecting pool
        ZXReflectingPool.Register<IZXDamageable>(this, transform);
    }

    protected virtual void OnEnable()
    {
        //Get renderers
        renderers = GetComponentsInChildren<MeshRenderer>();

        if (fadeDecal && renderers != null)
        {
            ZXCombatUI.e_DecalTo += new System.Action<Color, float, float>(Decal);

            if (renderers != null && renderers.Length > 0)
            {
                for (int i = 0; i < renderers.Length; i++)
                {
                    if (renderers[i] != null)
                        renderers[i].material.SetColor("_DecalColor", startDecalColor);
                }
            }
        }

        health = startingHealth;

        Fade(false);
    }

    /* Run time */
    void Update()
    {
        if (highlightTimer > 0)
            highlightTimer -= Time.deltaTime;
    }

    /* Core functions */
    public float GetDistance(Vector3 Origin)
    {
        return (Vector3.Distance(Origin, primaryCollider.ClosestPointOnBounds(Origin)));
    }

    public Vector3 GetNearestPoint(Vector3 Origin)
    {
        return primaryCollider.ClosestPointOnBounds(Origin);
    }

    public void Invulnerable(bool flag)
    {

    }

    public virtual void TakeDamage(ZXDamage Damage)
    {
        ZXParticleEffect effect;

        if (rigidbody != null)
        {
            rigidbody.isKinematic = false;

            if (Damage.DamageType == ZXDamage.Type.Projectile)
            {

            }
            else if (Damage.DamageType == ZXDamage.Type.Explosive)
            {

            }
        }

        //Ignore everything else if object has starting health of 0
        if (startingHealth == 0f)
            return;

        //Adjust damage to account for amour and penetration
        float adjDamage = Damage.Damage - (Damage.Damage * Mathf.Clamp(armour - Damage.Penetration, 0f, 0.9f));

        //Decrement health
        health -= adjDamage;

        if (e_HealthChanged != null)
            e_HealthChanged(health / startingHealth, adjDamage);

        if (health <= 0f)
            Die();
    }

    protected virtual void OnCollisionEnter(Collision Hit)
    { 
        IZXDamageable hitDamageable = Hit.gameObject.GetComponentInChildren<IZXDamageable>();

        if (hitDamageable != null)
        {
            //ZXReward reward;
            //float damageAmount = Vector3.Distance(Vector3.zero, Hit.relativeVelocity) * rigidBody.mass * density;

            //ZXDamage damage = new ZXDamage(Mathf.RoundToInt(damageAmount), ZXDamage.Type.Impact, new Ray(rigidBody.centerOfMass, rigidBody.velocity.normalized));

            //hitDamageable.TakeDamage(damage, out reward);
        }
    }

    protected virtual void Die()
    {
        if (dieEffect != null)
            dieEffect.Play();

        DeRegister();

        if (e_Dead != null)
            e_Dead(this);

        Fade(true);

        //If there is a dead prefab, fade it in
        if (deadPrefab != null)
        {
            deadPrefab.gameObject.SetActive(true);
			GetComponent<MeshRenderer>().enabled = false;

            for (int i = 0; i < deadRenderers.Length; i++)
            {
                if (deadRenderers[i] != null)
                    deadRenderers[i].material.DOFloat(1f, "_Transparency", fadeTime).SetDelay(fadeDelay);
            }
        }
    }

    protected virtual void Decal(Color endDecalColor, float fadeTime, float fadeDelay)
    {
        if (renderers != null && renderers.Length > 0)
        {
            for (int i = 0; i < renderers.Length; i++)
            {
                if (renderers[i] != null)
                    renderers[i].material.DOColor(endDecalColor, "_DecalColor", fadeTime).SetDelay(fadeDelay);
            }
        }
    }

    private void Fade(bool Visible)
    {
        if (Visible)
        {
            if (renderers != null && renderers.Length > 0)
            {
                for (int i = 0; i < renderers.Length; i++)
                {
                    if (renderers[i] != null && renderers[i].material.HasProperty("_Transparency"))
                        renderers[i].material.DOFloat(0f, "_Transparency", fadeTime).SetDelay(fadeDelay);
                        
                }
            }
        }
        else
        {
            if (renderers != null && renderers.Length > 0)
            {
                for (int i = 0; i < renderers.Length; i++)
                {
                    
                    if (renderers[i] != null && renderers[i].material.HasProperty("_Transparency"))
                        renderers[i].material.DOFloat(1f, "_Transparency", fadeTime);
                        
                }
            }
        }
    }

    protected virtual void MeshSwap()
    {

    }

    //Destruction
    protected virtual void Disable()
    {
        gameObject.SetActive(false);
    }

    protected virtual void DeRegister()
    {
        ZXReflectingPool.DeRegister<IZXDamageable>(transform);
    }

    protected virtual void OnDisable()
    {
        DeRegister();
    }

    protected virtual void OnDestroy()
    {
        DOTween.Kill(this);
        DeRegister();
    }
}
