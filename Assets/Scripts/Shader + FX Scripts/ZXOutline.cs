﻿using UnityEngine;
using UnityEngine.UI;
using System.Xml;

[RequireComponent(typeof(Outline))]
public class ZXOutline : MonoBehaviour
{
    [SerializeField] private string outlineKey;
    [SerializeField] private string colorKey;

    private Outline outline;

    private void Awake()
    {
        outline = GetComponent<Outline>();
    }

    private void OnEnable()
    {
        XmlNode node = ZXDataManager.values[outlineKey];
        Color color = ZXDataManager.colors[colorKey];

        outline.effectDistance = new Vector2(float.Parse(node.Attributes["x"].Value), float.Parse(node.Attributes["y"].Value));
        outline.effectColor = ZXDataManager.GetColor(colorKey);
    }
}
