﻿using UnityEngine;
using System.Collections;

public class ZXWeaponPoint : MonoBehaviour
{
    public enum Part
    {
        Muzzle = 0,
        Grip = 1,
        EjectionPort = 2,
        Barrel = 3
    }

    private new Collider collider;

    [SerializeField] private Part pointType;

    public Part PartType
    {
        get
        {
            return pointType;
        }
    }

    private void Awake()
    {
        collider = GetComponent<Collider>();
        
        if (collider != null)
        {
            collider.enabled = false;
        }
    }

    private void SetPartType(Part PartType)
    {
        pointType = PartType;
    }
}
