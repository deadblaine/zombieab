﻿using UnityEngine;
using System.Collections;
using ZXCore;

[System.Serializable]
public class ZXRifle : ZXFirearm
{
    protected override void Awake()
    {
        base.Awake();

        de_FirePattern = d_LinearFire;
    }

    protected override void DischargeWeapon()
    {
		if (transform.gameObject.name.Contains("Pink")){
			Debug.Log ("Firing Rifle: " + Time.frameCount);
		}
        if (DamageableList != null && DamageableList.Count > 0)
        {
            ZXDamage damage;
            int adjWeaponDamage = weaponDamage;
            float lastDensity = 0;
            Ray trajectory;

            for (int i = 0; i < DamageableList.Count; i++)
            {
                //Look on the objects parent
                if (DamageableList[i] != null)
                {
                    adjWeaponDamage -= Mathf.RoundToInt(weaponDamage * Mathf.Clamp(lastDensity - penetration, 0f, 0.9f));
                    lastDensity = DamageableList[i].Density;

                    if (adjWeaponDamage <= 0)
                        return;

                    trajectory = new Ray(AimTransform.position, (DamageableList[i].Center - AimTransform.position).normalized);

                    damage = new ZXDamage(adjWeaponDamage, penetration, forceMod, 0f, Race, ZXDamage.Type.Projectile, trajectory, weaponEffect);
                    DamageableList[i].TakeDamage(damage);
                }
            }
        }
    }
}
