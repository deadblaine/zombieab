﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;

public class ZXEquipment : MonoBehaviour
{
    public ZXWeaponPoint Grip
    {
        get
        {
            return grip;
        }
    }
    public ZXHolster AltBackHolster
    {
        get
        {
            return altBackHolster;
        }
    }

    private new Collider collider;
    private new Rigidbody rigidbody;

    private ZXWeaponPoint grip;
    private ZXHolster altBackHolster;


	// Use this for initialization
	private void Awake()
    {
        collider = GetComponent<Collider>();
        rigidbody = GetComponent<Rigidbody>();

        if (collider != null)
            collider.enabled = false;
	}

    public void Set()
    {
        grip = GetComponentInChildren<ZXWeaponPoint>();
        altBackHolster = GetComponentInChildren<ZXHolster>();

        if (altBackHolster != null)
            altBackHolster.SetType(transform, ZXHolster.Holster.Back_Alt_Holster);

        grip.transform.parent = null;
        transform.SetParent(grip.transform);
    }

    public void SnapToTransform(ZXHolster Holster)
    {
        grip.transform.SetParent(Holster.transform);
        grip.transform.position = Holster.transform.position;
        grip.transform.localRotation = Quaternion.identity;
    }
}
