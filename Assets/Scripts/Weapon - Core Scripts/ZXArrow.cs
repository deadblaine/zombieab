﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;

public class ZXArrow : ZXProjectile
{
    [SerializeField] private ParticleSystem arrowEffect;

    public override void Place(Vector3 Location, Quaternion Rotation, object Args)
    {
        base.Place(Location, Rotation, Args);

        if (arrowEffect != null)
            arrowEffect.Play();
    }

    protected override void ApplyBallisticDamage(Collider otherCollider)
    {
        base.ApplyBallisticDamage(otherCollider);

		for(int i = 0; i < colliders.Count; i++)
		{
			colliders[i].enabled = false;
		}
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        if (arrowEffect != null)
            arrowEffect.Stop();
    }
}
