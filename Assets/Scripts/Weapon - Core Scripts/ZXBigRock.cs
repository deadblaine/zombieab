﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using DG.Tweening;

public class ZXBigRock : ZXExplosive
{
    protected override void Start()
    {
        base.Start();

        triggerCondition = new System.Predicate<Collider>(delegate (Collider other)
        {
            IZXTargetable target = other.GetComponentInParent<IZXTargetable>();

            if (target != null && target.Race == ToonRace.Zombie)
                return false;

            return true;
        });

        gameObject.layer = 21;
    }
}
