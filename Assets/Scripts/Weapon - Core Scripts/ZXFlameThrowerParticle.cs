﻿using UnityEngine;
using System.Collections;
using ZXCore;
public class ZXFlameThrowerParticle : MonoBehaviour
{ 
    public event System.Action<IZXDamageable> e_DamagableCollision;

    private void Start()
    {
        //Set layer to fire
        gameObject.layer = 20;
    }

    private void OnParticleCollision(GameObject Other)
    {
        IZXDamageable damageable = Other.GetComponentInParent<IZXDamageable>();

        if (damageable != null && e_DamagableCollision != null)
            e_DamagableCollision(damageable);
    }
}
