﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;

public class ZXAccessory : MonoBehaviour
{
    //Constants
    private const float _maxDepen = 2.5f;
    private const float _defaultWeight = 5f;
    private const float _drag = 0.25f;
    private const float _fadeDelay = 4f;
    private const float _fadeDuration = 3f;

    private const float _maxPhysicsForce = 3000f;

    //Inspector variables
    [SerializeField] private bool holdOnRagdoll;

    //Private variables
    private ZXToon toon;
    private ZXShaderController r_Shader;

    private new Rigidbody rigidbody;
    private new Collider collider;
    private new Transform transform;

    //Original data values
    private Transform originalParent;
    private Vector3 originalLocalPosition;
    private Quaternion originalLocalRotation;

    private bool attached = true;

    //Initalization
    private void Awake()
    {
        //Get parent and transform
        toon = GetComponentInParent<ZXToon>();

        //Find rigidbody
        rigidbody = GetComponent<Rigidbody>();

        //Get transform
        transform = GetComponent<Transform>();
        SetParent(transform.parent);

        //Create rigidbody if not found
        if (rigidbody == null)
        {
            rigidbody = gameObject.AddComponent<Rigidbody>();
            rigidbody.mass = _defaultWeight;
        }

        //Set base physics values
        ConfigurePhysics();

        //Find collider if already attached
        collider = GetComponent<Collider>();
        
        //Create a collider if not found
        if (collider == null)
            collider = gameObject.AddComponent<BoxCollider>();

        //Set starting physics
        SetPhysics(false);

        r_Shader = GetComponent<ZXShaderController>();

        if (r_Shader == null)
        {
            r_Shader = gameObject.AddComponent<ZXShaderController>();
        }

        //Register with toon
        toon.e_Ragdoll += new System.Action<Vector3>(RagdollListener);
        toon.e_OnDisable += new System.Action<ZXToon>(DisableListener);
    }

    public void SetParent(Transform Parent)
    {
        //Get starting locations
        if (Parent != null)
        {
            originalParent = transform.parent;

            transform.SetParent(Parent);

            originalLocalPosition = transform.localPosition;
            originalLocalRotation = transform.localRotation;
        }
    }

    private void ConfigurePhysics()
    {
        rigidbody.maxDepenetrationVelocity = _maxDepen;
        rigidbody.drag = _drag;
    }

    //Physics
    private void SetPhysics(bool State)
    {
        if (State)
        {
            if (collider != null)
                collider.isTrigger = false;

            rigidbody.isKinematic = false;
            rigidbody.collisionDetectionMode = CollisionDetectionMode.Continuous;
            rigidbody.useGravity = true;
        }
        else
        {
            if (collider != null)
                collider.isTrigger = true;

            rigidbody.isKinematic = true;
            rigidbody.collisionDetectionMode = CollisionDetectionMode.Discrete;
            rigidbody.useGravity = false;
        }
    }

    //Listeners
    private void RagdollListener(Vector3 Force)
    {
        Vector3 adjForce = Force;

        if (adjForce.magnitude > _maxPhysicsForce)
            adjForce = adjForce.normalized * _maxPhysicsForce;

        if (attached && !holdOnRagdoll)
        {
            //Detach
            transform.SetParent(null);
            attached = false;

            //Turn on physics
            SetPhysics(true);

            //Apply force
            rigidbody.AddForce(Force * rigidbody.mass * 0.5f /*Only apply half the relative force */);

            //Override parent control of the shader
            r_Shader.UseParentController(false);

            //Start fade routine
            r_Shader.Fade(_fadeDelay, _fadeDuration);
        }
    }

    public void Drop(Vector3 Force)
    {
        RagdollListener(Force);
    }

    private void DisableListener(ZXToon Toon)
    {
        SetPhysics(false);

        //Reset position
        transform.SetParent(originalParent);
        transform.localPosition = originalLocalPosition;
        transform.localRotation = originalLocalRotation;

        attached = true;

        //Enable use of parent controller
        r_Shader.UseParentController(true);
    }
}
