﻿using UnityEngine;
using System.Collections;
using ZXCore;
using DarkTonic.MasterAudio;

public class ZXMinigunBarrel : MonoBehaviour
{
    [SoundGroupAttribute]
    [SerializeField] protected string s_Belt;
    [SerializeField] private float beltPitchMax;

    //Constant variables
    private const float _TimingBelt = 2.5f;
    private const float barrelAcceleration = 1.5f;
    private const float maxAngularVelocity = 129f;

    //Private variables
    private ZXMinigun weapon;
    private PlaySoundResult soundResult;
    private float currentAngularVelocity = 0;
    private float beltTimer = 0f;

    private void Awake()
    {
        weapon = GetComponentInParent<ZXMinigun>();
        weapon.e_Fire += new System.Action(Spin);
    }

    private void OnEnable()
    {
        soundResult = MasterAudio.PlaySound3DAtTransform(s_Belt, transform);
    }

    private void Update()
    {
        beltTimer -= Time.deltaTime;

        if (beltTimer <= 0)
            currentAngularVelocity = Mathf.Clamp(currentAngularVelocity - barrelAcceleration, 0f, maxAngularVelocity);
        else
            currentAngularVelocity = Mathf.Clamp(currentAngularVelocity + barrelAcceleration, 0f, maxAngularVelocity);

        if (currentAngularVelocity > 0)
            transform.Rotate(Vector3.forward, currentAngularVelocity);

        //MasterAudio.ChangePlaylistPitch(s_Belt, Mathf.Clamp(currentAngularVelocity / maxAngularVelocity * beltPitchMax, 1f, beltPitchMax));  
    }

    public void Spin()
    {
        beltTimer = _TimingBelt;
    }

    private void OnDisable()
    {
        MasterAudio.FadeOutSoundGroupOfTransform(transform, s_Belt, 0.25f);
    }
}
