﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;

public class ZXLauncher : ZXFirearm
{
    public enum Targeting
    {
        Root = 0,
        Center = 1
    }

    [SerializeField] private bool predictiveAiming;
    [SerializeField] private float projectileSpread;
    [SerializeField] private Targeting targetingType;
    //Missile pool
    [SerializeField] private ZXProjectile p_ProjectilePrefab;
    private ZXPoolLite<ZXProjectile> p_Projectiles;
    private int p_Size = 5; //Should calculate dynamically
    [SerializeField][Range(10f, 100f)] protected float projectileVelocity;
    private int explosiveDamage;

    //Accessors
    public float MissileVelocity
    {
        get
        {
            return projectileVelocity;
        }
    }

    //Private variables
    private ZXProjectile loadedProjectile;

    protected override void Awake()
    {
        base.Awake();

        //Load arrow pool
        p_Projectiles = new ZXPoolLite<ZXProjectile>(transform, p_ProjectilePrefab, p_Size);

        de_FirePattern = d_LinearFire;
    }

    protected override void Start()
    {
        base.Start();

        LoadMissile();
    }

    public override void SetWeaponDamage(int Damage)
    {
        explosiveDamage = Damage;
    }

    protected override void DischargeWeapon()
    {
        Vector3 attackPoint;
        Vector3 randomization = new Vector3(Random.Range(-projectileSpread, projectileSpread), 0f, Random.Range(-projectileSpread, projectileSpread));
        Vector3 defelection = Vector3.zero;
        float timeOnTarget;

        //Return if there is not a loaded projectile
        if (loadedProjectile == null)
            LoadMissile();

        if (loadedProjectile == null)
            return;

        if (parent.CurrentTarget != null)
        {
            //Pick attack location
            switch (targetingType)
            {
                case Targeting.Center:
                    attackPoint = parent.CurrentTarget.Center + randomization;
                    break;

                default:
                    attackPoint = parent.CurrentTarget.Base + randomization;
                    break;
            }

            timeOnTarget = Vector3.Distance(t_Muzzle.position, attackPoint) / projectileVelocity;

            //Calculate lead
            if (predictiveAiming)
                attackPoint = ZXProjectile.CalculateLeadPosition(attackPoint, parent.CurrentTarget.CurrentVelocity, timeOnTarget);

            //Arm explosive
            if (loadedProjectile is ZXExplosive)
                (loadedProjectile as ZXExplosive).ArmDelayed(0.1f); //Should not be hard coded

            if (loadedProjectile is ZXMissile)
            {
                (loadedProjectile as ZXMissile).Launch(new Vector3(0f, 0f, projectileVelocity), attackPoint, parent.CurrentTarget);
            }
            else if (loadedProjectile is ZXExplosive)
            {
                loadedProjectile.Launch(attackPoint, timeOnTarget);
            }
            else
            {
                loadedProjectile.Launch(attackPoint, timeOnTarget);
            }

            if (WeaponAmmo > 0)
                LoadMissile();
            else
                loadedProjectile = null;
        }
    }

    public override void EndReload()
    {
        base.EndReload();

        LoadMissile();
    }

    private void LoadMissile()
    {
        //Load first arrow
        loadedProjectile = p_Projectiles.Request();

        if (loadedProjectile != null)
        {
            loadedProjectile.Place(t_Muzzle.position, t_Muzzle.rotation, t_Muzzle.transform);
            loadedProjectile.SetDamage(explosiveDamage);
        }
        else
            Debug.Log(gameObject.name + ": Failed to load missile");
    }
}
