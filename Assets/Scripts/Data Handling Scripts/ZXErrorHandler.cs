﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;

public class ZXErrorHandler : MonoBehaviour
{
    //Types
    public enum ConnectionState
    {
        Connected = 0,
        Waiting = 1,
        Disconnected = 2
    }
    public class CloudScriptRequest
    {
        public ExecuteCloudScriptRequest Request;
        public System.Action<ExecuteCloudScriptResult> ResultCallback;
        public bool Fatal;

        public CloudScriptRequest(ExecuteCloudScriptRequest Request, System.Action<ExecuteCloudScriptResult> ResultCallback, bool Fatal = false)
        {
            this.Request = Request;
            this.ResultCallback = ResultCallback;
            this.Fatal = Fatal;
        }
    }

    //Editor fields
    [SerializeField] private bool debugLogging;

    //Accessors
    public static System.Action<PlayFabError> WebLoadingErrorHandler
    {
        get
        {
            return d_webLoadingErrorHandler;
        }
    }
    public static System.Action<PlayFabError> WebRequestErrorHandler
    {
        get
        {
            return d_webRequestErrorHandler;
        }
    }

    public static bool Logging
    {
        get
        {
            if (singleton)
                return singleton.debugLogging;
            else
                return false;
        }
    }

    public static ConnectionState currentConnectionState
    {
        get
        {
            return connectionState;
        }
    }

    //Events
    public static System.Action e_WebloadFailed;

    //Singleton
    private static ZXErrorHandler singleton;
    private static ConnectionState connectionState = ConnectionState.Connected;

    //Handlers
    private static System.Action<PlayFabError> d_webLoadingErrorHandler;
    private static System.Action<PlayFabError> d_webRequestErrorHandler;
    private static System.Action<ExecuteCloudScriptResult> d_webRequestCompleteHandler;

    //Current Request Variables
    private static Queue<CloudScriptRequest> requestQueue;
    private static CloudScriptRequest currentRequest = null;

    /* Initalization */
    private void Awake()
    {
        singleton = this;

        DontDestroyOnLoad(this);

        d_webLoadingErrorHandler = new System.Action<PlayFabError>(WebLoadingFailed);
        d_webRequestErrorHandler = new System.Action<PlayFabError>(WebRequestFailed);
        d_webRequestCompleteHandler = new System.Action<ExecuteCloudScriptResult>(WebRequestComplete);

        requestQueue = new Queue<ZXErrorHandler.CloudScriptRequest>(5);
    }

    /* Update */
    private void Update()
    {
        if (currentRequest == null && requestQueue != null && requestQueue.Count > 0)
        {
            currentRequest = requestQueue.Dequeue();

            PlayFabClientAPI.ExecuteCloudScript(currentRequest.Request, currentRequest.ResultCallback, d_webRequestErrorHandler);
        }
    }

    /* Reporting Functions */
    #region
    public static void ReportMessage(GameObject sender, string Message)
    {
        if (Logging)
        {
            Debug.Log(sender.name + ": " + Message);
        }
    }

    public static void ReportError(GameObject sender, string Message)
    {
        if (Logging)
        {
            Debug.LogError(sender.name + ": " + Message);
        }
    }

    public static void ReportWebError(GameObject sender, string Message)
    {
        if (Logging)
        {
            Debug.LogError(sender.name + ": " + Message);
        }
    }

    public static void ReportWarning(GameObject sender, string Message)
    {
        if (Logging)
        {
            Debug.LogWarning(sender.name + ": " + Message);
        }
    }
    #endregion

    /* Web Request Functions */
    public static void ExecuteCloudScript(ExecuteCloudScriptRequest Request, System.Action<ExecuteCloudScriptResult> ResultCallback, bool Fatal = false)
    {
        requestQueue.Enqueue(new ZXErrorHandler.CloudScriptRequest(Request, ResultCallback, Fatal));
    }

    private static void WebRequestComplete(ExecuteCloudScriptResult result)
    {
        currentRequest.ResultCallback(result);

        currentRequest = null;
    }

    private static void WebRequestFailed(PlayFabError Error)
    {
        ReportWebError(singleton.gameObject, Error.GenerateErrorReport());

        PlayFabClientAPI.ExecuteCloudScript(currentRequest.Request, currentRequest.ResultCallback, d_webRequestErrorHandler);
    }

    /// <summary>
    /// Playfab loading error handler
    /// </summary>
    /// <param name="Error"></param>
    private static void WebLoadingFailed(PlayFabError Error)
    {
        ReportWebError(singleton.gameObject, Error.GenerateErrorReport());

        ZXLoadingTextDisplay.ShowText(Error.GenerateErrorReport());

        //Attempt to load the resource again
        if (e_WebloadFailed != null)
            e_WebloadFailed();

        AbortLoading();
    }

    private static void AbortLoading()
    {
        ZXConnectionPopup.ShowPopup();
    }
}
