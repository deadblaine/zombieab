﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Json;
using System.IO;
using System.Xml;

/// <summary>
/// This is the global class to access all game data
/// </summary>
public class ZXDataManager : MonoBehaviour, ZXCore.IZXLoadable
{
    [SerializeField] private TextAsset gamedata;

    //Constants
    private const string gameDataVerKey = "GameData_Version";

    //Types
    public enum Scenes
    {
        Splash = 0,
        Loading = 1,
        Main = 2,
        Hospital = 3,
    }

    public enum Currency
    {
        Silver = 0,
        Gems = 1,
        Fuel = 2
    }

    //Editor variables
    [SerializeField] private bool enableDeveloperOptions;

    //Title Data Variables
    private static Dictionary<string, string> titleData;

    /*Title Data Accessors*/
    #region
    public static event System.Action e_TitleData_Updated;

    public static float Data_Game_Version
    {
        get
        {
            return float.Parse(titleData["Current_Game_Version"]);
        }
    }

    public static float Data_Language_Version
    {
        get
        {
            return float.Parse(titleData["Current_Language_Version"]);
        }
    }

    public static float Data_Event_Version
    {
        get
        {
            return float.Parse(titleData["Current_Event_Version"]);
        }
    }
    #endregion

    /*Combat Data*/
    public static float Combat_Mana_Recharge
    {
        get
        {
            return float.Parse(titleData["Combat_Engine_Mana_Recharge_Rate"]);
        }
    }

    /*Spawn Data Accessors*/
    #region
    public static int SpawnRate_Ambient
    {
        get
        {
            return int.Parse(titleData["Combat_Spawn_Rate_Ambient"]);
        }
    }
    public static int SpawnRate_Challenge
    {
        get
        {
            return int.Parse(titleData["Combat_Spawn_Rate_Challenge"]);
        }
    }
    public static int SpawnRate_Event
    {
        get
        {
            return int.Parse(titleData["Combat_Spawn_Rate_Event"]);
        }
    }
    public static int SpawnPool_Zombies
    {
        get
        {
            return int.Parse(titleData["Combat_Spawn_Pool_Basic_Zombie"]);
        }
    }
    public static int SpawnPool_Fast_Zombies
    {
        get
        {
            return int.Parse(titleData["Combat_Spawn_Pool_Fast_Zombie"]);
        }
    }
    public static int SpawnPool_Armored_Zombies
    {
        get
        {
            return int.Parse(titleData["Combat_Spawn_Pool_Armored_Zombie"]);
        }
    }
    public static int SpawnPool_Spitter_Zombies
    {
        get
        {
            return int.Parse(titleData["Combat_Spawn_Pool_Spitter_Zombie"]);
        }
    }
    public static int SpawnPool_Exploder_Zombies
    {
        get
        {
            return int.Parse(titleData["Combat_Spawn_Pool_Exploder_Zombie"]);
        }
    }
    public static int SpawnPool_Tank_Zombies
    {
        get
        {
            return int.Parse(titleData["Combat_Spawn_Pool_Tank_Zombie"]);
        }
    }
    #endregion

    /*Player Data Accessors*/

    /*Currency Accessors*/
    #region
    public static event System.Action e_Currency_Updated;

    private static string pp_Gems_id = "PP_Gems_Amount_ID";

    public static int Gems
    {
        get
        {
            if (PlayerPrefs.HasKey(pp_Gems_id))
            {
                return PlayerPrefs.GetInt(pp_Gems_id);
            }
            else
                return 500;
        }
        set
        {
            PlayerPrefs.SetInt(pp_Gems_id, value);

            if (e_Currency_Updated != null)
                e_Currency_Updated();
        }
    }

    private static string pp_Silver_id = "PP_Silver_Amount_ID";

    public static int Silver
    {
        get
        {
            if (PlayerPrefs.HasKey(pp_Silver_id))
            {
                return PlayerPrefs.GetInt(pp_Silver_id);
            }
            else
                return 1000;
        }
        set
        {
            PlayerPrefs.SetInt(pp_Silver_id, value);

            if (e_Currency_Updated != null)
                e_Currency_Updated();
        }
    }

    private static string pp_Kits_id = "PP_Kits_Amount_ID";

    public static int Kits
    {
        get
        {
            if (PlayerPrefs.HasKey(pp_Kits_id))
            {
                return PlayerPrefs.GetInt(pp_Kits_id);
            }
            else
                return 0;
        }
        set
        {
            PlayerPrefs.SetInt(pp_Kits_id, value);

            if (e_Currency_Updated != null)
                e_Currency_Updated();
        }
    }

    private static string pp_Fuel_id = "PP_Fuel_Amount_ID";
    public static int maxFuel = 24;

    public static int Fuel
    {
        get
        {
            if (PlayerPrefs.HasKey(pp_Fuel_id))
            {
                return PlayerPrefs.GetInt(pp_Fuel_id);
            }
            else
                return 24;
        }
        set
        {
            PlayerPrefs.SetInt(pp_Fuel_id, value);

            if (e_Currency_Updated != null)
                e_Currency_Updated();
        }
    }

    public static float FuelPercent
    {
        get
        {
            return Mathf.Clamp01((float)Fuel / (float)maxFuel);
        }
    }

    public static string FuelAmountString
    {
        get
        {
            return Fuel.ToString() + " / " + maxFuel.ToString();
        }
    }

    public static int FuelSecondsRemaining
    {
        get
        {
            return currencyRecharge["FL"].SecondsToRecharge;
        }
    }
    #endregion

    /*Player Experience Accessors*/
    #region
    private static int experience = 0;
    private const int maxExperience = 5000;
    private static string pp_experience_id = "GlobalPP_Experience_UID";
    public static event System.Action<int> e_ExperienceChanged;

    public static int Experience
    {
        get
        {
            if (PlayerPrefs.HasKey(pp_experience_id))
            {
                return PlayerPrefs.GetInt(pp_experience_id);
            }
            else
            {
                return 0;
            }
        }
        set
        {
            experience = Mathf.Clamp(value, 0, maxExperience);

            PlayerPrefs.SetInt(pp_experience_id, experience);

            if (e_ExperienceChanged != null)
                e_ExperienceChanged(experience);

            if (e_LevelChanged != null)
                e_LevelChanged(Level);

            if (e_LevelProgressChanged != null)
                e_LevelProgressChanged(LevelProgress);

        }
    }

    private static readonly int[] levelData = { 0, 250, 1000, 2500, 5000, 10000, 30000, 50000 }; /*Always need one more than max level */
    public static event System.Action<int> e_LevelChanged;

    public static int Level
    {
        get
        {
            int lvl = 0;

            for (int i = 0; i < levelData.Length; i++)
            {
                lvl = i;

                if (Experience < levelData[i])
                    break;
            }

            return Mathf.Clamp(lvl, 1 /*Min*/, levelData.Length);
        }
    }

    public static event System.Action<float> e_LevelProgressChanged;

    public static float LevelProgress
    {
        get
        {
            float progress = 0f;

            for (int i = 0; i < levelData.Length; i++)
            {
                if (levelData[i] > Experience)
                {
                    progress = (float)Experience / (float)levelData[i];
                    break;
                }
            }

            return Mathf.Clamp01(progress);
        }
    }
    #endregion

    //IZXLoadable Implimentation
    public event System.Action e_LoadingComplete;

    public static ZXCore.IZXLoadable Loadable
    {
        get
        {
            return singleton;
        }
    }
    public List<System.Action> LoadingList
    {
        get
        {
            return new List<System.Action>(new System.Action[] { LoadGameData, LoadTitleData, LoadTitleNews, LoadUserData, LoadUserInventory });
        }
    }

    //Playfab Variables
    private static List<TitleNewsItem> titleNews;
    private static Dictionary<string, UserDataRecord> userData;

    //Playfab Inventory Variables
    private static List<ItemInstance> items;
    private static Dictionary<string, int> currency;
    private static Dictionary<string, VirtualCurrencyRechargeTime> currencyRecharge;

    //GameData Variables
    private static XmlDocument gameDataXML;
    private static string GameDataVersion
    {
        get
        {
            return PlayerPrefs.GetString(gameDataVerKey);
        }
        set
        {
            PlayerPrefs.SetString(gameDataVerKey, value);
        }
    }

    //GameData
    public static Dictionary<string, XmlNode> values;
    public static Dictionary<string, Color> colors;
    public static Dictionary<string, ZXCard> cards;
    private static List<ZXCard> cardsList;
    public static Dictionary<int, ZXCard.Card_Level> card_Levels;
    public static Dictionary<int, ZXCard.Card_Rank> card_Ranks;
    public static Dictionary<string, ZXCard.Card_Class> card_Classes;
    public static Dictionary<string, XmlNode> card_Properties;

    //Static Varibles
    private static ZXDataManager singleton;

    /* Initalization */
    private void Awake()
    {
        singleton = this;

        DontDestroyOnLoad(this);

        //PlayerPrefs.DeleteAll();
    }

    /* Get Data Functions */
    public static Color GetColor(string Key)
    {
        if (colors.ContainsKey(Key))
        {
            return colors[Key];
        }
        else
            return Color.black;
    }

    /* Data Update Functions */
    #region
    private static void GetReward()
    {
        ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest();

        request.FunctionName = "GiveRewards";
        //request.FunctionParameter = "SomeObject";

        ZXErrorHandler.ExecuteCloudScript(request, Reward);
    }

    private static void Reward(ExecuteCloudScriptResult Result)
    {
        JsonObject result = (JsonObject)Result.FunctionResult;

        object messageValue;

        //JsonWrapper.DeserializeObject<()

        result.TryGetValue("messageValue", out messageValue);
    }
    #endregion

    /*Inital Loading Functions*/
    #region
    /* Title Data Loading */
    private void LoadGameData()
    {
        StartCoroutine(singleton.LoadGameDataXML());
    }

    private IEnumerator LoadGameDataXML()
    {
        gameDataXML = new XmlDocument();
        XmlNode sectionNode;
        XmlNodeList collectionNodes;

        /*
        string path;

        //File path specific to device
        #if UNITY_ANDROID
        path = "jar:file://" + Application.dataPath + "!/assets/GameData.xml";
        #endif

        #if UNITY_WEBGL
        path = Path.Combine("https://s3.us-east-2.amazonaws.com/blitzproject.com/StreamingAssets", "GameData.xml");
        #endif

        #if UNITY_EDITOR || UNITY_IOS
        path = Path.Combine(Application.streamingAssetsPath, "GameData.xml");
        #endif

        WWW gameDataFile = new WWW(path);

        //Wait until the file is done reading
        yield return new WaitUntil(delegate { return gameDataFile.isDone; });
        */

        yield return new WaitForSeconds(0.01f);

        gameDataXML.LoadXml(gamedata.text);

        /*Parse XML Data*/

        //Get version of the GameData file
        GameDataVersion = gameDataXML.DocumentElement.Attributes["version"].Value;

        /*Engine Data*/
        sectionNode = gameDataXML.DocumentElement.SelectSingleNode("engine");

        //Colors
        collectionNodes = sectionNode.SelectSingleNode("values").SelectNodes("value");

        values = new Dictionary<string, XmlNode>(collectionNodes.Count);

        foreach (XmlNode node in collectionNodes)
        {
            values.Add(node.Attributes["key"].Value, node);
        }

        //Colors
        collectionNodes = sectionNode.SelectSingleNode("colors").SelectNodes("color");

        colors = new Dictionary<string, Color>(collectionNodes.Count);

        foreach (XmlNode node in collectionNodes)
        {
            Color newColor;

            if (ColorUtility.TryParseHtmlString(node.InnerText, out newColor))
            {
                colors.Add(node.Attributes["key"].Value, newColor);
            }
        }

        /*Card Data*/
        sectionNode = gameDataXML.DocumentElement.SelectSingleNode("cards");

        //Levels
        collectionNodes = sectionNode.SelectSingleNode("card_levels").SelectNodes("level");

        card_Levels = new Dictionary<int, ZXCard.Card_Level>(collectionNodes.Count);

        foreach (XmlNode node in collectionNodes)
        {
            card_Levels.Add(int.Parse(node.InnerText), new ZXCard.Card_Level(node));
        }

        //Ranks
        collectionNodes = sectionNode.SelectSingleNode("card_ranks").SelectNodes("rank");

        card_Ranks = new Dictionary<int, ZXCard.Card_Rank>(collectionNodes.Count);

        foreach (XmlNode node in collectionNodes)
        {
            card_Ranks.Add(int.Parse(node.InnerText), new ZXCard.Card_Rank(node));
        }

        //Classes
        collectionNodes = sectionNode.SelectSingleNode("card_classes").SelectNodes("class");

        card_Classes = new Dictionary<string, ZXCard.Card_Class>(collectionNodes.Count);

        foreach (XmlNode node in collectionNodes)
        {
            card_Classes.Add(node.Attributes["key"].Value, new ZXCard.Card_Class(node));
        }

        //Properties
        collectionNodes = sectionNode.SelectSingleNode("card_properties").SelectNodes("property");

        card_Properties = new Dictionary<string, XmlNode>(collectionNodes.Count);

        foreach (XmlNode node in collectionNodes)
        {
            card_Properties.Add(node.Attributes["key"].Value, node);
        }

        //Cards
        collectionNodes = sectionNode.SelectSingleNode("card_data").SelectNodes("card");

        cards = new Dictionary<string, ZXCard>(collectionNodes.Count);

        foreach (XmlNode node in collectionNodes)
        {
            cards.Add(node.Attributes["key"].Value, new ZXCard(node));
        }

        cardsList = new List<ZXCard>(cards.Values);

        if (e_LoadingComplete != null)
            e_LoadingComplete();
    }

    public void LoadTitleData()
    {
        PlayFabClientAPI.GetTitleData(null, OnTitleDataLoaded, ZXErrorHandler.WebLoadingErrorHandler);
    }

    private void OnTitleDataLoaded(GetTitleDataResult Result)
    {
        titleData = Result.Data;

        if (e_LoadingComplete != null)
            e_LoadingComplete();
    }

    /* Title News Loading */
    public void LoadTitleNews()
    {
        PlayFabClientAPI.GetTitleNews(null, OnTitleNewsLoaded, ZXErrorHandler.WebLoadingErrorHandler);
    }

    private void OnTitleNewsLoaded(GetTitleNewsResult Result)
    {
        titleNews = Result.News;

        if (e_LoadingComplete != null)
            e_LoadingComplete();
    }

    /* User Data Loading */
    private void LoadUserData()
    {
        PlayFabClientAPI.GetUserData(null, OnUserDataLoaded, ZXErrorHandler.WebLoadingErrorHandler);
    }

    private void OnUserDataLoaded(GetUserDataResult Result)
    {
        userData = Result.Data;

        if (e_LoadingComplete != null)
            e_LoadingComplete();
    }

    /* User Inventory Loading */
    private void LoadUserInventory()
    {
        PlayFabClientAPI.GetUserInventory(null, OnUserInventoryLoaded, ZXErrorHandler.WebLoadingErrorHandler);
    }

    private void OnUserInventoryLoaded(GetUserInventoryResult Result)
    {
        items = Result.Inventory;
        currency = Result.VirtualCurrency;
        currencyRecharge = Result.VirtualCurrencyRechargeTimes;

        if (e_LoadingComplete != null)
            e_LoadingComplete();
    }

    /* Data Functions */
    public static List<ZXCard> CardsByType(ZXCard.Card_Type Type)
    {
        return cardsList.FindAll(delegate (ZXCard Card) { return Card.Type == Type; });
    }

    public static ZXCard GetHero()
    {
        return cardsList.Find(delegate (ZXCard Card) { return Card.Type == ZXCard.Card_Type.Hero; });
    }
#endregion
}
