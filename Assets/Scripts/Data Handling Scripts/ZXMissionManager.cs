﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class ZXMissionManager : MonoBehaviour
{
    private const string pp_Valid = "pp_Mission_Valid";
    private const string pp_Mission = "pp_Mission_Type";
    private const string pp_Difficulty = "pp_Mission_Difficulty";
    private const string pp_Victory = "pp_Mission_Victory";
    private const string pp_Stars = "pp_Mission_Stars";
    private const string pp_ID = "pp_Mission_ID";
    private const string pp_Mission_Count = "pp_Total_Mission_Count";

    private const string pp_Campaign_Clock = "pp_Clock_Campaign";
    private const int campLength = 7;
    private const string pp_Kit_Clock = "pp_Clock_Kit";
    private const int kitLength = 1;
    private const string pp_Gift_Clock = "pp_Clock_Gift";
    private const int giftLength = 1;

    public enum Type
    {
        Battle = 0,
        BasicKit = 1,
        ClassKit = 2,
        Campaign = 3,
    }

    public enum Difficulty
    {
        Easy = 0,
        Medium = 1,
        Hard = 2
    }

    //Accessors
    public static Type CurrentMissionType
    {
        get
        {
            return (Type)System.Enum.Parse(typeof(Type), PlayerPrefs.GetString(pp_Mission));
        }
        set
        {            
            PlayerPrefs.SetString(pp_Mission, value.ToString());
        }
    }

    public static Difficulty CurrentMissionDifficulty
    {
        get
        {
            return (Difficulty)System.Enum.Parse(typeof(Difficulty), PlayerPrefs.GetString(pp_Difficulty));
        }
        set
        {
            PlayerPrefs.SetString(pp_Difficulty, value.ToString());
        }
    }

    public static bool CurrentMissionVictory
    {
        get
        {
            return bool.Parse(PlayerPrefs.GetString(pp_Victory));
        }
        set
        {
            PlayerPrefs.SetString(pp_Victory, value.ToString());
        }
    }

    public static int CurrentMissionStars
    {
        get
        {
            return PlayerPrefs.GetInt(pp_Stars);
        }
        set
        {
            PlayerPrefs.SetInt(pp_Stars, value);
        }
    }

    public static string CurrentMissionID
    {
        get
        {
            return PlayerPrefs.GetString(pp_ID);
        }
        set
        {
            PlayerPrefs.SetString(pp_ID, value);
        }
    }

    public static bool CurrentMissionTokenValid
    {
        get
        {
            bool result = false;

            if (PlayerPrefs.HasKey(pp_Valid))
            {
                result = bool.Parse(PlayerPrefs.GetString(pp_Valid));

                PlayerPrefs.DeleteKey(pp_Valid);
            }

            return result;
        }
        set
        {
            PlayerPrefs.SetString(pp_Valid, value.ToString());
        }
    }

    public static int TotalMissionCount
    {
        get
        {
            if (PlayerPrefs.HasKey(pp_Mission_Count))
                return PlayerPrefs.GetInt(pp_Mission_Count);
            else
                return 0;
        }
        set
        {
            PlayerPrefs.SetInt(pp_Mission_Count, value);
        }
    }

    //Time Accessors
    private static DateTime CampaignEndDate
    {
        get
        {
            if (PlayerPrefs.HasKey(pp_Campaign_Clock))
                return DateTime.FromBinary(Convert.ToInt64(PlayerPrefs.GetString(pp_Campaign_Clock)));
            else
                return DateTime.Now;
        }
        set
        {
            PlayerPrefs.SetString(pp_Campaign_Clock, value.ToBinary().ToString());
        }
    }

    private static DateTime KitEndDate
    {
        get
        {
            if (PlayerPrefs.HasKey(pp_Kit_Clock))
                return DateTime.FromBinary(Convert.ToInt64(PlayerPrefs.GetString(pp_Kit_Clock)));
            else
                return DateTime.Now;
        }
        set
        {
            PlayerPrefs.SetString(pp_Kit_Clock, value.ToBinary().ToString());
        }
    }

    private static DateTime GiftEndDate
    {
        get
        {
            if (PlayerPrefs.HasKey(pp_Gift_Clock))
                return DateTime.FromBinary(Convert.ToInt64(PlayerPrefs.GetString(pp_Gift_Clock)));
            else
                return DateTime.Now;
        }
        set
        {
            PlayerPrefs.SetString(pp_Gift_Clock, value.ToBinary().ToString());
        }
    }

    //Events
    public static event System.Action<System.TimeSpan> e_Campaign_Clock;
    public static event System.Action<System.TimeSpan> e_Kit_Clock;
    public static event System.Action<System.TimeSpan> e_Gift_Clock;

    //Private variables
    private ZXMissionManager singleton;
    private UnityAction<Scene, LoadSceneMode> d_SceneChange;
    private System.Action d_Clocks = null;

    //Initialization
    private void Awake()
    {
        singleton = this;

        d_SceneChange = new UnityAction<Scene, LoadSceneMode>(MissionResults);

        SceneManager.sceneLoaded += d_SceneChange;
	}

    private void Update()
    {
        if (d_Clocks != null)
            d_Clocks();
    }

    public void UpdateClocks()
    {
        TimeSpan cTimeDif = CampaignEndDate.Subtract(DateTime.Now);
        TimeSpan kTimeDif = KitEndDate.Subtract(DateTime.Now);
        TimeSpan gTimeDif = GiftEndDate.Subtract(DateTime.Now);

        if (cTimeDif.TotalSeconds > 0)
        {
            if (e_Campaign_Clock != null)
                e_Campaign_Clock(cTimeDif);
        }
        else
        {
            ZXCampaignMission.ResetAllMissions();

            CampaignEndDate = DateTime.Now.Add(new TimeSpan(campLength, 0, 0, 0));
        }

        if (kTimeDif.TotalSeconds > 0)
        {
            if (e_Kit_Clock != null)
                e_Kit_Clock(kTimeDif);
        }
        else
        {
            ZXStarCounter.ResetStars(Type.BasicKit);
            ZXStarCounter.ResetStars(Type.ClassKit);

            KitEndDate = DateTime.Now.Add(new TimeSpan(kitLength, 0, 0, 0));
        }

        if (gTimeDif.TotalSeconds > 0)
        {
            if (e_Gift_Clock != null)
                e_Gift_Clock(gTimeDif);
        }
        else
        {
            ZXGiftButton.HasGift = true;

            GiftEndDate = DateTime.Now.Add(new TimeSpan(giftLength, 0, 0, 0));
        }
    }

    public static void StartMission(Type MissionType, Difficulty MissionDifficulty, int Cost, string ID)
    {
        if (Cost == 0 || ZXDataManager.Fuel >= Cost)
        {
            ZXDataManager.Fuel -= Cost;

            CurrentMissionTokenValid = true;
            CurrentMissionType = MissionType;
            CurrentMissionDifficulty = MissionDifficulty;
            CurrentMissionVictory = false;
            CurrentMissionStars = 0;
            CurrentMissionID = ID;

            int scene = UnityEngine.Random.Range(3, 6); /*Debug*/

            TotalMissionCount++;

            ZXCombatLoader.LoadCombatScene(scene);
        }
    }

    private void MissionResults(Scene NewScene, LoadSceneMode LoadMode)
    {
        if (NewScene.buildIndex == (int)ZXDataManager.Scenes.Main)
        {
            d_Clocks = new Action(UpdateClocks);

            if (CurrentMissionTokenValid)
            {
                switch (CurrentMissionType)
                {
                    case Type.Battle:
                        if (CurrentMissionVictory)
                            ZXChestSlot.PlaceChestInSlot();

                        ZXMainScrollview.SnapToSection(ZXCore.ZXSection.Type.Mission);
                        break;

                    case Type.BasicKit:
                        ZXMainScrollview.SnapToSection(ZXCore.ZXSection.Type.Daily);
                        ZXStarCounter.AddStars(CurrentMissionType, CurrentMissionDifficulty, CurrentMissionStars);
                        break;

                    case Type.ClassKit:
                        ZXMainScrollview.SnapToSection(ZXCore.ZXSection.Type.Daily);
                        ZXStarCounter.AddStars(CurrentMissionType, CurrentMissionDifficulty, CurrentMissionStars);
                        break;

                    case Type.Campaign:
                        PlayerPrefs.SetInt(CurrentMissionID, CurrentMissionStars);
                        ZXMainScrollview.SnapToSection(ZXCore.ZXSection.Type.Mission);
                        ZXCampaignPopup.ShowPopup(true);
                        break;
                }
            }
            else
            {
                ZXMainScrollview.SnapToSection(ZXCore.ZXSection.Type.Mission);
                ZXCampaignPopup.ShowPopup(true);
            }
        }
        else
        {
            d_Clocks = null;
        }
    }

    private void OnDestroy()
    {
        SceneManager.sceneLoaded -= d_SceneChange;
    }
}
