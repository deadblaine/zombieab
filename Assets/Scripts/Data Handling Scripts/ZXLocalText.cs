﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class ZXLocalText : MonoBehaviour
{
    //Constants
    private const float outlineWidth = 0.2f;
    private const float shadowXOffset = 0.2f;
    private const float shadowYOffset = 0.2f;

    //Inspector
    [SerializeField] private string Key;
    [SerializeField] private bool customAlignment; 

    //Private variables
    private TextMeshProUGUI textMesh;
    private System.Action d_LangChanged;
    private bool initalized = false;

    // Use this for initialization
    private void Awake()
    {
        d_LangChanged = new System.Action(GetText);

        textMesh = GetComponent<TextMeshProUGUI>();
    }

    private void Start()
    {
        GetText();

        ZXLocal.LanguageChanged += d_LangChanged;

        if (textMesh != null)
        {
            //Set alignment
            if (!customAlignment)
                textMesh.alignment = TextAlignmentOptions.Center;

            textMesh.fontSizeMin = 12;

            //Disable raycast target
            textMesh.raycastTarget = false;

            if (!textMesh.enableAutoSizing)
            {
                textMesh.fontSizeMax = textMesh.fontSize;
                textMesh.enableAutoSizing = true;
            }
        }

        initalized = true;
    }

    private void GetText()
    {
        if (textMesh.font != ZXLocal.Font)
            textMesh.font = ZXLocal.Font;

        if (Key != string.Empty)
            textMesh.text = ZXLocal.LocalText(Key);
    }

    private void OnDestroy()
    {
        if (initalized)
            ZXLocal.LanguageChanged -= d_LangChanged;
    }
}
