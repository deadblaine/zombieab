﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

/// <summary>
/// This class is used to login, authenticate, and download all needed data prior to launching the game
/// </summary>
public class ZXBootStrap : MonoBehaviour
{
    //Editor fields
    [SerializeField] private bool useWebLoad = true;
    [SerializeField] private bool useLocalFiles = true;

    //Master Loading Queue
    private Queue<System.Action> loadingSequence;

    public static bool LoadingComplete
    {
        get
        {
            return complete;
        }
    }

    //Events
    public static event System.Action<string, float> e_StartedLoading;

    //Private Delegates
    private UnityAction<Scene, LoadSceneMode> d_SceneLoaded;
    private System.Action d_LoadingComplete;
    private System.Action d_ReloadResource;

    //Private Variables
    private static ZXBootStrap singleton;
    List<ZXCore.IZXLoadable> loadables;
    private static System.Action loadingNow;
    private static bool complete = false;
    private static int loadCount = 0;
    private static int loadTalley = 0;

    /* Initalization */ 
    private void Awake()
    {
        singleton = this;

        loadingSequence = new Queue<System.Action>(10);

        d_SceneLoaded = new UnityAction<Scene, LoadSceneMode>(LoadingSequence);
        d_LoadingComplete = new System.Action(LoadResourceFromQueue);
        d_ReloadResource = new System.Action(LoadResource);

        SceneManager.sceneLoaded += d_SceneLoaded;
    }

    /* Loading Functions */
    private void LoadingSequence(Scene scene, LoadSceneMode mode)
    {
        /* Loading Notes: ZXCryptPrefs loads during Awake() */

        /* Master Loading List */
        loadables = new List<ZXCore.IZXLoadable>(new ZXCore.IZXLoadable[] { ZXLocal.Loadable, ZXPermissionManager.Loadable, ZXDataManager.Loadable, ZXImageServer.Loadable, ZXAnalytics.Loadable });

        foreach (ZXCore.IZXLoadable loadable in loadables)
        {
            loadable.e_LoadingComplete += d_LoadingComplete;

            //Get the loading list from CryptPrefs
            foreach (System.Action Load in loadable.LoadingList)
            {
                loadingSequence.Enqueue(Load);
            }
        }

        //Make sure all files are upto date
        //if (!useLocalFiles)
        //loadingSequence.Enqueue(new System.Action(CheckFileVersions));
        //Begin loading all other data

        loadCount = loadingSequence.Count;

        LoadResourceFromQueue();
    }

    /// <summary>
    /// Begins loading the next resource or attempts to load the resource again
    /// </summary>
    private void LoadResourceFromQueue()
    {
        if (loadingSequence.Count > 0)
        {
            loadingNow = loadingSequence.Dequeue();
            string actionName = loadingNow.Method.ToString();

            ZXErrorHandler.ReportMessage(gameObject, actionName);

            loadTalley++;

            LoadResource();
        }
        else
        {
            loadCount = 0;
            complete = true;

            SceneManager.LoadScene((int)ZXDataManager.Scenes.Main); //Load the main scene
        }
    }

    private void LoadResource()
    {
        loadingNow();

        if (e_StartedLoading != null)
            e_StartedLoading(loadingNow.Method.ToString(), (float)loadTalley / (float)loadCount);
    }

    /// <summary>
    /// Used to check file versions and begin download of needed files
    /// </summary>
    private void CheckFileVersions()
    {
        LoadResourceFromQueue();
    }

    //Destruction
    private void OnDisable()
    {
        foreach (ZXCore.IZXLoadable loadable in loadables)
        {
            loadable.e_LoadingComplete -= d_LoadingComplete;
        }

        SceneManager.sceneLoaded -= d_SceneLoaded;
    }       
}
