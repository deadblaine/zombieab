﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZXAnalytics : MonoBehaviour, ZXCore.IZXLoadable
{
    //IZXLoadable Implimentation
    public event System.Action e_LoadingComplete;

    public static ZXCore.IZXLoadable Loadable
    {
        get
        {
            return singleton;
        }
    }
    public List<System.Action> LoadingList
    {
        get
        {
            return new List<System.Action>(new System.Action[] { LoadAnalytics });
        }
    }

    private static ZXAnalytics singleton;

    private void Awake()
    {
        singleton = this;
    }

    private void LoadAnalytics()
    {
        if (e_LoadingComplete != null)
            e_LoadingComplete();
    }
}
