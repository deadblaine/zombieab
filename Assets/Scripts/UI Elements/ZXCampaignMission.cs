﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZXCampaignMission : MonoBehaviour
{
    private const string pp_Base_ID = "pp_Mission_";

    [SerializeField] private bool firstMission = false;
    [SerializeField] private Image fill;
    [SerializeField] private List<Image> stars;
    [SerializeField] private GameObject launchButton;
    [SerializeField] private GameObject lockImage;
    [SerializeField] private List<ZXCampaignPath> paths;

    public string Mission_ID
    {
        get
        {
            return pp_Mission_ID;
        }
    }

    private static List<ZXCampaignMission> missions;
    private string pp_Mission_ID;

    public bool Complete
    {
        get
        {
            return (PlayerPrefs.HasKey(pp_Mission_ID));
        }
    }

    public bool Playable
    {
        get
        {
            bool pathed = paths.Find(delegate (ZXCampaignPath Path) { return Path.Complete; }) != null;
            bool stars = Stars < 3;

            return (pathed && stars || firstMission && stars);
        }
    }

    public int Stars
    {
        get
        {
            if (PlayerPrefs.HasKey(pp_Mission_ID))
                return PlayerPrefs.GetInt(pp_Mission_ID);
            else
                return 0;
        }
        set
        {
            if (value > 0)
                PlayerPrefs.SetInt(pp_Mission_ID, value);
            else
            {
                if (PlayerPrefs.HasKey(pp_Mission_ID))
                    PlayerPrefs.DeleteKey(pp_Mission_ID);
            }
        }
    }

    public static int MaxStars
    {
        get
        {
            return missions.Count * 3;
        }
    }

    public static int TotalStars
    {
        get
        {
            int talley = 0;

            foreach (ZXCampaignMission mission in missions)
            {
                talley += mission.Stars;
            }

            return talley;
        }
    }

    public static float MissionProgress
    {
        get
        {
            return (float)TotalStars / (float)MaxStars;
        }
    }

    // Use this for initialization
    private void Awake()
    {
        pp_Mission_ID = pp_Base_ID + gameObject.name;

        launchButton.GetComponent<ZXLaunchButton>().SetID(pp_Mission_ID);
	}

    public void AddToList()
    {
        if (missions == null)
            missions = new List<ZXCampaignMission>(20);

        if (!missions.Contains(this))
            missions.Add(this);
    }

    public void BuildPaths()
    {
        if (Complete)
        {
            foreach (ZXCampaignPath path in paths)
            {
                path.CompletePath();
            }

            for (int i = 0; i < Stars; i++)
            {
                stars[i].color = Color.white;
            }
        }
    }

    public void Refresh()
    {
        if (Playable)
        {
            lockImage.SetActive(false);
            fill.gameObject.SetActive(true);
            fill.color = ZXDataManager.GetColor("GoGreen");
            launchButton.SetActive(true);
        }
        else if (Complete)
        {
            lockImage.SetActive(false);
            fill.gameObject.SetActive(true);
            fill.color = ZXDataManager.GetColor("ProgressBlue");
            launchButton.SetActive(false);
        }
        else
        {
            lockImage.SetActive(true);
            fill.gameObject.SetActive(false);
            launchButton.SetActive(false);
        }
    }

    public void ResetMission()
    {
        Stars = 0;
    }

    public static void ResetAllMissions()
    {
        foreach (ZXCampaignMission mission in missions)
        {
            mission.ResetMission();
        }
    }

    public static ZXCampaignMission FindMission(int Index)
    {
        if (Index < missions.Count)
            return missions[Index];
        else
            return null;
    }

    private void OnDestroy()
    {
        if (missions != null && missions.Contains(this))
            missions.Remove(this);
    }
}
