﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ZXVersionNumber : MonoBehaviour
{
    private TextMeshProUGUI versionText;

    // Use this for initialization
    private void Awake()
    {
        versionText = GetComponent<TextMeshProUGUI>();
    }

    private void Start ()
    {
        versionText.text = "v" + Application.version; 
	}
}
