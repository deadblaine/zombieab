﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ZXGiftButton : ZXButton, IPointerClickHandler
{
    //Constants
    private const string pp_Gifts = "pp_Gifts_Stored_Key";

    //Inspector Variables
    [SerializeField] private GameObject waitDisplay;

    //Accessors
    public static bool HasGift
    {
        get
        {
            return PlayerPrefs.HasKey(pp_Gifts);
        }
        set
        {
            if (value)
                PlayerPrefs.SetString(pp_Gifts, true.ToString());
            else
                PlayerPrefs.DeleteKey(pp_Gifts);
        }
    }

    //Private variables
    private ZXGiftButton singleton;

	// Use this for initialization
	protected override void Awake()
    {
        base.Awake();

        singleton = this;
	}

    private void OnEnable()
    {
        button.interactable = HasGift;
        waitDisplay.SetActive(!HasGift);
    }

    //Event handlers
    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);

        if (HasGift)
        {
            HasGift = false;

            ZXChestRewards.GrantChestRewards();
        }
    }
}
