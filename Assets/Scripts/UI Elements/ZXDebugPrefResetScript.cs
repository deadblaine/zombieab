﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class ZXDebugPrefResetScript : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.clickCount > 3)
        {
            PlayerPrefs.DeleteAll();
            SceneManager.LoadScene((int)ZXDataManager.Scenes.Loading);
        }
    }
}
