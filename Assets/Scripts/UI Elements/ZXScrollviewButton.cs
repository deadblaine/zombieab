﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ZXScrollviewButton : ZXButton, IPointerClickHandler
{
    //Inspector variables
    [SerializeField] ZXCore.ZXSection.Type section;

    //Private variables
    private ZXMainScrollview scrollview;

    //Initalization
    private void Start()
    {
        scrollview = GameObject.Find("Menu_Content").GetComponent<ZXMainScrollview>();
    }

    //Button function
    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);

        scrollview.ChangeSection(section);
    }
}
