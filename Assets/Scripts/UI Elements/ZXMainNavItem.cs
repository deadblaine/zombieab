﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using ZXCore;
using DG.Tweening;

public class ZXMainNavItem : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private ZXCore.ZXSection.Type section;
    [SerializeField] private Image background;
    [SerializeField] private RectTransform backgroundTransform;
    [SerializeField] private RectTransform icon;
    [SerializeField] private RectTransform leftIcon;
    [SerializeField] private RectTransform rightIcon;
    [SerializeField] private RectTransform text;

    //Static Variables
    private static ZXMainNavItem current;

    //Private variables
    private ZXMainScrollview scrollview;
    private System.Action<ZXCore.ZXSection.Type, float> d_SectionChange;

    //Initalization
    private void Awake()
    {
        d_SectionChange = new System.Action<ZXSection.Type, float>(SectionChange);
    }

    private void Start()
    {
        scrollview = GameObject.Find("Menu_Content").GetComponent<ZXMainScrollview>();
    }

    private void OnEnable()
    {
        ZXMainScrollview.e_NextSection += d_SectionChange;

        SnapSelect(ZXMainScrollview.Current);
    }

    //Selection Functions
    public void Select()
    {
        scrollview.ChangeSection(section);
    }

    public void SectionChange (ZXSection.Type NextSection, float Time)
    {
        DOTween.Kill(this);

        if (NextSection == section)
        {
            current = this;

            background.DOColor(ZXDataManager.GetColor("NavBarSelectBlue"), Time);
            backgroundTransform.DOSizeDelta(new Vector2(275, 175), Time);
            icon.DOAnchorPos(new Vector2(0, 50), Time);

            if (leftIcon != null)
                leftIcon.DOScale(1, Time);
            if (rightIcon != null)
                rightIcon.DOScale(1, Time);
            text.DOScale(1, Time);
        }
        else
        {
            background.DOColor(ZXDataManager.GetColor("NavBarBlue"), Time);
            backgroundTransform.DOSizeDelta(new Vector2(200, 175), Time);
            icon.DOAnchorPos(new Vector2(0, 10), Time);
            if (leftIcon != null)
                leftIcon.DOScale(0, Time);
            if (rightIcon != null)
                rightIcon.DOScale(0, Time);
            text.DOScale(0, Time);
        }
    }

    public void SnapSelect(ZXSection.Type NextSection)
    {
        if (NextSection == section)
        {
            background.color = ZXDataManager.GetColor("NavBarSelectBlue");
            backgroundTransform.sizeDelta = new Vector2(275, 175);
            icon.anchoredPosition = new Vector2(0, 50);
            if (leftIcon != null)
                leftIcon.localScale = Vector3.one;
            if (rightIcon != null)
                rightIcon.localScale = Vector3.one;
            text.localScale = Vector3.one;
        }
        else
        {
            background.color = ZXDataManager.GetColor("NavBarBlue");
            backgroundTransform.sizeDelta = new Vector2(200, 175);
            icon.anchoredPosition = new Vector2(0, 10);
            if (leftIcon != null)
                leftIcon.localScale = Vector3.zero;
            if (rightIcon != null)
                rightIcon.localScale = Vector3.zero;
            text.localScale = Vector3.zero;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Select();
    }

    //Deconstruction
    private void OnDisable()
    {
        ZXMainScrollview.e_NextSection -= d_SectionChange;
    }
}
