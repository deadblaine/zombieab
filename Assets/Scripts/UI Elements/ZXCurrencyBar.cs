﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class ZXCurrencyBar : MonoBehaviour
{
    private const string Tween_Base_ID = "tw_Currency_";
    private const string Tween_Base_Icon_ID = "tw_Currency_Icon_";
    private const float countVelocity = 250f;
    private const float pulseVelocity = 50f;
    private const float minPulseTime = 0.1f;
    private const float minCountTime = 0.25f;
    private const float maxCountTime = 2.5f;

    public enum Currency
    {
        Gems = 0,
        Silver = 1,
        Energy = 2,
        Kits = 3
    }

    [SerializeField] private Currency currency;
    [SerializeField] private Image icon;
    [SerializeField] private TextMeshProUGUI text;

    private int DisplayAmount
    {
        get
        {
            return displayAmount;
        }
        set
        {
            displayAmount = value;

            if (currency == Currency.Energy)
            {
                text.text = value.ToString() + " / " + ZXDataManager.maxFuel.ToString();
            }
            else
            {
                text.text = value.ToString("N0");
            }
        }
    }

    private string tween_ID;
    private string tween_Icon_ID;

    private int displayAmount = 0;
    private int currentAmount = 0;
    private System.Action d_Update;
    private RectTransform iconRect;

    // Use this for initialization
    private void Awake()
    {
        tween_ID = Tween_Base_ID + gameObject.name;
        tween_Icon_ID = Tween_Base_Icon_ID + gameObject.name;

        iconRect = icon.gameObject.GetComponent<RectTransform>();

        d_Update = new System.Action(UpdateAmount);
    }

    private void OnEnable()
    {
        ZXDataManager.e_Currency_Updated += d_Update;

        SnapAmount();
    }

    protected virtual void SnapAmount()
    {
        currentAmount = FetchAmount();

        if (DOTween.IsTweening(tween_ID))
            DOTween.Kill(tween_ID);

        if (DOTween.IsTweening(tween_Icon_ID))
            DOTween.Complete(tween_Icon_ID);

        DisplayAmount = currentAmount;
    }

    protected virtual void UpdateAmount()
    {
        currentAmount = FetchAmount();

        int diff = displayAmount - currentAmount;

        if (Mathf.Abs(diff) > 0)
        {

            float sign = Mathf.Sign(diff);
            float time = Mathf.Clamp(Mathf.Abs(diff) / countVelocity, minCountTime, maxCountTime);
            Vector3 pulse = sign > 0 ? new Vector3(0.1f, 0.1f, 0.1f) : new Vector3(-0.1f, -0.1f, -0.1f);
            int pulseCount = Mathf.RoundToInt(Mathf.Abs(diff) / pulseVelocity);
            float pulseTime = Mathf.Clamp(time / pulseCount, minPulseTime, minCountTime);

            //Clamp number of pulses
            pulseCount = Mathf.Min(pulseCount, Mathf.RoundToInt(time / pulseTime));

            if (DOTween.IsTweening(tween_ID))
                DOTween.Kill(tween_ID);

            DOTween.To(delegate () { return DisplayAmount; }, delegate (int x) { DisplayAmount = x; }, currentAmount, time).SetId(tween_ID);

            if (DOTween.IsTweening(tween_Icon_ID))
                DOTween.Complete(tween_Icon_ID);

            iconRect.DOPunchScale(pulse, pulseTime).SetLoops(pulseCount).SetId(tween_Icon_ID);
        }
    }

    private int FetchAmount()
    {
        int value = 0;

        switch (currency)
        {
            case (Currency.Gems):
                value = ZXDataManager.Gems;
                break;

            case (Currency.Silver):
                value = ZXDataManager.Silver;
                break;

            case (Currency.Energy):
                value = ZXDataManager.Fuel;
                break;

            case (Currency.Kits):
                value = ZXDataManager.Kits;
                break;
        }

        return value;
    }

    private void OnDisable()
    {
        if (DOTween.IsTweening(tween_ID))
            DOTween.Complete(tween_ID);

        if (DOTween.IsTweening(tween_Icon_ID))
            DOTween.Complete(tween_Icon_ID);

        ZXDataManager.e_Currency_Updated -= d_Update;
    }
}
