﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZXRomanceButton : MonoBehaviour
{
    [SerializeField] private Image romanceImage;
    [SerializeField] private GameObject lockedImage;
    [SerializeField] private Image background;

    private Sprite image;
    private Sprite thumb;
    private bool locked;

    private static ZXRomanceButton currentlySelected;

    public void LoadImage(Sprite image, Sprite thumb, bool locked)
    {
        this.image = image;
        this.thumb = thumb;
        this.locked = locked;

        romanceImage.sprite = thumb;
        lockedImage.SetActive(locked);
    }

    public void Select()
    {
        if (!locked)
        {
            background.color = Color.green;

            if (currentlySelected != null)
                currentlySelected.DeSelect();

            currentlySelected = this;

            ZXRomanceGamePopup.ShowNewRomanceImage(image);
        }
    }

    private void DeSelect()
    {
        currentlySelected = null;

        background.color = Color.grey;
    }
}
