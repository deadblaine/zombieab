﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ZXNotImplimentedButton : ZXButton, IPointerClickHandler
{
    //Button function
    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);

        ZXMessageScreen.ShowMessage("Feature Coming Soon!");
    }
}
