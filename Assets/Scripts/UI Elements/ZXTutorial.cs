﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class ZXTutorial : MonoBehaviour, IPointerClickHandler
{
    //Constants
    private const string pp_tut_ID = "pp_tutorial_shown";

    //Inspector variables
    [SerializeField] private Image image;
    [SerializeField] private List<Sprite> slides;

    public bool TutorialShown
    {
        get
        {
            return PlayerPrefs.HasKey(pp_tut_ID);
        }
        set
        {
            if (value)
                PlayerPrefs.SetString(pp_tut_ID, true.ToString());
            else
                PlayerPrefs.DeleteKey(pp_tut_ID);
        }
    }

    private int index = 0;    

    public void OnPointerClick(PointerEventData eventData)
    {
        NextImage();
    }

    public void Show()
    {
        ZXCombatUI.ChangeMissionState(ZXCombatUI.State.Paused);

        gameObject.SetActive(true);

        NextImage();
    }

    public void NextImage()
    {
        if (index < slides.Count)
        {
            image.sprite = slides[index];

            index++;
        }
        else
        {
            index = 0;
            TutorialShown = true;
            gameObject.SetActive(false);

            ZXPausePopup.ResumeGame();
        }

    }
}
