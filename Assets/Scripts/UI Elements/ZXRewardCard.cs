﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ZXRewardCard : MonoBehaviour
{
    [SerializeField] private Image image;
    [SerializeField] private TextMeshProUGUI name;
    [SerializeField] private TextMeshProUGUI rarity;
    [SerializeField] private TextMeshProUGUI level;
    [SerializeField] private TextMeshProUGUI amount;

    [SerializeField] private Image cardsFill;
    [SerializeField] private TextMeshProUGUI cardsText;

    public void Set(ZXCard Card, int Amount)
    {
        image.sprite = Card.Card_Image;
        name.text = Card.Name;
        rarity.text = Card.Rarity.ToString();
        level.text = Card.LevelText;

        amount.text = "x" + Amount.ToString();

        cardsFill.fillAmount = Card.CardsProgress;
        cardsText.text = Card.CardsProgressText;
    }
}
