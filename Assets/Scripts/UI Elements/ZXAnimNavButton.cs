﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ZXAnimNavButton : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private string setID;
    [SerializeField] private int stateID;

    private Animator anim;
    private static Dictionary<string, ZXAnimNavButton> current;

    // Use this for initialization
    private void Awake()
    {
        anim = GetComponentInParent<Animator>();

        if (current == null)
            current = new Dictionary<string, ZXAnimNavButton>(5);

        if (!current.ContainsKey(setID))
            current.Add(setID, null);
    }

    private void OnEnable()
    {
        if (stateID == anim.GetInteger("StateID"))
            Select();
        else
            DeSelect();
    }

    //Selection Functions
    public void Select()
    {
        if (current[setID] != null)
            current[setID].DeSelect();

        current[setID] = this;

        if (stateID != anim.GetInteger("StateID"))
            anim.SetInteger("StateID", stateID);
    }

    protected virtual void DeSelect()
    {
        if (current[setID] == this)
            current[setID] = null;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Select();
    }

    //Deconstruction
    protected virtual void OnDisable()
    {
        if (current[setID] == this)
            DeSelect();
    }
}
