﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ZXCheatPurchaseButton : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private ZXShopContent.CheatPurchase purchase;

    public void OnPointerClick(PointerEventData eventData)
    {
        ZXShopContent.GetCheatPurchase(purchase);
    }
}
