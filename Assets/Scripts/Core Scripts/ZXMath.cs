﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZXMath : MonoBehaviour
{
    [SerializeField] private AnimationCurve normalCurve;
    [SerializeField] private AnimationCurve missionDifficultyCurve;

    public enum Randomization
    {
        None = 0,
        Normal = 1,
        Mission = 2,
    }

    private static ZXMath singleton;

    private void Awake()
    {
        singleton = this;
    }

    public static int RandomFromSet(int Count, Randomization Randomize)
    {
        switch (Randomize)
        {
            case Randomization.None:
                return (Random.Range(0, Count));

            case Randomization.Normal:
                return Mathf.Clamp(Mathf.RoundToInt(RandomNormal() * Count), 0, Count - 1);

            default:
                return (Random.Range(0, Count));
        }
    }

    public static float RandomNormal()
    {
        return Mathf.Clamp(singleton.normalCurve.Evaluate(Random.value), 0f, 1f);
    }

    public static int RandomMission(int Count)
    {
        return Mathf.Clamp(Mathf.RoundToInt(singleton.missionDifficultyCurve.Evaluate(Random.value)), 0, Count - 1);
    }
}
