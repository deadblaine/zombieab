﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using RootMotion.FinalIK;
using Vectrosity;
using DarkTonic.MasterAudio;

namespace ZXCore
{
    /* Types */
    #region

    //Enums
    public enum ToonRace
    {
        None = 0,
        Human = 1,
        Raider = 2,
        Zombie = 3
    }

    //Structs
    public class ZXDamage
    {
        /* Enumeration */
        public enum Type
        {
            Impact = 0,
            Projectile = 1,
            Explosive = 2,
            Fire = 3,
            Gas = 4
        }
        public enum SpecialEffect
        {
            None = 0,
            Knockback = 1,
            ForceKnockback = 2,
            ForceRagdoll = 3,
            Fire = 4,
            ForceFire = 5,
            Frenzy = 6
        }

        //Conversion
        public static implicit operator int(ZXDamage Damage)
        {
            return Damage.damage;
        }

        //Accessors
        public Type DamageType
        {
            get
            {
                return damageType;
            }
        }
        public ToonRace Race
        {
            get
            {
                return race;
            }
        }
        public SpecialEffect Effect
        {
            get
            {
                return specialEffect;
            }
        }
        public int Damage
        {
            get
            {
                return damage;
            }
        }
        public float Radius
        {
            get
            {
                return radius;
            }
        }
        public float Penetration
        {
            get
            {
                return penetration;
            }
        }
        public float ForceMod
        {
            get
            {
                return forceMod;
            }
        }
        public Ray Trajectory
        {
            get
            {
                return trajectory;
            }
        }
        public Vector3 Force
        {
            get
            {
                return trajectory.direction * damage;
            }
        }

        //Variables
        private Type damageType;
        private ToonRace race;
        private int damage;
        private float radius;
        private float penetration;
        private float forceMod;
        private Ray trajectory;
        private RaycastHit hit;
        private SpecialEffect specialEffect;

        //Constructor
        public ZXDamage(int Damage, float Penetration, float ForceMod, float Radius, ToonRace Race, Type DamageType, Ray Trajectory, SpecialEffect Effect)
        {
            damage = Damage;
            penetration = Penetration;
            forceMod = ForceMod;
            damageType = DamageType;
            trajectory = Trajectory;
            hit = new RaycastHit();
            specialEffect = Effect;
            radius = Radius;
            race = Race;
        }
    }

    //A time stamped value, normalized and weighted by time value
    public struct XoTPoint
    {
        public static implicit operator Vector2(XoTPoint V)
        {
            return new Vector2(V.xt, V.t);
        }
        public static implicit operator XoTPoint(Vector2 V)
        {
            return new XoTPoint(V.x, V.y);
        }

        public float x;
        public float t;
        public float xt;
        public float timeStamp;

        //Constructors
        public XoTPoint(float x) : this(x, 1f) { }
        public XoTPoint(float x, float t)
        {
            if (t <= 0)
                t = 1;

            this.x = x;
            this.t = t;
            this.xt = x / t;
            timeStamp = Time.time;
        }
    }

    //An IComparer
    public class DuplicateKeyComparer<T> : IComparer<T> where T : System.IComparable
    {
        public int Compare(T x, T y)
        {
            int result = x.CompareTo(y);

            if (result == 0)
                return 1;   // Handle equality as beeing greater
            else
                return result;
        }
    }
    #endregion

    /* Interfaces */
    #region
    public interface IZXContent
    {
        void Show();
        void Hide();
    }
    public interface IZXLoadable
    {
        event System.Action e_LoadingComplete;

        List<System.Action> LoadingList
        {
            get;
        }
    }


    /// <summary>
    /// Used by objects that can be damaged
    /// </summary>
    public interface IZXDamageable
    {
        //Accessors
        float Density
        {
            get;
        }
        Vector3 Center
        {
            get;
        }
        ToonRace Race
        {
            get;
        }
        bool isAlive
        {
            get;
        }
        bool isFlammable
        {
            get;
        }

        //Percent of health remaining
        event System.Action<float, float> e_HealthChanged;

        //Methods
        void TakeDamage(ZXDamage Damage);
    }

    /// <summary>
    /// Used for objects that are targetable
    /// </summary>
    public interface IZXTargetable : IZXDamageable
    {
        //Accessors
        float GetDistance(Vector3 Origin);
        Vector3 GetNearestPoint(Vector3 Origin);
        Vector3 Base
        {
            get;
        }
        Vector3 CurrentVelocity
        {
            get;
        }

        //Events         
        event System.Action<IZXTargetable> e_Dead;
        event System.Action<IZXTargetable> e_BreakTargeting;

        //Accessors
        Transform LookAtTransform
        {
            get;
        }

        void Invulnerable(bool flag);
    }

    /// <summary>
    /// IZXPoolable: Used to notify registered objects of OnDisable(), OnDestroy()
    /// </summary>
    public interface IZXTracked<T> where T : MonoBehaviour
    {
        //Events 
        event System.Action<T> e_OnDisable;
        event System.Action<T> e_OnDestroy;

        //Methods
        void Place(Vector3 location, Quaternion rotation, object args);
    }

    public interface IZXReciever
    {
        void RecieveMessage(ZXInterestPoint Sender, Vector3 Location, ZXInterestPoint.Interest Header, ZXInterestPoint.BroadcastMessage Message);
    }

    public interface IZXInstructable : IZXTargetable
    {
        //Accessors
        bool canMove
        {
            get;
        }

        //Events
        event System.Action<float> e_MoveTimer;

        //Methods
        void PlayerInstruction(ZXHuman.Instruction Instruct, ZXHuman.Input In, object Args);
		void Place(Vector3 location, Quaternion rotation, object args);
		void AdjustValues(int level, int index);
    }

    public interface IZXAnimation
    {
        event System.Action<IZXAnimation> e_AnimationComplete;

        void Play();
    }

    /// <summary>
    /// Used to update logic
    /// </summary>
    public interface IZXAI
    {
        void Analyze();
        void Act();
        void ResetZXAI();

        bool ActionWait
        {
            get;
        }
    }
    #endregion

    /* Abstract Classes */
    #region

    /// <summary>
    /// Base class for all ambulatory toons
    /// </summary>
    #region ZXToon Requiered Components
    [System.Serializable]
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
    //[RequireComponent(typeof(BipedIK))]
    #endregion
    public abstract class ZXToon : MonoBehaviour, IZXAI, IZXInstructable, IZXReciever, IZXTracked<ZXToon>
    {
        //Constants
        private const float _ikAimSphereRad = 5f;
        private const float _highlightTime = 0.25f;
        protected const float _waitUntilStoppedDelay = 1f;
        protected float _fadeTime = 3f;
        protected float _fadeDelay = 4f;
        protected float _disableDelay = 0f;
        public const char _ParseChar = '_';
        protected const float _SnapToTeatherSpeed = 5f;
        protected const float _navRandom = 2;
        private const float _turnSnapAngle = 90f;
        private const float _turnMinAngle = 5f;
        private const float _animQuickDamp = 0.1f;
        private const float _animLongDamp = 0.25f;
        private const float _animIKDamp = 0.1f;
        private const float _maxActionWaitTime = 1.5f;
        private const float _avgBloodEffectDelay = 1f;

        private const float _maxPhysicsForce = 4000f;
        private const float _maxPhysicsExplosiveForce = 4500f;
        private const float _maxRagdollTime = 10f;

        private const float _burnDPS = 100f;
        private const float _burnTickRate = 0.5f;

        /* Support Types */
        #region

        public enum Posture
        {
            Ragdoll = 0,
            Prone = 1,
            Kneel = 2,
            Stand = 3,
            NotSet = 99
        }
        public enum Speed
        {
            Stop = 0,
            Walk = 1,
            PowerWalk = 2,
            Jog = 3,
            Run = 4,
            Sprint = 5,
            NotSet = 99
        }
        public enum Action
        {
            //Idles (0-99)
            Idle = 0,
            IdleZombieEat = 1,
            IdleWeapon = 2,

            //Combat (100-199)
            Aim = 200,
            Fire = 201,
            Throw = 202,
            Swap = 203,
            Reload = 204,
            Spit = 205,
            PickUpRock = 206,

            MeleeGeneric = 207,
            TakeDamage = 208,
            TakeBigDamage = 209,

            PoundGround = 210,
            ZombieExplode = 211,

            SpecialAttack = 212,

            //Movement actions
            StandFromBelly = 302,
            StandFromBack = 303,

            //Special
            FromRagdoll = 902,
            WorkWithHands = 903,
            DragFromUI = 904,
            WallPound = 905,
            LandFromFalling = 907,
            ZombieRise = 908
        }

        public enum IKState
        {
            All = 0,
            Look = 1,
            Face = 2,
            Aim = 3
        }
        public enum AnimState
        {
            Base = 0,
            Override = 1,
            UpperOverride = 2
        }
        public enum ShaderState
        {
            Default = 0,
            Highlight = 1,
            EndHighlight = 2,
            FadeIn = 3,
            FadeOut = 4
        }
        public enum Instruction
        {
            Move = 0,
            Retreat = 1,
            Activate = 2,
            Debug = 3
        }
        public enum Input
        {
            None = 0,
            Down = 1,
            Drag = 2,
            Up = 3
        }

        public class BodyPart
        {
            public Transform transform;
            public Vector3 storedPosition;
            public Quaternion storedRotation;
        }
        #endregion

        /* Accessors */
        #region
        public ToonRace Race
        {
            get
            {
                return toonRace;
            }
        }
        public int ToonLevel
        {
            get
            {
                return level;
            }
        }
        public float Density
        {
            get
            {
                return density;
            }
        }
        public float Weight
        {
            get
            {
                return weight;
            }
        }
        public Transform LookAtTransform
        {
            get
            {
                return t_Body.transform;
            }
        }
        public Transform HeadTransform
        {
            get
            {
                return t_Head.transform;
            }
        }
        public virtual bool isAlive
        {
            get
            {
                return health > 0 && gameObject.activeSelf;
            }
        }
        public bool isFlammable
        {
            get
            {
                return true;
            }
        }
        public float Health
        {
            get
            {
                return health;
            }
        }
        public Vector3 Center
        {
            get
            {
                return t_Body.transform.position;
            }
        }
        public Vector3 Base
        {
            get
            {
                return transform.position;
            }
        }
        public IZXTargetable CurrentTarget
        {
            get
            {
                return m_FollowObject;
            }
        }
        protected IZXTargetable FollowObject
        {
            get
            {
                return m_FollowObject;
            }
            set
            {
                if (m_FollowObject != null)
                {
                    m_FollowObject.e_Dead -= d_TargetKilled;
                    m_FollowObject.e_BreakTargeting -= d_TargetKilled;
                }

                m_FollowObject = value;

                if (m_FollowObject != null)
                {
                    m_FollowObject.e_Dead += d_TargetKilled;
                    m_FollowObject.e_BreakTargeting += d_TargetKilled;
                }
            }
        }

        public ZXShaderController Shader
        {
            get
            {
                return r_Shader;
            }
        }
        public bool ActionWait
        {
            get
            {
                return actionWaitFlag && de_CurrentBehaviorTranslation != null;
            }
        }
        public bool canMove
        {
            get
            {
                return moveTimer <= 0f && waypoints == null && waypoint == null && isAlive && n_Agent.isActiveAndEnabled && n_Agent.isOnNavMesh && !falling;
            }
        }
        public float AgroRange
        {
            get
            {
                return agroRange;
            }
        }
        public Vector3 CurrentVelocity
        {
            get
            {
                return currentVelocity;
            }
        }
        public bool Initalized
        {
            get
            {
                return initalized;
            }
        }
        public bool LogDebugging
        {
            get
            {
                return logDebugging;
            }
            set
            {
                logDebugging = value;
            }
        }
        #endregion

        //Declare a list of body parts, initialized in Start()
        public static List<ZXBodypart.Bodypart> PartsList = new List<ZXBodypart.Bodypart>(System.Enum.GetValues(typeof(ZXBodypart.Bodypart)) as ZXBodypart.Bodypart[]);
        List<BodyPart> bodyParts = new List<BodyPart>(20);

        /* Public Events */
        public event System.Action<Posture> e_PostureChanged;
        public event System.Action<Vector3> e_Ragdoll;
        public event System.Action<Speed> e_SpeedChanged;
        public event System.Action<float> e_MoveTimer;
        public event System.Action<bool> e_CanMove;
        public event System.Action<Vector3, ZXToon> e_teatherSet;
        public event System.Action<float, float> e_HealthChanged;
        public event System.Action<int> e_LevelChanged;
        public event System.Action<IZXTargetable> e_Dead;
        public event System.Action<IZXTargetable> e_BreakTargeting;
        public event System.Action<ZXToon> e_OnDisable;
        public event System.Action<ZXToon> e_OnDestroy;

        /* Inspector Fields */
        #region
        [SerializeField] protected ToonRace toonRace;
        [SerializeField] protected ZXBodypart.Configuration skeletonConfiguration;
        [SerializeField] protected float scale = 1;
        [SerializeField] protected float health;
        [SerializeField] protected bool ignoreGas;
        [SerializeField] protected bool ignoreHealing;
        [SerializeField] protected bool useLevelBounds;
        [SerializeField] [Range(40f, 500f)] protected float weight;
        [SerializeField] [Range(0, 0.9f)] protected float armour;
        [SerializeField] [Range(0.1f, 1f)] protected float density;
        [SerializeField] protected float aimHeightOffset;
        [SerializeField] protected Action defaultAction;
        [SerializeField] protected Speed defaultMoveSpeed;
        [SerializeField] protected Action defaultMoveAction;
        [SerializeField] protected float agroRange;
        protected float meleeRange = 4;
        protected float attackRange;
        [SerializeField] protected float moveTime;
        [SerializeField] protected int meleeDamage;
        [SerializeField] protected ZXDamage.SpecialEffect meleeDamageEffect;
        [SerializeField] protected bool logDebugging;
        [SerializeField] protected bool logUserInput;

        //Important transforms
        protected ZXBodypart t_Char;
        protected ZXBodypart t_Head;
        protected ZXBodypart t_Body;
        protected ZXBodypart t_LeftHand;
        protected ZXBodypart t_RightHand;

        //Sound data
        [SoundGroupAttribute] [SerializeField] protected string s_SpawnSound;
        [SoundGroupAttribute] [SerializeField] protected string s_meleeAttackSound;
        [SoundGroupAttribute] [SerializeField] protected string s_DeathSound;

        #endregion

        /* Private Variables */
        #region
        //General
        protected int uid;
        protected float startingHealth;
        protected bool invulnerable;
        protected bool initalized = false;
        protected int level = 1;

        //Components
        protected Animator m_Animator;
        protected UnityEngine.AI.NavMeshAgent n_Agent;
        protected ZXShaderController r_Shader;
        protected List<ZXBodypart> rb_BodyParts;
        protected List<ZXBodypart> rb_RigidBodyParts;
        protected List<ZXBodypart> rb_PhysicsParts;
        protected ZXCorpse rb_Corpse;
        protected new Transform transform;
        protected BipedIK ik_BipedIK;
        protected IKSolverLookAt ik_LookIK;
        protected IKSolverAim ik_AimIK;
        protected bool ik_LookEnabled = false;
        protected string ik_LookID;
        protected bool ik_AimEnabled = false;
        protected string ik_AimID;
        protected bool ik_FaceEnabled = false;
        protected string ik_FaceID;
        protected ZXAura aura;

        //Animator Hashes
        protected int m_PostureHashID;
        protected int m_ActionHashID;
        protected int m_WeaponHashID;
        protected int m_xVelocityHashID;
        protected int m_yVelocityHashID;
        protected int m_zVelocityHashID;
        protected int m_MagnitudeHashID;
        protected int m_MotionTypeHashID;

        protected string m_xVelocityID;
        protected string m_yVelocityID;
        protected string m_zVelocityID;
        protected string m_MagnitudeID;
        protected string m_AnimOverrideID;
        protected string m_AnimUpperID;

        private float ragdollingEndTime = -100;
        private float mecanimToGetUpTransitionTime = 0.1f;
        private float ragdollToMecanimBlendTime = 1f;
        protected bool canRagDoll = true;

        //Additional vectores for storing the pose the ragdoll ended up in.
        private Vector3 ragdolledHipPosition, ragdolledHeadPosition, ragdolledFeetPosition;

        //Animator states
        protected Posture posture = Posture.NotSet;
        protected Speed speed = Speed.NotSet;
        protected AnimState animState = AnimState.Base;
        protected ZXWeapon.Type weapon = ZXWeapon.Type.None;
        protected bool grounded = true;
        protected bool turning = false;
        protected Vector3 velocity = Vector3.zero;
        protected Vector3 angVelocity = Vector3.zero;
        protected bool useAnimRotation = false;
        protected bool useSnapRotation = false;
        protected Quaternion newRotation = Quaternion.identity;

        //Aim ball
        [SerializeField][Range(10, 30)] private float ik_AimSpeed;
        protected Transform ik_AimBall;
        protected Vector3 ik_DefaultAimPosition;
        protected bool lockAim = false;

        //Tracking
        protected Transform ik_PointBall;
        private IZXTargetable m_FollowObject;
        private List<IZXTargetable> tr_AllTargets;
        protected SortedList<float, IZXTargetable> tr_VisibleTargets;
        protected SortedList<float, IZXTargetable> tr_ObscuredTargets;
        protected ZXInterestPoint tr_ClosestExit;
        protected ZXInterestPoint tr_ClosestNexus;
        protected System.Predicate<IZXTargetable> d_TargetingParams;
        protected ZXSpawnRegion tr_CurrentRegion = null;

        //Internal delegates
        protected System.Action<float> d_MoveTranslation;
        protected System.Action<float> d_TrackTranslation;
        protected System.Action d_LevelEnd;
        protected System.Action d_Disable;
        protected System.Action<IZXTargetable> d_TargetKilled;
        protected System.Func<int> d_Idle;
        protected System.Predicate<IZXTargetable> d_StdCheck;
        protected System.Predicate<ZXObjective> d_StdObjCheck;
        protected System.Predicate<ZXObjective> d_StdObjVisibilityCheck;
        protected System.Action<ZXCombatUI.State> d_StateChange;
        protected System.Action<ZXParticleEffect> d_EndBurn;

        //Internal event delegates
        protected System.Action<float> de_CurrentMoveTranslation;
        protected System.Func<int> de_CurrentBehaviorTranslation;

        //Renderer settings
        protected bool visible;
        protected System.Action d_Visible;
        protected System.Action d_Invisible;
        protected bool reg_ZXAI = false;
        protected bool reg_RefPool = false;
        protected ZXParticleEffect burningEffect;

        //Action queue
        private Queue<int> actionQueue = new Queue<int>(5);
        protected int currentAction = 0;
        protected bool actionWaitFlag
        {
            get
            {
                return actionWait;
            }
            set
            {
                if (value)
                {
                    actionWait = true;
                    actionWaitTimer = _maxActionWaitTime;
                }
                else
                    EndAction();
            }
        }
        protected bool actionWait = false;
        protected float actionWaitTimer = 0f; 
        protected System.Action actionPostInstruction;
        protected float moveTimer = 0f;
        protected bool falling = false;
        protected bool tickMoveTimer = false;
        protected VectorLine waypoints = null;
        protected System.Nullable<Vector3> waypoint = null;
        protected System.Nullable<Vector3> teather = null;
        protected IZXTargetable attackTarget = null;

        //Ragdoll
        protected bool playDamageAnimations = true;
        protected bool blendFromRagFlag = false;
        protected Vector3 currentVelocity = Vector3.zero;

        //Sound timers
        private float maxRagdollTimer = 0f;
        private float bloodTimer = 0f;

        //Level data
        protected float original_Health;
        protected float original_Armour;
        protected Speed original_Speed;
        protected float original_Damage;
        #endregion

        /* Initalization */
        #region
        protected virtual void Awake()
        {
            //Get transform
            transform = base.transform;

            Transform[] children = GetComponentsInChildren<Transform>();
            ZXBodypart.Bodypart currentPart;

            //Create bodyparts
            for (int i = 0; i < children.Length; i++)
            {
                currentPart = IdentifyPart(children[i]);

                if (currentPart != ZXBodypart.Bodypart.NotRecognized)
                    (children[i].gameObject.AddComponent<ZXBodypart>() as ZXBodypart).SetPart(currentPart, skeletonConfiguration);
            }

            //Build parts list
            rb_BodyParts = new List<ZXBodypart>(GetComponentsInChildren<ZXBodypart>());
            rb_RigidBodyParts = rb_BodyParts.FindAll(delegate (ZXBodypart Object) { return Object.rigidbody != null; });
            rb_PhysicsParts = rb_RigidBodyParts.FindAll(delegate (ZXBodypart Object)
            {
                return Object.Part == ZXBodypart.Bodypart.Head || Object.Part == ZXBodypart.Bodypart.Spine || Object.Part == ZXBodypart.Bodypart.UpperArm_Left || Object.Part == ZXBodypart.Bodypart.UpperArm_Right || Object.Part == ZXBodypart.Bodypart.UpperLeg_Left || Object.Part == ZXBodypart.Bodypart.UpperLeg_Right;
            });

            //Assmble joints
            rb_BodyParts.ForEach(delegate (ZXBodypart Object) { Object.SetJoint(skeletonConfiguration); });

            //Get body parts
            t_Char = rb_BodyParts.Find(delegate (ZXBodypart Object) { return Object.Part == ZXBodypart.Bodypart.Char; });
            t_Head = rb_BodyParts.Find(delegate (ZXBodypart Object) { return Object.Part == ZXBodypart.Bodypart.Head; });
            t_Body = rb_BodyParts.Find(delegate (ZXBodypart Object) { return Object.Part == ZXBodypart.Bodypart.Body; });
            t_LeftHand = rb_BodyParts.Find(delegate (ZXBodypart Object) { return Object.Part == ZXBodypart.Bodypart.Left_Hand; });
            t_RightHand = rb_BodyParts.Find(delegate (ZXBodypart Object) { return Object.Part == ZXBodypart.Bodypart.Right_Hand; });

            rb_Corpse = t_Body.GetComponent<ZXCorpse>();

            Component[] components = GetComponentsInChildren(typeof(Transform));

            //For each of the transforms, create a BodyPart instance and store the transform 
            foreach (Component c in components)
            {
                BodyPart bodyPart = new BodyPart();
                bodyPart.transform = c as Transform;
                bodyParts.Add(bodyPart);
            }

            ik_DefaultAimPosition = transform.InverseTransformPoint(t_Head.transform.position + new Vector3(0f, aimHeightOffset, _ikAimSphereRad));

            //Get UID
            uid = gameObject.GetInstanceID();

            //Get the animator
            m_Animator = GetComponent<Animator>();

            //Get the shader controller
            r_Shader = GetComponent<ZXShaderController>();

            if (r_Shader == null)
                r_Shader = gameObject.AddComponent<ZXShaderController>();

            //Create targets list
            tr_AllTargets = new List<IZXTargetable>();
            tr_VisibleTargets = new SortedList<float, IZXTargetable>(new DuplicateKeyComparer<float>());
            tr_ObscuredTargets = new SortedList<float, IZXTargetable>(new DuplicateKeyComparer<float>());

            //Get all hashes
            m_PostureHashID = Animator.StringToHash("Posture");
            m_ActionHashID = Animator.StringToHash("Action");
            m_WeaponHashID = Animator.StringToHash("Weapon");
            m_xVelocityHashID = Animator.StringToHash("xVelocity");
            m_yVelocityHashID = Animator.StringToHash("yVelocity");
            m_zVelocityHashID = Animator.StringToHash("zVelocity");
            m_MagnitudeHashID = Animator.StringToHash("Magnitude");
            m_MotionTypeHashID = Animator.StringToHash("MovementType");

            m_xVelocityID = uid + "_xVid";
            m_yVelocityID = uid + "_yVid";
            m_zVelocityID = uid + "_zVid";
            m_MagnitudeID = uid + "_Mlid";
            m_AnimOverrideID = uid + "_overid";
            m_AnimUpperID = uid + "_upperid";

            //Get NavMeshAgent
            n_Agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
            n_Agent.updateRotation = false;
            n_Agent.stoppingDistance = 2.5f;
            newRotation = transform.rotation;

            //Get IK solvers
            ik_BipedIK = GetComponent<BipedIK>();

            if (ik_BipedIK != null)
            {
                ik_LookIK = ik_BipedIK.solvers.lookAt;
                ik_AimIK = ik_BipedIK.solvers.aim;
                ik_LookID = gameObject.GetInstanceID() + "_lid";
                ik_AimID = gameObject.GetInstanceID() + "_aid";
                ik_FaceID = gameObject.GetInstanceID() + "_fid";

                //Load IK aim ball
                ik_AimBall = Instantiate(Resources.Load<GameObject>("Ball")).transform;
                ik_AimBall.SetParent(ZXCombatUI.singleton.transform);
                ik_AimBall.gameObject.name = "ik_AimBall";
                ik_AimBall.GetComponent<MeshRenderer>().material.color = Color.blue;
                ik_AimBall.transform.localScale = new Vector3(1.25f, 1.25f, 1.25f);
                ik_BipedIK.solvers.aim.target = ik_AimBall;
                ik_BipedIK.solvers.lookAt.target = ik_AimBall;

                //Load IK waypoint ball
                ik_PointBall = Instantiate(Resources.Load<GameObject>("Ball")).transform;
                ik_PointBall.SetParent(ZXCombatUI.singleton.transform);
                ik_PointBall.gameObject.name = "ik_PointBall";
            }

            //Internal delegates
            d_MoveTranslation = new System.Action<float>(MoveTranslation);
            d_TrackTranslation = new System.Action<float>(TrackTranslation);
            d_TargetKilled = new System.Action<IZXTargetable>(TargetKilled);
            d_StateChange = new System.Action<ZXCombatUI.State>(StateChanged);
            d_Disable = new System.Action(Disable);

            d_Idle = new System.Func<int>(IdleBehavior);
            d_StdCheck = new System.Predicate<IZXTargetable>(StandardCheck);
            d_StdObjCheck = new System.Predicate<ZXObjective>(StandardObjectiveCheck);
            d_StdObjVisibilityCheck = new System.Predicate<ZXObjective>(StandardObjectiveVisibilityCheck);

            d_Visible = new System.Action(BecameVisible);
            d_Invisible = new System.Action(BecameInvisible);

            d_TargetingParams = d_StdCheck;
            d_EndBurn = new System.Action<ZXParticleEffect>(EndBurn);

            //Check for aura ability
            aura = GetComponent<ZXAura>();

            //Record starting health
            startingHealth = health;
            attackRange = meleeRange;

            //Record starting values
            original_Health = health;
            original_Damage = meleeDamage;

            //Get aura
            aura = GetComponent<ZXAura>();

        }

        protected virtual void Start()
        {
            transform.localScale = new Vector3(scale, scale, scale);

            initalized = true;
        }

        protected virtual void OnEnable()
        {
            health = startingHealth;

            ChangePosture(Posture.Stand);
            ChangeSpeed(Speed.Stop);
            SetIKState(IKState.All, false);
            SetAnimationState(AnimState.Base);

            ResetIKBalls();

            m_Animator.enabled = true;
        }

        public virtual void Register(bool ZXAI, bool RefPool)
        {
            if (ZXAI && !reg_ZXAI && health > 0)
            {
                //Register with AI spooler
                ZXCombatUI.Register((IZXAI)this);

                reg_ZXAI = true;

                if (aura != null)
                    aura.enabled = true;
            }
            else if (!ZXAI && reg_ZXAI)
            {
                //De-register with ai spooler
                ZXCombatUI.DeRegister((IZXAI)this);

                reg_ZXAI = false;

                if (aura != null)
                    aura.enabled = false;
            }

            if (RefPool && !reg_RefPool && health > 0)
            {
                ZXCombatUI.e_StateChange += d_StateChange;

                //Register with reflecting pool
                ZXReflectingPool.Register(this as ZXToon, transform);
                ZXReflectingPool.Register(this as IZXDamageable, transform);
                ZXReflectingPool.Register(this as IZXTargetable, transform);

                reg_RefPool = true;
            }
            else if (!RefPool && reg_RefPool)
            {
                ZXCombatUI.e_StateChange -= d_StateChange;

                ZXReflectingPool.DeRegister<ZXToon>(transform);
                ZXReflectingPool.DeRegister<IZXDamageable>(transform);
                ZXReflectingPool.DeRegister<IZXTargetable>(transform);

                reg_RefPool = false;
            }
        }

        public abstract void AdjustValues(int Level, int Index);
        public abstract void AdjustValues(int Level);

        protected void ChangedLevel(int Level)
        {
            if (e_LevelChanged != null)
                e_LevelChanged(Level);
        }

        private ZXBodypart.Bodypart IdentifyPart(Transform Object)
        {
            return PartsList.Find(delegate (ZXBodypart.Bodypart targetPart)
            {
                string[] searchStrings = targetPart.ToString().Split(_ParseChar);
                List<string> objectName = new List<string>(Object.gameObject.name.Split(_ParseChar));

                for (int i = 0; i < searchStrings.Length; i++)
                {
                    if (!objectName.Contains(searchStrings[i]))
                        return false;
                }

                return true;
            });
        }

        protected virtual void BecameVisible()
        {
            if (visible)
                return;

            Register(true, true);

            visible = true;
        }

        protected virtual void BecameInvisible()
        {
            if (!visible)
                return;

            Register(true, false);

            visible = false;
        }
        #endregion

        /* Runtime Update Functions */
        protected virtual void Update()
        {
            //Fire move translation
            if (de_CurrentMoveTranslation != null && posture != Posture.Ragdoll && posture != Posture.Prone)
                de_CurrentMoveTranslation(Time.deltaTime);

            //Process action queue
            if (health > 0)
                ProcessAction();

            if (actionWaitTimer > 0f)
            {
                actionWaitTimer -= Time.deltaTime;

                if (actionWaitTimer <= 0f)
                    EndAction();
            }

            if (maxRagdollTimer > 0f)
                maxRagdollTimer -= Time.deltaTime;

            if (tickMoveTimer && moveTimer > 0f)
            {
                moveTimer -= Time.deltaTime;
                if (e_MoveTimer != null)
                    e_MoveTimer(((float)moveTime - (float)moveTimer) / moveTime);

                if (moveTimer <= 0f && e_CanMove != null)
                {
                    e_CanMove(true);
                    tickMoveTimer = false;
                }
            }

            if (bloodTimer > 0f)
            {
                bloodTimer -= Time.deltaTime;
            }
        }

        protected virtual void LateUpdate()
        {
            if (blendFromRagFlag)
                BlendFromRagdoll();

            if (actionPostInstruction != null)
            {
                actionPostInstruction();
                actionPostInstruction = null;
            }
        }

        protected virtual void OnAnimatorMove()
        {
            //Apply root motion velocity
            if (m_Animator.enabled)
            {
                if (n_Agent.enabled)
                    n_Agent.velocity = m_Animator.velocity;

                currentVelocity = m_Animator.velocity;
            }

            //Apply adjusted rotation
            if (useAnimRotation)
            {
                transform.rotation = m_Animator.rootRotation;
                useAnimRotation = false;
            }
            else if (useSnapRotation)
            {
                transform.rotation = newRotation;
                useSnapRotation = false;

                if (ik_BipedIK != null)
                    ResetIKBalls();
            }
            else
                transform.rotation = newRotation;

            if (!lockAim && ik_BipedIK != null)
                AdjustAimBall();

            //Debug
            Debug.DrawLine(transform.position, n_Agent.steeringTarget, Color.yellow);
        }

        /* Player behavior user input */

        public virtual void PlayerInstruction(Instruction Instruct, Input In, object Args)
        {
            VectorLine line = null;

            //Cast args
            if (Args != null)
            {
                if (Args is Vector3)
                {
                }
                else if (Args is VectorLine)
                {
                    line = Args as VectorLine;
                }
            }

            //Process instruction
            switch (Instruct)
            {
                case Instruction.Move:
                    if (In == Input.Down) { } //Nothing
                    else if (In == Input.Drag) { } //Nothing

                    if (In == Input.Up)
                    {
                        waypoints = (VectorLine)Args;
                    }
                    break;

                case Instruction.Activate:

                    break;

                case Instruction.Debug:
                    logDebugging = !logDebugging;
                    break;
            }

            if (logUserInput)
                Debug.Log("UI Input: " + Instruct + ":" + In + " (" + Args + ")");
        }

        public void Invulnerable(bool Active)
        {
            invulnerable = Active;
        }

        public void Buff(ZXAura.Effect Buff, float Amount, float Duration)
        {
            switch (Buff)
            {
                case ZXAura.Effect.Healing:
                    HealDamage(Amount);
                    break;
            }
        }

        /* AI Functions */

        public virtual void Analyze()
        {
            List<IZXTargetable> tempList;

            //This is bad and needs to not happen!!!
            if (health <= 0)
            {
                Debug.Log(gameObject.name + " ran Analyze with 0 health, need to fix");
                return;
            }

            //Clear master list
            if (tr_AllTargets.Count > 0)
                tr_AllTargets.ForEach(delegate (IZXTargetable Object) { Object.e_Dead -= d_TargetKilled; });

            tr_AllTargets.Clear();

            //Clear visibility lists
            tr_VisibleTargets.Clear();
            tr_ObscuredTargets.Clear();

            //Get new list from reflecting pool
            tempList = ZXReflectingPool.FindInSphere(transform.position, agroRange, d_TargetingParams);

            if (tempList != null)
            {
                tr_AllTargets.AddRange(tempList);

                //Created sorted targeting data
                RaycastHit hit = new RaycastHit();
                float distance;
                bool visible;

                tr_AllTargets.ForEach(delegate (IZXTargetable Object)
                {
                    //Reg onto death event
                    Object.e_Dead += d_TargetKilled;

                    //Calculate distance and visibility
                    distance = Vector3.Distance(Center, Object.Center);
                    visible = !ZXReflectingPool.VisibilityCast(LookAtTransform, Object.LookAtTransform, out hit);

                    //Add to sorted lists
                    if (visible)
                        tr_VisibleTargets.Add(distance, Object);
                    else
                        tr_ObscuredTargets.Add(distance, Object);
                });
            }

            //Nexus checks
            tr_ClosestNexus = ZXReflectingPool.FindClosest<ZXInterestPoint>(transform, delegate (ZXInterestPoint Object)
            {
                return Object.PointType == ZXInterestPoint.Interest.Nexus;
            });

            //Map exit checks
            tr_ClosestExit = ZXReflectingPool.FindClosest<ZXMapExit>(transform, delegate (ZXMapExit Object)
            {
                return Object.Race == Race;
            });

            //Find current region
            ZXSpawnRegion newRegion = ZXReflectingPool.FindClosest<ZXSpawnRegion>(transform);

            if (newRegion != null && newRegion != tr_CurrentRegion)
            {
                if (tr_CurrentRegion != null)
                    tr_CurrentRegion.DeRegister(this);

                newRegion.Register(this);
                tr_CurrentRegion = newRegion;
            }
        }

        public void Act()
        {
            //Fire behavior translation
            if (de_CurrentBehaviorTranslation != null && posture != Posture.Ragdoll && posture != Posture.Prone)
                EnqueueAction(de_CurrentBehaviorTranslation());
        }

        public abstract void RecieveMessage(ZXInterestPoint Sender, Vector3 Location, ZXInterestPoint.Interest Header, ZXInterestPoint.BroadcastMessage Message);

        public void ResetZXAI()
        {
            FollowObject = null;
            attackTarget = null;
            waypoint = null;

            if (waypoints != null && waypoints.points3 != null)
            {
                waypoints.points3.Clear();
                waypoints.Draw3D();
                waypoints = null;
            }

            actionQueue.Clear();
            actionWaitFlag = false;

            if (logDebugging)
                Debug.Log("ZXAI Values Reset");
        }

        /* Movement Functions */
        #region
        public virtual void Place(Vector3 location, Quaternion rotation, object args)
        {

            Register(true, false);

            //Activate self
            if (!gameObject.activeSelf)
                gameObject.SetActive(true);

            //Set location
            transform.position = location;
            transform.rotation = rotation;

            newRotation = rotation;

            if (e_CanMove != null)
                e_CanMove(true);

            //Set starting teather position
            teather = transform.position;

            if (e_teatherSet != null)
                e_teatherSet(teather.Value, this);

            //Set IK balls into starting position
            ResetIKBalls();

            MasterAudio.PlaySound3DAtTransformAndForget(s_SpawnSound, transform);
        }

        //Moves the operator on the NavMesh
        public virtual void Move(VectorLine Waypoints)
        {
            bool valid = true;

            if (waypoints != null && waypoints.points3 != null)
            {
                for (int i = 0; i < waypoints.points3.Count; i++)
                {
                    if (waypoints.points3[i] == null)
                        valid = false;
                }
            }
            else
                valid = false;

            if (valid)
            {
                waypoints = Waypoints;
                Move(defaultMoveSpeed, waypoints.points3[0]);
            }
        }

        public virtual void Move(Vector3 Location)
        {
            if (ZXLevelBounds.Contains(Location))
            {
                Move(defaultMoveSpeed, Location);
            }
            else
            {
                waypoints = null;
                waypoint = null;
            }
        }

        protected virtual void Move(Speed TargetSpeed, Vector3 Location)
        {
            //Warp the char to the target location
            if (TargetSpeed == Speed.Stop)
            {
                ChangeSpeed(Speed.Stop);

                //Warp to position
                n_Agent.Warp(Location);
                //Set up new rotation
                newRotation = Quaternion.identity;
                transform.rotation = newRotation;

                //Warp the lookBall and aimBall
                ResetIKBalls();
            }
            //Set up move translation
            else if (!actionWaitFlag && actionQueue.Count <= 0)
            {
                UnityEngine.AI.NavMeshPath newPath = new UnityEngine.AI.NavMeshPath();
                UnityEngine.AI.NavMeshHit newLocation;
                bool navFailure = false;
                string errorCode = "Navigation Failure";

                try
                {
                    navFailure = !UnityEngine.AI.NavMesh.SamplePosition(Location, out newLocation, 50f, UnityEngine.AI.NavMesh.AllAreas);
                    navFailure = !UnityEngine.AI.NavMesh.CalculatePath(transform.position, newLocation.position, UnityEngine.AI.NavMesh.AllAreas, newPath);
                }
                catch
                {
                    navFailure = true;
                    errorCode = "Critical Navigation Failure";
                }

                if (navFailure)
                {
                    Debug.Log(gameObject.name + ": " + errorCode);
                    Die();
                }
                else
                {
                    //Set the anim state back to base
                    SetAnimationState(AnimState.Base);

                    ChangeSpeed(TargetSpeed);
                    n_Agent.path = newPath;
                }
            }
            else
            {
                waypoint = Location;
            }
        }

        //Drives movement during transition
        private void MoveTranslation(float deltaTime)
        {
            //Update the animator with new speed
            Vector3 velocity = Quaternion.Inverse(transform.rotation) * n_Agent.desiredVelocity;

            m_Animator.SetFloat(m_xVelocityHashID, velocity.x, _animLongDamp, deltaTime);
            m_Animator.SetFloat(m_yVelocityHashID, velocity.y, _animLongDamp, deltaTime);
            m_Animator.SetFloat(m_zVelocityHashID, velocity.z, _animLongDamp, deltaTime);
            m_Animator.SetFloat(m_MagnitudeHashID, velocity.magnitude, _animLongDamp, deltaTime);

            //Calculate target rotation
            Quaternion targetRotation;
            Vector3 lookDirection = (n_Agent.steeringTarget - transform.position).normalized;

            if (lookDirection != Vector3.zero)
                targetRotation = Quaternion.LookRotation(lookDirection);
            else
                targetRotation = Quaternion.identity;

            //Create new rotation
            newRotation = Quaternion.RotateTowards(transform.rotation, targetRotation, n_Agent.angularSpeed * deltaTime);

            //Stop when within stopping range
            if (n_Agent.remainingDistance <= n_Agent.stoppingDistance)
            {
                waypoint = null;

				if (waypoints != null && waypoints.points3 != null && waypoints.points3[0] != null)
                {
                    waypoints.points3.RemoveAt(0);
                    waypoints.Draw3D();

                    if (waypoints.points3.Count == 0)
                        waypoints = null;
                }

				if (waypoints == null || waypoints.points3 == null || waypoints.points3[0] == null)
                {
                    de_CurrentMoveTranslation = null;
                    ChangeSpeed(Speed.Stop);
                    tickMoveTimer = true;
                    moveTimer = moveTime;

                    if (e_CanMove != null)
                        e_CanMove(true);
                }
                else
                {
                    Move(defaultMoveSpeed, waypoints.points3[0]);

                    if (e_CanMove != null)
                        e_CanMove(false);
                }
            }
        }

        private void TrackTranslation(float deltaTime)
        {
            Vector3 targetPosition;
            Vector3 currentPosition;
            Vector3 targetDirection;
            float targetAngle;
            float targetSign;

            if (FollowObject != null)
                targetPosition = FollowObject.LookAtTransform.position;
            else
                return;

            currentPosition = transform.position;
            currentPosition.y = 0f;
            targetPosition.y = 0f;

            targetDirection = transform.InverseTransformDirection((targetPosition - currentPosition).normalized);

            targetAngle = Vector3.Angle(targetDirection, Vector3.forward);
            targetSign = Mathf.Sign(targetDirection.x);

            if (targetAngle > _turnSnapAngle)
            {
                Vector3 newDirection = (targetPosition - transform.position).normalized;
                newDirection.y = 0f;
                //newDirection.z = 0f;

                if (newDirection != Vector3.zero)
                    newRotation = Quaternion.LookRotation(newDirection);
                else
                    newRotation = transform.rotation;

                //Set for anim update
                useSnapRotation = true;
            }
            else if (targetAngle > _turnMinAngle)
            {
                Vector3 newDirection = (targetPosition - transform.position).normalized;
                newDirection.x = 0f;
                newDirection.z = 0f;

                float turnSpeed = n_Agent.angularSpeed * deltaTime;

                if (newDirection != Vector3.zero)
                    newRotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(newDirection), turnSpeed * targetSign);
                else
                    newRotation = transform.rotation;
            }
            else
            {
                newRotation = transform.rotation;
            }
        }

        /* Behavior Functions */
        protected int IdleBehavior()
        {
            return (int)defaultAction;
        }

        public float GetDistance(Vector3 Origin)
        {
            return Vector3.Distance(Origin, Center);
        }

        public Vector3 GetNearestPoint(Vector3 Origin)
        {
            return t_Body.GetCollider.ClosestPointOnBounds(Origin);
        }
        #endregion

        /* IK Functions */
        #region
        protected void SetIKState(IKState State, bool Active)
        {
            if (ik_BipedIK == null)
                return;

            //Update look IKState
            if (State == IKState.Look || State == IKState.All)
            {
                float target = Active ? 1f : 0f;

                if (ik_LookEnabled != Active)
                {
                    ik_LookEnabled = Active;

                    if (DOTween.IsTweening(ik_LookID))
                        DOTween.Kill(ik_LookID);

                    DOTween.To(delegate () { return ik_LookIK.IKPositionWeight; }, delegate (float val) { ik_LookIK.IKPositionWeight = val; }, target, _animIKDamp).SetId(ik_LookID);

                    if (logDebugging)
                        Debug.Log("Changed Aim IKstate to " + Active);
                }
            }

            //Update face IKState
            if (State == IKState.Face || State == IKState.All)
            {
                float target = Active ? 0.5f /*MAX*/ : 0f;

                if (ik_FaceEnabled != Active)
                {
                    ik_FaceEnabled = Active;

                    if (DOTween.IsTweening(ik_FaceID))
                        DOTween.Kill(ik_FaceID);

                    DOTween.To(delegate () { return ik_LookIK.bodyWeight; }, delegate (float val) { ik_LookIK.bodyWeight = val; }, target, _animIKDamp).SetId(ik_FaceID);

                    if (logDebugging)
                        Debug.Log("Changed Face IKstate to " + Active);
                }
            }

            //Update aim IKState
            if (State == IKState.Aim || State == IKState.All)
            {
                float target = Active ? 1f : 0f;

                if (ik_AimEnabled != Active)
                {
                    ik_AimEnabled = Active;

                    if (DOTween.IsTweening(ik_AimID))
                        DOTween.Kill(ik_AimID);

                    DOTween.To(delegate () { return ik_AimIK.IKPositionWeight; }, delegate (float val) { ik_AimIK.IKPositionWeight = val; }, target, _animIKDamp).SetId(ik_AimID);

                    //Make sure that animation state is set to override
                    if (Active && animState != AnimState.Override || animState != AnimState.UpperOverride)
                        SetAnimationState(AnimState.Override);

                    if (logDebugging)
                        Debug.Log("Changed Aim IKstate to " + Active);
                }
            }
        }

        protected void SetAnimationState(AnimState NewAnimState)
        {
            //Return if already set
            if (animState == NewAnimState)
                return;

            //Check to see if moving
            if (NewAnimState != AnimState.Base)
            {
                if (waypoint != null || waypoints != null)
                    return;
            }

            animState = NewAnimState;

            //Kill any exisiting tweens
            DOTween.Kill(m_AnimOverrideID);
            DOTween.Kill(m_AnimUpperID);

            switch (NewAnimState)
            {
                case AnimState.Base:
                    DOTween.To(delegate () { return m_Animator.GetLayerWeight(1); }, delegate (float val) { m_Animator.SetLayerWeight(1, val); }, 0, _animLongDamp).SetId(m_AnimOverrideID);
                    DOTween.To(delegate () { return m_Animator.GetLayerWeight(2); }, delegate (float val) { m_Animator.SetLayerWeight(2, val); }, 0, _animLongDamp).SetId(m_AnimUpperID);
                    break;
                case AnimState.Override:
                    DOTween.To(delegate () { return m_Animator.GetLayerWeight(1); }, delegate (float val) { m_Animator.SetLayerWeight(1, val); }, 1, _animLongDamp).SetId(m_AnimOverrideID).OnComplete(
                        delegate () { DOTween.To(delegate () { return m_Animator.GetLayerWeight(2); }, delegate (float val) { m_Animator.SetLayerWeight(2, val); }, 0, _animQuickDamp).SetId(m_AnimUpperID); });
                    break;
                case AnimState.UpperOverride:
                    DOTween.To(delegate () { return m_Animator.GetLayerWeight(2); }, delegate (float val) { m_Animator.SetLayerWeight(2, val); }, 1, _animLongDamp).SetId(m_AnimUpperID).OnComplete(
                        delegate () { DOTween.To(delegate () { return m_Animator.GetLayerWeight(1); }, delegate (float val) { m_Animator.SetLayerWeight(1, val); }, 0, _animQuickDamp).SetId(m_AnimOverrideID); });
                    break;
            }

            if (logDebugging)
                Debug.Log("Changed animation state to: " + animState);
        }

        protected ZXBodypart GetPart(ZXBodypart.Bodypart SearchPart)
        {
            return rb_BodyParts.Find(delegate (ZXBodypart Part) { return Part.Part == SearchPart; });

        }

        protected void ResetIKBalls()
        {
            if (ik_BipedIK != null)
            {
                ik_AimBall.position = transform.TransformPoint(ik_DefaultAimPosition);
                ik_PointBall.position = transform.TransformPoint(ik_DefaultAimPosition);
            }
        }

        protected void AdjustAimBall()
        {
            /*Aim balls are blue*/
            /*Point balls are yellow*/

            //Adjust ik_AimBall
            if (ik_BipedIK != null)
            {
                Vector3 targetPoint;
                Vector3 worldPos;
                Vector3 localPos;

                //Set position of pointball
                if (FollowObject != null)
                    ik_PointBall.position = FollowObject.Center;
                else
                    ik_PointBall.position = transform.TransformPoint(ik_DefaultAimPosition);

                //Get world position and local position
                worldPos = Vector3.MoveTowards(ik_AimBall.position, ik_PointBall.position, ik_AimSpeed * Time.deltaTime);
                localPos = transform.InverseTransformPoint(worldPos);

                //Clamp the values
                targetPoint = new Vector3(Mathf.Clamp(localPos.x, -10, 10), Mathf.Clamp(localPos.y, 0f, 5f), Mathf.Clamp(localPos.z, 3.5f, 15f));

                //Convert back to world space
                targetPoint = transform.TransformPoint(targetPoint);

                ik_AimBall.position = targetPoint;
            }
        }

        protected void Ragdoll(ZXDamage Damage)
        {
            //Get body part
            ZXBodypart part = rb_PhysicsParts[Random.Range(0, rb_PhysicsParts.Count)];
            Vector3 point = part.ClosestPointOnBounds(Damage.Trajectory.origin);
            Vector3 adjForce = Damage.Force * Damage.ForceMod;

            //Clamp force
            if (Damage.DamageType == ZXDamage.Type.Explosive && adjForce.magnitude > _maxPhysicsExplosiveForce)
                adjForce = adjForce.normalized * _maxPhysicsExplosiveForce;
            else if (adjForce.magnitude > _maxPhysicsForce)
                adjForce = adjForce.normalized * _maxPhysicsForce;

            if (posture != Posture.Ragdoll)
            {
                ChangePosture(Posture.Ragdoll);
                StartCoroutine(RagdollWaitUntilStopped());
            }

            if (e_Ragdoll != null)
                e_Ragdoll(adjForce);

            //Apply physics force

            switch (Damage.DamageType)
            {
                case ZXDamage.Type.Explosive:
                    rb_RigidBodyParts.ForEach(delegate (ZXBodypart Part)
                    {
                        Part.rigidbody.AddExplosionForce(adjForce.magnitude * Part.Weight, Damage.Trajectory.origin, Damage.Radius, 3f);
                    });
                    break;

                default:
                    part.rigidbody.AddForceAtPosition(adjForce * t_Body.Weight, point);
                    break;
            }
        }

        protected IEnumerator RagdollWaitUntilStopped()
        {
            //Pause for the ragdoll to take effect
            yield return new WaitForSeconds(_waitUntilStoppedDelay);

            //Wait until the spine velocity is zero    
            while (t_Body != null && t_Body.Velocity.magnitude > 0.05f)
            {
                if (maxRagdollTimer <= 0f)
                {
                    rb_RigidBodyParts.ForEach(delegate (ZXBodypart Part)
                    {
                        Part.rigidbody.Sleep();
                    });
                }
                else
                    yield return new WaitForSeconds(0.5f);
            }

            //Fade out the char
            if (health == 0)
            {
                r_Shader.Fade(_fadeDelay, _fadeTime, _disableDelay, d_Disable);
                PlaceBlood();
            }
            else
                EnqueueAction((int)Action.FromRagdoll);
        }

        protected void StartRagdollLerp()
        {
            //Store the ragdolled position for blending
            foreach (BodyPart b in bodyParts)
            {
                b.storedRotation = b.transform.rotation;
                b.storedPosition = b.transform.position;
            }

            ragdollingEndTime = Time.time;

            //Remember some key positions
            ragdolledFeetPosition = 0.5f * (m_Animator.GetBoneTransform(HumanBodyBones.LeftFoot).position + m_Animator.GetBoneTransform(HumanBodyBones.RightFoot).position);
            ragdolledHeadPosition = m_Animator.GetBoneTransform(HumanBodyBones.Head).position;
            ragdolledHipPosition = m_Animator.GetBoneTransform(HumanBodyBones.Hips).position;

            //Determine orientation
            if (Vector3.Dot(t_Body.transform.forward, Vector3.up) > 0)
                EnqueueAction((int)Action.StandFromBack);
            else
                EnqueueAction((int)Action.StandFromBelly);
        }

        private void BlendFromRagdoll()
        {
            if (Time.time <= ragdollingEndTime + mecanimToGetUpTransitionTime)
            {
                //If we are waiting for Mecanim to start playing the get up animations, update the root of the mecanim
                //character to the best match with the ragdoll
                Vector3 animatedToRagdolled = ragdolledHipPosition - m_Animator.GetBoneTransform(HumanBodyBones.Hips).position;
                Vector3 newRootPosition = transform.position + animatedToRagdolled;

                //Now cast a ray from the computed position downwards and find the highest hit that does not belong to the character 
                RaycastHit[] hits = Physics.RaycastAll(new Ray(newRootPosition, Vector3.down));
                newRootPosition.y = 0;
                foreach (RaycastHit hit in hits)
                {
                    if (!hit.transform.IsChildOf(transform))
                    {
                        newRootPosition.y = Mathf.Max(newRootPosition.y, hit.point.y);
                    }
                }
                transform.position = newRootPosition;

                //Get body orientation in ground plane for both the ragdolled pose and the animated get up pose
                Vector3 ragdolledDirection = ragdolledHeadPosition - ragdolledFeetPosition;
                ragdolledDirection.y = 0;

                Vector3 meanFeetPosition = 0.5f * (m_Animator.GetBoneTransform(HumanBodyBones.LeftFoot).position + m_Animator.GetBoneTransform(HumanBodyBones.RightFoot).position);
                Vector3 animatedDirection = m_Animator.GetBoneTransform(HumanBodyBones.Head).position - meanFeetPosition;
                animatedDirection.y = 0;

                //Try to match the rotations. Note that we can only rotate around Y axis, as the animated characted must stay upright,
                //hence setting the y components of the vectors to zero. 
                transform.rotation *= Quaternion.FromToRotation(animatedDirection.normalized, ragdolledDirection.normalized);
            }
            //compute the ragdoll blend amount in the range 0...1
            float ragdollBlendAmount = 1.0f - (Time.time - ragdollingEndTime - mecanimToGetUpTransitionTime) / ragdollToMecanimBlendTime;
            ragdollBlendAmount = Mathf.Clamp01(ragdollBlendAmount);

            //In LateUpdate(), Mecanim has already updated the body pose according to the animations. 
            //To enable smooth transitioning from a ragdoll to animation, we lerp the position of the hips 
            //and slerp all the rotations towards the ones stored when ending the ragdolling
            foreach (BodyPart b in bodyParts)
            {
                if (b.transform != transform)
                { //this if is to prevent us from modifying the root of the character, only the actual body parts
                  //position is only interpolated for the hips
                    if (b.transform == m_Animator.GetBoneTransform(HumanBodyBones.Hips))
                    {
                        b.transform.position = Vector3.Lerp(b.transform.position, b.storedPosition, ragdollBlendAmount);
                    }
                    //rotation is interpolated for all body parts
                    b.transform.rotation = Quaternion.Slerp(b.transform.rotation, b.storedRotation, ragdollBlendAmount);
                }
            }

            //if the ragdoll blend amount has decreased to zero, move to animated state
            if (ragdollBlendAmount == 0)
            {
                blendFromRagFlag = false;
                Invoke("EndRagdoll", 1f);
            }
        }
        #endregion

        /* Public Settings */
        public virtual void ChangeDefautSpeed(Speed NewDefaultSpeed)
        {
            defaultMoveSpeed = NewDefaultSpeed;
        }

        /* Combat Functions */
        public virtual void TakeDamage(ZXDamage Damage)
        {
            ZXParticleEffect damageEffect = null;
            Quaternion rotation;
            bool kill = false;

            if (Damage.DamageType == ZXDamage.Type.Gas && ignoreGas || invulnerable)
                return;

            //Adjust health
            if (health > 0)
            {
                //Adjust damage to account for amour and penetration
                float adjDamage = Damage.Damage - (Damage.Damage * Mathf.Clamp(armour - Damage.Penetration, 0f, 0.9f));

                health = Mathf.Clamp(health - adjDamage, 0, startingHealth);

                if (r_Shader != null)
                    //r_Shader.BlinkEmission();

                //Raise damage event
                if (e_HealthChanged != null)
                    e_HealthChanged(health / startingHealth, adjDamage);

                if (health == 0)
                {
                    Die();

                    kill = true;
                }
            }

            //Damage effect
            if (Damage.DamageType == ZXDamage.Type.Projectile || Damage.DamageType == ZXDamage.Type.Impact || Damage.DamageType == ZXDamage.Type.Explosive)
            {
                if (bloodTimer <= 0f)
                {
                    damageEffect = ZXCombatUI.p_BloodSplatter.Request();
                    bloodTimer = Random.value * _avgBloodEffectDelay;
                }
            }

            if (damageEffect != null)
            {
                if (Damage.Trajectory.direction != Vector3.zero)
                    rotation = Quaternion.LookRotation(Damage.Trajectory.direction);
                else
                    rotation = Quaternion.identity;

                damageEffect.Place(Center, rotation, t_Body.transform);
            }

            //Determine if still alive
            if (kill || posture == Posture.Ragdoll)
            {
                Ragdoll(Damage);
            }
            else if (health > 0 && playDamageAnimations && posture != Posture.Prone)
            {
                TakeDamageEffect(Damage);
            }
        }

        protected abstract void TakeDamageEffect(ZXDamage Damage);

        public virtual void HealDamage(float Amount)
        {
            if (health > 0 && !ignoreHealing)
            {
                health = Mathf.Clamp(health + Amount, 0, startingHealth);

                if (e_HealthChanged != null)
                    e_HealthChanged(health / startingHealth, Amount);
            }
        }

        public virtual void Burn()
        {
            if (burningEffect == null)
            {
                burningEffect = ZXCombatUI.p_Fire.Request();

                if (burningEffect != null)
                {
                    burningEffect.Place(t_Body.transform.position, t_Body.transform.rotation, t_Body.transform);
                    burningEffect.e_OnDisable += d_EndBurn;

                    StartCoroutine(BurnDamager());
                }
            }
        }

        protected virtual IEnumerator BurnDamager()
        {
            while (burningEffect != null && health > 0f)
            {
                ZXDamage damage = new ZXDamage(Mathf.RoundToInt(_burnDPS / _burnTickRate), 0.9f, 1f, 0f, ToonRace.None, ZXDamage.Type.Fire, new Ray(transform.position, Vector3.up), ZXDamage.SpecialEffect.None);

                TakeDamage(damage);

                yield return new WaitForSeconds(_burnTickRate);
            }
        }

        public virtual void EndBurn(ZXParticleEffect BurnEffect)
        {
            burningEffect.e_OnDisable -= d_EndBurn;
            burningEffect = null;
        }

        protected virtual void Die()
        {
            //Signal health
            health = 0;

            if (e_HealthChanged != null)
                e_HealthChanged(0f, 0f);

            //Reset all AI routines and deregister from object pools
            ResetZXAI();
            Register(false, false);

            //Stop blending from rag (if applicable)
            blendFromRagFlag = false;

            //Play death sound
            MasterAudio.PlaySound3DAtTransformAndForget(s_DeathSound, transform);

            //Raise event
            if (e_Dead != null)
                e_Dead(this);
        }

        protected void PlaceBlood()
        {
            ZXParticleEffect bloodPool = ZXCombatUI.p_BloodPoolLarge.Request();
            List<RaycastHit> hits = new List<RaycastHit>(Physics.RaycastAll(t_Body.transform.position, Vector3.down));
            RaycastHit groundHit = hits.Find(delegate (RaycastHit hit)
            {
                ZXFXable fxable = hit.transform.GetComponentInParent<ZXFXable>();

                if (fxable != null)
                    return fxable.MaterialType == ZXFXable.Material.Ground;
                else
                    return false;
            });

            if (bloodPool != null && groundHit.collider != null)
            {
                bloodPool.Place(groundHit.point + new Vector3(0f, 0.2f, 0f), Quaternion.Euler(groundHit.normal), null);
            }
        }

        public void ExitScene()
        {
            //Reset all AI routines and deregister from object pools
            ResetZXAI();
            Register(false, false);

            //Stop blending from rag (if applicable)
            blendFromRagFlag = false;

            //Raise event
            if (e_Dead != null)
                e_Dead(this);
        }

        /* Action Interpretation */
        #region
        protected void ChangeSpeed(Speed NewSpeed)
        {
            //Make sure speed is changing
            if (speed == NewSpeed)
                return;

            //Update speed
            speed = NewSpeed;

            //Update agent
            n_Agent.speed = SpeedToFloat(speed);

            if (speed == Speed.Stop)
            {
                if (n_Agent.isActiveAndEnabled && n_Agent.isOnNavMesh)
                    n_Agent.isStopped = true;

                n_Agent.velocity = Vector3.zero;

                DOTween.Kill(m_xVelocityID);

                if (m_Animator.gameObject.activeSelf)
                    DOTween.To(delegate () { return m_Animator.GetFloat(m_xVelocityHashID); }, delegate (float val) { m_Animator.SetFloat(m_xVelocityHashID, val); }, 0f, _animLongDamp).SetId(m_xVelocityID);
                else
                    m_Animator.SetFloat(m_xVelocityHashID, 0);

                DOTween.Kill(m_yVelocityID);

                if (m_Animator.gameObject.activeSelf)
                    DOTween.To(delegate () { return m_Animator.GetFloat(m_yVelocityHashID); }, delegate (float val) { m_Animator.SetFloat(m_yVelocityHashID, val); }, 0f, _animLongDamp).SetId(m_yVelocityID);
                else
                    m_Animator.SetFloat(m_yVelocityHashID, 0);

                DOTween.Kill(m_zVelocityID);

                if (m_Animator.gameObject.activeSelf)
                    DOTween.To(delegate () { return m_Animator.GetFloat(m_zVelocityHashID); }, delegate (float val) { m_Animator.SetFloat(m_zVelocityHashID, val); }, 0f, _animLongDamp).SetId(m_zVelocityID);
                else
                    m_Animator.SetFloat(m_zVelocityHashID, 0);

                DOTween.Kill(m_MagnitudeID);

                if (m_Animator.gameObject.activeSelf)
                    DOTween.To(delegate () { return m_Animator.GetFloat(m_MagnitudeHashID); }, delegate (float val) { m_Animator.SetFloat(m_MagnitudeHashID, val); }, 0f, _animLongDamp).SetId(m_MagnitudeID);
                else
                    m_Animator.SetFloat(m_MagnitudeHashID, 0);

                //Null out move trans delegate                
                de_CurrentMoveTranslation = d_TrackTranslation;

                SetIKState(IKState.All, false);
                SetAnimationState(AnimState.Override);
            }
            else
            {
                if (!n_Agent.enabled)
                    n_Agent.enabled = true;

                if (!n_Agent.isOnNavMesh)
                    Die();
                else
                    n_Agent.isStopped = false;

                SetIKState(IKState.All, false);
                SetAnimationState(AnimState.Base);

                //Assign move trans delegate
                de_CurrentMoveTranslation = d_MoveTranslation;
            }

            //Fire event
            if (e_SpeedChanged != null)
                e_SpeedChanged(speed);

            if (logDebugging)
                Debug.Log("Changed speed to: " + speed);
        }

        public void ChangePostureTesting(Posture NewPosture)
        {
            if (NewPosture == Posture.Stand)
            {
                EnqueueAction((int)Action.FromRagdoll);
            }
            else
            {
                ChangePosture(NewPosture);
            }
        }

        protected void ChangePosture(Posture NewPosture)
        {
            if (posture == NewPosture)
                return;

            if (NewPosture == Posture.Ragdoll)
            {
                ChangeSpeed(Speed.Stop);
                SetAnimationState(AnimState.Override);
                KillTweens();

                n_Agent.enabled = false;
                m_Animator.enabled = false;
                playDamageAnimations = false;

                if (ik_BipedIK != null)
                    ik_BipedIK.enabled = false;

                rb_BodyParts.ForEach(delegate (ZXBodypart Object) { Object.SetPhysics(true); });

                maxRagdollTimer = _maxRagdollTime;
            }
            else if (posture == Posture.Ragdoll)
            {
                //Make sure ragdoll is turned off
                rb_BodyParts.ForEach(delegate (ZXBodypart Object) { Object.SetPhysics(false); });

                m_Animator.enabled = true;
                n_Agent.enabled = true;
                playDamageAnimations = true;

                SetIKState(IKState.All, false);
                SetAnimationState(AnimState.Base);

                maxRagdollTimer = 0f;
            }

            posture = NewPosture;

            //Update animator
            m_Animator.SetInteger(m_PostureHashID, (int)posture);

            //Fire event
            if (e_PostureChanged != null)
                e_PostureChanged(posture);

            if (logDebugging)
                Debug.Log("Changed posture to: " + posture);
        }

        protected void EnqueueAction(int ActionCode)
        {
            if (currentAction != ActionCode && !actionQueue.Contains(ActionCode))
            {
                actionQueue.Enqueue(ActionCode);

                if (logDebugging)
                    Debug.Log("Enqued: " + (Action)ActionCode);
            }
        }

        private void ProcessAction()
        {
            if (!actionWaitFlag)
            {
                int action = speed == Speed.Stop ? (int)defaultAction : (int)defaultMoveAction;

                //If there are actions in the queue
                if (actionQueue.Count > 0)
                {
                    action = actionQueue.Dequeue();
                }
                //If there are VL waypoints left to move to
                else if (waypoints != null && speed == Speed.Stop)
                {
                    Move(waypoints);

                    if (logDebugging)
                        Debug.Log("VL: Moving");
                }
                //If there is a waypoint
                else if (waypoint != null && speed == Speed.Stop)
                {
                    Move(defaultMoveSpeed, waypoint.Value);

                    if (logDebugging)
                        Debug.Log("V3: Moving");
                }

                if (action != currentAction)
                {
                    currentAction = action;
                    m_Animator.SetInteger(m_ActionHashID, action);

                    PerformAction(action);

                    if (logDebugging)
                        Debug.Log((Action)action);
                }
            }
        }

        protected virtual void PerformAction(int ActionCode)
        {
            switch (ActionCode)
            {
                case (int)Action.FromRagdoll:
                    StartRagdollLerp();
                    break;

                case (int)Action.StandFromBack:
                    blendFromRagFlag = true;
                    ChangePosture(Posture.Prone);
                    actionWaitFlag = true;
                    break;

                case (int)Action.StandFromBelly:
                    blendFromRagFlag = true;
                    ChangePosture(Posture.Prone);
                    actionWaitFlag = true;
                    break;

                default:
                    throw new System.NotImplementedException("Action code not supported: " + (Action)ActionCode);
            }
        }

        public abstract void AnimationCallback(int Code);

        protected virtual void EndAction()
        {
            attackTarget = null;
            actionWait = false;
            actionWaitTimer = 0f;
            lockAim = false;

            if (logDebugging)
                Debug.Log("EndAction()");
        }

        protected void EndRagdoll()
        {
            ChangePosture(Posture.Stand);

            EndAction();

            if (logDebugging)
                Debug.Log("Ragdoll animation complete");
        }

        protected void TargetKilled(IZXTargetable Object)
        {
            if (FollowObject == Object)
            {
                FollowObject = null;
                attackTarget = null;
            }

            if (tr_AllTargets != null && tr_AllTargets.Contains(Object))
                tr_AllTargets.Remove(Object);

            if (tr_VisibleTargets != null && tr_VisibleTargets.ContainsValue(Object))
                tr_VisibleTargets.RemoveAt(tr_VisibleTargets.IndexOfValue(Object));

            if (tr_ObscuredTargets != null && tr_ObscuredTargets.ContainsValue(Object))
                tr_ObscuredTargets.RemoveAt(tr_ObscuredTargets.IndexOfValue(Object));
        }
        #endregion

        /* Physics Override Functions */
        protected void FindNavMeshTeather()
        {
            UnityEngine.AI.NavMeshHit newLocation;

            if (UnityEngine.AI.NavMesh.SamplePosition(gameObject.transform.position, out newLocation, 100f, UnityEngine.AI.NavMesh.AllAreas))
            {
                teather = newLocation.position;
            }
            else
            {
                Debug.Log("Failed to find NavMeshPoint to teather");
            }
        }

        protected virtual void SnapToTeather()
        {
            if (teather == null)
                return;

            transform.position = Vector3.MoveTowards(transform.position, teather.Value, _SnapToTeatherSpeed * Time.deltaTime);

            if (transform.position == teather.Value)
            {
                teather = null;
            }
        }

        public void PhysicsOverride(bool Override)
        {
            //Toggle NavMeshAgent
            n_Agent.enabled = !Override;
        }

        public void ActionLock(bool Lock)
        {
            actionWaitFlag = Lock;
        }

        public void AnimatorOverride(AnimState State, int ActionCode)
        {
            if (logDebugging)
                Debug.Log("AnimatorOverride: " + (Action)ActionCode);

            SetIKState(IKState.All, false);
            SetAnimationState(State);
            m_Animator.SetInteger(m_ActionHashID, ActionCode);
            currentAction = ActionCode;

            if (logDebugging)
                Debug.Log("AnimatorOverride: " + (Action)ActionCode);
        }

        /* Destruction */
        #region

        private void Disable()
        {
            ChangePosture(Posture.Stand);

            gameObject.SetActive(false);
        }

        //Called when victory state changed
        protected abstract void StateChanged(ZXCombatUI.State State);

        protected virtual void KillTweens()
        {
            if (!initalized)
                return;

            if (DOTween.IsTweening(m_xVelocityID))
                DOTween.Complete(m_xVelocityID, true);

            if (DOTween.IsTweening(m_yVelocityID))
                DOTween.Complete(m_yVelocityID, true);

            if (DOTween.IsTweening(m_zVelocityID))
                DOTween.Complete(m_zVelocityID, true);

            if (DOTween.IsTweening(m_MagnitudeID))
                DOTween.Complete(m_MagnitudeID, true);

            if (DOTween.IsTweening(m_AnimOverrideID))
                DOTween.Complete(m_AnimOverrideID, true);

            if (DOTween.IsTweening(m_AnimUpperID))
                DOTween.Complete(m_AnimUpperID, true);

            if (ik_LookID != null && DOTween.IsTweening(ik_LookID))
                DOTween.Kill(ik_LookID);

            if (ik_FaceID != null && DOTween.IsTweening(ik_FaceID))
                DOTween.Kill(ik_FaceID);

            if (ik_AimID != null && DOTween.IsTweening(ik_AimID))
                DOTween.Kill(ik_AimID);
        }

        //Called when the object is disabled
        protected virtual void OnDisable()
        {
            //Cancel all invokes
            CancelInvoke();
            StopAllCoroutines();
            KillTweens();

            ResetZXAI();

            rb_Corpse.enabled = false;

            if (e_OnDisable != null)
                e_OnDisable(this);

            if (logDebugging)
                Debug.Log(gameObject.name + " was disabled");
        }

        //Called when the object is destroyed
        protected virtual void OnDestroy()
        {
            if (e_OnDestroy != null)
                e_OnDestroy(this);
        }
        #endregion

        /* Static Support Functions */
        /// <summary>
        /// Converts a speed into a floating point value
        /// </summary>
        /// <param name="TargetSpeed">Speed to convert</param>
        /// <returns></returns>
        public static float SpeedToFloat(Speed TargetSpeed)
        {
            switch (TargetSpeed)
            {
                case Speed.Stop:
                    return 0f;
                case Speed.Walk:
                    return 0.333f;
                case Speed.PowerWalk:
                    return 0.455f;
                case Speed.Jog:
                    return 0.655f;
                case Speed.Run:
                    return 0.855f;
                case Speed.Sprint:
                    return 1f;
                default:
                    throw new System.Exception("SpeedToTime(Speed): This speed has not been implimented");
            }
        }

        private bool StandardCheck(IZXTargetable Object)
        {
            return Object.Race != Race && Object.isAlive && Vector3.Distance(Base, Object.Base) <= agroRange;
        }

        private bool StandardObjectiveCheck(ZXObjective Object)
        {
            switch (Object.Type)
            {
                case ZXObjective.ObjectiveType.KOTH:
                    return true;

                case ZXObjective.ObjectiveType.KOTH_Rescue:

                    if (Object.Race == Race)
                        return true;
                    else
                        return false;

                case ZXObjective.ObjectiveType.KOTH_Vehicle:

                    if (Object.Race == Race)
                        return true;
                    else
                        return false;

                case ZXObjective.ObjectiveType.KOTH_Repair:

                    if (Object.Race == Race)
                        return true;
                    else
                        return false;

                default:
                    Debug.Log("Objective type not recognized");
                    return false;
            }
        }

        private bool StandardObjectiveVisibilityCheck(ZXObjective Object)
        {
            RaycastHit hit = new RaycastHit();

            return StandardObjectiveCheck(Object) && !ZXReflectingPool.VisibilityCast(transform, Object.transform, out hit);
        }
    }

    /// <summary>
    /// Base class for all weapons
    /// </summary>
    [System.Serializable]
    public abstract class ZXWeapon : MonoBehaviour
    {
        //Constants
        private const float _fadeDelay = 5f;
        private const float _fadeTime = 1f;

        /* Support Types */
        public enum Type
        {
            None = 0,

            //Melee (100-199)
            Katana = 100,
            Machete = 101,
            Spear = 102,
            Pickaxe = 103,
            Knife = 104,
			Cleaver = 105,
			Bat = 106,
			FireAxe = 107,

            //Firearms (200-299)
            Pistol = 200,
            Shotgun = 201,
            SubMachineGun = 202,
            Rifle = 203,
            AssualtRifle = 204,
            FlameThrower = 205,
            MiniGun = 206,
            RPG = 207,
            Crossbow = 208,
        }

        //Inspector variables
        [SerializeField]
        protected Type weaponType;
        [SerializeField]
        protected ZXDamage.SpecialEffect weaponEffect;
        [SerializeField]
        protected ZXHolster.Holster targetHolster;
        [SerializeField]
        protected bool discardAfterUse;
        [SerializeField]
        protected float weaponRange;
        [SerializeField]
        protected int weaponDamage;
        protected float weaponWeight = 15f;
        [SerializeField]
        [Range(0f, 0.9f)]
        protected float penetration;
        [SerializeField]
        protected float forceMod;

        //Transforms
        protected Transform t_Grip;

        //Accessors
        public Type WeaponType
        {
            get
            {
                return weaponType;
            }
        }
        public float WeaponRange
        {
            get
            {
                return weaponRange;
            }
        }
        public int WeaponDamage
        {
            get
            {
                return weaponDamage;
            }
        }
        public ZXHolster.Holster TargetHolster
        {
            get
            {
                return targetHolster;
            }
        }
        public bool isDiscard
        {
            get
            {
                return discardAfterUse;
            }
        }
        public virtual Transform AimTransform
        {
            get
            {
                return t_Grip;
            }
        }
        public ToonRace Race
        {
            get
            {
                if (parent != null)
                    return parent.Race;
                else
                    return ToonRace.None;
            }
        }

        //Delegates
        protected System.Action d_Disable;
        protected System.Predicate<IZXDamageable> d_DamageFilter;

        //Private variables
        protected ZXShaderController r_Shader;
        protected List<ZXWeaponPoint> weaponPoints;
        protected new Collider collider;
        protected new Rigidbody rigidbody;
        protected ZXToon parent;
        protected Vector3 relativeLocation;
        protected Quaternion relativeRotation;

        //Initalization
        protected virtual void Awake()
        {
            //Get mesh renderer and create collider and rigidbody
            rigidbody = gameObject.AddComponent<Rigidbody>() as Rigidbody;
            collider = gameObject.AddComponent<BoxCollider>() as Collider;

            rigidbody.mass = weaponWeight;

            //Create weapon points
            weaponPoints = new List<ZXWeaponPoint>(GetComponentsInChildren<ZXWeaponPoint>());

            d_Disable = new System.Action(Disable);
            d_DamageFilter = new System.Predicate<IZXDamageable>(DamageablesFilter);

            //Get shader controller
            r_Shader = GetComponent<ZXShaderController>();

            if (r_Shader == null)
                r_Shader = gameObject.AddComponent<ZXShaderController>();

            //Set grip
            t_Grip = weaponPoints.Find(delegate (ZXWeaponPoint Obj) { return Obj.PartType == ZXWeaponPoint.Part.Grip; }).transform;
            t_Grip.transform.SetParent(null);
            transform.SetParent(t_Grip);

            relativeLocation = transform.localPosition;
            relativeRotation = transform.localRotation;
        }

        protected virtual void OnEnable()
        {
            r_Shader.Reset(false);
            ResetDrop();
        }

        public virtual void SetWeaponDamage(int Damage)
        {
            weaponDamage = Damage;
        }

        public abstract int Fire();

        public virtual void SetToon(ZXToon Parent)
        {
            parent = Parent;
        }

        public virtual void SnapToTransform(ZXHolster holster)
        {
            t_Grip.transform.SetParent(holster.transform);
            t_Grip.transform.localPosition = Vector3.zero;
            t_Grip.transform.localRotation = Quaternion.identity;

            gameObject.SetActive(true);
        }

        public virtual void ResetDrop()
        {
            collider.enabled = false;
            rigidbody.useGravity = false;
            rigidbody.isKinematic = true;

            transform.localPosition = relativeLocation;
            transform.localRotation = relativeRotation;
        }

        public virtual void Drop()
        {
            t_Grip.SetParent(null);
            collider.enabled = true;
            rigidbody.useGravity = true;
            rigidbody.isKinematic = false;

            rigidbody.AddRelativeForce(weaponWeight * 50f, 0f, weaponWeight * 150f);
            rigidbody.AddRelativeTorque(0f, weaponWeight * 150f, weaponWeight * -50f);

            r_Shader.Fade(5f, 2.5f, 1f, d_Disable);
        }

        protected void Disable()
        {
            gameObject.SetActive(false);
        }

        protected bool DamageablesFilter(IZXDamageable Damageable)
        {
            if (Damageable != null && parent != null)
                return Damageable.Race != parent.Race;
            else
                return true;
        }
    }

    public abstract class ZXMeleeWeapon : ZXWeapon
    {
        public enum Quad
        {
            Front_Right = 0,
            Front_Left = 1,
            Rear_Right = 2,
            Rear_Left = 3,
        }

        [SerializeField]
        protected float attackSpeed;
        [SoundGroupAttribute]
        [SerializeField]
        protected string s_Attack;
        [SoundGroupAttribute]
        [SerializeField]
        protected string s_Hit;

        [SerializeField] protected bool useSpecialAttack;
        [SerializeField] protected float specialAttackTimer; 

        [SerializeField]
        protected ZXDamage.SpecialEffect damageEffect;
        protected ParticleSystem trailParticle;

        public float AttackSpeed
        {
            get
            {
                return attackSpeed;
            }
        }

        protected override void Awake()
        {
            base.Awake();

            trailParticle = GetComponentInChildren<ParticleSystem>();
        }

        public override int Fire()
        {
            MasterAudio.PlaySound3DAtTransformAndForget(s_Attack, transform);

            if (useSpecialAttack && specialAttackTimer <= 0f)
            {
                return (int)ZXToon.Action.SpecialAttack;
            }
            else
                return (int)ZXToon.Action.MeleeGeneric;
        }

        public virtual void DealDamage(IZXTargetable Target)
        {
            Ray ray = new Ray(transform.position, transform.TransformDirection(Vector3.forward));
            ZXDamage damage = new ZXDamage(Mathf.RoundToInt(weaponDamage), penetration, forceMod, 0f, parent.Race, ZXDamage.Type.Impact, ray, damageEffect);

            if (Target != null)
            {
                MasterAudio.PlaySound3DAtTransformAndForget(s_Hit, transform);

                Target.TakeDamage(damage);
            }
        }

        public virtual void DealDamage(Quad Quadrant)
        {
            Ray traj;
            ZXDamage damage;

            List<IZXDamageable> DamageableList = ZXReflectingPool.FindInSphere<IZXDamageable>(transform.position, weaponRange, d_DamageFilter);
            List<IZXDamageable> finalDamageables = new List<IZXDamageable>(5);

            if (DamageableList != null)
            {
                Vector3 direction;

                foreach (IZXDamageable damageable in DamageableList)
                {
                    //Get direction in world space
                    direction = (damageable.Center - parent.Center).normalized;

                    //Transform to local rotation
                    direction = parent.transform.InverseTransformDirection(direction);

                    //Test quad
                    switch (Quadrant)
                    {
                        case Quad.Front_Right:
                            if (direction.z >= 0f && direction.x >= 0f)
                                finalDamageables.Add(damageable);
                            break;

                        case Quad.Front_Left:
                            if (direction.z >= 0f && direction.x <= 0f)
                                finalDamageables.Add(damageable);
                            break;

                        case Quad.Rear_Right:
                            if (direction.z <= 0f && direction.x >= 0f)
                                finalDamageables.Add(damageable);
                            break;

                        case Quad.Rear_Left:
                            if (direction.z <= 0f && direction.x <= 0f)
                                finalDamageables.Add(damageable);
                            break;
                    }
                }
            }

            //Deal damage
            if (finalDamageables != null && finalDamageables.Count > 0)
            {
                MasterAudio.PlaySound3DAtTransformAndForget(s_Hit, transform);

                for (int i = 0; i < finalDamageables.Count; i++)
                {
                    traj = new Ray(parent.Center, (finalDamageables[i].Center - parent.Center).normalized);
                    damage = new ZXDamage(weaponDamage, 1f, 1f, 0f, parent.Race, ZXDamage.Type.Impact, traj, damageEffect);

                    if (finalDamageables[i] != null)
                    {
                        finalDamageables[i].TakeDamage(damage);
                    }
                }
            }
        }

        public void StartTrail()
        {
            if (trailParticle != null)
                trailParticle.Play();
        }

        public void StopTrail()
        {
            if (trailParticle != null)
                trailParticle.Stop();
        }

        protected void Update()
        {
            if (specialAttackTimer > 0f)
                specialAttackTimer -= Time.deltaTime;
        }

        public override void SnapToTransform(ZXHolster holster)
        {
            base.SnapToTransform(holster);

            if (holster.HolsterType == ZXHolster.Holster.Back_Holster || holster.HolsterType == ZXHolster.Holster.Back_Alt_Holster)
            {
                t_Grip.transform.localRotation *= Quaternion.Euler(new Vector3(90f, 0f, 0f));
            }
        }
    }

    public abstract class ZXFirearm : ZXWeapon
    {
        //Constants
        protected float _preFireDelay = 0.25f;

        /* Inspector Variables */
        [SerializeField] protected int ammoCapacity;
        [SerializeField] protected int ammoReloads;
        [SerializeField] protected float fireWidth;
        [SerializeField] protected int roundsFired;
        [SerializeField] protected float fireDelay;
        [SerializeField] protected float rateOfFire;

        //Particle effects
        [SerializeField] protected GameObject muzzleFlashPrefab;
        protected ParticleSystem p_muzzleFlash;
        [SerializeField] protected GameObject tracerPrefab;
        protected ParticleSystem p_tracerRound;
        [SerializeField]
        protected bool p_tracerPlayEmission;
        [SerializeField]
        protected GameObject shellEjectionPrefab;
        protected ParticleSystem p_shellEjection;

        //Sounds
        [SoundGroupAttribute][SerializeField] protected string s_Tail;
        [SoundGroupAttribute][SerializeField] protected string s_Dry;
        [SoundGroupAttribute][SerializeField] protected string s_ClipOut;
        [SoundGroupAttribute][SerializeField] protected string s_ClipIn;
        [SoundGroupAttribute][SerializeField] protected string s_Fire;

        //Transforms
        protected Transform t_Muzzle;
        protected Transform t_EjectionPort;

        //Accessors
        public override Transform AimTransform
        {
            get
            {
                return t_Muzzle;
            }
        }
        public float WeaponAmmo
        {
            get
            {
                return currentAmmo;
            }
        }
        public int WeaponReloads
        {
            get
            {
                return ammoReloads;
            }
        }
        public float Penetration
        {
            get
            {
                return penetration;
            }
        }
        public float ForceMod
        {
            get
            {
                return forceMod;
            }
        }
        public float RateOfFire
        {
            get
            {
                return rateOfFire;
            }
        }
        public float FireTime
        {
            get
            {
                return roundsFired * rateOfFire;
            }
        }
        public bool isFiring
        {
            get
            {
                return fireTimer > 0f;
            }
        }

        //Events
        public event System.Action e_Fire;
        public event System.Action e_Reload;
        public event System.Action<float> e_AmmoChanged;
        public event System.Action<float> e_ReloadsChanged;
        //Delegate events
        protected System.Action de_FirePattern;

        //Delegates
        protected System.Action d_LinearFire;
        protected System.Action d_ConicFire;
        protected System.Comparison<IZXDamageable> d_DistanceSort;

        //Private variables
        protected List<IZXDamageable> DamageableList;
        private int fireIterations = 0;
        private int fireSound = 0;
        private int currentAmmo;
        private int currentReloads;
        private float fireTimer = 0f;

        //Initalization
        protected override void Awake()
        {
            base.Awake();

            currentAmmo = ammoCapacity;

            d_LinearFire = new System.Action(LinearFirePattern);
            d_ConicFire = new System.Action(ConicFirePattern);
            d_DistanceSort = new System.Comparison<IZXDamageable>(DistanceSort);

            DamageableList = new List<IZXDamageable>(20);

            t_Muzzle = weaponPoints.Find(delegate (ZXWeaponPoint Obj) { return Obj.PartType == ZXWeaponPoint.Part.Muzzle; }).transform;
            t_EjectionPort = weaponPoints.Find(delegate (ZXWeaponPoint Obj) { return Obj.PartType == ZXWeaponPoint.Part.EjectionPort; }).transform;

            LoadParticleEffects();
        }

        protected virtual void Start()
        {
            InitializeTracerRange();
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            currentAmmo = ammoCapacity;
            currentReloads = ammoReloads;
        }

        protected void LoadParticleEffects()
        {
            GameObject temp;

            //Instantiate particle effects
            if (muzzleFlashPrefab != null)
            {
                temp = Instantiate(muzzleFlashPrefab);
                temp.transform.parent = ZXCombatUI.ObjectMask;
                p_muzzleFlash = temp.GetComponent<ParticleSystem>();
            }

            if (tracerPrefab != null)
            {
                temp = Instantiate(tracerPrefab);
                temp.transform.parent = ZXCombatUI.ObjectMask;
                p_tracerRound = temp.GetComponent<ParticleSystem>();
            }

            if (shellEjectionPrefab != null)
            {
                temp = Instantiate(shellEjectionPrefab);
                temp.transform.parent = ZXCombatUI.ObjectMask;
                p_shellEjection = temp.GetComponent<ParticleSystem>();
            }

            UpdateParticleEffects();
        }

        //Runtime
        private void Update()
        {
            //Decrease timer
            if (fireTimer > 0f)
                fireTimer -= Time.deltaTime;
        }

        private void LateUpdate()
        {
            //Adjust particle effect locations
            UpdateParticleEffects();

            if (fireIterations > 0 && fireTimer <= 0f)
            { 
                if (de_FirePattern != null)
                    de_FirePattern();

                if (DamageableList != null && DamageableList.Count > 0)
                {
                    DamageableList.Sort(d_DistanceSort);
                }

                FireTranslation();
            }
        }

        private void UpdateParticleEffects()
        {
            //Instantiate particle effects
            if (p_muzzleFlash != null)
            {
                p_muzzleFlash.transform.position = t_Muzzle.position;
                p_muzzleFlash.transform.rotation = t_Muzzle.rotation;
            }

            if (p_tracerRound != null)
            {
                p_tracerRound.transform.position = t_Muzzle.position;
                p_tracerRound.transform.rotation = t_Muzzle.rotation;
            }

            if (p_shellEjection != null)
            {
                p_shellEjection.transform.position = t_EjectionPort.position;
                p_shellEjection.transform.rotation = t_EjectionPort.rotation;
            }
        }

        //Start firing sequence
        public override int Fire()
        {
            if (fireTimer > 0f)
                return (int)ZXToon.Action.Aim;
            else if (currentAmmo > 0)
            {
                if (fireIterations <= 0)
                    fireTimer = _preFireDelay;

                fireIterations = roundsFired;

                return (int)ZXToon.Action.Fire;
            }
            else if (currentReloads > 0 || ammoReloads == -1)
            {
                MasterAudio.PlaySound3DAtTransformAndForget(s_Dry, transform);
                return (int)ZXToon.Action.Reload;
            }
            else
                return (int)ZXToon.Action.Swap;
        }

        private void FireTranslation()
        {
            //Fire muzzle flash particle effect
            if (p_muzzleFlash != null)
            {
                p_muzzleFlash.Play(true);
            }

            //Fire tracer particle effect
            if (p_tracerRound != null)
            {
                if (p_tracerPlayEmission)
                    p_tracerRound.Play();
                else
                    p_tracerRound.Emit(1);
            }

            //Fire shell ejection particle effect
            if (p_shellEjection != null)
                p_shellEjection.Emit(1);

            //Play fire sound
            if (currentAmmo <= 0)
            {
                fireIterations = 0;

                MasterAudio.PlaySound3DAtTransformAndForget(s_Dry, transform);

                if (e_Reload != null)
                    e_Reload();

                return;
            }
            else if (fireIterations > 1 && s_Fire != null)
            {
                MasterAudio.PlaySound3DAtTransformAndForget(s_Fire, transform);
            }
            else if (s_Tail != null)
            {
                MasterAudio.PlaySound3DAtTransformAndForget(s_Tail, transform);
            }

            //Update counters
            currentAmmo--;
            fireIterations--;

            if (e_Fire != null)
                e_Fire();

            if (e_AmmoChanged != null)
                e_AmmoChanged((float)(currentAmmo) / ammoCapacity);

            if (fireIterations > 0)
                fireTimer = rateOfFire;
            else
                fireTimer = fireDelay;

            DischargeWeapon();

            if (currentAmmo <= 0)
            {
                fireIterations = 0;

                if (e_Reload != null)
                    e_Reload();
            }
        }

        private void LinearFirePattern()
        {
            //Generate ray and cast
            Ray ray = new Ray(AimTransform.position, AimTransform.TransformDirection(Vector3.forward));

            DamageableList = ZXReflectingPool.SphereCast<IZXDamageable>(ray, fireWidth, weaponRange, d_DamageFilter);
        }

        private void ConicFirePattern()
        {
            //Generate ray and cast
            Ray ray = new Ray(AimTransform.position, AimTransform.TransformDirection(Vector3.forward));

            DamageableList = ZXReflectingPool.ConeCast<IZXDamageable>(ray, new Vector2(fireWidth, weaponRange), weaponRange, d_DamageFilter);
        }

        protected abstract void DischargeWeapon();

        protected void InterruptFiring()
        {
            fireIterations = 0;
            fireTimer = 0;
        }

        public override void Drop()
        {
            base.Drop();

            InterruptFiring();
        }

        //Reloading
        public void StartReload()
        {
            MasterAudio.PlaySound3DAtTransformAndForget(s_ClipOut, transform);
        }

        public virtual void EndReload()
        {
            MasterAudio.PlaySound3DAtTransformAndForget(s_ClipIn, transform);

            currentAmmo = ammoCapacity;
            fireTimer = fireDelay;

            if (ammoReloads != -1)
            {
                currentReloads--;

                if (e_ReloadsChanged != null)
                    e_ReloadsChanged(currentReloads);
            }

            if (e_AmmoChanged != null)
                e_AmmoChanged((currentAmmo + (ammoCapacity * currentReloads)) / (ammoCapacity + (ammoCapacity * ammoReloads)));
        }

        //Support functions
        private void InitializeTracerRange()
        {
            if (p_tracerRound != null)
            {
                p_tracerRound.startLifetime = WeaponRange / p_tracerRound.startSpeed;
//				p_tracerRound.simulationSpace = ParticleSystemSimulationSpace.World;
            }
        }

        private int DistanceSort(IZXDamageable A, IZXDamageable B)
        {
            float aDist = Vector3.Distance(A.Center, transform.position);
            float bDist = Vector3.Distance(B.Center, transform.position);
            int value = aDist < bDist ? -1 : 1;

            if (aDist == bDist)
                value = 0;

            return value;
        }
    }

    [RequireComponent(typeof(Rigidbody))]
    public abstract class ZXProjectile : MonoBehaviour, IZXTracked<ZXProjectile>
    {
        protected float _fadeInTime = 0.1f;
        protected float _dragFactor = 1f;
        protected float _turningSpeed = 5f;

        //Support types
        public enum Torque
        {
            None = 0,
            Random = 1,
            SpinZ = 2,
        }
        public enum Impact
        {
            None = 0,
            stopOnCollision = 1,
            attachOnCollision = 2,
        }
        public enum FadeIn
        {
            OnPlace = 0,
            OnLaunch = 1,
        }
        public enum FadeOut
        {
            OnImpact = 0,
            OnDetonation = 1,
            OnEndOfLife = 2
        }

        //Inspector variables
        [SerializeField] protected ParticleSystem trailPrefab;
        [SerializeField] protected Torque torqueType;
        [SerializeField] protected bool gravityDriven;
        [SerializeField] protected bool rotateToTraj;
        [SerializeField] protected bool stopOnCollision;
        [SerializeField] protected bool attachOnCollision;
        [SerializeField] protected float projectileLifetime;
        [SerializeField] protected bool renderOnPlace;
        [SerializeField] protected float fadeOutSpeed = 0.2f;
        [SerializeField] protected bool useWeaponValues;
        [SerializeField] protected int projectileDamage;
        [SerializeField] [Range(0f, 0.9f)] protected float projectilePenetration;
        [SerializeField] [Range(0f, 2f)] protected float projectileForceMod;
        [SerializeField] protected ZXDamage.Type projectileDamageType;
        [SerializeField] protected ZXDamage.SpecialEffect projectileDamageEffect;
        [SoundGroupAttribute] [SerializeField] protected string s_Impact;

        //Events
        public event System.Action<ZXProjectile> e_OnDisable;
        public event System.Action<ZXProjectile> e_OnDestroy;

        private System.Action d_Disable;

        //Private varialbes
        protected new Rigidbody rigidbody;
        protected List<Collider> colliders;
        protected ZXShaderController r_Shader;
        protected ZXFirearm weapon;
        protected ParticleSystem trail;
        protected ToonRace race = ToonRace.None;
        protected bool rotateInFlight;

        //Accessors
        public ToonRace Race
        {
            get
            {
                return race;
            }
        }

        //Internal delegates
        protected System.Predicate<Collider> triggerCondition;

        /* Initalization */
        protected virtual void Awake()
        {
            colliders = new List<Collider>(GetComponents<Collider>());
            colliders.ForEach(delegate (Collider Object) { Object.enabled = false; });

            rigidbody = GetComponent<Rigidbody>();

            r_Shader = GetComponent<ZXShaderController>();

            if (r_Shader == null)
                r_Shader = gameObject.AddComponent<ZXShaderController>();

            EnableRigidbody(false);

            if (trailPrefab != null)
            {
                trail = Instantiate<ParticleSystem>(trailPrefab);
                trail.transform.SetParent(transform);
                trail.transform.localPosition = Vector3.zero;
                trail.transform.localRotation = Quaternion.identity;
            }

            d_Disable = new System.Action(Disable);
        }

        protected virtual void Start()
        {
            weapon = GetComponentInParent<ZXFirearm>();

            if (weapon != null)
            {
                if (useWeaponValues)
                {
                    projectileDamage = weapon.WeaponDamage;
                    projectileForceMod = weapon.ForceMod;
                }

                race = weapon.Race;
            }

            switch (race)
            {
                case ToonRace.Human:
                    gameObject.layer = 20;
                    break;

                case ToonRace.Zombie:
                    gameObject.layer = 21;
                    break;

                case ToonRace.Raider:
                    gameObject.layer = 22;
                    break;

                default:
                    gameObject.layer = 20;
                    break;
            }

            //Default trigger condition
            triggerCondition = new System.Predicate<Collider>(delegate (Collider Other)
            {
                IZXTargetable target;

                if (weapon != null)
                {
                    target = Other.GetComponentInParent<IZXTargetable>();

                    if (target != null && target.Race == weapon.Race)
                        return false;
                }

                return true;
            });
        }

        protected virtual void OnEnable()
        {
            transform.localScale = Vector3.one;

            rotateInFlight = rotateToTraj;
        }

        public virtual void SetDamage(int Damage)
        {
            projectileDamage = Damage;
        }

        protected virtual void EnableRigidbody(bool Active)
        {
            if (Active)
            {
                rigidbody.isKinematic = false;
                rigidbody.useGravity = gravityDriven;
                rigidbody.drag = 0f;
                rigidbody.collisionDetectionMode = CollisionDetectionMode.Continuous;
            }
            else
            {
                rigidbody.isKinematic = true;
                rigidbody.useGravity = false;
                rigidbody.drag = 0f;
                rigidbody.collisionDetectionMode = CollisionDetectionMode.Discrete;
            }
        }

        /* Runtime */
        protected virtual void FixedUpdate()
        {
            if (!rigidbody.isKinematic && rotateInFlight)
            {
                RotateTranslation();
            }
        }

        /* Main Functions */
        public virtual void Place(Vector3 Location, Quaternion Rotation, object Args)
        {
            transform.parent = Args as Transform;
            transform.position = Location;
            transform.rotation = Rotation;

            gameObject.SetActive(true);

            if (renderOnPlace)
                r_Shader.Fade(true, 0f, _fadeInTime);
            else
                r_Shader.Reset(true);
        }

        public virtual void Launch(Vector3 Velocity)
        {
            //Reset parent
            transform.SetParent(null);

            if (!renderOnPlace)
                r_Shader.Fade(true, 0f, _fadeInTime);

            //Enable colliders
            colliders.ForEach(delegate (Collider Object) { Object.enabled = true; });

            //Configure rigidbody
            EnableRigidbody(true);

            //Play trail
            if (trail != null)
                trail.Play();

            //Add force
            rigidbody.AddRelativeForce(Velocity, ForceMode.VelocityChange);
            rigidbody.AddRelativeTorque(GetTorque());
        }

        public virtual void Launch(Vector3 Location, float TimeToTarget)
        {
            //Get target velocity
            Vector3 targetVelocity = CalculateBallisticTrajectory(transform.position, Location, TimeToTarget);

            //Reset parent
            transform.SetParent(null);

            //Enable colliders
            colliders.ForEach(delegate (Collider Object) { Object.enabled = true; });

            //Configure rigidbody
            EnableRigidbody(true);

            //Play trail
            if (trail != null)
                trail.Play();

            //Add force
            rigidbody.AddForce(targetVelocity, ForceMode.VelocityChange);
            rigidbody.AddRelativeTorque(GetTorque());
        }

        protected static Vector3 CalculateBallisticTrajectory(Vector3 origin, Vector3 target, float timeToTarget)
        {
            // calculate vectors
            Vector3 toTarget = target - origin;
            Vector3 toTargetXZ = toTarget;
            toTargetXZ.y = 0;

            // calculate xz and y
            float y = toTarget.y;
            float xz = toTargetXZ.magnitude;

            // calculate starting speeds for xz and y. Physics forumulase deltaX = v0 * t + 1/2 * a * t * t
            // where a is "-gravity" but only on the y plane, and a is 0 in xz plane.
            // so xz = v0xz * t => v0xz = xz / t
            // and y = v0y * t - 1/2 * gravity * t * t => v0y * t = y + 1/2 * gravity * t * t => v0y = y / t + 1/2 * gravity * t
            float t = timeToTarget;
            float v0y = y / t + 0.5f * Physics.gravity.magnitude * t;
            float v0xz = xz / t;

            // create result vector for calculated starting speeds
            Vector3 result = toTargetXZ.normalized;        // get direction of xz but with magnitude 1
            result *= v0xz;                                // set magnitude of xz to v0xz (starting speed in xz plane)
            result.y = v0y;                                // set y to v0y (starting speed of y plane)

            return result;
        }

        public static Vector3 CalculateLeadPosition(Vector3 Location, Vector3 Velocity, float Time)
        {
            return Location + (Velocity * Time);
        }

        public Vector3 GetTorque()
        {
            Vector3 torque;

            //Calculate a rotation
            switch (torqueType)
            {
                case Torque.Random:
                    torque = Random.rotation.eulerAngles * rigidbody.mass;
                    break;

                case Torque.SpinZ:
                    torque = new Vector3(0f, 0f, Random.Range(20, 90)) * rigidbody.mass;
                    break;

                default:
                    torque = Vector3.zero;
                    break;
            }

            return torque;
        }

        protected virtual void RotateTranslation()
        {
            Vector3 direction = rigidbody.velocity.normalized;
            float amount = _turningSpeed * Time.fixedDeltaTime;

            direction = Vector3.RotateTowards(transform.forward, direction, amount, 0.0F);
            transform.rotation = Quaternion.LookRotation(direction);
        }

        protected virtual void ApplyBallisticDamage(Collider otherCollider)
        {
            ZXDamage damage;
            IZXDamageable damageable = null;
            Transform damageableTransform = null;
            Ray trajectory = new Ray(transform.position, transform.TransformDirection(Vector3.forward));

            damage = new ZXDamage(projectileDamage, projectilePenetration, projectileForceMod, 0f, Race, projectileDamageType, trajectory, projectileDamageEffect);

            damageableTransform = otherCollider.transform;

            if (damageableTransform != null)
                damageable = otherCollider.transform.GetComponentInParent<IZXDamageable>();

            if (damageable != null)
                damageable.TakeDamage(damage);

            rotateInFlight = false;

            //Play sound
            MasterAudio.PlaySound3DAtTransformAndForget(s_Impact, transform);

            //Stop trail
            if (trail != null)
                trail.Stop();

            if (stopOnCollision)
            {
                EnableRigidbody(false);
            }
            else if (rigidbody != null)
                rigidbody.drag = _dragFactor;

            if (attachOnCollision && damageableTransform != null && transform.parent == null)
            {
                transform.SetParent(otherCollider.transform, true);
                transform.localScale = Vector3.one;
            }

            r_Shader.Fade(projectileLifetime, fadeOutSpeed, 1f, d_Disable);
        }

        /* Collision */
        protected virtual void OnTriggerEnter(Collider otherCollider)
        {
            if (triggerCondition == null || triggerCondition(otherCollider))
            {
                ApplyBallisticDamage(otherCollider);
            }
        }


        /* Destruction */
        protected virtual void Disable()

        {
            colliders.ForEach(delegate (Collider Object) { Object.enabled = false; });
            EnableRigidbody(false);
            gameObject.SetActive(false);
        }

        protected virtual void OnDisable()
        {
            if (trail != null)
                trail.Stop();

            if (e_OnDisable != null)
                e_OnDisable(this);
        }

        private void OnDestroy()
        {
            if (e_OnDestroy != null)
                e_OnDestroy(this);
        }
    }

    public abstract class ZXExplosive : ZXProjectile
    {
        protected const float _dotTickSpeed = 0.25f;
        protected const float _dragAfterImpact = 10f;

        //Support types
        public enum Activation
        {
            Manual = 0,
            Trigger = 1,
            Collision = 2
        }

        //Inspector variables
        [SerializeField] protected ToonRace ignoreRace;
        [SerializeField] protected int explosiveDamage;
        [SerializeField] protected float explosiveRadius;
        [SerializeField] [Range(0f, 0.9f)] protected float explosiveDamagePenetration;
        [SerializeField] [Range(0f, 2f)] protected float explosiveForceMod;
        [SerializeField] protected float detonationDelay;
        [SerializeField] protected bool damageOverLifetime;
        [SerializeField] protected ZXDamage.Type explosiveDamageType;
        [SerializeField] protected ZXDamage.SpecialEffect explosiveDamageEffect;
        [SerializeField] protected Activation activationType;
        [SerializeField] protected ParticleSystem effectPrefab;
        [SerializeField] protected ParticleSystem scarPrefab;
        [SerializeField] protected float scarDuration;
        [SoundGroupAttribute] [SerializeField] protected string s_Explode;

        //Accessors
        public float Radius
        {
            get
            {
                return explosiveRadius;
            }
        }

        public float Duration
        {
            get
            {
                return projectileLifetime;
            }
        }

        //Internal events
        protected System.Predicate<Collision> collisionCondition;

        public int ExplosiveDamage
        {
            get
            {
                return explosiveDamage;
            }
        }

        //Private variables
        protected ParticleSystem effect;
        protected ParticleSystem scar;
        protected bool armed = false;

        /* Initalization */
        protected override void Awake()
        {
            base.Awake();

            //Instantiate effect
            if (effectPrefab != null)
            {
                effect = Instantiate<ParticleSystem>(effectPrefab);
                effect.transform.SetParent(transform);
                effect.transform.localPosition = Vector3.zero;
                effect.transform.localRotation = Quaternion.identity;
                effect.gameObject.SetActive(false);
            }

            if (scarPrefab != null)
            {
                scar = Instantiate<ParticleSystem>(scarPrefab);
                scar.transform.SetParent(transform);
                scar.transform.localPosition = Vector3.zero;
                scar.transform.localRotation = Quaternion.identity;
                scar.gameObject.SetActive(false);
            }

            //Update lifetime and fade speed
            projectileLifetime += detonationDelay + fadeOutSpeed + scarDuration;

            if (effect != null)
                projectileLifetime += effect.duration;
        }

        protected override void Start()
        {
            base.Start();

            //Default trigger condition
            collisionCondition = new System.Predicate<Collision>(delegate (Collision Other)
            {
                IZXTargetable target;
                Transform otherTrans;

                if (weapon != null)
                {
                    otherTrans = Other.transform;

                    if (otherTrans != null)
                    {
                        target = otherTrans.GetComponentInParent<IZXTargetable>();

                        if (target != null && target.Race == weapon.Race)
                            return false;
                    }
                }

                return true;
            });
        }

        public override void SetDamage(int Damage)
        {
                explosiveDamage = Damage;
        }
        
        public void SetExplosiveDamage(int Damage)
        {
            explosiveDamage = Damage;
        }

        public void SetExplosiveRadius(float Radius)
        {
            explosiveRadius = Radius;
        }

        public void SetExplosiveDuration(float Duration)
        {
            projectileLifetime = Duration;
        }

        public virtual void AdjustDamage(int Level)
        {
            //SetExplosiveDamage(Mathf.RoundToInt(explosiveDamage * Mathf.Pow(1 + ZXGLOBALS.SpellDamageModPerLevel, Level)));
        }

        public virtual void AdjustRadius(int Radius)
        {
            explosiveRadius = Radius;
        }

        /* Main Functions */
        public void Arm()
        {
            armed = true;
        }

        public void ArmDelayed(float ArmFuse)
        {
            Invoke("Arm", ArmFuse);
        }

        public virtual void Detonate()
        {
            //Disarm
            armed = false;

            StartCoroutine(ApplyExplosiveDamageOverTime());
        }

        protected virtual IEnumerator ApplyExplosiveDamageOverTime()
        {
            float timer = 0f;

            if (damageOverLifetime)
                timer = projectileLifetime - scarDuration;

            //play sound
            MasterAudio.PlaySound3DAtTransformAndForget(s_Impact, transform);

            //Wait for detonation delay
            yield return new WaitForSeconds(detonationDelay);

            //play sound
            MasterAudio.PlaySound3DAtTransformAndForget(s_Explode, transform);

            if (!stopOnCollision)
                EnableRigidbody(false);

            colliders.ForEach(delegate (Collider Object) { Object.enabled = false; });

            //Play the effects
            PlaceEffects();

            do
            {
                timer -= _dotTickSpeed;

                ApplyExplosiveDamage();

                yield return new WaitForSeconds(_dotTickSpeed);

            } while (timer > 0f);

            //Wait for fade out time
            yield return new WaitForSeconds(fadeOutSpeed);
        }

        protected virtual void ApplyExplosiveDamage()
        {
            List<IZXDamageable> damageableList;
            List<IZXDamageable> finalDamageableList;

            int tickDamage = Mathf.RoundToInt(damageOverLifetime ? explosiveDamage / (projectileLifetime - scarDuration) * _dotTickSpeed : explosiveDamage);

            damageableList = ZXReflectingPool.FindInSphere<IZXDamageable>(transform.position, explosiveRadius);

            if (damageableList != null && damageableList.Count > 0)
            {
                damageableList.RemoveAll(delegate (IZXDamageable Object) 
                {
                    return ignoreRace != ToonRace.None && Object.Race == ignoreRace;
                });
            }

            if (damageableList != null && damageableList.Count > 0)
            {
                ZXDamage damage;
                int adjTickDmg;
                Ray trajectory;

                for (int i = 0; i < damageableList.Count; i++)
                {
                    //Look on the objects parent
                    if (damageableList[i] != null)
                    {
                        adjTickDmg = Mathf.RoundToInt(Mathf.Lerp(tickDamage * .30f, tickDamage, Vector3.Distance(transform.position, damageableList[i].Center) / explosiveRadius));

                        trajectory = new Ray(transform.position, (damageableList[i].Center - transform.position).normalized);

                        damage = new ZXDamage(adjTickDmg, explosiveDamagePenetration, explosiveForceMod, explosiveRadius, Race, explosiveDamageType, trajectory, explosiveDamageEffect);
                        damageableList[i].TakeDamage(damage);
                    }
                }
            }
        }

        protected void PlaceEffects()
        {
            Vector3 location = transform.position;

            location.y = ZXCombatUI.SeaLevel;

                if (effect != null)
                {
                    if (transform.position.y < location.y)
                        effect.transform.position = location;

                    effect.transform.rotation = Quaternion.identity;
                    effect.gameObject.SetActive(true);
                    effect.Play();
                }
                if (scar != null)
                {
                    scar.transform.position = location;
                    scar.transform.rotation = Quaternion.identity;
                    scar.gameObject.SetActive(true);
                    scar.Play();
                }
        }

        /* Collision Events */
        protected virtual void OnCollisionEnter(Collision collision)
        {
            if (activationType == Activation.Collision && armed)
            {
                if (collisionCondition == null || collisionCondition(collision))
                {
                    ApplyBallisticDamage(collision.collider);

                    rigidbody.drag = _dragAfterImpact;

                    Detonate();
                }
            }
        }

        protected override void OnTriggerEnter(Collider otherCollider)
        {
            if (activationType == Activation.Trigger && armed)
            {
                if (triggerCondition == null || triggerCondition(otherCollider))
                {
                    ApplyBallisticDamage(otherCollider);
                    Detonate();
                }
            }
        }

        /* Destruction */
        protected override void Disable()
        {
            base.Disable();

            if (effect != null)
            {
                effect.Stop();
                effect.transform.localPosition = Vector3.zero;
                effect.gameObject.SetActive(false);
            }
            if (scar != null)
            {
                scar.Stop();
                scar.transform.localPosition = Vector3.zero;
                scar.gameObject.SetActive(false);
            }
        }
    }

    [System.Serializable]
    public abstract class ZXInterestPoint : MonoBehaviour, IZXTracked<ZXInterestPoint>
    {
        /* Support Types */
        public enum Interest
        {
            None = 0,
            Corpse = 1,
            Nexus = 2,
            Exit = 3,
            Barricade = 4,
            HeroSpawn = 5,
            Attractor = 6,
        }

        public new enum BroadcastMessage
        {
            NewCorpse = 0,
        }

        //Inspector variables
        [SerializeField]
        protected ToonRace race;
        [SerializeField]
        protected Interest pointType;
        [SerializeField]
        protected int maxInteractions;
        [SerializeField]
        protected float interactionRadius;

        public event System.Action<ZXInterestPoint> e_NoLongerInteresting;
        public event System.Action<ZXInterestPoint> e_OnDisable;
        public event System.Action<ZXInterestPoint> e_OnDestroy;

        //Accessors
        public Interest PointType
        {
            get
            {
                return pointType;
            }
        }
        public ToonRace Race
        {
            get
            {
                return race;
            }
        }
        public float InteractionRadius
        {
            get
            {
                return interactionRadius;
            }
            set
            {
                interactionRadius = value;
            }
        }
        public bool CanInteract
        {
            get
            {
                if (maxInteractions != 0 && interactors.Count < maxInteractions)
                    return true;
                else
                    return false;
            }
        }
        public Vector3 Base
        {
            get
            {
                return transform.position;
            }
        }

        //Private delegates
        private System.Action<IZXTargetable> d_ActorDead;

        //Private variables
        private bool registered = false;
        private List<IZXTargetable> interactors;

        //Initalization
        protected virtual void Awake()
        {
            d_ActorDead = new System.Action<IZXTargetable>(EndInteraction);

            if (!registered)
            {
                ZXReflectingPool.Register<ZXInterestPoint>(this, transform);
                registered = true;
            }
        }

        protected virtual void OnEnable()
        {
            if (!registered)
            {
                ZXReflectingPool.Register<ZXInterestPoint>(this, transform);
                registered = true;
            }
        }

        public void Place(Vector3 location, Quaternion rotation, object args)
        {
            gameObject.SetActive(true);
            transform.position = location;
            transform.rotation = rotation;
        }

        public virtual void Broadcast<T>(BroadcastMessage Message, float Radius) where T : class, IZXReciever
        {
            List<T> recievers = ZXReflectingPool.FindInSphere<T>(transform.position, Radius);

            if (recievers != null)
                recievers.ForEach(delegate (T Object) { Object.RecieveMessage(this, transform.position, pointType, Message); });
        }

        public virtual bool GetNearestPosition(out UnityEngine.AI.NavMeshHit NavLocation)
        {
            return UnityEngine.AI.NavMesh.SamplePosition(transform.position, out NavLocation, interactionRadius, UnityEngine.AI.NavMesh.AllAreas);
        }

        public abstract int Interact(IZXTargetable Actor);

        public virtual bool BeginInteraction(IZXTargetable Actor)
        {
            if (maxInteractions != 0 && interactors.Count < maxInteractions)
            {
                interactors.Add(Actor);
                Actor.e_Dead += d_ActorDead;
                return true;
            }
            else
                return false;
        }

        public virtual void EndInteraction(IZXTargetable Actor)
        {
            Actor.e_Dead -= d_ActorDead;

            if (interactors.Contains(Actor))
                interactors.Remove(Actor);
        }

        /* Destruction */

        protected virtual void OnDisable()
        {
            if (registered)
            {
                ZXReflectingPool.DeRegister<ZXInterestPoint>(transform);
                registered = false;
            }

            if (e_NoLongerInteresting != null)
                e_NoLongerInteresting(this);

            if (e_OnDisable != null)
                e_OnDisable(this);
        }

        protected void OnDestroy()
        {
            if (e_OnDestroy != null)
                e_OnDestroy(this);
        }
    }

    public abstract class ZXPopup : MonoBehaviour, IZXContent
    {
        private const string Base_Tween_ID = "tw_popup_";
        private const float punchTime = 0.1f;
        private const float transInTime = 0.15f;
        private const float transOutTime = 0.075f;

        //Public types
        public enum Size
        {
            Small = 0,
            Large = 1,
            Oversized = 2,
            Fullscreen = 3,
        }

        //Accessors
        public Size PopupSize
        {
            get
            {
                return windowSize;
            }
        }
        public bool Showing
        {
            get
            {
                return showing;
            }
        }
        public static bool PopupShowing
        {
            get
            {
                return current != null;
            }
        }
        

        //Inspector fields
        [SerializeField] private Size windowSize;
        [SerializeField] private bool mandatory = false;
        [SerializeField] private bool ignoreClickScreen = false;
        [SerializeField] private bool forceClickScreen = false;

        //Private static variables
        private static ZXPopup current;

        //Private variables
        private RectTransform rect;
        private string tween_id;
        private bool showing;

        protected virtual void Awake()
        {
            tween_id = Base_Tween_ID + gameObject.name;

            rect = GetComponent<RectTransform>();
        }

        //Activation functions
        public virtual void Show()
        {
            if (current != null)
                HideActive();

            current = this;

            //Set starting scale
            if (windowSize == Size.Large || windowSize == Size.Oversized)
                rect.localScale = Vector3.zero;
            else
                rect.localScale = Vector3.one;

            //Turn on game object
            gameObject.SetActive(true);

            //Check for playing transition
            if (DOTween.IsTweening(tween_id))
                DOTween.Kill(tween_id);

            //Do transitions if needed
            if (windowSize == Size.Small)
            {
                rect.DOPunchScale(new Vector3(0.1f, 0.1f, 0.1f), punchTime).SetId(tween_id);
            }
            else if (windowSize == Size.Large || windowSize == Size.Oversized)
            {
                ZXClickScreen.ShowClickScreen();
                rect.DOScale(1, transInTime).SetId(tween_id).SetEase(Ease.OutCirc);
            }

            if (forceClickScreen)
            {
                ZXClickScreen.ShowClickScreen();
            }

            showing = true;
        }

        public virtual void Hide()
        {
            if (current == this)
            {
                current = null;

                if (windowSize == Size.Large || windowSize == Size.Oversized)
                {
                    if (DOTween.IsTweening(tween_id))
                        DOTween.Kill(tween_id);

                    ZXClickScreen.HideClickScreen();
                    rect.DOScale(0, transOutTime).SetId(tween_id).OnComplete(delegate() { gameObject.SetActive(false); });
                }
                else
                    gameObject.SetActive(false);
            }
            else
                gameObject.SetActive(false);

            if (forceClickScreen)
            {
                ZXClickScreen.HideClickScreen();
            }

            showing = false;
        }

        //Static functions
        public static void HideActive()
        {
            if (current != null && !current.mandatory)
            {
                current.Hide();
            }
        }

        public static void ClickScreenTap()
        {
            if (current != null && !current.ignoreClickScreen)
            {
                current.Hide();
            }
            else if (current != null)
                current.Click();
        }

        protected virtual void Click()
        {

        }

        public static void BackButtonTap()
        {
            if (current != null && !current.mandatory)
            {
                current.Hide();
            }
        }

        protected virtual void OnDestroy()
        {
            if (DOTween.IsTweening(tween_id))
                DOTween.Kill(tween_id);

            if (showing)
                current = null;
        }
    }

    public abstract class ZXSection : MonoBehaviour, IZXContent
    {
        //Types
        [System.Serializable]
        public enum Type
        {
            Shop = 0,
            Deck = 1,
            Mission = 2,
            Daily = 3,
            Inventory = 4,
        }

        //Accessors
        public float Position
        {
            get
            {
                return rect.anchoredPosition.y;
            }
            set
            {
                RectTransform r = GetComponent<RectTransform>();

                r.anchoredPosition = new Vector2(value, r.anchoredPosition.y);
            }
        }
        public float Width
        {
            get
            {
                return rect.rect.width;
            }
            set
            {
                RectTransform r = GetComponent<RectTransform>();

                r.sizeDelta = new Vector2(value, r.sizeDelta.y);
            }
        }
        public Type Section
        {
            get
            {
                return section;
            }
        }

        //Inspector variables
        [SerializeField] private Type section;
        private RectTransform rect;

        protected virtual void Awake()
        {
            rect = GetComponent<RectTransform>();
        }

        //Activation functions
        public virtual void Show()
        {
            gameObject.SetActive(true);
        }

        public virtual void Hide()
        {
            gameObject.SetActive(false);
        }
    }
    #endregion

    /* Non-Abstract Classes */

    /// <summary>
    /// Simple sync object pool
    /// </summary>
    /// <typeparam name="T">Type of objects being pooled</typeparam>
    public sealed class ZXPoolLite<T> where T : MonoBehaviour, IZXTracked<T>
    {
        //Accessors
        public int count
        {
            get
            {
                return queue.Count;
            }
        }

        //Private variables
        private T[] originals;
        private Transform parent;
        private Queue<T> queue;

        //Delegates
        private System.Action<T> DisableDelegate;
        private System.Action<T> DestroyDelegate;

        //Constructor
        public ZXPoolLite(Transform Parent, T Prefab, int Count) : this(Parent, new T[1] { Prefab }, Count) { }
        public ZXPoolLite(Transform Parent, T[] Prefab, int Count)
        {
            //Set variables
            queue = new Queue<T>(Count);
            parent = Parent;
            originals = Prefab;
            DisableDelegate = new System.Action<T>(Return);
            DestroyDelegate = new System.Action<T>(ObjectDestroyed);

            //Create items
            if (Prefab != null && Count > 0 && Prefab.Length > 0)
                Add(Count);
        }

        //Creates object and adds them to queue
        private void Add(int count)
        {
            GameObject newObject;
            T newInterface;
            int index = Random.Range(0, originals.Length);

            //Create objects
            for (int i = 0; i < count; i++)
            {
                //Instantiate prefabs
                newObject = GameObject.Instantiate(originals[index].gameObject);

                //Increase and reset index
                index++;

                if (index >= originals.Length)
                    index = 0;

                //Get interface
                newInterface = newObject.GetComponent<T>();

                if (newObject.activeSelf)
                    newObject.SetActive(false);

                //Set parent and scale
                newObject.transform.SetParent(parent);
                newObject.transform.localScale = Vector3.one * 1.0f;
                newObject.transform.localPosition = Vector3.zero;
                newObject.transform.localRotation = Quaternion.identity;

                //Set return and dead delegates
                newInterface.e_OnDisable += DisableDelegate;
                newInterface.e_OnDestroy += DestroyDelegate;

                //Place object in queue
                queue.Enqueue(newInterface);
            }
        }

        //Get an item
        public T Request()
        {
            if (queue.Count > 0)
                return queue.Dequeue();
            else
                return null;
        }

        //Return items to queue
        private void Return(T Object)
        {
            //Object not disabled correctly
            if (Object.gameObject.activeSelf == true)
            {
                //Disable return event
                Object.e_OnDisable -= DisableDelegate;
                Object.e_OnDestroy -= DestroyDelegate;
                //Disable
                Object.gameObject.SetActive(false);
                //Register return event
                Object.e_OnDisable += DisableDelegate;
                Object.e_OnDestroy += DestroyDelegate;
            }

            //Place object back in queue
            queue.Enqueue(Object);
        }
        private void ObjectDestroyed(T Object)
        {
            //if (Object != null)
            //Debug.Log("Pool object was destroyed: " + Object.ToString());
        }
    }

    //DataPoint enumerable back buffered value/time collection
    public sealed class ZXfBuffer : ZXBuffer<XoTPoint>
    {
        //Accessors
        #region
        //Returns the scaled average of the collection
        public float Average
        {
            get
            {
                return curAverage;
            }
        }
        //Returns the total time signature of the collection
        public float TimeSiganture
        {
            get
            {
                return curTimeSig;
            }
        }
        //Returns the average variance of the data as a percent
        public float Variance
        {
            get
            {
                return curVariance;
            }
        }
        //Returns a Vector2 repesenting the relative slope for current analysis
        public Vector2 Slope
        {
            get
            {
                return curSlope;
            }
        }
        //Returns a Vector2 repesenting the slope from the last analysis to the end of current analysis
        public Vector2 LongSlope
        {
            get
            {
                return longSlope;
            }
        }
        //Last game time the data was analyzed
        public float AnalysisAge
        {
            get
            {
                return Time.time - analysisTimeStamp;
            }
        }
        #endregion

        //Private variables
        #region
        private float analysisTimeStamp = 0f;
        private float curAverage = 0f;
        private float curTimeSig = 0f;
        private float curVariance = 0f;
        private Vector2 curSlope = new Vector2(ZXCombatUI._targetFPS, 1f);
        private Vector2 longSlope = new Vector2(ZXCombatUI._targetFPS, 1f);
        #endregion

        //Functions
        #region
        //Constructor
        public ZXfBuffer(int bufferDepth) : base(bufferDepth) { }

        //Performs comprehensive analysis of the data
        public void Analyze()
        {
            //If not initalized return!
            if (Initalized == false)
                return;

            //Sum counters
            float variance = 0f;
            float totalX = 0f;
            float totalT = 0f;

            //Differential counters
            float previousXT = Current.xt;

            //Slope points
            XoTPoint frontPoint = new XoTPoint(1f, 1f);
            XoTPoint backPoint = new XoTPoint(1f, 1f);

            //Store curSlope frontPoint
            float lastAverage = curAverage;

            //Slope measurement points
            int front = buffer.Length / 3;
            int back = (buffer.Length / 2) + front;

            for (int i = 0; i < buffer.Length; i++)
            {
                //Move the head forward
                MoveNext();

                //Total
                totalX += Current.x;
                totalT += Current.t;

                //Grab data at slope points
                if (i == front)
                    frontPoint = new XoTPoint(totalX, totalT);
                if (i == back)
                    backPoint = new XoTPoint(totalX, totalT);

                //Calculate for totals
                variance += Mathf.Abs(Current.xt - previousXT);

                //Variance
                previousXT = Current.xt;
            }

            //Update data values
            curAverage = (totalX / totalT);
            curTimeSig = totalT;
            curVariance = variance / (buffer.Length - 1);
            //Update slope values
            curSlope = new Vector2(frontPoint.xt, backPoint.xt);
            longSlope = new Vector2(lastAverage, curAverage);

            //Reset the head
            Reset();

            //Time stamp analysis data
            analysisTimeStamp = Time.time;
        }
        #endregion
    }

    //Generic enumerable back buffered collection
    public class ZXBuffer<T> : IEnumerator<T>, IEnumerable<T>
    {
        //Accessors
        public T Current
        {
            get
            {
                return buffer[readHead];
            }
        }
        object IEnumerator.Current
        {
            get
            {
                return buffer[readHead];
            }
        }
        public int TotalEntries
        {
            get
            {
                return totalEntries;
            }
        }
        public bool Initalized
        {
            get
            {
                return iniatalized;
            }
        }

        //Private variables
        protected T[] buffer;
        protected int readHead = -1;
        protected int writeHead = -1;
        //Track initalization
        private bool iniatalized = false;
        private int totalEntries = 0;

        //Constructor
        public ZXBuffer(int bufferDepth)
        {
            buffer = new T[bufferDepth];
        }
        public ZXBuffer(T[] preFilledBuffer)
        {
            buffer = preFilledBuffer;
        }

        //Adds a value to the buffer
        public virtual void Add(T value)
        {
            //Move forward
            writeHead++;

            //Bump entry count
            totalEntries++;

            //Test for max
            if (totalEntries == int.MaxValue)
                totalEntries = 0;

            //Test for array end
            if (writeHead == buffer.Length)
                writeHead = 0;

            //Add value
            buffer[writeHead] = value;

            //If array has been filled set initalized
            if (totalEntries == buffer.Length)
                iniatalized = true;

            //Reset read head
            Reset();
        }

        /* IEnumerator<T> Implimentation */
        public bool MoveNext()
        {
            //Move forward
            readHead++;

            //Test for array end
            if (readHead == buffer.Length)
                readHead = 0;

            //Test for array end
            return (readHead != writeHead);
        }
        public void Reset()
        {
            readHead = writeHead;
        }

        /* IDisposable */
        public void Dispose()
        {
            //Nothing to do here
        }

        /* IEnumerable<T> Implimentation */
        public IEnumerator<T> GetEnumerator()
        {
            return new ZXBuffer<T>(buffer);
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}