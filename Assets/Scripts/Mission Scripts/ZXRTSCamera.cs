﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using ZXCore;

public class ZXRTSCamera : MonoBehaviour
{
    [Range(15, 30)]
    private const float minZoom = 15;
    private const float startZoom = 18;
    private const float maxZoom = 18;

    private float mainCameraHorizontal = 4;
    private float mainCameraVerticalLevelLength = 100;
    private float perspectiveZoomSpeed = 0.5f;        // The rate of change of the field of view in perspective mode.

    private float orthoZoomSpeed = 0.2f;        // The rate of change of the orthographic size in orthographic mode.

	public float panSpeed = 100;

    private Vector3 mainCameraInitPos;
    private Camera cam;
	private Camera groundCam;
	private Camera uiCam;
	private Camera lineCam;
	private Camera worldSpaceCam;
	public LayerMask clickColliderMask;

    private Vector2 xLimits;
    private Vector2 zLimits;
    private Vector3 deltaPos;
	public float boundaryPaddingX = 15;
	public float boundaryPaddingZ = 15;
	public Vector3 targetPos;

    public bool Inertia;

    private float orthoSize
    {
        get
        {
            return cam.orthographicSize;
        }
        set
        {
            float clampedValue = Mathf.Clamp(value, minZoom, maxZoom);

            cam.orthographicSize = clampedValue;
            groundCam.orthographicSize = clampedValue;
            uiCam.orthographicSize = clampedValue;
            lineCam.orthographicSize = clampedValue;
            worldSpaceCam.orthographicSize = clampedValue;
        }
    }

    [Range(0, 1)]
    public float moveSpeed = 0.35f;
    private void Awake()
    { 
        mainCameraInitPos = transform.position;
        cam = GetComponent<Camera>();

        groundCam = transform.Find("Ground_Cam").GetComponent<Camera>();
		uiCam = transform.Find("UICamera").GetComponent<Camera>();
		lineCam = transform.Find("TopDownLineCamera").GetComponent<Camera>();
		worldSpaceCam = transform.Find("Ground_Canvas_Cam").GetComponent<Camera>();

        orthoSize = startZoom;

        Transform Cube = GameObject.Find("ActiveCube").transform;

        float height = cam.orthographicSize * 2;
		float width = height * Screen.width/ Screen.height; // basically height * screen aspect ratio

		float difference = width - width * 0.9f;

        xLimits = new Vector2(cam.ScreenToWorldPoint(Vector3.zero).x, cam.ScreenToWorldPoint(new Vector2(cam.pixelWidth, cam.pixelHeight)).x);
        xLimits.x += boundaryPaddingX;
        xLimits.y -= boundaryPaddingZ;
        zLimits = new Vector2(cam.ScreenToWorldPoint(new Vector3(0, cam.pixelHeight, 0)).z, cam.ScreenToWorldPoint(new Vector2(cam.pixelWidth, 0)).z);
        zLimits.x -= boundaryPaddingX;
        zLimits.y += boundaryPaddingZ;

		targetPos = transform.position;

        //Cube.localScale = new Vector3(Mathf.Abs(xLimits.y - xLimits.x), 4f, height * Mathf.Sqrt(2) - difference * Mathf.Sqrt(2) );
        if (Application.loadedLevelName != "TestArena2")
        {
            Cube.localScale = new Vector3(Mathf.Abs(xLimits.y - xLimits.x) * 1.2f, 4f, Mathf.Abs(zLimits.y - zLimits.x) * 1.2f);
            Cube.transform.localEulerAngles = Vector3.zero;
        }

		RaycastHit[] hit;

		//Physics.RaycastAll(transform.position, transform.forward, hit, clickColliderMask);
		hit = Physics.RaycastAll(transform.position, transform.forward, clickColliderMask);

		for(int i = 0; i < hit.Length; i++)
		{
			if(hit[i].collider.GetComponent<ZXGroundEvents>())
			{
				hit[i].collider.transform.position = hit[i].point;
			}
		}
    }

    private void Start()
    {

        RectTransform positionHero = GameObject.Find("HeroSpawnMarker").GetComponent<RectTransform>();
        RaycastHit[] hits;

        Vector3 originPos = cam.ScreenToWorldPoint(positionHero.position);

        Ray ray = new Ray(originPos, transform.forward);

        hits = Physics.RaycastAll(ray, 1000, clickColliderMask);

        ZXHeroSpawn heroSpawn = GameObject.FindObjectOfType(typeof(ZXHeroSpawn)) as ZXHeroSpawn;
        heroSpawn.transform.position = hits[0].point;
        heroSpawn.transform.position = new Vector3(heroSpawn.transform.position.x, 0, heroSpawn.transform.position.z);

    }

    private void Update()
    {
        orthoSize = Mathf.Clamp(orthoSize, minZoom, maxZoom);
    }

    private void LateUpdate()
    {
        orthoSize = Mathf.Clamp(orthoSize, minZoom, maxZoom);

        if (Application.isMobilePlatform)
        {
            mobileZoom();
        }
        else
        {
            pcZoom();
        }

        if (Inertia)
        {
            transform.position = Vector3.Lerp(transform.position, targetPos, moveSpeed);
        }
        else
        {
            transform.position = targetPos;
        }
    }

    private void pcZoom()
    {
        if (Input.mouseScrollDelta.y != 0)
        {
            orthoSize += Input.mouseScrollDelta.y * orthoZoomSpeed;
        
            //Debug.Log(Input.mouseScrollDelta.y);
            if(Input.mouseScrollDelta.y > 0)
            {
               transform.position = Vector3.Lerp(transform.position, mainCameraInitPos, 0.2f);
            }
        }
    }

    private void mobileZoom()
    {

        // If there are two touches on the device...
        if (Input.touchCount == 2)
        {
            // Store both touches.
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            // If the camera is orthographic...
            // ... change the orthographic size based on the change in distance between the touches.
            orthoSize += deltaMagnitudeDiff * orthoZoomSpeed;
        }
    }

    public void MoveCamera(Vector2 delta)
	{
		deltaPos = delta*panSpeed;

		deltaPos = Quaternion.Inverse(Quaternion.Euler(new Vector3(0, 135, 0))) * new Vector3(deltaPos.x/ Screen.width, 0, deltaPos.y/Screen.height);

		targetPos -= deltaPos;

		Vector3 minLimits;
		Vector3 maxLimits;

		minLimits = new Vector3(mainCameraInitPos.x - boundaryPaddingX, 0, mainCameraInitPos.z - boundaryPaddingZ);
		maxLimits = new Vector3(mainCameraInitPos.x + boundaryPaddingX, 0, mainCameraInitPos.z + boundaryPaddingZ);

        targetPos.x = Mathf.Clamp(targetPos.x, minLimits.x, maxLimits.x);
        targetPos.y = 35;
        targetPos.z = Mathf.Clamp(targetPos.z, minLimits.z, maxLimits.z);
    }
}