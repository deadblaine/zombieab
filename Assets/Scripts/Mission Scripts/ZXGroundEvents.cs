﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;
using Vectrosity;
using UnityEngine.UI;
using ZXCore;
using DG.Tweening;
using DarkTonic.MasterAudio;

public class ZXGroundEvents : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    private enum Tracking
    {
        None = 0,
        HeroLine = 1,
        CameraMove = 2,
    }
    public enum Turn
    {
        none,
        left,
        right
    }

    public static ZXGroundEvents instance;
	public LayerMask ignoreMask;
	public float offsetAngle;
	public Texture2D directionTexture;
    public Texture2D frontTexture;
    public LayerMask lineLayer;
    private LayerMask dontDrawLayer;
	public BoxCollider activeCube;
	private Camera groundCamera;
    private Vector3 VerticleOffset = new Vector3(0, 0.1f, 0f);

    private Camera lineCamera;
    private ZXRTSCamera mainCam;
    private IZXInstructable closestPlayer;

	public static bool CanMoveHero = true;
    public Action<Vector3> d_TetherChanged;
    public VectorLine tutorialMovePath = null;

    //Public events
    public static event System.Action e_HeroPathStarted;
    public static event System.Action<bool> e_HeroPathEnded;

    //Delegates
    private System.Func<Ray> d_GetRay;

    //Private tracking variables
    private Tracking currentTracking = Tracking.None;
    private int trackingTouchID; 
    private List<VectorLine> myLines;
    private int lineIndex;

    private float timer;
    private IZXInstructable lastChar;

	private static Color heroPathColor = new Color(0, 0.33f, 1, 1); //Blue
    private static Color heroPathSuccessColor = new Color(0, 0.33f, 1, 1f); //Blue
    private static Color heroPathFailColor = new Color(0.66f, 0, 0, 1f); //Red
    private const float lineWidth = 0.018f;

    void Update()
    {
        timer -= Time.deltaTime;
    }

    void Awake()
    {
        instance = this;
        mainCam = Camera.main.GetComponent<ZXRTSCamera>();
		activeCube = transform.Find("ActiveCube").GetComponent<BoxCollider>();

        if (Application.isMobilePlatform)
            d_GetRay = new Func<Ray>(MobileInput);
        else
            d_GetRay = new Func<Ray>(PCInput);
    }

    void Start()
    {
		groundCamera = Camera.main;

        myLines = new List<VectorLine>();

        if(GameObject.Find("TopDownLineCamera"))
        {
            lineCamera = GameObject.Find("TopDownLineCamera").GetComponent<Camera>();
        }
        else
        {
            Debug.Log("NEED TO ADD TOPDOWNLINECAMERA TO SCENE");
        }
			
		VectorLine.SetEndCap(gameObject.GetInstanceID().ToString(), EndCap.Back, 0, 0, 1.0f, 2.0f, directionTexture, frontTexture);

        for (int i = 0; i < 1; i++)
        {
			myLines.Add(new VectorLine("Path" + i.ToString(), new List<Vector3>(), lineWidth * Screen.width, LineType.Continuous, Joins.Weld));
            myLines[i].endCap = gameObject.GetInstanceID().ToString();
            myLines[i].SetColor(heroPathColor);
        }

        tutorialMovePath = (new VectorLine("TutorialPath", new List<Vector3>(), lineWidth * Screen.width, LineType.Continuous, Joins.Weld));
        VectorLine.SetCamera3D(lineCamera);
    }

    //Event handlers
    public void OnBeginDrag(PointerEventData eventData)
    {
        float maxHeroDistance = 5f;
        Vector3 currentWorldPosition = eventData.pointerCurrentRaycast.worldPosition;
        RaycastHit hit = new RaycastHit();

        if (currentTracking != Tracking.None)
            return;
        else
            trackingTouchID = eventData.pointerId;

        closestPlayer = FindClosestPlayer(currentWorldPosition, maxHeroDistance);

        if (closestPlayer != null)
        {
            if (Physics.Raycast(d_GetRay(), out hit, 1000, ignoreMask) && PlotPoint(hit.point))
            {
                //Being line
                LineStarted(currentWorldPosition);

                //Send message to player char
                closestPlayer.PlayerInstruction(ZXToon.Instruction.Move, ZXToon.Input.Down, null);

                //Set up tracking
                currentTracking = Tracking.HeroLine;
            }
        }
        else
            currentTracking = Tracking.CameraMove;
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector3 currentWorldPosition = eventData.pointerCurrentRaycast.worldPosition;
        RaycastHit hit = new RaycastHit();

        //If we are tracking a camera move
        if (currentTracking == Tracking.CameraMove)
        {
            mainCam.MoveCamera(eventData.delta);
        }

        if (currentTracking == Tracking.HeroLine)
        {
            //Get world point from screen position
            if (Physics.Raycast(d_GetRay(), out hit, 1000, ignoreMask))
            {
                if (PlotPoint(hit.point))
                    myLines[lineIndex].Draw3D();
                else
                {
                    LineFinished(false);
                    ResetDrag();
                }
            }
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (currentTracking == Tracking.HeroLine)
        {
            if (closestPlayer != null && ValidateLine(myLines[lineIndex]))
            {
                MovePlayer(closestPlayer);

                LineFinished(true);
            }
            else
                LineFinished(false);
        }

        ResetDrag();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (ZXCombatUI.ActivePlayers.Count == 0) return;

        IZXInstructable closestPlayer1;
        closestPlayer1 = ZXCombatUI.ActivePlayers[0];

        for (int i = 1; i < ZXCombatUI.ActivePlayers.Count; i++)
        {
            if (Vector3.Distance(closestPlayer1.LookAtTransform.position, eventData.pointerCurrentRaycast.worldPosition)
                > Vector3.Distance(ZXCombatUI.ActivePlayers[i].LookAtTransform.position, eventData.pointerCurrentRaycast.worldPosition))
            {
                closestPlayer1 = ZXCombatUI.ActivePlayers[i];
            }
        }

        if (Vector3.Distance(closestPlayer1.LookAtTransform.position, eventData.pointerCurrentRaycast.worldPosition) > 6)
        {
            closestPlayer1 = null;
            return;
        }

        lastChar = closestPlayer1;
        timer = 0.5f;        
    }

    //Line managment
    private IZXInstructable FindClosestPlayer(Vector3 WorldPosition, float maxDistance)
    {
        if (ZXCombatUI.ActivePlayers.Count == 0 || !CanMoveHero)
            return null;

        int index = 0;
        int closestIndex = 0;
        float closestDistance = float.MaxValue;
        float currentDistance = float.MaxValue;

        while (index != ZXCombatUI.ActivePlayers.Count)
        {
            currentDistance = Vector3.Distance(ZXCombatUI.ActivePlayers[index].LookAtTransform.position, WorldPosition);

            if (ZXCombatUI.ActivePlayers[index].canMove && currentDistance < closestDistance)
            {
                closestDistance = currentDistance;
                closestIndex = index;
            }

            index++;
        }

        if (closestDistance <= maxDistance)
        {
            lineIndex = closestIndex;
            return ZXCombatUI.ActivePlayers[closestIndex];
        }
        else
            return null;
    }

    private void LineStarted(Vector3 WorldPosition)
    {
        myLines[lineIndex].color = heroPathColor;

        if (e_HeroPathStarted != null)
            e_HeroPathStarted();
    }

    private void LineFinished(bool success)
    {
        if (myLines[lineIndex] != null)
        {
            Sequence newColorSequence = DOTween.Sequence();

            newColorSequence.SetUpdate(true);

            //Set new line color
            if (success)
            {
                //Tween color to success color
                newColorSequence.Append(DOTween.To(delegate () { return myLines[lineIndex].color; }, delegate (Color val) { myLines[lineIndex].color = val; }, heroPathSuccessColor, 0.5f /*Time*/));
            }
            else
            {
                //Tween color to failure color
                newColorSequence.Append(DOTween.To(delegate () { return myLines[lineIndex].color; }, delegate (Color val) { myLines[lineIndex].color = val; }, heroPathFailColor, 0.5f /*Time*/));

                //Fade color to alpha
                newColorSequence.Append(DOTween.ToAlpha(delegate () { return myLines[lineIndex].color; }, delegate (Color val) { myLines[lineIndex].color = val; }, 0, 1.0f /*Time*/).OnComplete(delegate ()
                {
                    myLines[lineIndex].points3.Clear();
                }));
            }

            myLines[lineIndex].Draw3D();
        }

        if (e_HeroPathEnded != null)
            e_HeroPathEnded(success);
    }

    private void MovePlayer(IZXInstructable Player)
    {
        //Check for tutorial move path
        if (tutorialMovePath != null && tutorialMovePath.points3.Count > 0)
        {
            myLines[lineIndex].points3.Clear();

            foreach (Vector3 V in tutorialMovePath.points3)
            {
                myLines[lineIndex].points3.Add(V);
            }

            myLines[lineIndex].Draw3D();
        }

        //Check for a valid path
        if (myLines[lineIndex] != null && myLines[lineIndex].points3 != null && myLines[lineIndex].points3.Count > 0)
        {
            Player.PlayerInstruction(ZXToon.Instruction.Move, ZXToon.Input.Up, myLines[lineIndex]);
        }
    }

    private void ResetDrag()
    {
        //Stop tracking player
        closestPlayer = null;

        currentTracking = Tracking.None;
    }

    //Support functions
    private bool ValidateLine(VectorLine NewLine)
    {
        //Validate line
        if (NewLine == null || NewLine.points3 == null || NewLine.points3.Count <= 0)
            return false;

        //Validate each line point
        for (int i = 0; i < NewLine.points3.Count; i++)
        {
            if (NewLine.points3[i] == null)
            {
                return false;
            }
        }

        return true;
    }

    private Ray MobileInput()
    {
        foreach (Touch T in Input.touches)
        {
            if (T.fingerId == trackingTouchID)
                return groundCamera.ScreenPointToRay(T.position);
        }

        Debug.Log("Failed to find tracked touch");

        //Bad default, above function should always return something
        return groundCamera.ScreenPointToRay(Input.GetTouch(0).position);
    }

    private Ray PCInput()
    {
        return groundCamera.ScreenPointToRay(Input.mousePosition);
    }

    private bool PlotPoint(Vector3 Point)
    {
        float minPlotDistance = 1f;
        int maxPoints = 30;
        UnityEngine.AI.NavMeshHit hit = new UnityEngine.AI.NavMeshHit(); 
        float currentDistance = 0f;
        int currentCount = myLines[lineIndex].points3.Count;

        //Get nearest point on NavMesh
        if (UnityEngine.AI.NavMesh.SamplePosition(Point, out hit, 2f /*Max Distance*/, UnityEngine.AI.NavMesh.AllAreas))
        {
            //Validate the point
            if (myLines[lineIndex].points3.Count > 2)
            {
                //Get distance from last point
                currentDistance = Vector3.Distance(myLines[lineIndex].points3[currentCount - 1], hit.position);

                //Ensure you are at least x distance away from last point
                if (currentDistance >= minPlotDistance)
                {
                    //Compair against all other points in the line
                    for (int i = 0; i < myLines[lineIndex].points3.Count - 1; i++)
                    {
                        //If line intersects remove points
                        if (Vector3.Distance(myLines[lineIndex].points3[i], hit.position) < minPlotDistance)
                        {
                            myLines[lineIndex].points3.RemoveRange(i, myLines[lineIndex].points3.Count - i);
                        }
                    }

                    //If line is longer than 40 points do nothing
                    if (myLines[lineIndex].points3.Count > maxPoints)
                    {
                        return true;
                    }
                    //Add the point
                    else
                    {
                        myLines[lineIndex].points3.Add(hit.position + VerticleOffset);
                    }
                }
            }
            else
            {
                myLines[lineIndex].points3.Add(hit.position + VerticleOffset);
            }

            //Adjust texture size
            if (myLines[lineIndex].points3.Count > 1 && myLines[lineIndex].textureScale != 5)
                myLines[lineIndex].textureScale = 5;

            return true;
        }
        else
        {
            Debug.Log("No point on NavMesh could be found near: " + Point.ToString());
            return false;
        }
    }
}
