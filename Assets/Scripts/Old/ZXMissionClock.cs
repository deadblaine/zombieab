﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using DarkTonic.MasterAudio;
using TMPro;

public class ZXMissionClock : MonoBehaviour
{
    //Public accessors
    public static int SecondsLeft
    {
        get
        {
            return singleton.secondsLeft;
        }
    }
    public static ZXMissionClock instance
    {
        get
        {
            return singleton;
        }
    }
    public static int StarCount
    {
        get
        {
            return singleton.starIndex;
        }
    }
    public static int ZombiesKilled
    {
        get
        {
            return killed.Count;
        }
    }

    //Private transforms
    [SerializeField] private GameObject Star1;
    [SerializeField] private GameObject Star1Fill;
    [SerializeField] private GameObject Star2;
    [SerializeField] private GameObject Star2Fill;
    [SerializeField] private GameObject Star3;
    [SerializeField] private GameObject Star3Fill;
    [SerializeField] private GameObject time;
    [SerializeField] private TextMeshProUGUI timeDisplay;
    [SerializeField] private GameObject paused;
    [SerializeField] private TextMeshProUGUI pausedDisplay;

    //Private delegates
    private System.Action<float> d_MissionClock;
    private System.Action<ZXCombatUI.State> d_MissionStateChanged;
    private System.Action<ZXObjectiveTrigger> d_AddStar;

    //Private tracking variables
    private static ZXMissionClock singleton;
    private static List<ZXZombie> killed;
    private int starIndex;
    private int secondsLeft;
    private bool twoMinutesLeft;
    private bool oneMinutesLeft;
    private bool tenSecondsLeft;

    private void Awake()
    {
        //Create singleton
        singleton = this;

        //Create delegates
        d_MissionClock = new System.Action<float>(UpdateClock);
        d_MissionStateChanged = new System.Action<ZXCombatUI.State>(UpdateGameState);
        d_AddStar = new System.Action<ZXObjectiveTrigger>(AddStar);

        //Find child text
        time = transform.Find("Clock").gameObject;
        timeDisplay = time.GetComponent<TextMeshProUGUI>();
        paused = transform.Find("Paused").gameObject;
        pausedDisplay = paused.GetComponent<TextMeshProUGUI>();

        //Disable all stars
        Star1.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
        Star1.SetActive(false);
        Star2.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
        Star2.SetActive(false);
        Star3.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
        Star3.SetActive(false);
        Star1Fill.SetActive(false);
        Star2Fill.SetActive(false);
        Star3Fill.SetActive(false);

        //Set time local scale
        time.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
        time.SetActive(false);

        killed = new List<ZXZombie>(50);
    }

    private void OnEnable()
    {
        //Reg delegates
        ZXCombatUI.e_MissionClock += d_MissionClock;
        ZXCombatUI.e_StateChange += d_MissionStateChanged;
    }

    private void UpdateClock(float time)
    {
        System.TimeSpan timeRemaining = new System.TimeSpan(0, 0, (int)time);
        secondsLeft = Mathf.RoundToInt(time);

        if (timeRemaining.Minutes == 1 && twoMinutesLeft == false)
        {
            twoMinutesLeft = true;
        }
        else if (timeRemaining.Minutes == 0 && oneMinutesLeft == false)
        {
            oneMinutesLeft = true;
            ZXCombatUI.Spawner.BoostDifficulty();

        }
        else if (timeRemaining.Minutes == 0 && timeRemaining.Seconds == 30 && tenSecondsLeft == false)
        {
            tenSecondsLeft = true;
        }

        if (timeRemaining.Minutes > 0 || timeRemaining.Seconds >= 0)
            timeDisplay.text = /*ZXLocal.LocalText("TimeLeft")*/ "Time Left:" + " " + timeRemaining.Minutes.ToString("0") + ":" + timeRemaining.Seconds.ToString("00");

        if (Input.GetKey(KeyCode.Space))
        {
            starIndex = 3;
            ZXCombatUI.ChangeMissionState(ZXCombatUI.State.Victory);
        }
    }

    private void UpdateGameState(ZXCombatUI.State newState)
    {
        if (newState == ZXCombatUI.State.Paused)
        {
            paused.SetActive(true);

            pausedDisplay.text = ZXLocal.LocalText("Paused");
        }
        else
        {
            if (paused != null)
                paused.SetActive(false);
        }

    }

    public void AddStar(ZXObjectiveTrigger trigger)
    {
        RectTransform targetStar = null;

        //Get correct star
        switch (starIndex)
        {
            case 0:
                targetStar = Star1Fill.GetComponent<RectTransform>();
                break;

            case 1:
                targetStar = Star2Fill.GetComponent<RectTransform>();
                break;

            case 2:
                targetStar = Star3Fill.GetComponent<RectTransform>();
                break;
        }

        if (targetStar != null)
        {
            targetStar.gameObject.SetActive(true);
            targetStar.sizeDelta = Vector2.zero;
            targetStar.DOSizeDelta(new Vector2(62, 58), 1).SetEase(Ease.OutBack).SetUpdate(true).SetDelay(0.95f);
        }

        if (trigger != null)
        {
            Vector3 worldPos = Camera.main.ScreenToWorldPoint(targetStar.position);

            if (trigger.star != null)
                DOTween.Kill(trigger.star.transform);

            trigger.star.transform.DOMove(worldPos, 1).SetUpdate(true).OnComplete(delegate ()
            {
                trigger.star.gameObject.SetActive(false);
            });
            trigger.star.transform.DOScale(1, 1).SetUpdate(true);
        }

        starIndex++;

        if (starIndex == 2)
        {
            if (secondsLeft > 60)
                AddStar(null);

            ZXCombatUI.ChangeMissionState(ZXCombatUI.State.Victory);
        }
    }

    public void ShowClock()
    {
        time.SetActive(true);
        time.transform.DOScale(1, 0.25f).SetUpdate(true).SetEase(Ease.InOutQuad);
    }

    public void ShowTutorialClock()
    {
        //Show the clock
        ShowClock();

        //Unreg from timer
        ZXCombatUI.e_MissionClock -= d_MissionClock;

        //Update text display
        timeDisplay.text = ZXLocal.LocalText("genericTutorial");
    }

    public void ShowStars()
    {
        float baseSpeed = 0.5f;
        float baseDelay = 0.25f;

        Star1.SetActive(true);
        Star2.SetActive(true);
        Star3.SetActive(true);

        Star1.transform.DOScale(1, baseSpeed).SetUpdate(true).SetEase(Ease.OutBack).SetDelay(baseDelay * 2);
        Star2.transform.DOScale(1, baseSpeed).SetUpdate(true).SetEase(Ease.OutBack).SetDelay(baseDelay * 3);
        Star3.transform.DOScale(1, baseSpeed).SetUpdate(true).SetEase(Ease.OutBack).SetDelay(baseDelay * 4);
    }

    public void PunchStars()
    {
        float baseSpeed = 0.75f;
        float baseDelay = 0.25f;

        Star1.SetActive(true);
        Star2.SetActive(true);
        Star3.SetActive(true);

        Star1.transform.DOPunchScale(new Vector3(0.2f, 0.2f, 0.2f), baseSpeed).SetUpdate(true).SetEase(Ease.OutBack).SetDelay(baseDelay * 2);
        Star2.transform.DOPunchScale(new Vector3(0.2f, 0.2f, 0.2f), baseSpeed).SetUpdate(true).SetEase(Ease.OutBack).SetDelay(baseDelay * 3);
        Star3.transform.DOPunchScale(new Vector3(0.2f, 0.2f, 0.2f), baseSpeed).SetUpdate(true).SetEase(Ease.OutBack).SetDelay(baseDelay * 4);
    }

    public static void ReportKilled(ZXZombie Zombie)
    {
        killed.Add(Zombie);
    }

    private void OnDisable()
    {
        ZXCombatUI.e_MissionClock -= d_MissionClock;
        ZXCombatUI.e_StateChange -= d_MissionStateChanged;
    }
}
