﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Vectrosity;

public class ZXTutorialHand : MonoBehaviour
{
    //Public accessors
    public bool AnimationShowing
    {
        get
        {
            return showingAnimation;
        }
    }

    //Important transforms
    private Transform Pointer;
    private Transform Hand;
    private Image HandImage;

    //Private tracking variables
    private bool showingAnimation = false;
    private Sequence animationSequence;
    private string animationID = "TutorialHandSequence";
    private Sequence fadeSequence;
    private string fadeID = "fadeSequence";

    //Delegates
    private System.Action d_HideAnimation;
    private System.Action<bool> d_AnimationListener;

    //Vectorlines
    [SerializeField] private LayerMask lineLayer;
    private VectorLine path;
    private VectorLine path2d;

    private void Awake()
    {
        //Get parts
        Pointer = transform;
        Hand = transform.Find("Hand");
        HandImage = Hand.GetComponent<Image>();

        //Create tutorial lines
        path = (new VectorLine("Path", new List<Vector3>(), 0.01f * Screen.width, LineType.Continuous, Joins.Weld));
        path.layer = lineLayer;
        path.color = Color.white;

        path2d = (new VectorLine("Path2d", new List<Vector2>(), 0.01f * Screen.width, LineType.Continuous, Joins.Weld));
        path2d.layer = lineLayer;
        path2d.color = Color.white;

        //Create delegates
        d_HideAnimation = new System.Action(ShowHand);
        d_AnimationListener = new System.Action<bool>(AnimationCallback);
    }

    private void Start()
    {
        //Set end caps
        path.endCap = ZXGroundEvents.instance.gameObject.GetInstanceID().ToString();
        path2d.endCap = ZXGroundEvents.instance.gameObject.GetInstanceID().ToString();
    }

    /* public void showHandDragSequence(int index, Vector3 pos)
    {
        Vector3 pos1;
        Vector3 pos2;

        //Enable card
        EnableSelectCard(index);

        //Get index position
        switch (index)
        {
            case 0:
                pos1 = Card1Button.transform.position;
                break;
            case 1:
                pos1 = Card2Button.transform.position;
                break;
            case 2:
                pos1 = Card3Button.transform.position;
                break;

            //Default to card 1
            default:
                pos1 = Card1Button.transform.position;
                break;
        }

        //Get hand position
        if (pos == Vector3.zero)
            pos2 = Camera.main.WorldToScreenPoint(ZXGroundEvents.instance.SpawnArea.transform.position + new Vector3(-30, 0, 0));
        else
            pos2 = Camera.main.WorldToScreenPoint(pos);

        //Set hand position
        TutorialHand.transform.position = pos1;

        //Create new animation sequence
        handSequence = DOTween.Sequence();
        handSequence.SetUpdate(true);
        handSequence.SetLoops(-1);
        handSequence.SetEase(Ease.Linear);

        //Fade in
        handSequence.Append(TutorialHandTransform.DOScale(1, 0.25f)).OnStart(delegate ()
        {
            TutorialHand.transform.position = pos1;
        });

        //Move through points
        handSequence.Append(TutorialHandTransform.DOMove(pos2, 1.5f, false).OnUpdate(delegate ()
        {
            path2d.points2.Add(TutorialHand.transform.position + VerticleOffset);
            path2d.Draw();
        }));

        handSequence.AppendCallback(delegate ()
        {
            instance.path2d.points2.Clear();
            instance.path2d.Draw();
        });

        ShowAnimation();
    }*/

    /*public void showHeroPathSequence(ZXTutorialMarker.Series group)
    {
        List<ZXTutorialMarker> markers = ZXTutorialMarker.getGroup(group);

        ZXGroundEvents.instance.tutorialMovePath.points3.Clear();

        //Build list of points
        foreach (ZXTutorialMarker marker in markers)
        {
            ZXGroundEvents.instance.tutorialMovePath.points3.Add(marker.transform.position);
        }

        handSequence = DOTween.Sequence();
        handSequence.SetUpdate(true);
        handSequence.SetLoops(-1);
        handSequence.SetEase(Ease.Linear);

        handSequence.AppendCallback(delegate ()
        {
            instance.path.points3.Clear();
            instance.path.Draw3D();
            TutorialHand.transform.position = Camera.main.WorldToScreenPoint(markers[0].transform.position);
        });

        handSequence.AppendInterval(0.5f);

        //Add all marker points
        for (int i = 1; i < markers.Count; i++)
        {
            handSequence.Append(TutorialHandTransform.DOMove(Camera.main.WorldToScreenPoint(markers[i].transform.position), 1.5f, false).OnUpdate(delegate ()
            {
                path.points3.Add(Camera.main.ScreenToWorldPoint(TutorialHand.transform.position) + VerticleOffset);
                path.Draw3D();
            }));
        }

        handSequence.AppendCallback(delegate ()
        {
            instance.path.points3.Clear();
            instance.path.Draw3D();
        });

        ShowAnimation();
    }*/

    public void AnimateHand()
    {
        //Get sequence
        animationSequence = DOTween.Sequence();
        animationSequence.SetId(animationID);
        animationSequence.SetUpdate(true);
    }

    public void ShowHand()
    {
        if (!showingAnimation)
            ShowHand(true);
    }

    public void ShowHand(bool show)
    {
        float duration = 0.25f;
        float endValue = show ? 1 : 0;

        if (!gameObject.activeSelf)
            gameObject.SetActive(true);

        showingAnimation = show;

        if (DOTween.IsTweening(fadeID))
            DOTween.Kill(fadeID);

        fadeSequence = DOTween.Sequence();
        fadeSequence.SetId(fadeID);
        fadeSequence.SetUpdate(true);

        //Fade in hand
        fadeSequence.Insert(0f, Hand.DOScale(endValue, duration));
        fadeSequence.Insert(0f, DOTween.ToAlpha(delegate () { return path.color; }, delegate (Color val) { path.color = val; }, endValue, duration));
        fadeSequence.Insert(0f, DOTween.ToAlpha(delegate () { return path2d.color; }, delegate (Color val) { path2d.color = val; }, endValue, duration));

        //Register delegates
        ZXGroundEvents.e_HeroPathStarted += d_HideAnimation;
        ZXGroundEvents.e_HeroPathEnded += d_AnimationListener;
    }

    public void HideHand()
    {
        if (showingAnimation)
            ShowHand(false);
    }

    public void AnimationCallback(bool restart)
    {
        /*if (!restart)
            ShowAnimation();
        else
            EndAnimation();*/
    }

    public void EndAnimation()
    {
        showingAnimation = false;

        //Kill the tween
        if (animationSequence != null)
            animationSequence.Kill();

        //Reset paths
        path.points3.Clear();
        path.color = Color.clear;
        path.Draw3D();

        path2d.points2.Clear();
        path2d.color = Color.clear;
        path2d.Draw();

        //Disable hand object
        Hand.gameObject.SetActive(false);
    }
}
