﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ZXViewingArea : MonoBehaviour {

    private ZXRTSCamera RTScamera;
    public float sizeScalerX;
    public float sizeScalerZ;
	
	// Update is called once per frame
	void Update ()
    {
        if (Application.isPlaying)
        {
            gameObject.SetActive(false);
            RTScamera = GameObject.Find("Main Camera").GetComponent<ZXRTSCamera>();
            transform.localScale = new Vector3(RTScamera.boundaryPaddingX * sizeScalerX, 1, RTScamera.boundaryPaddingZ * sizeScalerZ);
        }
        else
        {
            gameObject.SetActive(true);
        }
	}
}
