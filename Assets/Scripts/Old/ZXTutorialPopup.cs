﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ZXCore;
using TMPro;

public class ZXTutorialPopup : ZXPopup
{
    TextMeshProUGUI title;
    TextMeshProUGUI body;
    TextMeshProUGUI charName;
    Button conButton;

    protected override void Build()
    {
        title = transform.Find("TitleText").GetComponent<TextMeshProUGUI>();
        body = transform.Find("BodyText").GetComponent<TextMeshProUGUI>();
        charName = transform.Find("CharText").GetComponent<TextMeshProUGUI>();
        conButton = transform.Find("ContinueButton").GetComponent<Button>();
    }

    public void Open(string TitleKey, string BodyKey, string CharText)
    {
        Open(TitleKey, BodyKey, CharText, true);
    }
    public void Open(string TitleKey, string BodyKey, string CharText, bool ContinueButton)
    {
        base.Open();

        title.text = ZXLocal.LocalText(TitleKey);
        body.text = ZXLocal.LocalText(BodyKey);
        charName.text = ZXLocal.LocalText(CharText);
        conButton.gameObject.SetActive(ContinueButton);
    }
}
