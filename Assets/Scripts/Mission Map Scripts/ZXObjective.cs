﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using DG.Tweening;
using Vectrosity;

//[RequireComponent(typeof(Collider))]
public class ZXObjective : ZXFXable, IZXTargetable
{
    public enum ObjectiveType
    {
        KOTH = 0,
        KOTH_Rescue = 1,
        KOTH_Vehicle = 2,
        KOTH_Repair = 3
    }
    public enum ObjectiveOrientation
    {
        Left = 0,
        Right = 1
    }

    //Inspector fields
	[SerializeField] private Transform FloatingIcon;
    [SerializeField] private ObjectiveType type;
    [SerializeField] public ZXObjectiveTrigger trigger;
    [SerializeField] private Transform vehicleExit;
    private IZXAnimation endAnimation;
    private Collider triggerCollider;

    //Events
    public event System.Action<ZXObjective> e_Complete;
    public event System.Action<ZXObjective> e_Failed;
    public new event System.Action<IZXTargetable> e_Dead;
    public new event System.Action<IZXTargetable> e_BreakTargeting;

    //Accessors
    public ObjectiveType Type
    {
        get
        {
            return type;
        }
    }
    public new Transform LookAtTransform
    {
        get
        {
            return transform;
        }
    }
    public ObjectiveOrientation Orientation
    {
        get
        {
            return orientation;
        }
    }
    public new Vector3 Center
    {
        get
        {
            return colliders[0].bounds.center;
        }
    }
    public new Vector3 CurrentVelocity
    {
        get
        {
            return Vector3.zero;
        }
    }

    private ObjectiveOrientation orientation;
    private ZXHeroSpawn heroSpawn;
    private ZXEventSpawner spawner;
    //private ZXAutomobile vehicle;

    private static List<ZXObjective> objectives = null;

    public static List<ZXObjective> AllObjectives
    {
        get
        {
            return objectives;
        }
    }

    //Initalization
    protected override void Awake()
    {
        base.Awake();

        if (objectives == null)
            objectives = new List<ZXObjective>(2);

        objectives.Add(this);
    }

    protected override void Start()
    {
        base.Start();

        //Register with game manager and reflecting pool
        ZXReflectingPool.Register<ZXObjective>(this, transform);

        //Move die effect
        if (dieEffect)
            dieEffect.transform.position = Center;

        //Register with trigger object
        if (trigger != null)
        {
            trigger.e_OnCompleted += new System.Action<ZXObjectiveTrigger>(ObjectiveComplete);
            triggerCollider = trigger.GetComponent<Collider>();
        }

		if(FloatingIcon)
		{
			FloatingIcon.DOLocalMoveY(FloatingIcon.localPosition.y + 1, 1, false).SetLoops(-1, LoopType.Yoyo);
			FloatingIcon.DORotate(new Vector3(0, 360, 0), 10, RotateMode.LocalAxisAdd).SetLoops(-1).SetEase(Ease.Linear);

			if(trigger)
			{
				trigger.star = FloatingIcon.gameObject;
			}
		}

        //Determine if right or left
        heroSpawn = ZXReflectingPool.FindClosest<ZXHeroSpawn>(transform);

        if (heroSpawn == null)
            heroSpawn = GameObject.Find("HeroSpawn").GetComponent<ZXHeroSpawn>();

        orientation = transform.position.z < heroSpawn.transform.position.z ? ObjectiveOrientation.Left : ObjectiveOrientation.Right;

        spawner = GetComponentInChildren<ZXEventSpawner>();
        endAnimation = GetComponent<IZXAnimation>();
    }

    //Will throw an error if no ocjective trigger is attached!!!
    public Vector3 GetKOTHInteractionPoint(Vector3 Origin)
    {
        UnityEngine.AI.NavMeshHit hit;
        Vector3 point = triggerCollider.bounds.center;

        if (UnityEngine.AI.NavMesh.SamplePosition(point, out hit, 10f, UnityEngine.AI.NavMesh.AllAreas))
        {
            return hit.position;
        }

        throw new System.Exception("Failed to find KOTH NavMesh point");
    }

    public void Blink()
    {
        if (trigger != null)
        {
            trigger.Blink();
        }
    }

    public void Scale()
    {
        if (trigger != null)
        {
            trigger.Scale();
        }
    }

    protected void ObjectiveComplete(ZXObjectiveTrigger Trigger)
    {
        DeRegister();

        if (race == ToonRace.Human)
        {
            CompletionEffect();

            if (e_Complete != null)
                e_Complete(this);
        }
        else if (race != ToonRace.Human && e_Failed != null)
            e_Failed(this);

        if (type == ObjectiveType.KOTH_Repair)
        {
            base.Die();
        }
    }

    protected override void Die()
    {
        base.Die();

        if (trigger != null)
        {
            trigger.star.transform.SetParent(null);
            trigger.star.GetComponent<MeshRenderer>().material.color = Color.red;
            trigger.star.GetComponent<MeshRenderer>().material.DOFade(0, 3).OnComplete(delegate ()
            {
                trigger.star.gameObject.SetActive(false);
            });

            trigger.star.transform.DOScale(0, 3);
        }
		else
		{
            //ZXMissionClock.instance.AddStar(null);
		}

        /* if (race == ToonRace.Human && e_Failed != null)
            e_Failed(this);
        else if (race != ToonRace.Human && e_Complete != null)
            e_Complete(this);*/
    }

    protected virtual void CompletionEffect()
    {
        if (spawner != null)
            spawner.StartSpawning();

        /*
        if (vehicle != null)
        {
            vehicle.LeaveScene();
        }
        */

        if (endAnimation != null)
            endAnimation.Play();
    }

    //Destruction
    protected override void DeRegister()
    {
        base.DeRegister();
        objectives.Remove(this);

        ZXReflectingPool.DeRegister<ZXObjective>(transform);

        if (e_Dead != null)
            e_Dead(this);
    }

    //Tutorial Overrides
    public static void ManualPoolRegistration(ObjectiveOrientation Orient, bool Register)
    {
        ZXObjective targetObjective = objectives.Find(delegate (ZXObjective obj) { return obj.orientation == Orient; });

        if (targetObjective != null)
        {
            if (Register)
                ZXReflectingPool.Register<ZXObjective>(targetObjective, targetObjective.transform);
            else
                ZXReflectingPool.DeRegister<ZXObjective>(targetObjective.transform);
        }
    }
}
