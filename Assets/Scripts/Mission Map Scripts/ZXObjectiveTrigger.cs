﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using ZXCore;
using DG.Tweening;
using System.Collections.Generic;

public class ZXObjectiveTrigger : MonoBehaviour {

    private const float _blinkTime = 0.5f;
    private const float _scaleSize = 1.25f;

    public enum cappingStatus
    {
        humans,
        zombies,
        mixed
    }

    private cappingStatus status;

	public Color CompletedColor;
    public Color BlinkColor;
	public float TimeToTrigger;
    private float timeLeft;
	private Image fillImage;
	private Image whiteImage;
    private RectTransform rectTrans;
    public GameObject star;

    protected System.Action<IZXTargetable> d_UnitKilled;

    private List<ZXToon> humans;
    private List<ZXToon> zombies;

    private int uid;
    private string r_BlinkColorID;
    private string r_ScaleID;

    public event System.Action<ZXObjectiveTrigger> e_OnCompleted;
    //public GameObject spawnAreaGrowth;
	// Use this for initialization
    private void Awake()
    {
		fillImage = transform.GetChild(0).Find("Fill").GetComponent<Image>();
		whiteImage = transform.GetChild(0).Find("Image").GetComponent<Image>();

        rectTrans = whiteImage.GetComponent<RectTransform>();

        d_UnitKilled = new System.Action<IZXTargetable>(removeFromList);

        humans = new List<ZXToon>();
        zombies = new List<ZXToon>();

        //Get UID
        uid = gameObject.GetInstanceID();

        //Configure IDs
        r_BlinkColorID = uid + "_blinkColorID";
        r_ScaleID = uid + "_scaleID";
    }

	void Start ()
	{
        fillImage = transform.GetChild(0).Find("Fill").GetComponent<Image>();

        e_OnCompleted += new System.Action<ZXObjectiveTrigger>(ZXMissionClock.instance.AddStar);
        timeLeft = TimeToTrigger;

        /*
		if(transform.GetChild(0).position.z > ZXGroundEvents.instance.SpawnArea.transform.position.z)
        {
            spawnAreaGrowth = ZXGroundEvents.instance.SpawnArea.transform.Find("Right").gameObject;
        }
        else
        {
            spawnAreaGrowth = ZXGroundEvents.instance.SpawnArea.transform.Find("Left").gameObject;
        }
        */
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(status == cappingStatus.humans && timeLeft > 0 && ZXCombatUI.CurrentState == ZXCombatUI.State.Combat)
		{
            timeLeft -= Time.deltaTime * humans.Count;

            transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(11, 11);
            fillImage.fillAmount = (TimeToTrigger - timeLeft) / TimeToTrigger;

			if(timeLeft <= 0)
			{
                fillImage.color = CompletedColor;
                fillImage.DOFade(0, 1).OnComplete(turnOff);
                whiteImage.DOFade(0, 1).OnComplete(turnOff);
                //spawnAreaGrowth.SetActive(true);

                if (e_OnCompleted != null)
                    e_OnCompleted(this);
            }
		}
        else if (status == cappingStatus.zombies && timeLeft > 0)
        {
            if (TimeToTrigger > timeLeft)
            {
                timeLeft += Time.deltaTime * zombies.Count;

                fillImage.fillAmount = (TimeToTrigger - timeLeft) / TimeToTrigger;
            }
        }
    }

	private void turnOff()
	{
		gameObject.SetActive(false);
	}

    void checkCap()
    {
        if(humans.Count > 0 && zombies.Count <= 0)
        {
            status = cappingStatus.humans;
        }
        else if(humans.Count > 0 && zombies.Count > 0)
        {
            status = cappingStatus.mixed;
        }
        else if(humans.Count == 0 && zombies.Count > 0)
        {
            status = cappingStatus.zombies;
        }
    }

    public void Blink()
    {
        if (DOTween.IsTweening(r_BlinkColorID))
            return;

        whiteImage.DOColor(BlinkColor, _blinkTime).SetId(r_BlinkColorID).SetLoops(2, LoopType.Yoyo).SetUpdate(true);
    }

    public void Scale()
    {
        if (DOTween.IsTweening(r_ScaleID))
            return;

        rectTrans.DOScale(_scaleSize, _blinkTime).SetId(r_ScaleID).SetLoops(2, LoopType.Yoyo).SetUpdate(true);
    }

    void removeFromList(IZXTargetable tar)
    {
        if (this == null) return;

        if (tar == null) return;

        if (tar.LookAtTransform == null) return;

        ZXToon toon;

        if (tar.LookAtTransform.GetComponentInParent<ZXToon>())
        {
            toon = tar.LookAtTransform.GetComponentInParent<ZXToon>();
        }
        else
        {
            return;
        }

        if (zombies.Contains(toon))
        {
            zombies.Remove(toon);
        }
         
        if (humans.Contains(toon)) humans.Remove(toon);

		checkCap();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.transform.GetComponentInParent<ZXToon>() && other.transform.GetComponentInParent<ZXToon>().Race == ToonRace.Human && other.GetComponent<ZXBodypart>() && other.GetComponent<ZXBodypart>().Part == ZXBodypart.Bodypart.Body)
        {
            if(humans.Contains(other.transform.GetComponentInParent<ZXToon>()) == false)
            {
                if (!other.GetComponentInParent<IZXTargetable>().isAlive) return;

                humans.Add(other.transform.GetComponentInParent<ZXToon>());
                other.transform.GetComponentInParent<ZXToon>().e_Dead += d_UnitKilled;
            }
            checkCap();
        }
		else if (other.transform.GetComponentInParent<ZXToon>() && other.transform.GetComponentInParent<ZXToon>().Race == ToonRace.Zombie && other.GetComponent<ZXBodypart>() && other.GetComponent<ZXBodypart>().Part == ZXBodypart.Bodypart.Body)
        {
            if (zombies.Contains(other.transform.GetComponentInParent<ZXToon>()) == false)
            {
                if (!other.GetComponentInParent<IZXTargetable>().isAlive) return;
                zombies.Add(other.transform.GetComponentInParent<ZXToon>());
                other.transform.GetComponentInParent<ZXToon>().e_Dead += d_UnitKilled;
            }
            checkCap();
        }   
    }

    void OnTriggerExit(Collider other)
    {
		if (other.transform.GetComponentInParent<ZXToon>() && other.transform.GetComponentInParent<ZXToon>().Race == ToonRace.Human && other.GetComponent<ZXBodypart>() && other.GetComponent<ZXBodypart>().Part == ZXBodypart.Bodypart.Body)
        {
            if (humans.Contains(other.transform.GetComponentInParent<ZXToon>()) == true)
            {
                humans.Remove(other.transform.GetComponentInParent<ZXToon>());
                other.transform.GetComponentInParent<ZXToon>().e_Dead -= d_UnitKilled;
            }
            checkCap();
        }
		else if (other.transform.GetComponentInParent<ZXToon>() && other.transform.GetComponentInParent<ZXToon>().Race == ToonRace.Zombie && other.GetComponent<ZXBodypart>() && other.GetComponent<ZXBodypart>().Part == ZXBodypart.Bodypart.Body)
        {
            if (zombies.Contains(other.transform.GetComponentInParent<ZXToon>()) == true)
            {
                zombies.Remove(other.transform.GetComponentInParent<ZXToon>());
                other.transform.GetComponentInParent<ZXToon>().e_Dead -= d_UnitKilled;
            }
            checkCap();
        }
    }

    private void OnDisable()
    {
        DOTween.Complete(true);
    }
}
