﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using DG.Tweening;

public class ZXBarricadePart : MonoBehaviour
{
    private const float _fadeTime = 2f;
    private const float _fadeDelay = 4f;

    protected new Collider collider;
    protected new Rigidbody rigidbody;
    protected List<Renderer> renderers;

	//Initialization
	private void Awake ()
    {
        //Get of create rigidbody
        rigidbody = GetComponent<Rigidbody>();

        if (rigidbody == null)
            rigidbody = gameObject.AddComponent<Rigidbody>();

        rigidbody.useGravity = true;
        rigidbody.isKinematic = true;

        //Get or create collider
        collider = GetComponent<Collider>();

        if (collider == null)
            collider = gameObject.AddComponent<BoxCollider>();

        collider.enabled = false;

        //Get renderers
        renderers = new List<Renderer>(GetComponentsInChildren<Renderer>());
    }

    //Main function
    public void Break(Vector3 Force)
    {
        transform.parent = null;

        collider.enabled = true;
        rigidbody.useGravity = true;
        rigidbody.isKinematic = false;

        rigidbody.AddRelativeForce(Force * rigidbody.mass, ForceMode.Force);

        if (renderers != null)
            renderers.ForEach(delegate (Renderer Object)
            {
                Object.material.DOFloat(0f, "_Transparency", _fadeTime).SetDelay(_fadeDelay);
            });

        Invoke("Disable", _fadeDelay + _fadeTime);
    }

    //Destruction
    private void Disable()
    {
        gameObject.SetActive(false);
    }
}
