﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using DG.Tweening;
using DarkTonic.MasterAudio;

[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(ZXFXable))]
public class ZXBarricade : ZXInterestPoint
{
    [SoundGroupAttribute] [SerializeField] protected string s_BarricadeBreak;
    private ZXFXable fxable;
    private UnityEngine.AI.NavMeshObstacle obstacle;
    private List<ZXBarricadePart> parts;

    private new Collider collider;

    public IZXTargetable GetTarget
    {
        get
        {
            return fxable as IZXTargetable;
        }
    }

    /* Initalization */

    protected override void Awake()
    {
        base.Awake();

        //Set base values
        pointType = Interest.Barricade;
        maxInteractions = 0;
        interactionRadius = 2f;

        fxable = GetComponent<ZXFXable>();

        //Get collider
        collider = GetComponent<Collider>(); 

        //Create NavMesh Obstacle
        obstacle = GetComponent<UnityEngine.AI.NavMeshObstacle>();

        if (obstacle == null)
            obstacle = gameObject.AddComponent<UnityEngine.AI.NavMeshObstacle>();

        obstacle.center = transform.InverseTransformPoint(collider.bounds.center);
        obstacle.size = transform.rotation * collider.bounds.size;
        obstacle.carving = true;

        //Register with fxable script
        fxable.e_Dead += new System.Action<IZXTargetable>(DeadCallback);
    }

    protected void Start()
    {
        parts = new List<ZXBarricadePart>(GetComponentsInChildren<ZXBarricadePart>());
    }

    public void Joe()
    {
        fxable.TakeDamage(new ZXDamage(1000, 1, 1, 0f, ToonRace.None, ZXDamage.Type.Impact, new Ray(), ZXDamage.SpecialEffect.None));
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        ZXReflectingPool.Register<ZXBarricade>(this, transform);
    }

    /* Main Functions */

    public override int Interact(IZXTargetable Actor)
    {
        return 0;
    }

    /* Destruction */

    private void DeadCallback(IZXTargetable Self)
    {
        obstacle.enabled = false;
        collider.enabled = false;

        MasterAudio.PlaySound3DAtTransformAndForget(s_BarricadeBreak, transform);

        if (parts != null && parts.Count > 0)
        {
            parts.ForEach(delegate (ZXBarricadePart part) { part.Break(new Vector3(-200f, 0f, 0f)); });
        }

        gameObject.SetActive(false);
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        ZXReflectingPool.DeRegister<ZXBarricade>(transform);
    }
}
