﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using DG.Tweening;

public sealed class ZXMapExit : ZXInterestPoint
{
    //Initalization
    protected override void Awake()
    {
        base.Awake();

        pointType = Interest.Exit;

        ZXReflectingPool.Register<ZXMapExit>(this, transform);
    }

    private void Start()
    {
        ZXHeroSpawn heroSpawn = ZXReflectingPool.FindClosest<ZXHeroSpawn>(transform);

        if (heroSpawn == null)
            heroSpawn = GameObject.Find("HeroSpawn").GetComponent<ZXHeroSpawn>();

        Vector3 newPosition = heroSpawn.transform.position;

        newPosition.x += 25f;

        transform.position = newPosition;
    }

    public override int Interact(IZXTargetable Actor)
    {
        return 0;
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        ZXReflectingPool.DeRegister<ZXMapExit>(transform);
    }
}
