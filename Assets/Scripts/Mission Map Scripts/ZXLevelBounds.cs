﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZXLevelBounds : MonoBehaviour
{
    private static BoxCollider boundry;

	private void Awake ()
    {
        boundry = GetComponent<BoxCollider>();
	}
	
    public static bool Contains(Vector3 point)
    {
        if (boundry != null)
            return boundry.bounds.Contains(point);
        else
            return true;
    }
}
