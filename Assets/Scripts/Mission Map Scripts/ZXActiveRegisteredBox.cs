﻿using UnityEngine;
using System.Collections;
using ZXCore;

public class ZXActiveRegisteredBox : MonoBehaviour
{
	private void OnTriggerEnter(Collider _other)
	{
        ZXToon toon = _other.transform.GetComponentInParent<ZXToon>();

        if (toon)
		{
			toon.Register(true, true);
		}
	}
}
