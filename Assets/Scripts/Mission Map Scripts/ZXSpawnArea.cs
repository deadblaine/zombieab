﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZXSpawnArea : MonoBehaviour
{
    public static Collider BoxCollider
    {
        get
        {
            return boxCollider;
        }
    }

    private static Collider boxCollider;
    private static ZXSpawnArea singleton;

	// Use this for initialization
	private void Awake()
    {
        singleton = this;

        boxCollider = GetComponent<Collider>();
	}

    private void Start()
    {
        SetActive(false);
    }

    public static void SetActive(bool Active)
    {
        singleton.gameObject.SetActive(Active);
    }
}
